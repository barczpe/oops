#!/bin/bash -ex

cd ${PROJECT_ROOT}
if [ "$PWD" != "${PROJECT_ROOT}" ]
then
    echo "PROJECT_ROOT is not properly defined: " ${PROJECT_ROOT}
    exit
fi

echo "Delete build directory..."
rm -fr oops_build_dbg/*
rm -fr oops_build_rel/*
