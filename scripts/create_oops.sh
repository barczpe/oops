#!/bin/bash -ex

cd ${PROJECT_ROOT}
if [ "$PWD" != "${PROJECT_ROOT}" ]
then
    echo "PROJECT_ROOT is not properly defined: " ${PROJECT_ROOT}
    exit
fi

: ${CMAKE_GENERATOR:="Unix Makefiles"}
PARALLEL="--jobs=8"

if [[ $# -ne 1 ]]; then
    export CXX_STANDARD=Cpp17
else
    # (Cpp11), Cpp14, Cpp17, (Cpp20, Cpp23)
    export CXX_STANDARD=$1
fi

rm -f ${PROJECT_ROOT}/oops_build_dbg/CMakeCache.txt

cmake \
-DCMAKE_PREFIX_PATH="${CMAKE_PREFIX_PATH}" \
-DCMAKE_BUILD_TYPE=Debug \
-DOOPS_CPP_STANDARD=${CXX_STANDARD} \
-B${PROJECT_ROOT}/oops_build_dbg \
-H${PROJECT_ROOT}/oops \
-G"$CMAKE_GENERATOR"
cmake --build "${PROJECT_ROOT}/oops_build_dbg" --config "Debug" -- "$PARALLEL"

rm -f ${PROJECT_ROOT}/oops_build_rel/CMakeCache.txt

cmake \
-DCMAKE_PREFIX_PATH="${CMAKE_PREFIX_PATH}" \
-DCMAKE_BUILD_TYPE=RelWithDebInfo \
-DOOPS_CPP_STANDARD=${CXX_STANDARD} \
-B${PROJECT_ROOT}/oops_build_rel \
-H${PROJECT_ROOT}/oops \
-G"$CMAKE_GENERATOR"
cmake --build "${PROJECT_ROOT}/oops_build_rel" --config "RelWithDebInfo" -- "$PARALLEL"
cmake --install oops_build_rel --prefix ${PROJECT_ROOT}/install
