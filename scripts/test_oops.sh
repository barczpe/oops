#!/bin/bash -ex

cd ${PROJECT_ROOT}
if [ "$PWD" != "${PROJECT_ROOT}" ]
then
    echo "PROJECT_ROOT is not properly defined: " ${PROJECT_ROOT}
    exit
fi

cd ${PROJECT_ROOT}/oops_build_dbg/test
./oops_test $*
