#!/bin/bash -ex

cd ${PROJECT_ROOT}
if [ "$PWD" != "${PROJECT_ROOT}" ]
then
    echo "PROJECT_ROOT is not properly defined: " ${PROJECT_ROOT}
    exit
fi

PARALLEL="--jobs=8"

cmake --build "${PROJECT_ROOT}/oops_build_dbg" --config "Debug" -- "$PARALLEL"
cmake --build "${PROJECT_ROOT}/oops_build_rel" --config "RelWithDebInfo" -- "$PARALLEL"
cmake --install oops_build_rel --prefix ${PROJECT_ROOT}/install
