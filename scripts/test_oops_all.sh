#!/bin/bash -ex

cd ${PROJECT_ROOT}
if [ "$PWD" != "${PROJECT_ROOT}" ]
then
    echo "PROJECT_ROOT is not properly defined: " ${PROJECT_ROOT}
    exit
fi

cd ${PROJECT_ROOT}/oops_build_dbg/example
./oops_example
retVal=$?
if [ $retVal -ne 0 ]; then
    echo "debug/oops_example error"
    exit $retVal
fi

cd ${PROJECT_ROOT}/oops_build_rel/example
./oops_example
retVal=$?
if [ $retVal -ne 0 ]; then
    echo "release/oops_example error"
    exit $retVal
fi

cd ${PROJECT_ROOT}/oops_build_dbg/test
./oops_test $*
retVal=$?
if [ $retVal -ne 0 ]; then
    echo "debug/oops_test error"
    exit $retVal
fi

cd ${PROJECT_ROOT}/oops_build_rel/test
./oops_test $*
retVal=$?
if [ $retVal -ne 0 ]; then
    echo "release/oops_test error"
    exit $retVal
fi
