/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

//#include <oops/rPropTypeTraits.h>

#include "BaseDerived.h"
#include "SerializationTest.h"
#include <oops/rPropInterface.h>
#include <gtest/gtest.h>
#if __cplusplus > 201402L
#include <optional>
#if __has_include(<boost/optional.hpp>)
#include <boost/optional.hpp>
#define HAS_BOOST_OPTIONAL
#endif
#endif
#include <string>

using namespace rOops;

#if __cplusplus > 201402L
TEST(OptionalTest, Std)
{
    class StdOptional
    {
    public:
        virtual ~StdOptional() = default;
        StdOptional() = default;
        bool operator==(const StdOptional& arL) const
        {
            if (optBaseClass != arL.optBaseClass) return false;
            if (optDerivedClass != arL.optDerivedClass) return false;
            if (optString != arL.optString) return false;
            return true;
        }
        // Members are not private for simpler test program.
        std::optional<BaseClass> optBaseClass;
        std::optional<DerivedClass1> optDerivedClass;
        std::optional<std::string> optString;
        rOOPS_ADD_PROPERTY_INTERFACE(StdOptional)
        {
            rOOPS_VERSION(1);
            rOOPS_PROPERTY(optBaseClass);
            rOOPS_PROPERTY(optDerivedClass);
            rOOPS_PROPERTY(optString);
        }
    };
    auto apply_ptr = rOops::createApplyPointer<std::optional<std::string>>();
    EXPECT_TRUE(nullptr!=dynamic_cast<rOops::rApplyOptional<std::optional<std::string>>*>(apply_ptr.get()));
    StdOptional opt;
    opt.optString = "AIM::COMMON";
    testSerialization("SerializeEmptyOptionals", opt);
    opt.optString = std::string("ABCD");
    testSerialization("SerializeOptionals_1", opt);
    opt.optBaseClass = BaseClass(123);
    opt.optDerivedClass = DerivedClass1("OptionalDerivedClass1");
    testSerialization("SerializeOptionals_2", opt);
}
#endif

#ifdef HAS_BOOST_OPTIONAL
namespace rOops
{
    template <typename T> struct is_Optional<boost::optional<T>> : std::true_type {};
    template <typename T> struct rPropRemovePointer<boost::optional<T>> { typedef T type; };
    template <typename T> struct rPropRemovePointer<boost::optional<T const>> { typedef T type; };
}

TEST(OptionalTest, Boost)
{
    class BoostOptionalTestClass
    {
    public:
        virtual ~BoostOptionalTestClass() = default;
        BoostOptionalTestClass() = default;
        bool operator==(const BoostOptionalTestClass& arR) const
        {
            if (name != arR.name) return false;
            if (optBaseClass != arR.optBaseClass) return false;
            if (optDerivedClass != arR.optDerivedClass) return false;
            if (optString != arR.optString) return false;
            return true;
        }
        // Members are not private for simpler test program.
        std::string name; // Yaml Parser fails when block is empty.
        boost::optional<BaseClass> optBaseClass;
        boost::optional<DerivedClass1> optDerivedClass;
        boost::optional<std::string> optString;
        rOOPS_ADD_PROPERTY_INTERFACE(BoostOptionalTestClass)
        {
            rOOPS_VERSION(1);
            rOOPS_PROPERTY(name);
            rOOPS_PROPERTY(optBaseClass);
            rOOPS_PROPERTY(optDerivedClass);
            rOOPS_PROPERTY(optString);
        }
    };
    auto apply_ptr = createApplyPointer<boost::optional<std::string>>();
    EXPECT_TRUE(nullptr!=dynamic_cast<rOops::rApplyOptional<boost::optional<std::string>>*>(apply_ptr.get()));
    BoostOptionalTestClass opt;
    opt.name = "BOOST";
    testSerialization("SerializeEmptyOptionals", opt);
    opt.optString = std::string("ABCD");
    testSerialization("SerializeOptionals_1", opt);
    opt.optBaseClass = BaseClass(123);
    opt.optDerivedClass = DerivedClass1("OptionalDerivedClass1");
    testSerialization("SerializeOptionals_2", opt);
}
#endif //HAS_BOOST_OPTIONAL

// TODO: Add test for optional in containers.
