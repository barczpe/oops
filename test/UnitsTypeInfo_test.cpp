/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rPropTypeInfoUnits.h>
#include <oops/rPropInterface.h>
#include "SerializationTest.h"
#include <units.h>

using namespace rOops;
using namespace units;
using namespace units::literals;

TEST(UnitsTypeInfoTest, UnitsToString)
{
    {
        // TODO: How to format floating point numbers?
        length::meter_t ddm{ 0 };
        EXPECT_EQ("0.000000m", rPropGetTypeInfo(&ddm)->value(&ddm));
        length::centimeter_t dd2 = 10_mm;
        EXPECT_EQ("1.000000cm", rPropGetTypeInfo(&dd2)->value(&dd2));
    }
}

/// Almost the same as EXPECT_THROW(), but this function checks the error message of the exception.
template < typename UnitsT >
void testWrongUnits(const std::string& arValue, const std::string& arExpectedErrorMsg)
{
    try {
        UnitsT dd{ 0.0 };
        const rPropTypeInfo* pTI = rPropGetTypeInfo(&dd);
        pTI->setValue(&dd, arValue);
        FAIL() << "No exception has thrown.";
    }
    catch(const std::runtime_error& arEx) {
        EXPECT_EQ(arExpectedErrorMsg, arEx.what());
    }
    catch(...) {
        FAIL() << "Unexpected exception caught.";
    }
}

TEST(UnitsTypeInfoTest, StringToUnits)
{
    // Test string to units variable conversion.
    length::meter_t dd = 0_m;
    const rPropTypeInfo* pTI = rPropGetTypeInfo(&dd);
    pTI->setValue(&dd, std::string("888m"));
    EXPECT_EQ(length::meter_t(888), dd);

    testWrongUnits<length::meter_t>("", "Units library type value is wrong. String to value conversion failed: ");
    testWrongUnits<length::meter_t>("123mm", "Different units. Expecting m, but found 123mm");
}

TEST(UnitsTypeInfoTest, TypeInfoClass)
{
    length::meter_t dd1 = 1_m;
    length::meter_t dd2 = 0_m;
    const rPropTypeInfo* pTI = rPropGetTypeInfo(&dd1);

    // Set/get functions:
    std::string str = pTI->value(&dd1);
    EXPECT_EQ("1.000000m", str);
    pTI->setValue(&dd2, str);
    EXPECT_EQ(dd1, dd2);

    pTI->setValue(&dd2, std::string("888m"));
    EXPECT_EQ(888_m, dd2);
    pTI->setValue(&dd2, std::string("999m"));
    EXPECT_EQ(999_m, dd2);

    // Fractional part tests.
    pTI->setValue(&dd2, std::string("98.76m"));
    EXPECT_EQ(98.76_m, dd2);
}


TEST(UnitsTypeInfoTest, TestStruct)
{
    struct UnitsTest
    {
        length::meter_t height = 0_m;
        virtual ~UnitsTest() = default;
        UnitsTest() = default;
        rOOPS_ADD_PROPERTY_INTERFACE(UnitsTest)
        {
            rOOPS_PROPERTY(height);
        }
    };

    try {
        UnitsTest dtOops;
        std::string oops_str =
            "TestData = !UnitsTest {\n"
            "  height = 100mm;\n"
            "};\n";
        std::stringstream strm(oops_str);
        rOops::rOopsTextParser oops_parser(strm, "UnitsTypeInfoTest.TestStruct");
        load(oops_parser, dtOops);
        FAIL() << "No exception has thrown.";
    }
    catch(const std::runtime_error& arEx) {
        EXPECT_EQ("UnitsTypeInfoTest.TestStruct(2): Different units. Expecting m, but found 100mm", std::string(arEx.what()));
    }
    catch(...) {
        FAIL() << "Unexpected exception.";
    }

    try {
        UnitsTest dtYaml;
        std::string yaml_str =
            "TestData: !UnitsTest\n"
            "  height: abc\n";
        std::stringstream strm(yaml_str);
        rOops::rOopsYamlParser yaml_parser(strm, "UnitLibTest");
        load(yaml_parser, dtYaml);
        FAIL() << "No exception has thrown.";
    }
    catch(const std::runtime_error& arEx) {
        EXPECT_EQ("UnitLibTest(2): Units library type value is wrong. String to value conversion failed: abc", std::string(arEx.what()));
    }
    catch(...) {
        FAIL() << "Unexpected exception caught.";
    }

}

rOOPS_DECLARE_TYPEINFO(units::time::second_t)
rOOPS_CreateNamedTypeInfoObject(units::time::second_t, rOops::rPropTypeInfoUnits<units::time::second_t>, UnitsTimeSecond)

TEST(UnitsTypeInfoTest, TestAll)
{
    struct AllUnits
    {
        length::meter_t height = 0_m;
        time::second_t second = 0_s;
        virtual ~AllUnits() = default;
        AllUnits() = default;
        void init(int aInt)
        {
            height = aInt * 2_m;
            second = aInt * 3_s;
        }
        bool operator==(const AllUnits& arR) const
        {
            return cmp::eq(*this, arR, &AllUnits::height)
                && cmp::eq(*this, arR, &AllUnits::second);
        }
        rOOPS_ADD_PROPERTY_INTERFACE(AllUnits)
        {
            rOOPS_PROPERTY(height);
            rOOPS_PROPERTY(second);
        }
    };
    AllUnits ut;
    ut.init(2);
    testSerialization("Units", ut, true);
}
