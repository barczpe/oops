/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include "BaseDerived.h"
#include "SerializationTest.h"
#include <oops/rPropInterface.h>
#include <gtest/gtest.h>
#include <string>

using namespace rOops;

template<class ValueT>
class Point
{
public:
    virtual ~Point() = default;
    Point()
        : _x(0), _y(0) {};
    Point( ValueT aX, ValueT aY )
        : _x(aX), _y(aY) {};
    bool operator==(const Point<ValueT>& arP) const
    {
        return (_x==arP._x) && (_y == arP._y);
    }
    bool operator!=(const Point<ValueT>& arP) const
    {
        return ! operator==(arP);
    }
protected:
    ValueT _x, _y;
    rOOPS_ADD_PROPERTY_INTERFACE(Point<ValueT>)
    {
        rOOPS_PROPERTY(_x);
        rOOPS_PROPERTY(_y);
    }
}; //class Point<>

// Instantiate 'Point<>' template class with 'int32_t'.
using PointInt32 = Point<std::int32_t>;

TEST(TemplateTest, Int32)
{
    PointInt32 intP;
    const auto* pT = rPropGetTypeInfo(&intP);
    EXPECT_EQ("Point<int32_t>", pT->typeName());
    testSerialization("Int32", intP);
}

TEST(TemplateTest, Int16)
{
    Point<std::int16_t> intP;
    const auto* pT1 = rPropGetTypeInfo(&intP);
    EXPECT_EQ("Point<int16_t>", pT1->typeName());
    auto type_name = getTypeName<Point<std::int16_t>>();
    const auto* pT2 = rPropGetTypeInfoByName(type_name);
    EXPECT_EQ(pT1, pT2);
    if (nullptr != pT2) {
        EXPECT_EQ("Point<int16_t>", pT2->typeName());
    }
    testSerialization("Int16", intP);
}

TEST(TemplateTest, VectorOfInt32)
{
    auto vector_type_name = getTypeName<std::vector<std::int32_t>>();
    auto point_type_name = getTypeName<Point<std::vector<std::int32_t>>>();
#if defined(__clang__)
    EXPECT_EQ("std::vector<int>", vector_type_name);
    EXPECT_EQ("Point<std::vector<int> >", point_type_name);
#elif defined(__GNUC__)
    EXPECT_EQ("std::vector<int>", vector_type_name);
    EXPECT_EQ("Point<std::vector<int> >", point_type_name);
#elif defined(_MSC_VER)
    EXPECT_EQ("std::vector<int,std::allocator<int> >", vector_type_name);
    EXPECT_EQ("Point<std::vector<int,std::allocator<int> > >", point_type_name);
#endif
    Point<std::vector<std::int32_t>> intP;
    const auto* pT1 = rPropGetTypeInfo(&intP);
    EXPECT_EQ("Point<std::vector<std::int32_t>>", pT1->typeName());
    const auto* pT2 = rPropGetTypeInfoByName(point_type_name);
    EXPECT_EQ(pT1, pT2);
    if (nullptr != pT2) {
        EXPECT_EQ("Point<std::vector<std::int32_t>>", pT2->typeName());
    }
    testSerialization("VectorOfInt32", intP);
}
