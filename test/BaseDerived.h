#pragma once

/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
Base and derived classes for testing polymorphism.
*/

#include "Enums.h"
#include <oops/rPropInterface.h>
#include <oops/rPropEnumStringTool.h>
#include <memory>
#include <string>
#include <deque>
#include <list>
#include <set>
#include <utility>
#include <vector>

class BaseClass
{
    static int baseClassCounter;
public:
    BaseClass()
        : index_(++baseClassCounter)
    {}
    explicit BaseClass(int arIndex)
        : index_(arIndex)
    {}
    virtual ~BaseClass() = default;
public:
    virtual std::string getTitle() const { return "BaseClass"; }
    int getIndex() const { return index_; }
    uint32_t getVersion() const { return version_;  }
    virtual bool compareObjectPointers(const BaseClass* apR) const
    {
        return operator==(*apR);
    }
    bool operator==(const BaseClass& arR) const
    {
        return version_ == arR.version_ && index_ == arR.index_;
    }
    bool operator!=(const BaseClass& arR) const
    {
        return ! operator==(arR);
    }
    bool operator<(const BaseClass& arR) const
    {
        return index_ < arR.index_;
    }
private:
    uint32_t version_{ 0 };
    int index_{ 0 };
    rOOPS_ADD_PROPERTY_INTERFACE(BaseClass)
    {
        rOOPS_PROPERTY(version_), "version", rOops::rPropDefault("1"); // version will be different when BaseClass is default costructed or loaded from property stream and getting a default value.
        rOOPS_PROPERTY(index_), "index";
    }
};

class DerivedClass1 : public BaseClass
{
public:
    explicit DerivedClass1(std::string aTitle = "")
        : BaseClass()
        , title_(std::move(aTitle))
    {}
    explicit DerivedClass1(int arIndex)
        : BaseClass(arIndex)
        , title_(std::to_string(arIndex))
    {}
    ~DerivedClass1() override = default;
    std::string getTitle() const override
        { return title_; }
    bool compareObjectPointers(const BaseClass* apR) const override
    {
        if (!BaseClass::compareObjectPointers(apR)) {
            return false;
        }
        if (nullptr == apR) {
            return false;
        }
        const auto* pR = dynamic_cast<const DerivedClass1*>(apR);
        if (nullptr == pR) {
            return false;
        }
        if (title_ != pR->title_) {
            return false;
        }
        if (month_ != pR->month_) {
            return false;
        }
        return true;
    }
    bool operator==(const DerivedClass1& arR) const
    {
        return BaseClass::operator==(arR) && title_ == arR.title_ && month_ == arR.month_;
    }
    bool operator!=(const DerivedClass1& arR) const
    {
        return ! operator==(arR);
    }
private:
    std::string title_;
    test_enums::Month_e month_ = test_enums::Month_e::eJanuary;
    rOOPS_ADD_PROPERTY_INTERFACE(DerivedClass1)
    {
        rOOPS_INHERIT(BaseClass);
        rOOPS_PROPERTY(title_), "title";
        rOOPS_PROPERTY(month_), "month";
    }
};

class DerivedClass2 : public BaseClass
{
public:
    explicit DerivedClass2(int aValue=0)
        : BaseClass(aValue)
        , value_(10*aValue)
        {}
    ~DerivedClass2() override = default;
    std::string getTitle() const override
    {
        return "DerivedClass2/" + std::to_string(getIndex());
    }
    bool compareObjectPointers(const BaseClass* apR) const override
    {
        if (!BaseClass::compareObjectPointers(apR)) {
            return false;
        }
        if (nullptr == apR) {
            return false;
        }
        const auto* pR = dynamic_cast<const DerivedClass2*>(apR);
        if (nullptr == pR) {
            return false;
        }
        if (value_ != pR->value_) {
            return false;
        }
        return true;
    }
    bool operator==(const DerivedClass2& arR) const
    {
        return BaseClass::operator==(arR) && value_ == arR.value_;
    }
private:
    int value_;
    rOOPS_ADD_PROPERTY_INTERFACE(DerivedClass2)
    {
        rOOPS_INHERIT(BaseClass);
        rOOPS_PROPERTY(value_);
    }
};


struct BaseClassPointerComparator
{
    bool operator() (const std::shared_ptr<BaseClass>& arL, const std::shared_ptr<BaseClass>& arR) const
    {
        return *arL < *arR;
    }
};

using int2doubleMap_t = std::map<int, double>;
rOOPS_DECLARE_ASSOCIATIVE_CONTAINER_TYPE_INFO(int2doubleMap_t)

rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::vector<BaseClass*>)
rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::deque<BaseClass*>)
rOOPS_DECLARE_STL_SET_TYPE_INFO(std::set<BaseClass*>)
rOOPS_DECLARE_STL_SET_TYPE_INFO(std::multiset<BaseClass*>)
rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::vector<std::unique_ptr<BaseClass>>)
rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::deque<std::unique_ptr<BaseClass>>)
rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::list<std::shared_ptr<BaseClass>>)
rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::list<DerivedClass1>)

using BaseClassPointerSet = std::set<std::shared_ptr<BaseClass>, BaseClassPointerComparator>;
rOOPS_DECLARE_STL_SET_TYPE_INFO(std::set<std::shared_ptr<BaseClass>>)
rOOPS_DECLARE_STL_SET_TYPE_INFO(BaseClassPointerSet)

using BaseClassPointerMultiSet = std::multiset<std::shared_ptr<BaseClass>, BaseClassPointerComparator>;
rOOPS_DECLARE_STL_SET_TYPE_INFO(std::multiset<std::shared_ptr<BaseClass>>)
rOOPS_DECLARE_STL_SET_TYPE_INFO(BaseClassPointerMultiSet)

rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::vector<test_enums::Month_e>)
rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::deque<test_enums::Month_e>)
rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::list<test_enums::Month_e>)
rOOPS_DECLARE_STL_SET_TYPE_INFO(std::set<test_enums::Month_e>)
