/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rPropTypeInfoCompound.h> // for testing if it is independent of other include files.
#include <oops/rPropInterface.h>
#include <oops/rOopsConfig.h>
#include "BaseDerived.h"
#include "SerializationTest.h"
#include <gtest/gtest.h>
#include <memory>
#include <string>

using namespace rOops;

struct PointerTestClass
{
    virtual ~PointerTestClass()
    {
        delete rawPtr;
    }
    PointerTestClass() = default;
    explicit PointerTestClass(const char* apType)
        : type(apType)
    {
    }
    std::string type;
    BaseClass* rawPtr{ nullptr };
    BaseClass* rawPtr2{ nullptr };
    std::unique_ptr<BaseClass> uniquePtr;
    // TODO: This does not work?.
    BaseClass* uniquePtr2{ nullptr }; // Raw pointer pointed to the object owned by uniquePtr.
    std::shared_ptr<BaseClass> sharedPtr;
    std::shared_ptr<BaseClass> sharedPtr2;
    bool check() const
    {
        if (nullptr != rawPtr2 && rawPtr != rawPtr2) return false;
        if (nullptr != uniquePtr2 && uniquePtr.get() != uniquePtr2) return false;
        if (sharedPtr2 && sharedPtr.get() != sharedPtr2.get()) return false;
        return true;
    }
    bool operator==(const PointerTestClass& arR) const
    {
        using namespace cmp;
        using T = PointerTestClass;
        using D = DerivedClass1;
        if (!check()) return false;
        if (!eq(*this, arR, &T::type)) return false;
        if (!eqPtr<D>(*this, arR, &T::rawPtr)) return false;
        if (!eqPtr<D>(*this, arR, &T::rawPtr2)) return false;
        if (!eqPtr<D>(*this, arR, &T::uniquePtr)) return false;
        if (!eqPtr<D>(*this, arR, &T::uniquePtr2)) return false;
        if (!eqPtr<D>(*this, arR, &T::sharedPtr)) return false;
        if (!eqPtr<D>(*this, arR, &T::sharedPtr2)) return false;
        return true;
    }
    rOOPS_ADD_PROPERTY_INTERFACE(PointerTestClass)
        {
            rOOPS_PROPERTY(rawPtr);
            rOOPS_PROPERTY(rawPtr2);
            rOOPS_PROPERTY(uniquePtr);
            rOOPS_PROPERTY(uniquePtr2);
            rOOPS_PROPERTY(sharedPtr);
            rOOPS_PROPERTY(sharedPtr2);
            rOOPS_PROPERTY(type);
        }
};

TEST(PointerTest, DelayedSavingPointedObjects)
{
    rOops::globalConfig().saveAddress = true;
    try {
        PointerTestClass pp("SavePointer");
        pp.rawPtr = pp.rawPtr2 = new DerivedClass1("Test derived 1.");
        pp.uniquePtr = std::make_unique<DerivedClass1>("Test derived 2.");
        //pp.uniquePtr2 = pp.uniquePtr.get();
        pp.sharedPtr = pp.sharedPtr2 = std::make_shared<DerivedClass1>("Test derived 3.");

        auto pTI = checkClassPropTypeInfo<PointerTestClass>("PointerTestClass", 7);
        // rawPtr
        {
            auto pd = pTI->getPropDesc(0);
            EXPECT_TRUE(nullptr != pd);
            if (nullptr != pd) {
                EXPECT_EQ("rawPtr", pd->propName());
                EXPECT_TRUE(pd->isPointer());
            }
        }
        // uniquePtr
        {
            auto pd = pTI->getPropDesc(2);
            EXPECT_TRUE(nullptr != pd);
            if (nullptr != pd) {
                EXPECT_EQ("uniquePtr", pd->propName());
                EXPECT_TRUE(pd->isPointer());
            }
        }
        // sharedPtr
        {
            auto pd = pTI->getPropDesc(4);
            EXPECT_TRUE(nullptr != pd);
            if (nullptr != pd) {
                EXPECT_EQ("sharedPtr", pd->propName());
                EXPECT_TRUE(pd->isPointer());
            }
        }
        // title
        {
            auto pd = pTI->getPropDesc(6);
            EXPECT_TRUE(nullptr != pd);
            if (nullptr != pd) {
                EXPECT_EQ("type", pd->propName());
                EXPECT_FALSE( pd->isPointer());
            }
        }
        EXPECT_TRUE(nullptr == pTI->getPropDesc(7));

        testSerialization("PointerTestClassSaveAddress", pp);
    }
    catch (const std::exception& arEx) {
        FAIL() << arEx.what();
    }
}

struct PointerTestClassMulti
{
    virtual ~PointerTestClassMulti() = default;
    PointerTestClassMulti() = default;
    explicit PointerTestClassMulti(const char* apType)
            : type(apType)
    {
    }
    PointerTestClassMulti(const PointerTestClassMulti& arR)
        : type(arR.type)
        , sharedPtr(arR.sharedPtr)
        , sharedPtr2(arR.sharedPtr2)
    {
    }
    std::string type;
    std::shared_ptr<BaseClass> sharedPtr;
    std::shared_ptr<BaseClass> sharedPtr2;
    bool check() const
    {
        if (sharedPtr2 && sharedPtr.get() != sharedPtr2.get()) return false;
        return true;
    }
    bool operator==(const PointerTestClassMulti& arR) const
    {
        using namespace cmp;
        using T = PointerTestClassMulti;
        using D = DerivedClass1;
        if (!check()) return false;
        if (!eq(*this, arR, &T::type)) return false;
        if (!eqPtr<D>(*this, arR, &T::sharedPtr)) return false;
        if (!eqPtr<D>(*this, arR, &T::sharedPtr2)) return false;
        return true;
    }
    rOOPS_ADD_PROPERTY_INTERFACE(PointerTestClassMulti)
    {
        rOOPS_PROPERTY(sharedPtr);
        rOOPS_PROPERTY(sharedPtr2);
        rOOPS_PROPERTY(type);
    }
};

TEST(PointerTest, DelayedSavingPointedObjectsMulti)
{
    rOops::globalConfig().saveAddress = true;
    try {
        std::vector<PointerTestClassMulti> vv;
        for (int idx=0; idx<5; ++idx) {
            vv.emplace_back("SavePointer");
            auto& pp = vv.back();
            pp.sharedPtr = pp.sharedPtr2 = std::make_shared<DerivedClass1>("Test derived" + std::to_string(idx));
        }
        testSerializationMulti("PointerTestClassSaveAddressMulti", vv);
    }
    catch (const std::exception& arEx) {
        FAIL() << arEx.what();
    }
}

TEST(PointerTest, DelayedSavingNullPointer)
{
    rOops::globalConfig().saveAddress = true;
    try {
        PointerTestClass pp("SavePointer");
        pp.rawPtr = pp.rawPtr2 = nullptr;
        testSerialization("DelayedSavingNullPointer", pp);
    }
    catch (const std::exception& arEx) {
        FAIL() << arEx.what();
    }
}

TEST(PointerTest, SaveObjectsInPlace)
{
    rOops::globalConfig().saveAddress = false;
    try {
        PointerTestClass pp("SaveInPlace");
        pp.rawPtr = pp.rawPtr2 = new DerivedClass1("Test derived 10.");
        pp.uniquePtr = std::make_unique<DerivedClass1>("Test derived 20.");
        //pp.uniquePtr2 = pp.uniquePtr.get();
        pp.sharedPtr = pp.sharedPtr2 = std::make_shared<DerivedClass1>("Test derived 30.");

        auto pTI = checkClassPropTypeInfo<PointerTestClass>("PointerTestClass", 7);
        // rawPtr
        {
            auto pd = pTI->getPropDesc(0);
            EXPECT_TRUE(nullptr != pd);
            if (nullptr != pd) {
                EXPECT_EQ("rawPtr", pd->propName());
                EXPECT_TRUE(pd->isPointer());
            }
        }
        // uniquePtr
        {
            auto pd = pTI->getPropDesc(2);
            EXPECT_TRUE(nullptr != pd);
            if (nullptr != pd) {
                EXPECT_EQ("uniquePtr", pd->propName());
                EXPECT_TRUE(pd->isPointer());
            }
        }
        // sharedPtr
        {
            auto pd = pTI->getPropDesc(4);
            EXPECT_TRUE(nullptr != pd);
            if (nullptr != pd) {
                EXPECT_EQ("sharedPtr", pd->propName());
                EXPECT_TRUE(pd->isPointer());
            }
        }
        // title
        {
            auto pd = pTI->getPropDesc(6);
            EXPECT_TRUE(nullptr != pd);
            if (nullptr != pd) {
                EXPECT_EQ("type", pd->propName());
                EXPECT_FALSE( pd->isPointer());
            }
        }
        EXPECT_TRUE(nullptr == pTI->getPropDesc(7));

        testSerialization("PointerTestClassSaveInPlace", pp);
    }
    catch (const std::exception& arEx) {
        FAIL() << arEx.what();
    }
}

TEST(PointerTest, SaveInPlaceNullPointer)
{
    rOops::globalConfig().saveAddress = false;
    try {
        PointerTestClass pp("SaveInPlace");
        pp.rawPtr = pp.rawPtr2 = nullptr;
        testSerialization("SaveInPlaceNullPointer", pp);
    }
    catch (const std::exception& arEx) {
        FAIL() << arEx.what();
    }
}

TEST(PointerTest, LoadWithoutAddress)
{
    rOops::globalConfig().saveAddress = false;
    PointerTestClass pp;
    pp.rawPtr = new DerivedClass1("Test derived 1.");
    pp.rawPtr2 = nullptr;
    pp.uniquePtr = std::make_unique<DerivedClass1>("Test derived 2.");
    pp.uniquePtr2 = nullptr;
    pp.sharedPtr = std::make_shared<DerivedClass1>("Test derived 3.");
    pp.sharedPtr2.reset();
    {
        PointerTestClass copy;
        testSerializationClearAddress<PointerTestClass, rOops::rOopsTextFormat, rOops::rOopsTextParser>('T', "LoadWithoutAddressPointerTestClass", pp, copy);
        EXPECT_EQ(pp, copy);
    }
    {
        PointerTestClass copy;
        testSerializationClearAddress<PointerTestClass, rOops::rOopsYamlFormat, rOops::rOopsYamlParser>('T', "LoadWithoutAddressPointerTestClass", pp, copy);
        EXPECT_EQ(pp, copy);
    }
}
