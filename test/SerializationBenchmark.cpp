/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include "BaseDerived.h"
#include "SerializationTest.h"
#include <oops/ExecDataMeas.h>
#include <oops/rOopsSaveLoad.h>
#include <oops/rPropInterface.h>
#include <gtest/gtest.h>
#include <algorithm>
#include <sstream>
#include <string>
#include <deque>
#include <list>
#include <map>
#include <vector>

#if __cplusplus <= 201703L
#define OOPS_BENCHMARK_USE_CEREAL
#endif

#ifdef OOPS_BENCHMARK_USE_CEREAL
#include <cereal/archives/portable_binary.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/deque.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>
#endif

// Config:
constexpr static std::uint64_t cNrOfMsgs = 1000;

using ExecDataMeas3 = rOops::ExecDataMeas<3>;
using namespace rOops;

static unsigned message_counter = 0;

class TestBaseClass
{
    static int baseClassCounter;
public:
    TestBaseClass()
        : index_(++baseClassCounter)
    {}
    explicit TestBaseClass(int arIndex)
        : index_(arIndex)
    {}
    virtual ~TestBaseClass() = default;
public:
    virtual bool compareObjectPointers(const TestBaseClass* apR) const
    {
        return operator==(*apR);
    }
    bool operator==(const TestBaseClass& arR) const
    {
        return version_ == arR.version_ && index_ == arR.index_;
    }
    bool operator!=(const TestBaseClass& arR) const
    {
        return ! operator==(arR);
    }
    bool operator<(const TestBaseClass& arR) const
    {
        return index_ < arR.index_;
    }
public:
    uint32_t version_{ 0 };
    int index_{ 0 };
    #ifdef OOPS_BENCHMARK_USE_CEREAL
    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(CEREAL_NVP(version_));
        archive(CEREAL_NVP(index_));
    }
    #endif
    rOOPS_ADD_PROPERTY_INTERFACE(TestBaseClass)
    {
        rOOPS_PROPERTY(version_), "version", rOops::rPropDefault("1"); // version will be different when BaseClass is default constructed or loaded from property stream and getting a default value.
        rOOPS_PROPERTY(index_), "index";
    }
};

int TestBaseClass::baseClassCounter = 0;

class TestDerivedClass : public TestBaseClass
{
public:
    explicit TestDerivedClass(std::string aTitle = "")
        : TestBaseClass()
        , title_(std::move(aTitle))
    {}
    explicit TestDerivedClass(int arIndex)
        : TestBaseClass(arIndex)
        , title_(std::to_string(arIndex))
    {}
    ~TestDerivedClass() override = default;
    bool compareObjectPointers(const TestBaseClass* apR) const override
    {
        if (!TestBaseClass::compareObjectPointers(apR)) {
            return false;
        }
        if (nullptr == apR) {
            return false;
        }
        const auto* pR = dynamic_cast<const TestDerivedClass*>(apR);
        if (nullptr == pR) {
            return false;
        }
        if (title_ != pR->title_) {
            return false;
        }
        return true;
    }
    bool operator==(const TestDerivedClass& arR) const
    {
        return TestBaseClass::operator==(arR) && title_ == arR.title_;
    }
    bool operator!=(const TestDerivedClass& arR) const
    {
        return ! operator==(arR);
    }
public:
    std::string title_;
    #ifdef OOPS_BENCHMARK_USE_CEREAL
    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(cereal::base_class<TestBaseClass>(this));
        archive(CEREAL_NVP(title_));
    }
    #endif
    rOOPS_ADD_PROPERTY_INTERFACE(TestDerivedClass)
    {
        rOOPS_INHERIT(TestBaseClass);
        rOOPS_PROPERTY(title_), "title";
    }
};

rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::list<TestDerivedClass>)

class TestMessage
{
public:
    virtual ~TestMessage() = default;
    TestMessage() = default;
    TestMessage(std::size_t size, int aValue)
        : int_short(static_cast<short>(++message_counter))
        , int_unsigned(static_cast<unsigned>(10*aValue))
        , int_long(static_cast<long long>(100*aValue))
        , real4((static_cast<float>(0.1f*static_cast<float>(aValue))))
        , real8(0.01*aValue)
    {
        title = "TestMessage_";
        title += std::to_string(message_counter);
        int_vector.reserve(size);
        for (int counter=0; static_cast<std::size_t>(counter) < size; ++counter) {
            int_vector.push_back(counter * aValue);
            double_deque.push_back(3.14 * counter * aValue);
            int2double_map[counter] = 10.0 * counter * aValue;
            message_list.emplace_back(counter * aValue);
        }
    }
    void clear()
    {
        title.clear();
        int_short = 0;
        int_unsigned = 0u;
        int_long = 0;
        real4 = 0.0f;
        real8 = 0.0;
        int_vector.clear();
        double_deque.clear();
        int2double_map.clear();
        message_list.clear();
    }
    std::size_t bytes() const
    {
        std::size_t n = sizeof(*this);
        n += int_vector.size() * sizeof(int_vector[0]);
        n += double_deque.size() * sizeof(double_deque[0]);
        n += int2double_map.size() * sizeof(std::map<int, double>::value_type);
        n += message_list.size() * sizeof(TestDerivedClass);
        return n;
    }
private:
    std::string title = "TestMessage";
    short int_short = 0;
    unsigned int_unsigned = 0;
    long long int_long = 0;
    float real4 = 0.0;
    double real8 = 0.0;
    std::vector<int> int_vector;
    std::deque<double> double_deque;
    int2doubleMap_t int2double_map;  // TODO: YAML format does not support std::map.
    std::list<TestDerivedClass> message_list;
public:
    bool operator==(const TestMessage& arR) const
    {
        using namespace cmp;
        using T = TestMessage;
        if (!eq(*this, arR, &T::title)) return false;
        if (!eq(*this, arR, &T::int_short)) return false;
        if (!eq(*this, arR, &T::int_unsigned)) return false;
        if (!eq(*this, arR, &T::int_long)) return false;
        if (!eq(*this, arR, &T::real4)) return false;
        if (!eq(*this, arR, &T::real8)) return false;
        if (!eq(*this, arR, &T::int_vector)) return false;
        if (!eq(*this, arR, &T::double_deque)) return false;
        if (!eq(*this, arR, &T::int2double_map)) return false;
        if (!eq(*this, arR, &T::message_list)) return false;
        return true;
    }
    #ifdef OOPS_BENCHMARK_USE_CEREAL
    static const uint32_t VERSION = 1U;
    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(CEREAL_NVP(title));
        archive(CEREAL_NVP(int_short));
        archive(CEREAL_NVP(int_unsigned));
        archive(CEREAL_NVP(int_long));
        archive(CEREAL_NVP(real4));
        archive(CEREAL_NVP(real8));
        archive(CEREAL_NVP(int_vector));
        archive(CEREAL_NVP(double_deque));
        archive(CEREAL_NVP(int2double_map));
        archive(CEREAL_NVP(message_list));
    }
    #endif
    rOOPS_ADD_PROPERTY_INTERFACE(TestMessage)
    {
        rOOPS_PROPERTY(title);
        rOOPS_PROPERTY(int_short);
        rOOPS_PROPERTY(int_unsigned);
        rOOPS_PROPERTY(int_long);
        rOOPS_PROPERTY(real4);
        rOOPS_PROPERTY(real8);
        rOOPS_PROPERTY(int_vector);
        rOOPS_PROPERTY(double_deque);
        rOOPS_PROPERTY(int2double_map);
        rOOPS_PROPERTY(message_list);
    }
};
#ifdef OOPS_BENCHMARK_USE_CEREAL
CEREAL_REGISTER_TYPE(TestMessage)
#endif

template <typename MessageT>
class TestData
{
public:
    explicit TestData(std::size_t nrOfMessages)
    {
        msgs.reserve(nrOfMessages);
        for (std::size_t counter=1; counter<=nrOfMessages; ++counter) {
            msgs.emplace_back(100u, static_cast<int>(counter));
        }
    }
    std::vector<MessageT> msgs;
};

template<typename FormatT, typename ParserT, typename MessageT>
void MeasureOops(const std::string& file_name, std::uint8_t protocol)
{
    TestData<MessageT> data(cNrOfMsgs);
    ExecDataMeas3 measure(0, data.msgs.size());
    measure.addHeader({"Size [bytes]", "saving [ns]", "loading [ns]"});
    std::stringstream strm(cStringStreamMode);
    FormatT format(strm, protocol);
    ParserT parser(strm, file_name);
    MessageT copy;
    std::size_t msg_cntr = 0;
    for (const auto& value: data.msgs) {
        ExecDataMeas3::ExecDataArray samples{ 0u };
        auto strm_size_before = strm.tellp();
        auto bgn_save = std::chrono::steady_clock::now();
        save(format, value, "Message");
        strm.flush();
        auto end_save = std::chrono::steady_clock::now();
        samples[0] = static_cast<std::uint64_t>(strm.tellp() - strm_size_before);
        copy.clear();
        auto bgn_load = std::chrono::steady_clock::now();
        load(parser, copy);
        auto end_load = std::chrono::steady_clock::now();
        strm.clear();
        EXPECT_EQ(value, copy);
        samples[1] = static_cast<std::uint64_t>((end_save - bgn_save).count());
        samples[2] = static_cast<std::uint64_t>((end_load - bgn_load).count());
        measure.addExecData(samples);
        ++msg_cntr;
    }
    EXPECT_EQ(data.msgs.size(), msg_cntr);

    // print results
    measure.printStat(file_name, "oops");
}

TEST(SerializationBenchmark, OopsSaveBinaryName)
{
    rOops::globalConfig().writeTypeNameLevel = rOops::OopsConfig::WriteTypeName::eWhenNeeded;
    MeasureOops<rOopsBinaryFormat, rOopsBinaryParser, TestMessage>("TestMessageOopsSaveBinaryName", 'N');
}

TEST(SerializationBenchmark, OopsSaveBinaryNameAlways)
{
    rOops::globalConfig().writeTypeNameLevel = rOops::OopsConfig::WriteTypeName::eAlways;
    MeasureOops<rOopsBinaryFormat, rOopsBinaryParser, TestMessage>("TestMessageOopsSaveBinaryNameAlways", 'N');
}

TEST(SerializationBenchmark, OopsSaveText)
{
    rOops::globalConfig().writeTypeNameLevel = rOops::OopsConfig::WriteTypeName::eWhenNeeded;
    MeasureOops<rOopsTextFormat, rOopsTextParser, TestMessage>("TestMessageOopsSaveText", 'T');
}

TEST(SerializationBenchmark, DISABLED_OopsSaveYaml)
{
    rOops::globalConfig().writeTypeNameLevel = rOops::OopsConfig::WriteTypeName::eWhenNeeded;
    MeasureOops<rOopsYamlFormat, rOopsYamlParser, TestMessage>("TestMessageOopsSaveTextAlways", 'Y');
}


#ifdef OOPS_BENCHMARK_USE_CEREAL
template<typename OutputArchiveT, typename InputArchiveT, typename MessageT>
void MeasureCereal(const std::string& file_name)
{
    TestData<MessageT> data(cNrOfMsgs);
    ExecDataMeas3 measure(0, data.msgs.size());
    measure.addHeader({"Size [bytes]", "saving [ns]", "loading [ns]"});

    std::stringstream strm(cStringStreamMode);
    OutputArchiveT out_archive(strm);
    InputArchiveT inp_archive(strm);

    MessageT copy;
    std::size_t msg_cntr = 0;
    for (const auto& value: data.msgs) {
        ExecDataMeas3::ExecDataArray samples{ 0u };
        auto strm_size_before = strm.tellp();
        auto bgn_save = std::chrono::steady_clock::now();
        out_archive(value);
        strm.flush();
        auto end_save = std::chrono::steady_clock::now();
        samples[0] = static_cast<std::uint64_t>(strm.tellp() - strm_size_before);
        copy.clear();
        auto bgn_load = std::chrono::steady_clock::now();
        inp_archive(copy);
        auto end_load = std::chrono::steady_clock::now();
        strm.clear();
        EXPECT_EQ(value, copy);
        samples[1] = static_cast<std::uint64_t>((end_save - bgn_save).count());
        samples[2] = static_cast<std::uint64_t>((end_load - bgn_load).count());
        measure.addExecData(samples);
        ++msg_cntr;
    }
    EXPECT_EQ(data.msgs.size(), msg_cntr);

    // print results
    measure.printStat(file_name, "_cereal.bin");
}
#endif

TEST(SerializationBenchmark, CerealPortableBinary)
{
    #ifdef OOPS_BENCHMARK_USE_CEREAL
    MeasureCereal<cereal::PortableBinaryOutputArchive, cereal::PortableBinaryInputArchive, TestMessage>("TestMessageCerealBinary");
    #else
    GTEST_SKIP() << "Cereal cannot be compiled for C++20. SerializationBenchmark for Cereal Portable Binary is skipped.";
    #endif
}

TEST(SerializationBenchmark, CerealBinary)
{
    #ifdef OOPS_BENCHMARK_USE_CEREAL
    MeasureCereal<cereal::BinaryOutputArchive, cereal::BinaryInputArchive, TestMessage>("TestMessageCerealBinary");
    #else
    GTEST_SKIP() << "Cereal cannot be compiled for C++20. SerializationBenchmark for Cereal Binary is skipped.";
    #endif
}

