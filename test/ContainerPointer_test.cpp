/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rPropTypeInfoContainer.h> // for testing if it is independent of other include files.
#include <oops/rPropInterface.h>
#include <oops/rOopsConfig.h>
#include "BaseDerived.h"
#include "SerializationTest.h"
#include <gtest/gtest.h>
#include <list>
#include <string>

rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::list<int*>)

struct ContainerPointerTestClass
{
    std::string type{ "ContainerPointerTestClass" };
    std::list<int*> intPtrList;
    std::vector<BaseClass*> rawPtrVector;
    std::deque<std::unique_ptr<BaseClass>> uniquePtrDeque;
    std::shared_ptr<BaseClass> objectPtr;
    std::list<std::shared_ptr<BaseClass>> sharedPtrList;
    BaseClassPointerSet sharedPtrSet;
    BaseClassPointerMultiSet sharedPtrMultiSet;
    virtual ~ContainerPointerTestClass()
    {
        // Delete allocated objects.
        if (!intPtrList.empty()) {
            delete intPtrList.front();
            delete intPtrList.back();
        }
        for (auto obj_ptr : rawPtrVector) {
            delete obj_ptr;
        }
    }
    ContainerPointerTestClass()
        : sharedPtrSet(BaseClassPointerComparator())
        , sharedPtrMultiSet(BaseClassPointerComparator())
    {}
    void init()
    {
        auto int_ptr = new int(1000);
        intPtrList.push_back(int_ptr);
        intPtrList.push_back(int_ptr);
        int_ptr = new int(2000);
        intPtrList.push_back(int_ptr);
        intPtrList.push_back(int_ptr);
        rawPtrVector.push_back( new DerivedClass1("Test derived 1.") );
        rawPtrVector.push_back( new DerivedClass2(1) );
        uniquePtrDeque.push_back( std::make_unique<DerivedClass1>("Test derived 2.") );
        uniquePtrDeque.push_back( std::make_unique<DerivedClass2>(2) );
        auto shared_ptr1 = std::make_shared<DerivedClass1>("Test derived 3.");
        sharedPtrList.push_back(shared_ptr1);
        sharedPtrList.push_back(shared_ptr1);
        objectPtr = std::make_shared<DerivedClass2>(3);
        sharedPtrSet.insert(objectPtr);
        sharedPtrSet.insert(std::make_shared<DerivedClass2>(4));
        auto shared_ptr2 = std::make_shared<DerivedClass1>("Test derived 4.");
        sharedPtrMultiSet.insert(shared_ptr2);
        sharedPtrMultiSet.insert(shared_ptr2);
    }
    void initNoAddress()
    {
        intPtrList.push_back(new int(1000));
        intPtrList.push_back(new int(2000));
        rawPtrVector.push_back( new DerivedClass1("Test derived 1.") );
        rawPtrVector.push_back( new DerivedClass2(1) );
        uniquePtrDeque.push_back( std::make_unique<DerivedClass1>("Test derived 2.") );
        uniquePtrDeque.push_back( std::make_unique<DerivedClass2>(3) );
        sharedPtrList.push_back(std::make_shared<DerivedClass1>("Test derived 4."));
        sharedPtrList.push_back(std::make_shared<DerivedClass2>(5));
        sharedPtrSet.insert(std::make_shared<DerivedClass2>(6));
        sharedPtrSet.insert(std::make_shared<DerivedClass2>(7));
        sharedPtrMultiSet.insert(std::make_shared<DerivedClass1>("Test derived 8."));
        sharedPtrMultiSet.insert(std::make_shared<DerivedClass1>("Test derived 9."));
    }
    bool operator==(const ContainerPointerTestClass& arR) const
    {
        if (type != arR.type) {
            return false;
        }
        if (!std::equal(intPtrList.begin(), intPtrList.end(), arR.intPtrList.begin(), arR.intPtrList.end(), cmp::comparePointers<int*>)) {
            return false;
        }
        if (!std::equal(rawPtrVector.begin(), rawPtrVector.end(), arR.rawPtrVector.begin(), arR.rawPtrVector.end(), cmp::compareObjectPointers<BaseClass*>)) {
            return false;
        }
        if (!std::equal(uniquePtrDeque.begin(), uniquePtrDeque.end(), arR.uniquePtrDeque.begin(), arR.uniquePtrDeque.end(), cmp::compareSmartPointers<std::unique_ptr<BaseClass>>)) {
            return false;
        }
        if (!cmp::eqPtr<DerivedClass2>(*this, arR, &ContainerPointerTestClass::objectPtr)) {
            return false;
        }
        if (!std::equal(sharedPtrList.begin(), sharedPtrList.end(), arR.sharedPtrList.begin(), arR.sharedPtrList.end(), cmp::compareSmartPointers<std::shared_ptr<BaseClass>>)) {
            return false;
        }
        if (!std::equal(sharedPtrSet.begin(), sharedPtrSet.end(), arR.sharedPtrSet.begin(), arR.sharedPtrSet.end(), cmp::compareSmartPointers<std::shared_ptr<BaseClass>>)) {
            return false;
        }
        if (!std::equal(sharedPtrMultiSet.begin(), sharedPtrMultiSet.end(), arR.sharedPtrMultiSet.begin(), arR.sharedPtrMultiSet.end(), cmp::compareSmartPointers<std::shared_ptr<BaseClass>>)) {
            return false;
        }
        return true;
    }
    rOOPS_ADD_PROPERTY_INTERFACE(ContainerPointerTestClass)
        {
            rOOPS_PROPERTY(intPtrList);
            rOOPS_PROPERTY(rawPtrVector);
            rOOPS_PROPERTY(uniquePtrDeque);
            rOOPS_PROPERTY(objectPtr);
            rOOPS_PROPERTY(sharedPtrList);
            rOOPS_PROPERTY(sharedPtrSet);
            rOOPS_PROPERTY(sharedPtrMultiSet);
            rOOPS_PROPERTY(type);
        }
};



TEST(ContainerPointerTest, SerializeDelayedSavingPointedObjects)
{
    rOops::globalConfig().saveAddress = true;
    try {
        ContainerPointerTestClass pp;
        pp.init();

        auto pTI = checkClassPropTypeInfo<ContainerPointerTestClass>("ContainerPointerTestClass", 8);
        // intPtrList
        {
            auto pd = pTI->getPropDesc(0);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("intPtrList", pd->propName());
            EXPECT_FALSE(pd->isPointer());
            EXPECT_TRUE(pd->isSTLContainer());
        }
        // rawPtrVector
        {
            auto pd = pTI->getPropDesc(1);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("rawPtrVector", pd->propName());
            EXPECT_FALSE(pd->isPointer());
            EXPECT_TRUE(pd->isSTLContainer());
        }
        // uniquePtrDeque
        {
            auto pd = pTI->getPropDesc(2);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("uniquePtrDeque", pd->propName());
            EXPECT_FALSE(pd->isPointer());
            EXPECT_TRUE(pd->isSTLContainer());
        }
        // objectPtr
        {
            auto pd = pTI->getPropDesc(3);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("objectPtr", pd->propName());
            EXPECT_TRUE(pd->isPointer());
            EXPECT_FALSE(pd->isSTLContainer());
        }
        // sharedPtrList
        {
            auto pd = pTI->getPropDesc(4);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("sharedPtrList", pd->propName());
            EXPECT_FALSE(pd->isPointer());
            EXPECT_TRUE(pd->isSTLContainer());
        }
        // sharedPtrSet
        {
            auto pd = pTI->getPropDesc(5);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("sharedPtrSet", pd->propName());
            EXPECT_FALSE(pd->isPointer());
            EXPECT_TRUE(pd->isSTLContainer());
        }
        // sharedPtrMultiSet
        {
            auto pd = pTI->getPropDesc(6);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("sharedPtrMultiSet", pd->propName());
            EXPECT_FALSE(pd->isPointer());
            EXPECT_TRUE(pd->isSTLContainer());
        }
        // title
        {
            auto pd = pTI->getPropDesc(7);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("type", pd->propName());
            EXPECT_FALSE( pd->isPointer());
        }
        EXPECT_TRUE(nullptr == pTI->getPropDesc(8));

        testSerialization("SaveAddressContainerPointerTestClass", pp);
    }
    catch (const std::exception& arEx) {
        FAIL() << arEx.what();
    }
}

TEST(ContainerPointerTest, SerializePointedObjectsInPlace)
{
    rOops::globalConfig().saveAddress = false;
    try {
        ContainerPointerTestClass pp;
        pp.init();

        auto pTI = checkClassPropTypeInfo<ContainerPointerTestClass>("ContainerPointerTestClass", 8);
        // intPtrList
        {
            auto pd = pTI->getPropDesc(0);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("intPtrList", pd->propName());
            EXPECT_FALSE(pd->isPointer());
            EXPECT_TRUE(pd->isSTLContainer());
        }
        // rawPtrVector
        {
            auto pd = pTI->getPropDesc(1);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("rawPtrVector", pd->propName());
            EXPECT_FALSE(pd->isPointer());
            EXPECT_TRUE(pd->isSTLContainer());
        }
        // uniquePtrDeque
        {
            auto pd = pTI->getPropDesc(2);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("uniquePtrDeque", pd->propName());
            EXPECT_FALSE(pd->isPointer());
            EXPECT_TRUE(pd->isSTLContainer());
        }
        // objectPtr
        {
            auto pd = pTI->getPropDesc(3);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("objectPtr", pd->propName());
            EXPECT_TRUE(pd->isPointer());
            EXPECT_FALSE(pd->isSTLContainer());
        }
        // sharedPtrList
        {
            auto pd = pTI->getPropDesc(4);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("sharedPtrList", pd->propName());
            EXPECT_FALSE(pd->isPointer());
            EXPECT_TRUE(pd->isSTLContainer());
        }
        // sharedPtrSet
        {
            auto pd = pTI->getPropDesc(5);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("sharedPtrSet", pd->propName());
            EXPECT_FALSE(pd->isPointer());
            EXPECT_TRUE(pd->isSTLContainer());
        }
        // sharedPtrMultiSet
        {
            auto pd = pTI->getPropDesc(6);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("sharedPtrMultiSet", pd->propName());
            EXPECT_FALSE(pd->isPointer());
            EXPECT_TRUE(pd->isSTLContainer());
        }
        // title
        {
            auto pd = pTI->getPropDesc(7);
            EXPECT_TRUE(nullptr != pd);
            EXPECT_EQ("type", pd->propName());
            EXPECT_FALSE( pd->isPointer());
        }
        EXPECT_TRUE(nullptr == pTI->getPropDesc(8));

        testSerialization("SaveInPlaceContainerPointerTestClass", pp);
    }
    catch (const std::exception& arEx) {
        FAIL() << arEx.what();
    }
}

TEST(ContainerPointerTest, LoadWithoutAddress)
{
    rOops::globalConfig().saveAddress = false;
    ContainerPointerTestClass pp;
    pp.initNoAddress();
    {
        ContainerPointerTestClass copy;
        testSerializationClearAddress<ContainerPointerTestClass, rOops::rOopsTextFormat, rOops::rOopsTextParser>('T', "LoadWithoutAddressPointerTestClass", pp, copy);
        EXPECT_EQ(pp, copy);
    }
    {
        ContainerPointerTestClass copy;
        testSerializationClearAddress<ContainerPointerTestClass, rOops::rOopsYamlFormat, rOops::rOopsYamlParser>('T', "LoadWithoutAddressPointerTestClass", pp, copy);
        EXPECT_EQ(pp, copy);
    }
}
