/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include "../example/sTut_02_inherit.h"
#include "../example/sTut_04a_enum.h"
#include "../example/sTut_04b_enum.h"
#include "../example/sTut_05_list.h"
#include "../example/sTut_06_map.h"
#include "../example/s2D.h"
#include "SerializationTest.h"
#include <gtest/gtest.h>
#include <string>

using namespace rOops;


TEST(TutorialTest, sTut_02)
{
    sTut_02::InternalB::B b(11, 22);
    testSerialization("TutorialTest_sTut_02", b);
}

TEST(TutorialTest, sTut_02_mult)
{
    std::vector<sTut_02::InternalA::A> vb;
    vb.emplace_back(11, 111);
    vb.emplace_back(22, 222);
    vb.emplace_back(33, 333);
    testSerializationMulti("TutorialTest_sTut_02_multi", vb);
}

TEST(TutorialTest, sTut_04a)
{
    A::DateTime date_time;
    date_time.Date.Year = 1997;
    date_time.Date.Month = A::Month_e::eMay;
    date_time.Date.Day = 19;
    date_time.Date.WeekDay = A::eFriday;
    date_time.Time.Hour = 22;
    date_time.Time.Min = 33;
    date_time.Time.Sec = 44;
    testSerialization("TutorialTest_sTut_04a", date_time);
}

TEST(TutorialTest, sTut_04b)
{
    B::DateTime date_time;
    date_time.Date.Year = 1997;
    date_time.Date.Month = B::Months::eMay;
    date_time.Date.Day = 19;
    date_time.Date.WeekDay = B::Days::eFriday;
    date_time.Time.Hour = 22;
    date_time.Time.Min = 33;
    date_time.Time.Sec = 44;
    testSerialization("TutorialTest_sTut_04b", date_time);
}

TEST(TutorialTest, sTut_05)
{
    sTut_05::STLLists lists;
    lists.init( 123 );
    testSerialization("TutorialTest_sTut_05", lists, false);
}

TEST(TutorialTest, sTut_05_multi)
{
    std::vector<sTut_05::STLLists> lists;
    lists.emplace_back();
    lists.back().init( 123 );
    lists.emplace_back();
    lists.back().init( 456 );
    lists.emplace_back();
    lists.back().init( 789 );
    testSerializationMulti("TutorialTest_sTut_05_multi", lists, false);
}

TEST(TutorialTest, sTut_06)
{
    sTut_06::STLMaps maps;
    maps.init( 123 );
    testSerialization("TutorialTest_sTut_06", maps, false);
}

TEST(TutorialTest, sTut_06_multi)
{
    std::vector<sTut_06::STLMaps> maps;
    maps.emplace_back();
    maps.back().init( 123 );
    maps.emplace_back();
    maps.back().init( 456 );
    maps.emplace_back();
    maps.back().init( 789 );
    testSerializationMulti("TutorialTest_sTut_06_multi", maps, false);
}

TEST(TutorialTest, s2D)
{
    s2D::s2DColor_s color;
    testSerialization("TutorialTest_s2DColor", color);

    s2D::s2DRect rect;
    testSerialization("TutorialTest_s2DRect", rect);
}

TEST(TutorialTest, s2D_multi)
{
    std::vector<s2D::s2DColor_s> color;
    color.emplace_back(200, 0, 0);
    color.emplace_back(0, 200, 0);
    color.emplace_back(0, 0, 200);
    testSerializationMulti("TutorialTest_s2DColor_multi", color);

    vector<s2D::s2DRect> rect;
    rect.emplace_back(0,0,10,20);
    rect.emplace_back(100,100,110,120);
    testSerializationMulti("TutorialTest_s2DRect_multi", rect);
}
