/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include "../example/sTut_02_inherit.h"
#include "../example/sTut_05_list.h"
#include "../example/sTut_06_map.h"
#include "../example/s2D.h"
#include "rOopsOStream.h"
#include <gtest/gtest.h>
#include <string>

using namespace rOops;

template < class ObjT>
void iteratorTest(const char* aTitle, const ObjT& aObj)
{
    auto config = rOops::globalConfig();
    config.writeTypeNameLevel = rOops::OopsConfig::WriteTypeName::eAlways;
    // Serialize the original and oops loaded version using FormatT and compare the strings.
    std::stringstream strmOrig;
    rOops::rOopsTextFormat frmtOrig(strmOrig, 'T', config);
    save(frmtOrig, aObj, "IteratorTest");
    std::stringstream strmIter;
    rOops::rOopsTextFormat frmtIter(strmIter, 'T', config);
    rOops::rOopsOStream<rOops::rOopsTextFormat> iter_strm(frmtIter);
    iter_strm.saveObj(rPropGetTypeInfo(&aObj)->getIter(&aObj, "IteratorTest"));

    std::string orig_str = strmOrig.str();
    std::string iter_str = strmIter.str();
//    std::string orig_str = clearAddressInOopsText(strmOrig.str());
//    std::string iter_str = clearAddressInOopsText(strmIter.str());
    if (orig_str != iter_str) {
        std::cout << aTitle << ": Original != Iterator:\n";
        std::cout << "Orig : " << orig_str << std::endl;
        std::cout << "Iter : " << iter_str << std::endl;
        EXPECT_EQ(orig_str, iter_str);
    }
}

TEST(IteratorTest, sTut_02)
{
    sTut_02::InternalB::B b(11, 22);
    iteratorTest("sTut_02", b);
}

TEST(IteratorTest, sTut_05)
{
    sTut_05::STLLists lists;
    lists.init( 123 );
    iteratorTest("TutorialTest_sTut_05_list", lists);
}

TEST(IteratorTest, sTut_06)
{
    sTut_06::STLMaps maps;
    maps.init( 123 );
    iteratorTest("TutorialTest_sTut_06", maps);
}

TEST(IteratorTest, s2D)
{
    s2D::s2DColor_s color;
    iteratorTest("TutorialTest_s2DColor", color);

    s2D::s2DRect rect;
    iteratorTest("TutorialTest_s2DRect", rect);
}
