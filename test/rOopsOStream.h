#pragma once

/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
Output Stream class based on oops iterators for testing if iterators work correctly.
*/

#include <oops/rPropConstIter.h>
#include <oops/rPropTypeInfo.h>
#include <oops/rPropLog.h>
#include <oops/rPropDescriptor.h>
#include <list>
#include <set>
#include <string>

namespace rOops
{

template <typename FormatT>
class rOopsOStream
{
public:
    explicit rOopsOStream(FormatT& arFrmt);
    ~rOopsOStream();
    //void reset() {}
    void saveObj( const rPropConstIterator& arPropIter );
protected:
    enum class SaveState_e {eKey, eBlock, eList, eMap};
    void _saveObjRecursive(const rPropConstIterator &arPropIter, SaveState_e aSaveState);
    void _saveUnsaved();
    void writeData(const void* apObjAddr);
    void writePointedData(const void* apObjAddr);
protected:
    FormatT& frmt_;
    /// Map of address and node of objects.
    /// It is used for finding multiple references to the same object.
    std::set<const void*> objIdSet_;
    std::list<rPropConstIterator> unsaved_;
}; //class rOopsOStream


template <typename FormatT>
rOopsOStream<FormatT>::rOopsOStream(FormatT& arFrmt)
    : frmt_(arFrmt)
    , objIdSet_()
    , unsaved_()
{
} //Ctor()

template <typename FormatT>
rOopsOStream<FormatT>::~rOopsOStream()
{
    objIdSet_.clear();
    unsaved_.clear();
} //Dtor()

template <typename FormatT>
void rOopsOStream<FormatT>::saveObj( const rPropConstIterator& arPropIter )
{
    // Probably never happen.
    if(nullptr == arPropIter.objAddr() ) throw std::runtime_error(rOOPS_BUILD_EXCEPTION_MESSAGE("Object address is NULL!"));
    // Clean items of previous calls.
    objIdSet_.clear();
    unsaved_.clear();
    // Create root node. Cannot get the name of the object through the iterator.
    frmt_.writeProtocolVersion();
    frmt_.setTopLevel(arPropIter.itemObjTypeInfo(), arPropIter.propName() );
    frmt_.writeBlockBegin();
    _saveObjRecursive(arPropIter, SaveState_e::eBlock);
    frmt_.writeBlockEnd();
    frmt_.writeValueSeparator(true);
    _saveUnsaved();
}

template <typename FormatT>
void rOopsOStream<FormatT>::_saveObjRecursive(const rPropConstIterator &arPropIter, SaveState_e aSaveState)
{
    // Iterate through all properties of *arPropIter.
    // Open branch.
    rPropConstIterator iP = arPropIter.begin();
    bool data_is_written = false;
    while( !iP.isEnd() ) {
        // Get object address.
        const void* objAddr = iP.objAddr();
        if (nullptr != iP.keyAddr()) {
            _saveObjRecursive(iP.keyIter(), SaveState_e::eKey);
            frmt_.writeKeyValueSeparator();
        }
        // Get property name.
        frmt_.setName(iP.propName());
        // TODO: Saving object, and later having a pointer to the object does not work properly. It saves the object twice.
        //if( iP.isPointer() && (iP.checkFlags(rPropFlag::cSaveAddress) || objIdSet_.end()!=objIdSet_.find(objAddr)) ) {
        if( iP.isPointer() && frmt_.config().saveAddress ) {
            // Pointer, create reference node.
            frmt_.setType( iP.itemObjTypeInfo(), OopsConfig::WriteTypeName::eAlways, 1 );
            frmt_.writePointer(objAddr);
            if(nullptr != objAddr && objIdSet_.end()==objIdSet_.find(objAddr) ) {
                // Add the iterator of the object to the list of unsaved objects.
                unsaved_.push_back( iP );
            } //if
            data_is_written = true;
        } //if
        else {
            auto write_type_name = (iP.isPointer()) ? OopsConfig::WriteTypeName::eNever : OopsConfig::WriteTypeName::eWhenNeeded;
            if (iP.isPointer()) {
                frmt_.setAddress(objAddr);
            }
            if (nullptr == objAddr ) {
                if( iP.isPointer() ) {
                    frmt_.setType(iP.itemObjTypeInfo(), OopsConfig::WriteTypeName::eAlways, 1);
                    frmt_.writePointer(objAddr);
                    data_is_written = true;
                }
                else {
                    // objAddr is null when std::optional does not contain a value.
                    // Skip this member.
                    data_is_written = false;
                }
            }
            else if( iP.isValue() ) {
                // Simple property or key of a map. Store value.
                frmt_.setType( iP.itemObjTypeInfo(), OopsConfig::WriteTypeName::eAlways, 1 );
                if (iP.isRunTime()) {
                    std::string value = iP.itemObjTypeInfo()->value(objAddr);
                    if (value.empty()) {
                        data_is_written = false;
                    }
                    else {
                        writeData(objAddr);
                        data_is_written = true;
                    }
                }
                else {
                    writeData(objAddr);
                    data_is_written = true;
                }
                // Do not save every single data object to the set of saved objects.
                // It is unlikely, that applications use references to single data members.
                // If they do, it just will be saved twice.
                //objIdSet_.insert( objAddr );
            }
            else if( iP.isCompound() ) {
                // Compound property (struct or class).
                frmt_.setType( iP.itemObjTypeInfo(), write_type_name, 1 );
                frmt_.writeBlockBegin();
                _saveObjRecursive(iP, SaveState_e::eBlock);
                frmt_.writeBlockEnd();
                objIdSet_.insert( objAddr );
                data_is_written = true;
            } else if( iP.isSTLContainer() ) {
                // Container property.
                frmt_.setType( iP.itemObjTypeInfo(), write_type_name, iP.nrOfElements() );
                frmt_.writeListBegin();
                _saveObjRecursive(iP, SaveState_e::eList);
                frmt_.writeListEnd();
                objIdSet_.insert( objAddr );
                data_is_written = true;
            } else if( iP.isAssociativeContainer() ) {
                // Container property.
                frmt_.setType( iP.itemObjTypeInfo(), write_type_name, iP.nrOfElements() );
                frmt_.writeMapBegin();
                _saveObjRecursive(iP, SaveState_e::eMap);
                frmt_.writeMapEnd();
                objIdSet_.insert( objAddr );
                data_is_written = true;
            } else {
                throw std::runtime_error(rOOPS_BUILD_EXCEPTION_MESSAGE("Program error"));
            } //if ... else
        }
        ++iP;
        // Write separator according to the current state.
        // It may need to take into account that the item written before the separator
        // is the last element of a container, and skip writing the separator in that case.
        switch (aSaveState) {
            case SaveState_e::eBlock:
                if (data_is_written) {
                    frmt_.writeValueSeparator(iP.isEnd());
                }
                break;
            case SaveState_e::eList:
            case SaveState_e::eMap: // there is no writeMapSeparator(aEnd) function.
                frmt_.writeListSeparator(iP.isEnd());
                break;
            case SaveState_e::eKey:
                // key separator is already written.
                break;
            default:
                throw std::runtime_error(rOOPS_BUILD_EXCEPTION_MESSAGE("ERROR: unexpected state!"));
        } //switch
    } //while
} //rOopsOStream::_saveObjRecursive()


template <typename FormatT>
void rOopsOStream<FormatT>::_saveUnsaved()
{
    while( !unsaved_.empty() ) {
        rPropConstIterator iP = unsaved_.front();
        if( objIdSet_.end() == objIdSet_.find(iP.objAddr()) ) {
            // Have not saved.
            const void* objAddr = iP.objAddr();
            if (nullptr == objAddr) {
                throw std::runtime_error(rOOPS_BUILD_EXCEPTION_MESSAGE("NULL pointer in the unsaved objects list."));
            }
            frmt_.setType(iP.itemObjTypeInfo(), OopsConfig::WriteTypeName::eNever, 1);
            if( iP.isValue() ) {
                writePointedData(objAddr);
            }
            else {
                frmt_.writeBlockBegin();
                _saveObjRecursive(iP, SaveState_e::eBlock);
                objIdSet_.insert( iP.objAddr() );
                writeEnd(frmt_, iP.itemObjTypeInfo());
            }
            frmt_.writeValueSeparator(iP.isEnd());
        } //if
        unsaved_.pop_front();
    } //while
    frmt_.flush();
} //_saveUnsaved()

template <typename FormatT>
void rOopsOStream<FormatT>::writeData(const void* apObjAddr)
{
    std::string value = frmt_.typeInfo()->value(apObjAddr);
    if (!value.empty()) {
        // Write value when it is not empty.
        // If property name is empty, and type name is not required only the value is written!
        frmt_.writeSizeTypeAndName();
        // String and character values may need bounding characters.
        char separator1 = frmt_.typeInfo()->getOpeningCharacter();
        char separator2 = frmt_.typeInfo()->getClosingCharacter();
        if (separator1) frmt_.strm() << separator1;
        frmt_.strm() << value;
        if (separator2) frmt_.strm() << separator2;
    }
}

template <typename FormatT>
void rOopsOStream<FormatT>::writePointedData(const void* apObjAddr)
{
    // Use address as name if name is empty.
    frmt_.setPropName('*' + std::to_string(reinterpret_cast<std::uintptr_t>(apObjAddr)));
    //frmt_.writeTypeNameAbove_ = OopsConfig::WriteTypeName::eNever;
    frmt_.writeSizeTypeAndName();
    char separator1 = frmt_.typeInfo()->getOpeningCharacter();
    char separator2 = frmt_.typeInfo()->getClosingCharacter();
    if (separator1) frmt_.strm() << separator1;
    frmt_.strm() << frmt_.typeInfo()->value(apObjAddr);
    if (separator2) frmt_.strm() << separator2;
}

} //namespace rOops
