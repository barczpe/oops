/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include "BaseDerived.h"
#include "SerializationTest.h"
#include <oops/rPropInterface.h>
#include <oops/rPropFind.h>
#include <oops/rOopsConfig.h>
#include <gtest/gtest.h>

// BaseDerived.h is header only.
// The static class member of BaseClass is defined here.
int BaseClass::baseClassCounter = 0;

void ClassVersionsUnknownProperties_test()
{
    rOops::PreprocessorDefines_t ext_def = { {"HAS_TITLE_2","TRUE"} };
    std::string strOops =
        "MapContainerOops = !DerivedClass1 {\n"
        "  BaseClass = !BaseClass {\n"
        "    xyz=\"Kovacs\";\n"
        "    version = 1;\n"
        "    index = 111;\n"
        "  };\n"
        "  title_2 = \"title_2 of derived1\";\n"
        "  title = \"title of derived1\";\n"
        "};\n";
    DerivedClass1 dc1Oops(11);
    std::stringstream oops_strm(strOops);
    rOops::rOopsTextParser oops_parser(oops_strm, "UnknownPropertiesOops", ext_def);
    load(oops_parser, dc1Oops);
    EXPECT_EQ("title of derived1", dc1Oops.getTitle());
    // findPath() works only with begin() here. getIter() returns an iterator for the top level having one element only.
    // This would not work:
    //      auto iterStartOops = rPropGetTypeInfo(&dc1Oops)->getIter(&dc1Oops, "dc1Oops");
    auto iterStartOops = rPropGetTypeInfo(&dc1Oops)->begin(&dc1Oops, "dc1Oops");
    // Test property 'title'.
    auto iterOops = iterStartOops;
    rOops::findPath(iterOops, "title");
    EXPECT_FALSE(iterOops.isEnd());
    if (!iterOops.isEnd()) {
        EXPECT_EQ("title of derived1", iterOops.value());
    }
    // Test unknown property 'title_2'.
    iterOops = iterStartOops;
    rOops::findPath(iterOops, "title_2");
    EXPECT_FALSE(iterOops.isEnd());
    if (!iterOops.isEnd()) {
        EXPECT_EQ("title_2 of derived1", iterOops.value());
    }
    // Test property 'version' in the base class.
    iterOops = iterStartOops;
    rOops::findPath(iterOops, "BaseClass.version");
    EXPECT_FALSE(iterOops.isEnd());
    if (!iterOops.isEnd()) {
        EXPECT_EQ("1", iterOops.value());
    }
    // Test unknown property 'xyz' in the base class.
    iterOops = iterStartOops;
    rOops::findPath(iterOops, "BaseClass.xyz");
    EXPECT_FALSE(iterOops.isEnd());
    if (!iterOops.isEnd()) {
        EXPECT_EQ("Kovacs", iterOops.value());
    }
    // Save back to file in oops text format.
    std::stringstream strmOops;
    rOops::rOopsTextFormat frmtOops(strmOops);
    save(frmtOops, dc1Oops, "UnknownPropertiesOops");
    //std::cout << strmOops.str() << std::endl;
    std::string expected_oops;
    switch (rOops::globalConfig().writeTypeNameLevel) {
    case rOops::OopsConfig::WriteTypeName::eNever:
    case rOops::OopsConfig::WriteTypeName::eWhenNeeded:
        expected_oops =
            "# Oops Text Stream v2.0\n"
            "UnknownPropertiesOops = !DerivedClass1 {\n"
            "  BaseClass = {\n"
            "    version = 1;\n"
            "    index = 111;\n"
            "    xyz = \"Kovacs\";\n"
            "  };\n"
            "  title = \"title of derived1\";\n"
            "  month = January;\n"
            "  title_2 = \"title_2 of derived1\";\n"
            "};\n";
        break;
    case rOops::OopsConfig::WriteTypeName::eAlways:
        expected_oops =
            "# Oops Text Stream v2.0\n"
            "UnknownPropertiesOops = !DerivedClass1 {\n"
            "  BaseClass = !BaseClass {\n"
            "    version = !uint32_t 1;\n"
            "    index = !int32_t 111;\n"
            "    xyz = \"Kovacs\";\n"
            "  };\n"
            "  title = !std::string \"title of derived1\";\n"
            "  month = !test_enums::Month_e January;\n"
            "  title_2 = \"title_2 of derived1\";\n"
            "};\n";
        break;
    }
    EXPECT_EQ(expected_oops, strmOops.str());

    // This test is not implemented for flow style yaml.
    if (rOops::globalConfig().flowStyleBlock || rOops::globalConfig().flowStyleList) return;

    std::string strYaml =
        "MapContainerYaml: !DerivedClass1\n"
        "  BaseClass: !BaseClass\n"
        "    abcd: !float 54321.09876\n"
        "    version: 1\n"
        "    index = 111;\n"
        "  title_2: \"title_2 of yaml\"\n"
        "  title: \"title of yaml\"\n";
    DerivedClass1 dc1Yaml(123);
    std::stringstream yaml_strm(strYaml);
    rOops::rOopsYamlParser yaml_parser(yaml_strm, "UnknownPropertiesYaml", ext_def);
    load(yaml_parser, dc1Yaml);
    EXPECT_EQ("title of yaml", dc1Yaml.getTitle());
    auto iterYamlStart = rPropGetTypeInfo(&dc1Yaml)->begin(&dc1Yaml,"dc1Yaml");
    // Test title_2.
    auto iterYaml = iterYamlStart;
    rOops::findPath(iterYaml, "title_2");
    EXPECT_FALSE(iterYaml.isEnd());
    if (!iterYaml.isEnd()) {
        EXPECT_EQ("title_2 of yaml", iterYaml.value());
    }
    // Test abcd.
    iterYaml = iterYamlStart;
    rOops::findPath(iterYaml, "BaseClass.abcd");
    EXPECT_FALSE(iterYaml.isEnd());
    if (!iterYaml.isEnd()) {
        EXPECT_EQ("54321.09876", iterYaml.value());
    }
    // Save back to yaml file.
    std::stringstream strmYaml;
    rOops::rOopsYamlFormat frmtYaml(strmYaml);
    save(frmtYaml, dc1Yaml, "UnknownPropertiesYaml");
    //std::cout << strmYaml.str() << std::endl;
    std::string expected_yaml =
        "UnknownPropertiesYaml:\n"
        "  BaseClass:\n"
        "    version: 1\n"
        "    index: 123\n"
        "    abcd: \"54321.09876\"\n"
        "  title: \"title of yaml\"\n"
        "  month: January\n"
        "  title_2: \"title_2 of yaml\"\n";
    switch (rOops::globalConfig().writeTypeNameLevel) {
    case rOops::OopsConfig::WriteTypeName::eNever:
    case rOops::OopsConfig::WriteTypeName::eWhenNeeded:
        expected_yaml =
            "UnknownPropertiesYaml: !DerivedClass1\n"
            "  BaseClass:\n"
            "    version: 1\n"
            "    index: 123\n"
            "    abcd: \"54321.09876\"\n"
            "  title: \"title of yaml\"\n"
            "  month: January\n"
            "  title_2: \"title_2 of yaml\"\n";
        break;
    case rOops::OopsConfig::WriteTypeName::eAlways:
        expected_yaml =
            "UnknownPropertiesYaml: !DerivedClass1\n"
            "  BaseClass: !BaseClass\n"
            "    version: !uint32_t 1\n"
            "    index: !int32_t 123\n"
            "    abcd: !float \"54321.09876\"\n"
            "  title: !std::string \"title of yaml\"\n"
            "  month: !test_enums::Month_e January\n"
            "  title_2: \"title_2 of yaml\"\n";
        break;
    }
    EXPECT_EQ(expected_yaml, strmYaml.str());
}

TEST(ClassVersions, UnknownProperties)
{
    ClassVersionsUnknownProperties_test();
    // Repeat this test. It stores the unknown properties in the property descriptor table.
    ClassVersionsUnknownProperties_test();
    ClassVersionsUnknownProperties_test();
}

TEST(ClassVersions, DefaultValue)
{
    std::string strOops1 =
        "MapContainerOops = !DerivedClass1 {\n"
        "  BaseClass = !BaseClass {\n"
        "    xyz=\"Kovacs\";\n"
        "  };\n"
        "  title_2 = \"title_2 of derived1\";\n"
        "  title = \"title of derived1\";\n"
        "};\n";
    DerivedClass1 dc1Oops(222);
    EXPECT_EQ(0u, dc1Oops.getVersion()) << "Version set int the default constructor.";
    std::stringstream strm1(strOops1);
    rOops::rOopsTextParser oops_parser1(strm1, "DefaultValue");
    load(oops_parser1, dc1Oops);
    EXPECT_EQ(1u, dc1Oops.getVersion()) << "Version set to default, because it is missing from the stream.";
    std::string strOops2 =
        "MapContainerOops = !DerivedClass1 {\n"
        "  BaseClass = !BaseClass {\n"
        "    xyz=\"Szabo\";\n"
        "    version = 2;\n"
        "    index = 222;\n"
        "  };\n"
        "  title_2 = \"title_2 of derived1\";\n"
        "  title = \"title of derived1\";\n"
        "};\n";
    std::stringstream strm2(strOops2);
    rOops::rOopsTextParser oops_parser2(strm2, "DefaultValue");
    load(oops_parser2, dc1Oops);
    EXPECT_EQ(2u, dc1Oops.getVersion()) << "Version loaded from the stream.";
}

class TestObject1
{
public:
    virtual ~TestObject1() = default;
    explicit TestObject1(int aInt=1)
        : str1(std::to_string(aInt))
        , int1(aInt)
    {}
    // Members are not private for simpler test program.
    std::string str1;
    int int1{ 0 };
    rOOPS_ADD_PROPERTY_INTERFACE(TestObject1)
        {
            rOOPS_VERSION(1);
            rOOPS_PROPERTY(str1);
            rOOPS_PROPERTY(int1);
        }
};

class TestObject2
{
public:
    virtual ~TestObject2() = default;
    explicit TestObject2(int aInt=1)
        : str2(std::to_string(aInt))
        , int2(aInt)
    {}
    void validate(std::uint8_t aVersion)
    {
        auto pTI = rPropGetTypeInfo(this);
        if (aVersion < pTI->version()) {
            rOops::updateProperty(this, "str1", str2);
            rOops::updateProperty(this, "int1", int2);
        }
    }
    // Members are not private for simpler test program.
    std::string str2;
    int int2{ 0 };
    rOOPS_ADD_PROPERTY_INTERFACE(TestObject2)
        {
            rOOPS_VERSION(2);
            rOOPS_PROPERTY(str2);
            rOOPS_PROPERTY(int2);
            rOOPS_VALIDATE(&TestObject2::validate);
        }
};

TEST(ClassVersions, AddVersionNumber)
{
    // Save TestObj1. It is version 1.0 of TestObject.
    TestObject1 to1_123(123);
    std::stringstream strmOops;
    rOops::rOopsTextFormat frmtOops(strmOops);
    save(frmtOops, to1_123, "TestObject_123");

    std::cout << strmOops.str() << std::endl;

    // Load the stream back to TestObject2, which is version 1.2 of TestObject.
    TestObject2 to2_123;
    rOops::rOopsTextParser oops_parser(strmOops, "TestObject_123");
    load(oops_parser, to2_123);
    EXPECT_EQ("123", to2_123.str2);
    EXPECT_EQ(123, to2_123.int2);
}

TEST(ClassVersions, MajorVersionNumberConflict)
{
    // Load the TestObject1 from a stream containing a different major version of TestObject.
    std::string TestObjectStr =
        "TestObject = !TestObject1/2 {\n"
        "  int1 = 123;\n"
        "};    TestObject1 to1;\n";
    TestObject1 to1;
    try {
        std::stringstream strm(TestObjectStr);
        rOops::rOopsTextParser oops_parser(strm, "TestObject_v2");
        load(oops_parser, to1);
        FAIL() << "load() did not throw!";
    }
    catch (const std::runtime_error& arEx) {
        EXPECT_EQ("TestObject_v2(3): Version numbers are different: 1 != 2", std::string(arEx.what()));
    }
    catch (...) {
        FAIL() << "load() thrown an unexpected exception.";
    }
}

struct Port
{
    virtual ~Port() = default;
    std::string id;
    unsigned count = 0;
    std::string datafile;
    std::string metafile;
    rOOPS_ADD_PROPERTY_INTERFACE(Port)
    {
        rOOPS_PROPERTY(id);
        rOOPS_PROPERTY(count);
        rOOPS_PROPERTY(datafile);
        rOOPS_PROPERTY(metafile);
    }
};
using PortList = std::vector<Port>;
rOOPS_DECLARE_STL_LIST_TYPE_INFO(PortList)

struct Module
{
    virtual ~Module() = default;
    std::string id;
    std::string type;
    Port port;
    PortList ports;
    bool operator<(const Module& rhs) const { return id < rhs.id;  }
    rOOPS_ADD_PROPERTY_INTERFACE(Module)
    {
        rOOPS_PROPERTY(id);
        rOOPS_PROPERTY(type);
        rOOPS_PROPERTY(port);
        rOOPS_PROPERTY(ports);
    }
};
using ModuleList = std::vector<Module>;
rOOPS_DECLARE_STL_LIST_TYPE_INFO(ModuleList)
using ModuleMap = std::set<Module>;
rOOPS_DECLARE_STL_SET_TYPE_INFO(ModuleMap)

struct Session
{
    virtual ~Session() = default;
    std::string id;
    std::string type;
    ModuleList modules;
    //ModuleMap modules;
    rOOPS_ADD_PROPERTY_INTERFACE(Session)
    {
        rOOPS_PROPERTY(id);
        rOOPS_PROPERTY(type);
        rOOPS_PROPERTY(modules);
    }
};

TEST(ClassVersions, ReproduceFileLoadErrorYaml)
{
    std::string strYaml1 =
            "session:\n"
            "  id: 'session_1'\n"
            "  type: 'session_type_1'\n"
            "  modules:\n"
            "    -\n"
            "      id: \"radar\"\n"
            "      type: \"RadarLiveModule\"\n"
            "      port:\n"
            "        id: \"F_RADAR_C\"\n"
            "        count: 32\n"
            "        datafile: \"radar.F_RADAR_C.dat\"\n"
            "        metafile: \"radar.F_RADAR_C_meta.dat\"\n"
            "        files: xyz\n"
            "        meta:\n";
    std::string strYaml2 =
            "session:\n"
            "  id: 'session_2'\n"
            "  type: 'session_type_2'\n"
            "  modules:\n"
            "    -\n"
            "      id: \"radar\"\n"
            "      type: \"RadarLiveModule\"\n"
            "      ports:\n"
            "        - id: \"F_RADAR_C\"\n"
            "          count: 32\n"
            "          datafile: \"radar.F_RADAR_C.dat\"\n"
            "          metafile: \"radar.F_RADAR_C_meta.dat\"\n"
            "          files: xyz\n"
            "          meta:\n";
    Session session1;
    std::stringstream strm1(strYaml1);
    rOops::rOopsYamlParser yaml_parser1(strm1, "session");
    load(yaml_parser1, session1);
    auto iterStartOops = rPropGetTypeInfo(&session1)->begin(&session1, "session");
    // Test property 'type'.
    auto iterOops = iterStartOops;
    rOops::findPath(iterOops, "type");
    EXPECT_FALSE(iterOops.isEnd());
    if (!iterOops.isEnd()) {
        EXPECT_EQ("session_type_1", iterOops.value());
    }
    // Test property 'modules.port.datafile'.
    iterOops = iterStartOops;
    rOops::findPath(iterOops, "modules.port.datafile");
    EXPECT_FALSE(iterOops.isEnd());
    if (!iterOops.isEnd()) {
        EXPECT_EQ("radar.F_RADAR_C.dat", iterOops.value());
    }
    // Test unknown property 'modules.port.files'.
    iterOops = iterStartOops;
    rOops::findPath(iterOops, "modules.port.files");
    EXPECT_FALSE(iterOops.isEnd());
    if (!iterOops.isEnd()) {
        EXPECT_EQ("xyz", iterOops.value());
    }

    Session session2;
    std::stringstream strm2(strYaml2);
    rOops::rOopsYamlParser yaml_parser2(strm2, "session");
    load(yaml_parser2, session2);
}

TEST(ClassVersions, ReproduceFileLoadErrorText)
{
    std::string str1 =
            "session = {\n"
            "  id = 'session_1';\n"
            "  type = 'session_type_1';\n"
            "  modules = [ {\n"
            "      id = \"radar\";\n"
            "      type = \"RadarLiveModule\";\n"
            "      port = {\n"
            "        id = \"F_RADAR_C\";\n"
            "        count = 32;\n"
            "        datafile = \"radar.F_RADAR_C.dat\";\n"
            "        metafile = \"radar.F_RADAR_C_meta.dat\";\n"
            "        files = \"xyz\";\n"
            "        meta = \"\";\n"
            "      };\n"
            "   } ];\n"
            "};\n";
    std::string str2 =
            "session = {\n"
            "  id = 'session_2';\n"
            "  type = 'session_type_2';\n"
            "  modules = [ {\n"
            "      id = \"radar\";\n"
            "      type = \"RadarLiveModule\";\n"
            "      ports = [\n"
            "        {\n"
            "          id = \"F_RADAR_C\";\n"
            "          count = 32;\n"
            "          datafile = \"radar.F_RADAR_C.dat\";\n"
            "          metafile = \"radar.F_RADAR_C_meta.dat\";\n"
            "          files = \"xyz\";\n"
            "          meta = \"\";\n"
            "        }"
            "      ];\n"
            "   } ];\n"
            "};\n";
    Session session1;
    std::stringstream strm1(str1);
    rOops::rOopsTextParser text_parser1(strm1, "session");
    load(text_parser1, session1);
    auto iterStartOops = rPropGetTypeInfo(&session1)->begin(&session1, "session");
    // Test property 'type'.
    auto iterOops = iterStartOops;
    rOops::findPath(iterOops, "type");
    EXPECT_FALSE(iterOops.isEnd());
    if (!iterOops.isEnd()) {
        EXPECT_EQ("session_type_1", iterOops.value());
    }
    // Test property 'modules.port.datafile'.
    iterOops = iterStartOops;
    rOops::findPath(iterOops, "modules.port.datafile");
    EXPECT_FALSE(iterOops.isEnd());
    if (!iterOops.isEnd()) {
        EXPECT_EQ("radar.F_RADAR_C.dat", iterOops.value());
    }
    // Test unknown property 'modules.port.files'.
    iterOops = iterStartOops;
    rOops::findPath(iterOops, "modules.port.files");
    EXPECT_FALSE(iterOops.isEnd());
    if (!iterOops.isEnd()) {
        EXPECT_EQ("xyz", iterOops.value());
    }

    Session session2;
    std::stringstream strm2(str2);
    rOops::rOopsTextParser text_parser2(strm2, "session");
    load(text_parser2, session2);
}
