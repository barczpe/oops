/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rPropTypeInfoAssociativeContainer.h> // for testing if it is independent of other include files.
#include <oops/rPropInterface.h>
#include "BaseDerived.h"
#include "SerializationTest.h"
#include <string>
#include <map>
#include <unordered_map>
#include <gtest/gtest.h>


namespace rOops
{
    namespace test
    {

        struct MapData
        {
            explicit MapData(int aVal=0)
                : val(aVal)
                , str(std::string("str") + std::to_string(aVal))
                {}
            virtual ~MapData() = default;
            int val{ 0 };
            std::string str{ "0" };
            bool operator==(const MapData& arR) const
            {
                return val == arR.val && str == arR.str;
            }
            rOOPS_ADD_PROPERTY_INTERFACE(rOops::test::MapData)
                {
                    rOOPS_PROPERTY(val);
                    rOOPS_PROPERTY(str);
                }
        };
        using IntStringMap = std::multimap<short, std::string>;
        using IntDataMap = std::map<int32_t, rOops::test::MapData>;
        using IntDataHash = std::unordered_map<int, rOops::test::MapData>;
        using BaseClassMap = std::map<BaseClass, rOops::test::MapData>;
        using UniquePointerHash = std::unordered_map<int32_t, std::unique_ptr<BaseClass>>;
        using SharedPointerMultiMap = std::multimap<int32_t, std::shared_ptr<BaseClass>>;
    }
}
rOOPS_DECLARE_ASSOCIATIVE_CONTAINER_TYPE_INFO(rOops::test::IntStringMap)
rOOPS_DECLARE_ASSOCIATIVE_CONTAINER_TYPE_INFO(rOops::test::IntDataMap)
rOOPS_DECLARE_ASSOCIATIVE_CONTAINER_TYPE_INFO(rOops::test::IntDataHash)
rOOPS_DECLARE_ASSOCIATIVE_CONTAINER_TYPE_INFO(rOops::test::BaseClassMap)
rOOPS_DECLARE_ASSOCIATIVE_CONTAINER_TYPE_INFO(rOops::test::UniquePointerHash)
rOOPS_DECLARE_ASSOCIATIVE_CONTAINER_TYPE_INFO(rOops::test::SharedPointerMultiMap)

class MapContainer
{
public:
    virtual ~MapContainer() = default;
    void add(int aKey, int aVal)
    {
        strMap_.insert( rOops::test::IntStringMap::value_type((short)aKey, std::string("str") + std::to_string(aVal)) );
        dataMap_[aKey] = rOops::test::MapData(3*aVal);
        dataHash_[aKey] = rOops::test::MapData(5*aVal);
        baseClassMap_[BaseClass(aKey)] = rOops::test::MapData(7*aVal);
        uniquePtrHash_.emplace(3*aKey,   std::make_unique<BaseClass>(aKey));
        uniquePtrHash_.emplace(3*aKey+1, std::make_unique<DerivedClass1>(aKey));
        uniquePtrHash_.emplace(3*aKey+2, std::make_unique<DerivedClass2>(aKey));
        sharedPtrMultiMap_.emplace(aKey, std::make_shared<BaseClass>(aKey));
        sharedPtrMultiMap_.emplace(aKey, std::make_shared<DerivedClass1>(aKey));
        sharedPtrMultiMap_.emplace(aKey, std::make_shared<DerivedClass2>(aKey));
    }
    void addToDataMap(int aKey, int aVal)
    {
        dataMap_[aKey] = rOops::test::MapData(aVal);
    }
    bool operator==(const MapContainer& arR) const
    {
        if (strMap_ != arR.strMap_) return false;
        if (dataMap_ != arR.dataMap_) return false;
        if (dataHash_ != arR.dataHash_) return false;
        if (baseClassMap_ != arR.baseClassMap_) return false;
        if (uniquePtrHash_.size() != arR.uniquePtrHash_.size()) return false;
        for (const auto& pair : uniquePtrHash_) {
            const std::unique_ptr<BaseClass>& lhs = pair.second;
            const std::unique_ptr<BaseClass>& rhs = arR.uniquePtrHash_.at(pair.first);
            if (!cmp::compareSmartPointers(lhs,rhs)) return false;
        }
        if (sharedPtrMultiMap_.size() != arR.sharedPtrMultiMap_.size()) return false;
        auto iL = sharedPtrMultiMap_.begin();
        auto iR = arR.sharedPtrMultiMap_.begin();
        while (iL != sharedPtrMultiMap_.end()) {
            if (iL->first != iR->first) return false;
            if (!cmp::compareSmartPointers(iL->second,iR->second)) return false;
            ++iR; ++iL;
        }
        return true;
    }
private:
    rOops::test::IntStringMap strMap_;
    rOops::test::IntDataMap dataMap_;
    rOops::test::IntDataHash dataHash_;
    rOops::test::BaseClassMap baseClassMap_;
    rOops::test::UniquePointerHash uniquePtrHash_;
    rOops::test::SharedPointerMultiMap sharedPtrMultiMap_;
    rOOPS_ADD_PROPERTY_INTERFACE(MapContainer)
    {
        rOOPS_PROPERTY(strMap_).name("strMap");
        rOOPS_PROPERTY(dataMap_).name("dataMap");
        rOOPS_PROPERTY(dataHash_).name("dataHash");
        rOOPS_PROPERTY(baseClassMap_).name("baseClassMap");
        rOOPS_PROPERTY(uniquePtrHash_).name("uniquePtrHash");
        rOOPS_PROPERTY(sharedPtrMultiMap_).name("sharedPtrMultiMap");
    }
};

TEST(AssociativeContainerTest, PropTypeInfoAssociative)
{
    {
        auto pTI = rPropGetTypeInfo( static_cast<rOops::test::IntDataMap*>(nullptr) );
        EXPECT_TRUE(pTI->isAssociativeContainer());
        EXPECT_FALSE(pTI->isSTLContainer());
        EXPECT_FALSE(pTI->isCompound());
        EXPECT_FALSE(pTI->isValue());
        EXPECT_EQ(pTI, rOops::rPropGetTypeInfoByName("rOops::test::IntDataMap"));

        auto pTIKey = pTI->keyTypeInfo();
        EXPECT_EQ(pTIKey, rOops::rPropGetTypeInfoByName("int32_t"));
        auto pTIElement = pTI->elementTypeInfo();
        EXPECT_EQ(pTIElement, rOops::rPropGetTypeInfoByName("rOops::test::MapData"));
    }
    {
        auto pTI = rPropGetTypeInfo( static_cast<rOops::test::IntDataHash*>(nullptr) );
        EXPECT_TRUE(pTI->isAssociativeContainer());
        EXPECT_FALSE(pTI->isSTLContainer());
        EXPECT_FALSE(pTI->isCompound());
        EXPECT_FALSE(pTI->isValue());
        EXPECT_EQ(pTI, rOops::rPropGetTypeInfoByName("rOops::test::IntDataHash"));

        auto pTIKey = pTI->keyTypeInfo();
        EXPECT_EQ(pTIKey, rOops::rPropGetTypeInfoByName("int32_t"));
        auto pTIElement = pTI->elementTypeInfo();
        EXPECT_EQ(pTIElement, rOops::rPropGetTypeInfoByName("rOops::test::MapData"));
    }
}

TEST(AssociativeContainerTest, MapOfStructuresDifferentOrder)
{
    // Empty lines are tested as well.
    std::string str = {
          "\n"
          "MapContainerOops = !MapContainer {\n"
          "\n"
          "  dataMap[3] = !rOops::test::IntDataMap <\n"
          "    3 = !rOops::test::MapData {\n"
          "      val = 333;\n"
          "      str = \"str333\";\n"
          "    },\n"
          "\n"
          "    2 = !rOops::test::MapData {\n"
          "      val = 222;\n"
          "      str = \"str222\";\n"
          "    },\n"
          "\n"
          "    1 = !rOops::test::MapData {\n"
          "      val = 111;\n"
          "      str = \"str111\";\n"
          "    }\n"
          "  >;\n"
          "\n"
          "};\n"
          "\n"
    };
    MapContainer dOops;
    std::stringstream strm(str);
    rOops::rOopsTextParser oops_parser(strm, "AssociativeContainerTest.MapOfStructuresDifferentOrder");
    load(oops_parser, dOops);

    MapContainer dc;
    dc.addToDataMap(3, 333);
    dc.addToDataMap(2, 222);
    dc.addToDataMap(1, 111);
    EXPECT_EQ(dc, dOops);
}

TEST(AssociativeContainerTest, MapOfStructures)
{
    MapContainer dc;
    for (int ii=1; ii<4; ++ii) {
        dc.add(ii, 1111*ii);
    }
    testSerialization("SaveMapOfStructures", dc, false); // Do not test Yaml format.
}

TEST(AssociativeContainerTest, EmptyMapOfStructures)
{
    MapContainer dc;
    testSerialization("EmptyMapOfStructures", dc, false); // Do not test Yaml format.
}
