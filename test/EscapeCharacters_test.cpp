/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include "SerializationTest.h"
#include <oops/rPropInterface.h>
#include <gtest/gtest.h>
#include <string>

using namespace rOops;

template <typename FormatT, typename DataT>
std::string testSave(const DataT& data)
{
    const auto* pT = rPropGetTypeInfo(&data);
    std::stringstream strm;
    rOops::OopsConfig cfg;
    cfg.writeTypeNameLevel = rOops::OopsConfig::WriteTypeName::eNever;
    FormatT frmt(strm, 'T', cfg);
    frmt.setName("v");
    frmt.setType(pT, rOops::OopsConfig::WriteTypeName::eAlways, 1);
    pT->save(frmt, &data);
    return strm.str();
}

TEST(EscapeCharacters, SaveCharOops)
{
    using format_t = rOops::rOopsTextFormat;
    EXPECT_EQ("v = ' '", testSave<format_t>(' '));
    EXPECT_EQ(R"(v = '\n')", testSave<format_t>('\n'));
    EXPECT_EQ(R"(v = '\n')", testSave<format_t>('\012'));
    EXPECT_EQ(R"(v = '\r')", testSave<format_t>('\x0D'));
    EXPECT_EQ(R"(v = 'z')", testSave<format_t>('z'));
}

TEST(EscapeCharacters, SaveCharYaml)
{
    using format_t = rOops::rOopsYamlFormat;
    EXPECT_EQ("v: ' '", testSave<format_t>(' '));
    EXPECT_EQ(R"(v: '\n')", testSave<format_t>('\n'));
    EXPECT_EQ(R"(v: '\n')", testSave<format_t>('\012'));
    EXPECT_EQ(R"(v: '\r')", testSave<format_t>('\x0D'));
    EXPECT_EQ(R"(v: 'z')", testSave<format_t>('z'));
}

TEST(EscapeCharacters, SaveStringOops)
{
    using format_t = rOops::rOopsTextFormat;
    EXPECT_EQ(R"(v = "xyz")", testSave<format_t>(std::string("xyz")));
    EXPECT_EQ(R"(v = "\tabc\n\tcdf\r\n")", testSave<format_t>(std::string("\tabc\n\tcdf\r\n")));
}

TEST(EscapeCharacters, SaveStringYaml)
{
    using format_t = rOops::rOopsYamlFormat;
    EXPECT_EQ(R"(v: "xyz")", testSave<format_t>(std::string("xyz")));
    EXPECT_EQ(R"(v: "\tabc\n\tcdf\r\n")", testSave<format_t>(std::string("\tabc\n\tcdf\r\n")));
}

template <typename ParserT, typename DataT>
DataT testLoad(const std::string& input)
{
    const auto* pT = rPropGetTypeInfo(static_cast<DataT*>(nullptr));
    std::stringstream strm(input);
    ParserT parser(strm, "Test");
    DataT data;
    rOopsParserContext ctx;
    parser.parse(ctx);
    pT->load(parser, &data, ctx);
    EXPECT_EQ("v", ctx.propName_);
    return data;
}

TEST(EscapeCharacters, LoadCharOops)
{
    using parser_t = rOops::rOopsTextParser;
    EXPECT_EQ(' ', (testLoad<parser_t, char>("v = ' '")));
    EXPECT_EQ('\'', (testLoad<parser_t, char>(R"(v = '\'')")));
    EXPECT_EQ('\"', (testLoad<parser_t, char>(R"(v = '\"')")));
    EXPECT_EQ('\?', (testLoad<parser_t, char>(R"(v = '\?')")));
    EXPECT_EQ('\\', (testLoad<parser_t, char>(R"(v = '\\')")));
    EXPECT_EQ('\a', (testLoad<parser_t, char>(R"(v = '\a')")));
    EXPECT_EQ('\b', (testLoad<parser_t, char>(R"(v = '\b')")));
    EXPECT_EQ('\f', (testLoad<parser_t, char>(R"(v = '\f')")));
    EXPECT_EQ('\n', (testLoad<parser_t, char>(R"(v = '\n')")));
    EXPECT_EQ('\r', (testLoad<parser_t, char>(R"(v = '\r')")));
    EXPECT_EQ('\t', (testLoad<parser_t, char>(R"(v = '\t')")));
    EXPECT_EQ('\v', (testLoad<parser_t, char>(R"(v = '\v')")));
    // Octal numbers:
    EXPECT_EQ('\n', (testLoad<parser_t, char>(R"(v = '\12')")));
    EXPECT_EQ('\n', (testLoad<parser_t, char>(R"(v = '\012')")));
    EXPECT_EQ('\n', (testLoad<parser_t, char>(R"(v = '\0012')")));
    // Hexadecimal numbers:
    EXPECT_EQ('\r', (testLoad<parser_t, char>(R"(v = '\xd')")));
    EXPECT_EQ('\r', (testLoad<parser_t, char>(R"(v = '\x0D')")));
    EXPECT_EQ('\r', (testLoad<parser_t, char>(R"(v = '\x00D')")));
    EXPECT_EQ('z', (testLoad<parser_t, char>(R"(v = '\x7A')")));
}

TEST(EscapeCharacters, LoadCharYaml)
{
    using parser_t = rOops::rOopsYamlParser;
    EXPECT_EQ(' ', (testLoad<parser_t, char>("v: ' '")));
    EXPECT_EQ('\'', (testLoad<parser_t, char>(R"(v: '\'')")));
    EXPECT_EQ('\"', (testLoad<parser_t, char>(R"(v: '\"')")));
    EXPECT_EQ('\?', (testLoad<parser_t, char>(R"(v: '\?')")));
    EXPECT_EQ('\\', (testLoad<parser_t, char>(R"(v: '\\')")));
    EXPECT_EQ('\a', (testLoad<parser_t, char>(R"(v: '\a')")));
    EXPECT_EQ('\b', (testLoad<parser_t, char>(R"(v: '\b')")));
    EXPECT_EQ('\f', (testLoad<parser_t, char>(R"(v: '\f')")));
    EXPECT_EQ('\n', (testLoad<parser_t, char>(R"(v: '\n')")));
    EXPECT_EQ('\r', (testLoad<parser_t, char>(R"(v: '\r')")));
    EXPECT_EQ('\t', (testLoad<parser_t, char>(R"(v: '\t')")));
    EXPECT_EQ('\v', (testLoad<parser_t, char>(R"(v: '\v')")));
    // Octal numbers:
    EXPECT_EQ('\n', (testLoad<parser_t, char>(R"(v: '\12')")));
    EXPECT_EQ('\n', (testLoad<parser_t, char>(R"(v: '\012')")));
    EXPECT_EQ('\n', (testLoad<parser_t, char>(R"(v: '\0012')")));
    // Hexadecimal numbers:
    EXPECT_EQ('\r', (testLoad<parser_t, char>(R"(v: '\xd')")));
    EXPECT_EQ('\r', (testLoad<parser_t, char>(R"(v: '\x0D')")));
    EXPECT_EQ('\r', (testLoad<parser_t, char>(R"(v: '\x00D')")));
    EXPECT_EQ('z', (testLoad<parser_t, char>(R"(v: '\x7A')")));
}

TEST(EscapeCharacters, LoadStringOops)
{
    using parser_t = rOops::rOopsTextParser;
    EXPECT_EQ("  ! A-Z a-z", (testLoad<parser_t, std::string>(R"(v = "\40 \41 \101-\132 \141-\172")")));
    EXPECT_EQ("  ! A-Z a-z", (testLoad<parser_t, std::string>(R"(v = "\x20 \x21 \x41-\x5A \x61-\x7A")")));
}

TEST(EscapeCharacters, LoadStringYaml)
{
    using parser_t = rOops::rOopsYamlParser;
    EXPECT_EQ("  ! A-Z a-z", (testLoad<parser_t, std::string>(R"(v: "\40 \41 \101-\132 \141-\172")")));
    EXPECT_EQ("  ! A-Z a-z", (testLoad<parser_t, std::string>(R"(v: "\x20 \x21 \x41-\x5A \x61-\x7A")")));
}

TEST(EscapeCharacters, Integration)
{
    struct CharStringTest
    {
        virtual ~CharStringTest() = default;
        char c{ ' ' };
        std::string s;
        rOOPS_ADD_PROPERTY_INTERFACE(CharStringTest)
        {
            rOOPS_PROPERTY(c);
            rOOPS_PROPERTY(s);
        }
        bool operator==(const CharStringTest& arR) const
        {
            return (c==arR.c && s==arR.s);
        }
    }; //class Point<>

    CharStringTest t;
    t.c = 'A';  t.s = "0x41"; testSerialization("Integration", t);
    t.c = '\0'; t.s = "escapes: \' \" \? \\ \a \b \f \n \r \t \v"; testSerialization("Integration", t);
}

