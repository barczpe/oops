#pragma once

/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
Base and derived classes for testing polymorphism.
*/

#include <oops/rPropInterface.h>

class FlatBase
{
public:
    FlatBase() = default;
    FlatBase(uint32_t aCounter, double aValue)
        : counter_(aCounter)
        , value_(aValue)
    {}
    virtual ~FlatBase() = default;
    uint32_t counter() const noexcept { return counter_;  }
    double value() const noexcept { return value_; }
private:
    uint32_t counter_ = 0;
    double   value_ = 0.0;
    rOOPS_ADD_PROPERTY_INTERFACE(FlatBase)
    {
        rOOPS_PROPERTY(counter_).name("counter");
        rOOPS_PROPERTY(value_).name("value");
    }
};

class FlatDerivedA : public FlatBase
{
public:
    FlatDerivedA() = default;
    FlatDerivedA(const std::string& aTitle, uint32_t aCounter, double aValue)
        : FlatBase(aCounter, aValue)
        , title_(aTitle)
        {}
    ~FlatDerivedA() override = default;
    std::string title() const { return title_; }
    bool operator==(const FlatDerivedA& arR) const
    {
        return
            counter() == arR.counter() &&
            value() == arR.value() &&
            title_ == arR.title_;
    }
private:
    std::string title_;
    rOOPS_ADD_PROPERTY_INTERFACE(FlatDerivedA)
        {
            rOOPS_ADD_BASE_CLASS_PROPERTIES(FlatBase);
            rOOPS_PROPERTY(title_).name("title");
        }
};
