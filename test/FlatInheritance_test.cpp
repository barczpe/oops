/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include "BaseDerivedFlat.h"
#include "SerializationTest.h"
#include <oops/rPropInterface.h>
#include <gtest/gtest.h>
#include <string>


using namespace rOops;


TEST(FlatInheritanceTest, DerivedA)
{
    FlatDerivedA a1("a1", 1, 0.0);
    const rPropTypeInfo* pTI = checkClassPropTypeInfo<FlatDerivedA>("FlatDerivedA", 3);
    EXPECT_EQ( "counter", pTI->getPropDesc(0)->propName() );
    EXPECT_EQ( "value", pTI->getPropDesc(1)->propName() );
    EXPECT_EQ( "title", pTI->getPropDesc(2)->propName() );
    EXPECT_EQ( 1u, pTI->nrOfElements(&a1) );

    std::stringstream str_strm1;
    rOops::rOopsTextFormat frmt1(str_strm1);
    save( frmt1, a1, "Flat_a1" );
    std::stringstream str_strm2;
    std::string ref1;
    switch (rOops::globalConfig().writeTypeNameLevel) {
    case rOops::OopsConfig::WriteTypeName::eNever:
    case rOops::OopsConfig::WriteTypeName::eWhenNeeded:
        ref1 = "# Oops Text Stream v2.0\nFlat_a1 = !FlatDerivedA {\n  counter = 1;\n  value = 0;\n  title = \"a1\";\n};\n";
        break;
    case rOops::OopsConfig::WriteTypeName::eAlways:
        ref1 = "# Oops Text Stream v2.0\nFlat_a1 = !FlatDerivedA {\n  counter = !uint32_t 1;\n  value = !double 0;\n  title = !std::string \"a1\";\n};\n";
        break;
    }
    EXPECT_EQ(ref1, str_strm1.str());
}

TEST(FlatInheritanceTest, SaveDerivedA)
{
    FlatDerivedA a1("A1", 456, 7.89);
    testSerialization("Flat_a1", a1);
}
