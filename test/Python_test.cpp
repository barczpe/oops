/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2021:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include "../example/sTut_02_inherit.h"
#include "../example/sTut_04a_enum.h"
#include "../example/sTut_04b_enum.h"
#include "../example/sTut_05_list.h"
#include "../example/sTut_06_map.h"
#include "../example/s2D.h"
#include "SerializationTest.h"
#include <oops/rOops2Python.h>
#include <gtest/gtest.h>
#include <string>

using namespace rOops;

TEST(PythonTest, DISABLED_AllClassDefinition)
{
    std::stringstream py_strm(cStringStreamMode);
    rOops::rOops2Python py_frmt(py_strm);
    std::ofstream os("AllClassDefinition.py");
    py_frmt.saveAllClassDefinitions(os);
    auto result = system("python3.8 AllClassDefinition.py");
    EXPECT_EQ(0, result);
}

template <class ObjT>
void testPython(const char* aName, const ObjT& arObj, bool aDoPrint=false)
{
    if (0 != system("python3.8 --version")) {
        GTEST_SKIP() << "python3.8 is missing. Skip PythonTest.";
    }

    auto config = rOops::globalConfig();
    config.writeTypeNameLevel = rOops::OopsConfig::WriteTypeName::eNever;
    auto yaml_file_name = std::string(aName) + ".yaml";
    std::ofstream yaml_strm(yaml_file_name);
    rOops::rOopsYamlFormat yaml_frmt(yaml_strm, 'Y', config);
    save(yaml_frmt, arObj, aName);
    yaml_strm.close();

    std::stringstream py_strm(cStringStreamMode);
    rOops::rOops2Python py_frmt(py_strm);
    py_frmt.saveObj(rPropGetTypeInfo(&arObj)->getIter(&arObj, aName));
    std::stringstream class_def_strm(cStringStreamMode);
    py_frmt.saveClassDefinitions(class_def_strm);

    // Write to file and execute it in Python.
    auto file_name = std::string(aName) + ".py";
    std::ofstream os(file_name);
    os << class_def_strm.str() << std::endl;
    os << "\n# Data:\n\n";
    os << py_strm.str() << std::endl;
    os.close();
    auto cmd = std::string("python3.8 ") + file_name;
    auto result = system(cmd.c_str());
    EXPECT_EQ(0, result);

    if (aDoPrint || result!=0) {
        std::cout << "========== python ==========" << std::endl;
        std::cout << class_def_strm.str() << std::endl;
        std::cout << py_strm.str() << std::endl;
        std::cout << "============================" << std::endl;
    }
}

TEST(PythonTest, sTut_02)
{
    sTut_02::InternalA::A a(11, 22);
    testPython("sTut_02_A", a);

    sTut_02::InternalB::B b(11, 22);
    testPython("sTut_02_B", b);
}

TEST(PythonTest, sTut_05)
{
    sTut_05::STLLists lists;
    lists.init( 123 );
    testPython("TutorialTest_sTut_05", lists, false);
}

TEST(PythonTest, sTut_06)
{
    sTut_06::STLMaps maps;
    maps.init( 123 );
    testPython("TutorialTest_sTut_06", maps, false);
}

TEST(PythonTest, s2D)
{
    s2D::s2DColor_s color;
    testSerialization("TutorialTest_s2DColor", color);

    s2D::s2DRect rect;
    testPython("TutorialTest_s2DRect", rect);
}
