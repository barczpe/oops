/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2021:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rPropInterface.h>
#include <oops/rOopsTextFormat.h>
#include <oops/rOopsTextParser.h>
#include "SerializationTest.h"
#include <gtest/gtest.h>

using namespace rOops;

// Complex number
template < class NumT >
class Complex
{
public:
    explicit Complex(NumT aR=0, NumT aI=0)
        : _real(aR), _imag(aI)
    {}
    bool operator==(const Complex& arR) const
    {
        return _real == arR._real && _imag == arR._imag;
    }
    NumT _real;
    NumT _imag;
};


template< typename NumT >
class rPropTypeInfoComplex : public rPropTypeInfoBase< Complex<NumT> >
{
public:
    explicit rPropTypeInfoComplex(const std::string& arTypeName)
        : rPropTypeInfoBase<Complex<NumT>>(arTypeName)
    {}
    ~rPropTypeInfoComplex() override = default;
    char getOpeningCharacter() const override
    {
        return '(';
    }
    char getClosingCharacter() const override
    {
        return ')';
    }
    std::string value(const void *apVariable) const override
    {
        const Complex<NumT>& rv = *(static_cast<const Complex<NumT>*>(apVariable));
        std::string str = std::to_string(rv._real);
        str += "+j";
        str += std::to_string(rv._imag);
        return str;
    }
    void value(const void *apVariable, std::string& arValue) const override
    {
        const Complex<NumT>& rv = *(static_cast<const Complex<NumT>*>(apVariable));
        arValue = std::to_string(rv._real);
        arValue += "+j";
        arValue += std::to_string(rv._imag);
    }
    void setValue(void *apVariable, const std::string& arValue) const override
    {
        auto pos = arValue.find("+j");
        Complex<NumT>& rv = *(static_cast<Complex<NumT>*>(apVariable));
        rv._real = string_to<NumT>(arValue.substr(0, pos));
        rv._imag = string_to<NumT>(arValue.substr(pos + 2));
    }
    void save(rOopsBinaryFormat& arFormat, const void *apVal) const override
    {
        const Complex<NumT>& rv = *(static_cast<const Complex<NumT>*>(apVal));
        if (0==rv._real && 0==rv._imag) {
            arFormat.setSize(0);
            arFormat.writeCtrlSizeTypeName(enum2integral(ControlByte::eValue));
        }
        else {
            arFormat.setSize(sizeof(rv._real) + sizeof(rv._imag));
            arFormat.writeCtrlSizeTypeName(enum2integral(ControlByte::eValue));
            arFormat.write(&rv._real, sizeof(rv._real));
            arFormat.write(&rv._imag, sizeof(rv._imag));
        }
    }
    void save(rOopsTextFormat& arFormat, const void *apVal) const override
    {
        const Complex<NumT>& rv = *(static_cast<const Complex<NumT>*>(apVal));
        arFormat.writeSizeTypeAndName();
        arFormat.writeValueBegin();
        arFormat.strm() << rv._real << "+j" << rv._imag;
        arFormat.writeValueEnd();
    }
    void save(rOopsYamlFormat& arFormat, const void *apVal) const override
    {
        const Complex<NumT>& rv = *(static_cast<const Complex<NumT>*>(apVal));
        arFormat.writeSizeTypeAndName(false);
        arFormat.writeValueBegin();
        arFormat.strm() << rv._real << "+j" << rv._imag;
        arFormat.writeValueEnd();
    }
    void load(rOopsBinaryParser& arParser, void* apVal, rOopsParserContext& arCtx) const override
    {
        Complex<NumT>& complex_number = *static_cast<Complex<NumT>*>(apVal);
        if (0==arCtx.size_) {
            complex_number = Complex<NumT>(0, 0);
        }
        else {
            if (2*sizeof(NumT) != arCtx.size_) {
                throw std::runtime_error(rOOPS_BUILD_EXCEPTION_MESSAGE("Object size and the size of the value on the stream are different: " << this->Size() << " != " << arCtx.size_));
            }
            arParser.read(complex_number._real, arParser.doNotSwapInteger());
            arParser.read(complex_number._imag, arParser.doNotSwapInteger());
        }
    }
    void load(rOopsTextParser&, void* apVal, rOopsParserContext& arCtx) const override
    {
        setValue(apVal, arCtx.value_);
    }
    void load(rOopsYamlParser&, void* apVal, rOopsParserContext& arCtx) const override
    {
        setValue(apVal, arCtx.value_);
    }
}; //rPropTypeInfoComplex

rOOPS_DECLARE_USER_DEFINED_TYPE_INFO(Complex<int>, rPropTypeInfoComplex<int>)
rOOPS_DECLARE_USER_DEFINED_TYPE_INFO(Complex<double>, rPropTypeInfoComplex<double>)

struct ComplexNumbers
{
    explicit ComplexNumbers(int aN=0)
        : ci(aN, 2 * aN)
        , cd(11.1*aN, 22.2*aN)
        {}
    virtual ~ComplexNumbers() = default;
    Complex<int> ci;
    Complex<double> cd;
    bool operator==(const ComplexNumbers& arR) const
    {
        return ci == arR.ci && cd == arR.cd;
    }
    rOOPS_ADD_PROPERTY_INTERFACE(ComplexNumbers)
        {
            rOOPS_PROPERTY(ci);
            rOOPS_PROPERTY(cd);
        }
};

TEST(UserDefinedTypeDescTest, SaveLoad)
{
    ComplexNumbers cn(2);
    testSerialization("ListOfStructure", cn);
}
