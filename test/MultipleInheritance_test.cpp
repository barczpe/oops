/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2019:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rPropInterface.h>
#include "SerializationTest.h"
#include <gtest/gtest.h>
#include <string>


using namespace rOops;

class A
{
public:
    virtual ~A() = default;
    A()
        : i1(1)
        {}
    bool operator==(const A& arR) const
    {
        return i1 == arR.i1;
    }
    std::int16_t i1;
    rOOPS_ADD_PROPERTY_INTERFACE(A)
    {
        rOOPS_PROPERTY(i1);
    }
}; //class A


class B
{
public:
    virtual ~B() = default;
    B()
        : i2(2)
        {}
    bool operator==(const B& arR) const
    {
        return i2 == arR.i2;
    }
    std::int32_t i2;
    rOOPS_ADD_PROPERTY_INTERFACE(B)
    {
        rOOPS_PROPERTY(i2);
    }
}; //class B


class C : public B
{
public:
    C()
        : B(), i3(3)
        {}
    bool operator==(const C& arR) const
    {
        return B::operator==(arR) && i3 == arR.i3;
    }
    std::int64_t i3;
    rOOPS_ADD_PROPERTY_INTERFACE(C)
    {
        rOOPS_INHERIT(B);
        rOOPS_PROPERTY(i3);
    }
}; //class C


class AB : public A, public B
{
public:
    AB()
        : A(), B(), u3(123u)
        {}
    bool operator==(const AB& arR) const
    {
        return A::operator==(arR) && B::operator==(arR) && u3 == arR.u3;
    }
    std::uint32_t u3;
    rOOPS_ADD_PROPERTY_INTERFACE(AB)
    {
        rOOPS_INHERIT(A);
        rOOPS_INHERIT(B);
        rOOPS_PROPERTY(u3);
    }
}; //class AB


class D : public AB
{
public:
    D()
        : AB(), i4(4)
        {}
    bool operator==(const D& arR) const
    {
        return AB::operator==(arR) && i4 == arR.i4;
    }
    std::uint64_t i4;
    rOOPS_ADD_PROPERTY_INTERFACE(D)
    {
        rOOPS_INHERIT(AB);
        rOOPS_PROPERTY(i4);
    }
}; //class D


TEST(MultipleInheritanceTest, Class_B)
{
    checkClassPropTypeInfo<A>("A", 1);
    checkClassPropTypeInfo<B>("B", 1);
    B b;
    testSerialization("Test_Class_B", b);
}

TEST(MultipleInheritanceTest, Inheritance_C)
{
    const rPropTypeInfo* pTI = checkClassPropTypeInfo<C>("C", 2);
    EXPECT_TRUE( !pTI->isClass(rPropGetTypeInfo(static_cast<const A*>(nullptr))) );
    EXPECT_TRUE( pTI->isClass(rPropGetTypeInfo(static_cast<const B*>(nullptr))) );
    EXPECT_TRUE( !pTI->isClass(rPropGetTypeInfo(static_cast<const AB*>(nullptr))) );
    EXPECT_TRUE( pTI->isClass(rPropGetTypeInfo(static_cast<const C*>(nullptr))) );
    EXPECT_TRUE( !pTI->isClass(rPropGetTypeInfo(static_cast<const D*>(nullptr))) );
    C c;
    c.i2 = -222222;
    c.i3 = -333333;
    testSerialization("Test_Class_C", c);
}


TEST(MultipleInheritanceTest, Test_MultipleInheritance_AB)
{
    const rPropTypeInfo* pTI = checkClassPropTypeInfo<AB>("AB", 3);
    EXPECT_TRUE( pTI->isClass(rPropGetTypeInfo(static_cast<const A*>(nullptr))) );
    EXPECT_TRUE( pTI->isClass(rPropGetTypeInfo(static_cast<const B*>(nullptr))) );
    EXPECT_TRUE( pTI->isClass(rPropGetTypeInfo(static_cast<const AB*>(nullptr))) );
    EXPECT_TRUE( !pTI->isClass(rPropGetTypeInfo(static_cast<const C*>(nullptr))) );
    EXPECT_TRUE( !pTI->isClass(rPropGetTypeInfo(static_cast<const D*>(nullptr))) );

    AB ab;
    rOops::rPropIterator iProp;
    iProp = pTI->find1st( &ab, "u3" );
    EXPECT_TRUE( !iProp.isEnd() );
    if(!iProp.isEnd()) { EXPECT_EQ( iProp.propName(), "u3" ); }
    iProp = pTI->find1st( &ab, "i2" );
    EXPECT_TRUE( !iProp.isEnd() );
    if(!iProp.isEnd()) { EXPECT_EQ( iProp.propName(), "i2" ); }
    iProp = pTI->find1st( &ab, "i1" );
    EXPECT_TRUE( !iProp.isEnd() );
    if(!iProp.isEnd()) { EXPECT_EQ( iProp.propName(), "i1" ); }

    iProp = pTI->find1st( &ab, "i3" );
    EXPECT_TRUE( iProp.isEnd() );
    iProp = pTI->find1st( &ab, "i4" );
    EXPECT_TRUE( iProp.isEnd() );

    ab.i1 = -11;
    ab.i2 = -22;
    ab.u3 = 333u;
    AB ab1, ab2, ab3;
    testSerialization("Test_MultipleInheritance_AB", ab);
}

TEST(MultipleInheritanceTest, Test_MultipleInheritance_D)
{
    const rPropTypeInfo* pTI = checkClassPropTypeInfo<D>("D", 2);

    const rPropDescriptor* pPD = nullptr;
    EXPECT_TRUE(nullptr != (pPD=pTI->getPropDesc(0)) ); // AB
    EXPECT_TRUE( pPD->isAncestor() );
    EXPECT_TRUE(nullptr != (pPD=pTI->getPropDesc(1)) ); // i4
    EXPECT_FALSE( pPD->isAncestor() );
    EXPECT_TRUE(nullptr == pTI->getPropDesc(2) );

    EXPECT_TRUE( pTI->isClass(rPropGetTypeInfo((A*)nullptr)) );
    EXPECT_TRUE( pTI->isClass(rPropGetTypeInfo((B*)nullptr)) );
    EXPECT_TRUE( pTI->isClass(rPropGetTypeInfo((AB*)nullptr)) );
    EXPECT_TRUE( !pTI->isClass(rPropGetTypeInfo((C*)nullptr)) );
    EXPECT_TRUE( pTI->isClass(rPropGetTypeInfo((D*)nullptr)) );
    EXPECT_TRUE( !pTI->isClass(rPropGetTypeInfo((int*)nullptr)) );

    D d;
    rOops::rPropIterator iProp;
    iProp = pTI->find1st( &d, "i4" );
    EXPECT_TRUE( !iProp.isEnd() );
    if(!iProp.isEnd()) { EXPECT_EQ( iProp.propName(), "i4" ); }
    iProp = pTI->find1st( &d, "u3" );
    EXPECT_TRUE( !iProp.isEnd() );
    if(!iProp.isEnd()) { EXPECT_EQ( iProp.propName(), "u3" ); }
    iProp = pTI->find1st( &d, "i2" );
    EXPECT_TRUE( !iProp.isEnd() );
    if(!iProp.isEnd()) { EXPECT_EQ( iProp.propName(), "i2" ); }
    iProp = pTI->find1st( &d, "i1" );
    EXPECT_TRUE( !iProp.isEnd() );
    if(!iProp.isEnd()) { EXPECT_EQ( iProp.propName(), "i1" ); }

    iProp = pTI->find1st( &d, "i3" );
    EXPECT_TRUE( iProp.isEnd() );

    std::stringstream str_strm;
    rOops::rOopsTextFormat frmt(str_strm);
    save( frmt, d, "Test_MultipleInheritance_D" );

    d.i1 = -111;
    d.i2 = -222;
    d.i4 = -444;
    d.u3 = 3333u;
    testSerialization("Test_MultipleInheritance_D", d);
}
