/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rOopsTextParser.h>
#include "SerializationTest.h"
#include <gtest/gtest.h>

TEST(TextParserTest, EmptyValues)
{
    std::string TestStream =
        // Spirit parser had different comment character.
        " # TestStream1:\n"
        "B = !sTut_01a::B {\n"
        "    sTut_01a::A = !sTut_01a::A {\n"
        "        chr = ;\n"
        "        str = ;\n"
        "        num = ;\n"
        "    };\n"
        "    float = -10.0e+3;\n"
        "};\n";
    RefDataVector TestTokens = {
        "sTut_01a::B, B, 1, eBlockBegin",
        "sTut_01a::A, sTut_01a::A, 1, eBlockBegin",
        ", chr, 1, eValue",
        ", str, 1, eValue",
        ", num, 1, eValue",
        "eBlockEnd",
        ", float, 1, eValue(-10.0e+3)",
        "eBlockEnd"
    };
    testParser<rOops::rOopsTextParser>(TestStream, TestTokens);
}

TEST(TextParserTest, Structure)
{
    std::string TestStream =
        " # TestStream1:\n"
        "B = !sTut_01a::B {\n"
        "    sTut_01a::A = !sTut_01a::A {\n"
        "        chr = 'C';\n"
        "        str = \"default,file-name.ini,2.ini,3.ini,4.ini,5.ini,6.ini,7.ini,image_name.ini,image header.ini\";\n"
        "        num = 300;\n"
        "    };\n"
        "    float = -10.0e+3;\n"
        "};\n";
    RefDataVector TestTokens = {
        "sTut_01a::B, B, 1, eBlockBegin",
        "sTut_01a::A, sTut_01a::A, 1, eBlockBegin",
        ", chr, 1, eValue(C)",
        ", str, 1, eValue(default,file-name.ini,2.ini,3.ini,4.ini,5.ini,6.ini,7.ini,image_name.ini,image header.ini)",
        ", num, 1, eValue(300)",
        "eBlockEnd",
        ", float, 1, eValue(-10.0e+3)",
        "eBlockEnd"
    };
    testParser<rOops::rOopsTextParser>(TestStream, TestTokens);
}

TEST(TextParserTest, MissingTopLevelTypeName)
{
    std::string TestStream =
        " # TestStream1:\n"
        "B = {\n"
        "    sTut_01a::A = !sTut_01a::A {\n"
        "        chr = 'C';\n"
        "        str = \"default,file-name.ini,2.ini,3.ini,4.ini,5.ini,6.ini,7.ini,image_name.ini,image header.ini\";\n"
        "        num = 300;\n"
        "    };\n"
        "    float = -10.0e+3;\n"
        "};\n";
    RefDataVector TestTokens = {
        ", B, 1, eBlockBegin",
        "sTut_01a::A, sTut_01a::A, 1, eBlockBegin",
        ", chr, 1, eValue(C)",
        ", str, 1, eValue(default,file-name.ini,2.ini,3.ini,4.ini,5.ini,6.ini,7.ini,image_name.ini,image header.ini)",
        ", num, 1, eValue(300)",
        "eBlockEnd",
        ", float, 1, eValue(-10.0e+3)",
        "eBlockEnd"
    };
    testParser<rOops::rOopsTextParser>(TestStream, TestTokens);
}

namespace TextParserTest {
    class A {
    public:
        A() = default;
        A(int i, std::string s)
            : str(std::move(s))
            , num(i)
            {}
        bool operator==( const A& rhs ) const
            { return str==rhs.str && num==rhs.num; }
    private:
        std::string str;
        int num = 0;
        rOOPS_ADD_PROPERTY_INTERFACE_NON_POLYMORPH(TextParserTest::A)
        {
            rOOPS_PROPERTY(str);
            rOOPS_PROPERTY(num);
        }
    };
    class B : public A {
    public:
        B() = default;
        B(int i, std::string s)
            : A(i, std::move(s))
            , chr(static_cast<char>('A' + (i % 20)))
            {}
        bool operator==( const B& rhs ) const
            { return A::operator==(rhs) && chr==rhs.chr; }
    private:
        char chr = 'A';
        rOOPS_ADD_PROPERTY_INTERFACE_NON_POLYMORPH(TextParserTest::B)
        {
            rOOPS_INHERIT(TextParserTest::A);
            rOOPS_PROPERTY(chr);
        }
    };
}

TEST(TextParserTest, LoadWithoutTypeName)
{
    std::string TestStream =
        "B = {\n"
        "    TextParserTest::A = {\n"
        "        str= \"xyz\";\n"
        "        num= 1;\n"
        "    };\n"
        "    chr= 'B';\n"
        "}\n";
    std::stringstream strm(cStringStreamMode);
    strm << TestStream;
    rOops::rOopsTextParser parser(strm, "B");
    TextParserTest::B l;
    load(parser, l);
    TextParserTest::B r(1, "xyz");
    EXPECT_EQ(r, l);
}

TEST(TextParserTest, Classes)
{
    std::string TestStream =
        "B = !sTut_02a::Internal::B {\n"
        "  sTut_02a::Internal::A = !sTut_02a::Internal::A {\n"
        "    _S32 = -11;\n"
        "    _S16 = -22;\n"
        "  };\n"
        "  enabled = true;\n"
        "  _F4 = 3.666667;\n"
        "  _F8 = 7.333333;\n"
        "};\n";
    RefDataVector TestTokens = {
        "sTut_02a::Internal::B, B, 1, eBlockBegin",
        "sTut_02a::Internal::A, sTut_02a::Internal::A, 1, eBlockBegin",
        ", _S32, 1, eValue(-11)",
        ", _S16, 1, eValue(-22)",
        "eBlockEnd",
        ", enabled, 1, eValue(true)",
        ", _F4, 1, eValue(3.666667)",
        ", _F8, 1, eValue(7.333333)",
        "eBlockEnd",
    };
    testParser<rOops::rOopsTextParser>(TestStream, TestTokens);
}

TEST(TextParserTest, ListAndPointers)
{
    std::string TestStream =
        "stl = !sTut_06a::STL {\n"
        "    StringDeque[1] = !sTut_06a::StringDeque_t [\"aaa\"];\n"
        "    IntList[2] = !sTut_06a::IntList_t [-90, -120];\n"
        "    IntPtrList[2] = !sTut_06a::IntPtrList_t [*1635937476896, *1635937475808];\n"
        "};\n"
        "*1635937476896 = !int32_t 0;\n"
        "*1635937475808 = !int32_t 300;\n";
    RefDataVector TestTokens = {
        "sTut_06a::STL, stl, 1, eBlockBegin",
        "sTut_06a::StringDeque_t, StringDeque, 1, eListBegin",
        ", , 1, eValue(aaa)",
        "eListEnd",
        "sTut_06a::IntList_t, IntList, 2, eListBegin",
        ", , 1, eValue(-90)",
        ", , 1, eValue(-120)",
        "eListEnd",
        "sTut_06a::IntPtrList_t, IntPtrList, 2, eListBegin",
        ", , 1, ePointer(&1635937476896)",
        ", , 1, ePointer(&1635937475808)",
        "eListEnd",
        "eBlockEnd",
        ", , 1, eTopLevel",
        "int32_t, *1635937476896, 1, eValue(0)",
        "int32_t, *1635937475808, 1, eValue(300)",
    };
    testParser<rOops::rOopsTextParser>(TestStream, TestTokens);
}

TEST(TextParserTest, ListOfStructures1)
{
    std::string TestStream =
        "ListOfStructure = !DataContainer {\n"
        "  dataList[3] = !std::list<rOops::test::Data> [ !rOops::test::Data { val=1; str=\"1\"; }, !rOops::test::Data { val=2; str=\"2\"; }, !rOops::test::Data { val=3; str=\"3\"; } ];\n"
        "};\n";
    RefDataVector TestTokens = {
        "DataContainer, ListOfStructure, 1, eBlockBegin",
        "std::list<rOops::test::Data>, dataList, 3, eListBegin",
        "rOops::test::Data, , 1, eBlockBegin",
        ", val, 1, eValue(1)",
        ", str, 1, eValue(1)",
        "eBlockEnd",
        "rOops::test::Data, , 1, eBlockBegin",
        ", val, 1, eValue(2)",
        ", str, 1, eValue(2)",
        "eBlockEnd",
        "rOops::test::Data, , 1, eBlockBegin",
        ", val, 1, eValue(3)",
        ", str, 1, eValue(3)",
        "eBlockEnd",
        "eListEnd",
        "eBlockEnd",
    };
    testParser<rOops::rOopsTextParser>(TestStream, TestTokens);
}

TEST(TextParserTest, ListOfStructures2)
{
    std::string TestStream =
        "ListOfStructure = !DataContainer {\n"
        "  dataList = !std::list<rOops::test::Data> [\n"
        "    { val=1; str=\"1\"; },\n"
        "    { val=2; str=\"2\"; },\n"
        "    { val=3; str=\"3\"; } ];\n"
        "};\n";
    RefDataVector TestTokens = {
        "DataContainer, ListOfStructure, 1, eBlockBegin",
        "std::list<rOops::test::Data>, dataList, 1, eListBegin",
        ", , 1, eBlockBegin",
        ", val, 1, eValue(1)",
        ", str, 1, eValue(1)",
        "eBlockEnd",
        ", , 1, eBlockBegin",
        ", val, 1, eValue(2)",
        ", str, 1, eValue(2)",
        "eBlockEnd",
        ", , 1, eBlockBegin",
        ", val, 1, eValue(3)",
        ", str, 1, eValue(3)",
        "eBlockEnd",
        "eListEnd",
        "eBlockEnd",
    };
    testParser<rOops::rOopsTextParser>(TestStream, TestTokens);
}

TEST(TextParserTest, Pointers)
{
    std::string TestStream =
            "TestDisplayModule = !PointerTestClass {\n"
            "  rawPtr=*obj_1;\n"
            "  uniquePtr=*obj_2;\n"
            "  sharedPtr=*obj_3;\n"
            "  type=\"Derived1\";\n"
            "};\n"
            "*obj_1 = !DerivedClass1 {\n"
            "  BaseClass = !BaseClass {\n"
            "    version=0;\n"
            "  };\n"
            "  title=\"Test derived 1.\";\n"
            "};\n"
            "*obj_2 = !DerivedClass1 {\n"
            "  BaseClass = !BaseClass {\n"
            "    version=0;\n"
            "  };\n"
            "  title=\"Test derived 2.\";\n"
            "};\n"
            "*obj_3 = !DerivedClass1 {\n"
            "  BaseClass = !BaseClass {\n"
            "    version=0;\n"
            "  };\n"
            "  title=\"Test derived 3.\";\n"
            "};\n";
    RefDataVector TestTokens = {
        "PointerTestClass, TestDisplayModule, 1, eBlockBegin",
        ", rawPtr, 1, ePointer(&obj_1)",
        ", uniquePtr, 1, ePointer(&obj_2)",
        ", sharedPtr, 1, ePointer(&obj_3)",
        ", type, 1, eValue(Derived1)",
        "eBlockEnd",
        ", , 1, eTopLevel",
        "DerivedClass1, *obj_1, 1, eBlockBegin",
        "BaseClass, BaseClass, 1, eBlockBegin",
        ", version, 1, eValue(0)",
        "eBlockEnd",
        ", title, 1, eValue(Test derived 1.)",
        "eBlockEnd",
        ", , 1, eTopLevel",
        "DerivedClass1, *obj_2, 1, eBlockBegin",
        "BaseClass, BaseClass, 1, eBlockBegin",
        ", version, 1, eValue(0)",
        "eBlockEnd",
        ", title, 1, eValue(Test derived 2.)",
        "eBlockEnd",
        ", , 1, eTopLevel",
        "DerivedClass1, *obj_3, 1, eBlockBegin",
        "BaseClass, BaseClass, 1, eBlockBegin",
        ", version, 1, eValue(0)",
        "eBlockEnd",
        ", title, 1, eValue(Test derived 3.)",
        "eBlockEnd",
        ", , 1, eTopLevel",
    };
    testParser<rOops::rOopsTextParser>(TestStream, TestTokens);
}
