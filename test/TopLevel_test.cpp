/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
 * All other tests use a class as a top-level object, which is the most common use-case.
 * However the top-level object can be anything: smart pointer, container, associative container,
 * and all of these may store polymorph objects.
 *
 * This file contains test-cases for different top-level objects.
 */

#include "BaseDerived.h"
#include "SerializationTest.h"
#include <gtest/gtest.h>
#include <string>

using namespace rOops;

TEST(TopLevelTest, UniquePointer)
{
    std::unique_ptr<BaseClass> p1 = std::make_unique<DerivedClass1>("T1");
    testSerializationPointer("UniquePointer", p1);
}

TEST(TopLevelTest, UniquePointer_multi)
{
    std::vector< std::unique_ptr<BaseClass> > vv;
    vv.push_back( std::make_unique<DerivedClass1>("T1") );
    vv.push_back( std::make_unique<DerivedClass1>("T2") );
    vv.push_back( std::make_unique<DerivedClass1>("T3") );
    vv.push_back( std::make_unique<DerivedClass1>("T4") );
    testSerializationPointerMulti("UniquePointer_multi", vv, true, true);
}

TEST(TopLevelTest, SharedPointer)
{
    std::shared_ptr<BaseClass> p1 = std::make_shared<DerivedClass2>(111);
    testSerializationPointer("UniquePointer", p1);
}

TEST(TopLevelTest, SharedPointerMulti)
{
    std::vector< std::shared_ptr<BaseClass> > vv;
    for (int ii=111; ii<=555; ii+=111) {
        vv.push_back(std::make_shared<DerivedClass2>(111));
    }
    testSerializationPointerMulti("UniquePointer", vv);
}

TEST(TopLevelTest, VectorOfInt)
{
    std::vector<std::int32_t> v1;
    v1.push_back(1);
    v1.push_back(2);
    v1.push_back(3);
    testSerializationContainer("Vector", v1);
}

TEST(TopLevelTest, VectorOfUnique)
{
    std::vector<std::unique_ptr<BaseClass>> v1;
    v1.push_back(std::make_unique<BaseClass>(1));
    v1.push_back(std::make_unique<DerivedClass1>(2));
    v1.push_back(std::make_unique<DerivedClass2>(3));
    testSerializationContainerOfPointers("VectorOfUnique", v1);
}

TEST(TopLevelTest, SetOfShared)
{
    BaseClassPointerSet v1;
    v1.insert(std::make_unique<BaseClass>(1));
    v1.insert(std::make_unique<DerivedClass1>(2));
    v1.insert(std::make_unique<DerivedClass2>(3));
    testSerializationContainerOfPointers("SetOfShared", v1);
}

TEST(TopLevelTest, MapOfIntDouble)
{
    std::map<int, double> m1;
    m1.emplace(1, 10.0);
    m1.emplace(2, 20.0);
    m1.emplace(3, 30.0);
    testSerializationContainer("MapOfIntDouble", m1, false);
}

