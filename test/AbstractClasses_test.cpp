/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include "SerializationTest.h"
#include <oops/Enum2StringTool.h>
#include <oops/rPropInterface.h>
#include <gtest/gtest.h>
#include <string>


using namespace rOops;

class IComponent
{
public:
    virtual ~IComponent() = default;
    IComponent() = default;
    virtual void init(short) = 0;
    rOOPS_ADD_PROPERTY_INTERFACE_ABSTRACT(IComponent)
    {
    }
};

DECLARE_ENUM_CLASS(CameraType, std::uint8_t,
    eNone = 0,
    eType1,
    eType2
);
rOOPS_DECLARE_ENUM_TYPE_INFO( CameraType, CameraType2String )

class Camera : public IComponent {
public:
    Camera() = default;
    void init(short p) override
    {
        height = width = p;
    }
private:
    CameraType type{CameraType::eNone};
    int height{0};
    int width{0};

    rOOPS_ADD_PROPERTY_INTERFACE(Camera)
    {
        rOOPS_INHERIT(IComponent);
        rOOPS_PROPERTY(type);
        rOOPS_PROPERTY(height);
        rOOPS_PROPERTY(width);
    }
};

rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::vector<long>)

class Mesh : public IComponent
{
public:
    Mesh() = default;
    void init(short p) override
    {
        indices.push_back(p);
        path = std::to_string(p);
    }
private:
    std::vector<long> indices;
    std::string path;
    rOOPS_ADD_PROPERTY_INTERFACE(Mesh)
    {
        rOOPS_INHERIT(IComponent);
        rOOPS_PROPERTY(indices);
        rOOPS_PROPERTY(path);
    }
};

class ILight : public IComponent
{
public:
    ~ILight() override = default;
    ILight() = default;
    rOOPS_ADD_PROPERTY_INTERFACE_ABSTRACT(ILight)
    {
        rOOPS_INHERIT(IComponent);
    }
};

using Vec3 = std::array<double, 3>;
rOOPS_DECLARE_STL_ARRAY_TYPE_INFO(Vec3)

class PointLight : public ILight
{
public:
    PointLight() = default;
    void init(short p) override
    {
        AO[0] = Diffuse[1] = Specular[2] = p;
    }
private:
    Vec3 AO{};
    Vec3 Diffuse{};
    Vec3 Specular{};
    rOOPS_ADD_PROPERTY_INTERFACE(PointLight)
    {
        rOOPS_INHERIT(ILight);
        rOOPS_PROPERTY(AO);
        rOOPS_PROPERTY(Diffuse);
        rOOPS_PROPERTY(Specular);
    }
};

rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::vector<std::unique_ptr<IComponent>>)

TEST(AbstractClassesTest, IComponents)
{
    std::vector<std::unique_ptr<IComponent>> v;
    v.push_back(std::make_unique<Camera>()); v.back()->init(1);
    v.push_back(std::make_unique<Mesh>()); v.back()->init(2);
    v.push_back(std::make_unique<PointLight>()); v.back()->init(3);

    std::stringstream strm(cStringStreamMode);
    rOopsYamlFormat format(strm);
    save(format, v, "Components");
    std::cout << "========== save ==========" << std::endl;
    std::cout << strm.str() << std::endl;
    std::cout << "==========================" << std::endl;

    rOopsYamlParser parser(strm, "Components");
    std::vector<std::unique_ptr<IComponent>> v2;
    load(parser, v2);
}

class ClassWithoutDefaultCtor
{
public:
    virtual ~ClassWithoutDefaultCtor() = default;
    ClassWithoutDefaultCtor() = delete;
    explicit ClassWithoutDefaultCtor(int aM)
        : m(aM)
    {}
    int m = 0;
    rOOPS_ADD_PROPERTY_INTERFACE_ABSTRACT(ClassWithoutDefaultCtor)
    {
        rOOPS_PROPERTY(m);
    }
};

TEST(AbstractClassesTest, ClassWithoutDefaultCtor)
{
    ClassWithoutDefaultCtor instance{42};
}
