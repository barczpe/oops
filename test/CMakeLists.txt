cmake_minimum_required(VERSION 3.10)

project(oops_test)

include_directories( "$ENV{CEREAL_ROOT}/include" )

# If for any reason find_package does not work, set GTest_ROOT.
# e.g.: export GTest_ROOT=D:/3rd/googletest-master/googletest

if (DEFINED ENV{Gtest_ROOT})
    message("Gtest_ROOT: " $ENV{Gtest_ROOT})
    set(GTEST_INCLUDE_DIRS "$ENV{Gtest_ROOT}/include" CACHE PATH "Path to googletest")
    link_directories("$ENV{Gtest_ROOT}/lib" "$ENV{Gtest_ROOT}/build/lib")

    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
        set(GTEST_BOTH_LIBRARIES gtest gtest_main)
    elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
        # using Visual Studio C++
        set(GTEST_BOTH_LIBRARIES
            optimized gtest
            debug gtestd
            optimized gtest_main
            debug gtest_maind)
    else()
        set(GTEST_BOTH_LIBRARIES gtest gtest_main)
    endif()
else()
    # find_package() sets GTEST_INCLUDE_DIRS and GTEST_BOTH_LIBRARIES
    message("Use find_package(GTest REQUIRED)")
    find_package(GTest REQUIRED)
endif()

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    if (DEFINED ENV{QNX_TARGET})
        # No pthread library on QNX. It is part of the standard library.
        set(GTEST_BOTH_LIBRARIES "${GTEST_BOTH_LIBRARIES}")
    else()
        set(GTEST_BOTH_LIBRARIES "${GTEST_BOTH_LIBRARIES}" pthread)
    endif()
endif()

message("GTEST_INCLUDE_DIRS: " ${GTEST_INCLUDE_DIRS})
message("GTEST_BOTH_LIBRARIES: " ${GTEST_BOTH_LIBRARIES})

set(TEST_SOURCES
        Enums.h
        BaseDerived.h
        BaseDerivedFlat.h
        rOopsOStream.h
        SerializationTest.h
        Enum2StringTool_test.cpp
        AbstractClasses_test.cpp
        AssociativeContainer_test.cpp
        BackwardCompatibility_test.cpp
        ChronoTypeInfo_test.cpp
        EscapeCharacters_test.cpp
        ClassVersions_test.cpp
        Container_test.cpp
        ContainerPointer_test.cpp
        FlatInheritance_test.cpp
        MultipleInheritance_test.cpp
        Optional_test.cpp
        Pointer_test.cpp
        PropDescTable_test.cpp
        rOops_test.cpp
        TemplateClasses_test.cpp
        TextParser_test.cpp
        TextTokenizer_test.cpp
        ../example/sTut_02_inherit.h
        ../example/s2D.h
        ../example/s2D.cpp
        Iterator_test.cpp
        Python_test.cpp
        Tutorial_test.cpp
        TopLevel_test.cpp
        UnitsTypeInfo_test.cpp
        UserDefinedPropDesc_test.cpp
        VirtualInheritance_test.cpp
        YamlParser_test.cpp
        YamlTokenizer_test.cpp
        SerializationBenchmark.cpp
        main.cpp
)

add_executable(${PROJECT_NAME} ${TEST_SOURCES})
target_include_directories(${PROJECT_NAME} PRIVATE ${GTEST_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME} PRIVATE oops ${GTEST_BOTH_LIBRARIES})
