#pragma once

/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rPropInterface.h>
#include <oops/rOopsBinaryFormat.h>
#include <oops/rOopsTextFormat.h>
#include <oops/rOopsYamlFormat.h>
#include <oops/rOopsBinaryParser.h>
#include <oops/rOopsTextParser.h>
#include <oops/rOopsYamlParser.h>
#include <oops/rOopsSaveLoad.h>
#include <gtest/gtest.h>
#include <iostream>
#include <sstream>
#include <typeinfo>
#include <vector>

constexpr std::ios_base::openmode cStringStreamMode = std::stringstream::in|std::stringstream::out|std::stringstream::binary;
constexpr bool cPrintByDefault = false;

namespace cmp
{
    template <typename DynamicT, typename ClassT, typename MemberPointerT>
    bool eqPtr(ClassT& aL, ClassT& aR, MemberPointerT aMemberPointer)
    {
        if (nullptr == aL.*aMemberPointer && nullptr == aR.*aMemberPointer) return true;
        if (nullptr == aL.*aMemberPointer || nullptr == aR.*aMemberPointer) return false;
        auto* lp = dynamic_cast<DynamicT*>(&(*(aL.*aMemberPointer)));  // de-referencing smart pointer.
        if (nullptr == lp) return false;
        if (typeid(*lp) != typeid(DynamicT)) return false;
        auto* rp = dynamic_cast<DynamicT*>(&(*(aR.*aMemberPointer)));  // de-referencing smart pointer.
        if (nullptr == rp) return false;
        if (typeid(*rp) != typeid(DynamicT)) return false;
        return (*lp == *rp);
    }

    template <typename ClassT, typename MemberPointerT>
    bool eq(const ClassT& arL, const ClassT& arR, MemberPointerT aMemberPointer)
    {
        return arL.*aMemberPointer == arR.*aMemberPointer;
    }

    template <typename PointerT>
    bool comparePointers(const PointerT& arL, const PointerT& arR)
    {
        if (nullptr == arL && nullptr == arR) return true;
        if (nullptr == arL || nullptr == arR) return false;
        return *arL == *arR;
    }

    template <typename PointerT>
    bool compareObjectPointers(const PointerT& arL, const PointerT& arR)
    {
        if (nullptr == arL && nullptr == arR) return true;
        if (nullptr == arL || nullptr == arR) return false;
        return arL->compareObjectPointers(arR);
    }

    template <typename PointerT>
    bool compareSmartPointers(const PointerT& arL, const PointerT& arR)
    {
        if (nullptr == arL && nullptr == arR) return true;
        if (nullptr == arL || nullptr == arR) return false;
        return arL->compareObjectPointers(arR.get());
    }
}


/**
Common test function for checking some features of the PropTypeInfo of the given class type.
ObjT has to be a compound type: class or struct.
apClassName is the name of the class including its namespaces, e.g. A::B::TestObject.
Returns the PropTypeInfo object of the given type.
Usage:
    auto pTI = checkClassPropTypeInfo<A::B::TestObject>("A::B::TestObject");
*/
template < class ObjT >
const rOops::rPropTypeInfo* checkClassPropTypeInfo(const char* apClassName, std::size_t aPropDescSize)
{
    const rOops::rPropTypeInfo* pTI = rPropGetTypeInfo(static_cast<const ObjT*>(nullptr));
    rOops::rPropTypeInfo::id_type type_id = pTI->TypeId();
    EXPECT_TRUE(pTI == rOops::rPropGetTypeInfoByName(apClassName));
    EXPECT_TRUE(pTI == rOops::rPropGetTypeInfoById(type_id));
    EXPECT_EQ(apClassName, pTI->typeName());
    EXPECT_EQ(sizeof(ObjT), pTI->Size());

    EXPECT_FALSE(pTI->isValue());
    EXPECT_TRUE(pTI->isCompound());
    EXPECT_FALSE(pTI->isAbstract());
    EXPECT_FALSE(pTI->isPolymorphic());
    EXPECT_FALSE(pTI->isIntrusive());
    EXPECT_FALSE(pTI->isSTLContainer());
    EXPECT_EQ(aPropDescSize, pTI->getPropDescSize());
    EXPECT_EQ(static_cast<const rOops::rPropTypeInfo*>(nullptr), pTI->elementTypeInfo());
    EXPECT_EQ(static_cast<const rOops::rPropDescriptor*>(nullptr), pTI->elementPropDesc());

    for (std::size_t idx = 0; idx < aPropDescSize; ++idx) {
        EXPECT_TRUE(nullptr != pTI->getPropDesc(idx) );
    }
    EXPECT_TRUE(nullptr == pTI->getPropDesc(aPropDescSize) );

    return pTI;
}

/**
* Save and load the given object using all formats and parsers.
* Check if the original and restored objects are the same.
* ObjT must have an equality operator for EXPECT_EQ().
*/
template <class ObjT, class FormatT, class ParserT>
void testSerializationKernel(std::uint8_t protocol, const char* aName, const ObjT& arObj, ObjT& arCopy, bool aDoPrint, const std::function<std::string(const std::stringstream& arStream)>& aPrinter)
{
    std::stringstream strm(cStringStreamMode);
    FormatT frmt(strm, protocol);
    save(frmt, arObj, aName);
    if (aDoPrint) {
        std::cout << "========== save ==========" << std::endl;
        std::cout << aPrinter(strm) << std::endl;
        std::cout << "==========================" << std::endl;
    }
    ParserT parser(strm, aName);
    load(parser, arCopy);
    EXPECT_EQ(0, strm.rdbuf()->in_avail());
}

template <class ObjT, class FormatT, class ParserT>
void testSerializationKernelMulti(std::uint8_t protocol, const char* aName, const std::vector<ObjT>& arObj, std::vector<ObjT>& arCopy, bool aDoPrint, const std::function<std::string(const std::stringstream& arStream)>& aPrinter)
{
    std::stringstream strm(cStringStreamMode);
    FormatT frmt(strm, protocol);
    for (const auto& obj : arObj) {
        save(frmt, obj, aName);
    }
    if (aDoPrint) {
        std::cout << "========== save ==========" << std::endl;
        std::cout << aPrinter(strm) << std::endl;
        std::cout << "==========================" << std::endl;
    }
    ParserT parser(strm, aName);
    for (std::size_t cntr = 0; cntr < arObj.size(); ++cntr) {
        arCopy.emplace_back();
        load(parser, arCopy.back());
    }
    EXPECT_EQ(0, strm.rdbuf()->in_avail());
}

template <class ObjT, class FormatT, class ParserT>
void testSerializationCmpObj(std::uint8_t protocol, const char* aName, const ObjT& arObj, bool aDoPrint, const std::function<std::string(const std::stringstream& arStream)>& aPrinter)
{
    ObjT copy;
    testSerializationKernel<ObjT, FormatT, ParserT>(protocol, aName, arObj, copy, aDoPrint, aPrinter);
    EXPECT_EQ(arObj, copy) << aName;
}

template <class ObjT, class FormatT, class ParserT>
void testSerializationCmpObjMulti(std::uint8_t protocol, const char* aName, const std::vector<ObjT>& arObj, bool aDoPrint, const std::function<std::string(const std::stringstream& arStream)>& aPrinter)
{
    std::vector<ObjT> copy;
    testSerializationKernelMulti<ObjT, FormatT, ParserT>(protocol, aName, arObj, copy, aDoPrint, aPrinter);
    ASSERT_EQ(arObj.size(), copy.size());
    for (std::size_t idx = 0; idx < arObj.size(); ++idx) {
        EXPECT_EQ(arObj[idx], copy[idx]) << aName << ": " << idx;
    }
}

template <class ObjT, class FormatT, class ParserT>
void testSerializationCmpObjPtr(std::uint8_t protocol, const char* aName, const ObjT& arObj, bool aDoPrint, const std::function<std::string(const std::stringstream& arStream)>& aPrinter)
{
    ObjT copy;
    testSerializationKernel<ObjT, FormatT, ParserT>(protocol, aName, arObj, copy, aDoPrint, aPrinter);
    EXPECT_EQ(*arObj, *copy) << aName;
}

template <class ObjT, class FormatT, class ParserT>
void testSerializationCmpObjPtrMulti(std::uint8_t protocol, const char* aName, const std::vector<ObjT>& arObj, bool aDoPrint, const std::function<std::string(const std::stringstream& arStream)>& aPrinter)
{
    std::vector<ObjT> copy;
    testSerializationKernelMulti<ObjT, FormatT, ParserT>(protocol, aName, arObj, copy, aDoPrint, aPrinter);
    ASSERT_EQ(arObj.size(), copy.size());
    for (std::size_t idx = 0; idx < arObj.size(); ++idx) {
        EXPECT_EQ(*arObj[idx], *copy[idx]) << aName << ": " << idx;
    }
}

template <class ObjT, class FormatT, class ParserT>
void testSerializationCmpContainer(std::uint8_t protocol, const char* aName, const ObjT& arObj, bool aDoPrint, const std::function<std::string(const std::stringstream& arStream)>& aPrinter)
{
    ObjT copy;
    testSerializationKernel<ObjT, FormatT, ParserT>(protocol, aName, arObj, copy, aDoPrint, aPrinter);
    EXPECT_EQ(arObj.size(), copy.size()) << aName;
    auto iObj=std::begin(arObj);
    auto iCopy=std::begin(copy);
    std::size_t cntr = 0;
    while (iObj!=std::end(arObj) && iCopy!=std::end(copy)) {
        EXPECT_EQ(*iObj, *iCopy) << cntr;
        ++iObj;
        ++iCopy;
        ++cntr;
    }
}

template <class ObjT, class FormatT, class ParserT>
void testSerializationCmpContainerMulti(std::uint8_t protocol, const char* aName, const std::vector<ObjT>& arObj, bool aDoPrint, const std::function<std::string(const std::stringstream& arStream)>& aPrinter)
{
    std::vector<ObjT> copy;
    testSerializationKernelMulti<ObjT, FormatT, ParserT>(protocol, aName, arObj, copy, aDoPrint, aPrinter);
    ASSERT_EQ(arObj.size(), copy.size());
    for (std::size_t idx = 0; idx < arObj.size(); ++idx) {
        EXPECT_EQ(arObj[idx].size(), copy[idx].size()) << aName;
        auto iObj = std::begin(arObj[idx]);
        auto iCopy = std::begin(copy[idx]);
        std::size_t cntr = 0;
        while (iObj != std::end(arObj) && iCopy != std::end(copy)) {
            EXPECT_EQ(*iObj, *iCopy) << cntr;
            ++iObj;
            ++iCopy;
            ++cntr;
        }
    }
}

template <class ObjT, class FormatT, class ParserT>
void testSerializationCmpContainerOfPointers(std::uint8_t protocol, const char* aName, const ObjT& arObj, bool aDoPrint, const std::function<std::string(const std::stringstream& arStream)>& aPrinter)
{
    ObjT copy;
    testSerializationKernel<ObjT, FormatT, ParserT>(protocol, aName, arObj, copy, aDoPrint, aPrinter);
    EXPECT_EQ(arObj.size(), copy.size()) << aName;
    auto iObj=std::begin(arObj);
    auto iCopy=std::begin(copy);
    std::size_t cntr = 0;
    while (iObj!=std::end(arObj) && iCopy!=std::end(copy)) {
        EXPECT_EQ(**iObj, **iCopy) << cntr;
        ++iObj;
        ++iCopy;
        ++cntr;
    }
}

#define CREATE_TEST_SERIALIZATION(testSerializationCmp)                                                                                                                                                                        \
    testSerializationCmp<ObjT, rOops::rOopsTextFormat, rOops::rOopsTextParser>('T', aName, arObj, aDoPrint, [](const std::stringstream& arStrm) { return arStrm.str(); });                                                     \
    testSerializationCmp<ObjT, rOops::rOopsBinaryFormat, rOops::rOopsBinaryParser>('N', aName, arObj, aDoPrint, [](const std::stringstream& arStrm) { rOops::rOopsBinaryPrinter p; return p.printOopsBinaryFile(arStrm); });   \
    if (aTestYamlFormat) { testSerializationCmp<ObjT, rOops::rOopsYamlFormat, rOops::rOopsYamlParser>('Y', aName, arObj, aDoPrint, [](const std::stringstream& arStrm) { return arStrm.str(); }); }

template <class ObjT>
void testSerialization(const char* aName, const ObjT& arObj, bool aTestYamlFormat=true, bool aDoPrint=cPrintByDefault)
{
    CREATE_TEST_SERIALIZATION(testSerializationCmpObj)
}

template <class ObjT>
void testSerializationMulti(const char* aName, const std::vector<ObjT>& arObj, bool aTestYamlFormat=true, bool aDoPrint=cPrintByDefault)
{
    CREATE_TEST_SERIALIZATION(testSerializationCmpObjMulti)
}

template <class ObjT>
void testSerializationPointer(const char* aName, const ObjT& arObj, bool aTestYamlFormat=true, bool aDoPrint=cPrintByDefault)
{
    CREATE_TEST_SERIALIZATION(testSerializationCmpObjPtr)
}

template <class ObjT>
void testSerializationPointerMulti(const char* aName, const std::vector<ObjT>& arObj, bool aTestYamlFormat=true, bool aDoPrint=cPrintByDefault)
{
    CREATE_TEST_SERIALIZATION(testSerializationCmpObjPtrMulti)
}

template <class ObjT>
void testSerializationContainer(const char* aName, const ObjT& arObj, bool aTestYamlFormat=true, bool aDoPrint=cPrintByDefault)
{
    CREATE_TEST_SERIALIZATION(testSerializationCmpContainer)
}

template <class ObjT>
void testSerializationContainerMulti(const char* aName, const std::vector<ObjT>& arObj, bool aTestYamlFormat=true, bool aDoPrint=cPrintByDefault)
{
    CREATE_TEST_SERIALIZATION(testSerializationCmpContainerMulti)
}

template <class ObjT>
void testSerializationContainerOfPointers(const char* aName, const ObjT& arObj, bool aTestYamlFormat=true, bool aDoPrint=cPrintByDefault)
{
    CREATE_TEST_SERIALIZATION(testSerializationCmpContainerOfPointers)
}

inline std::string printParserContext(rOops::ParserReturnCode aReturnCode, const rOops::rOopsParserContext& ctx)
{
    using namespace rOops;
    std::stringstream print_strm;
    if (ParserReturnCode::eBlockEnd==aReturnCode || ParserReturnCode::eListEnd==aReturnCode || ParserReturnCode::eMapEnd==aReturnCode) {
        print_strm << rOops::to_string(aReturnCode);
    }
    else {
        print_strm << ctx.typeName_ << ", ";
        if (!ctx.propName_.empty()) {
            print_strm << ctx.propName_;
        }
        if (!ctx.address_.empty()) {
            print_strm << '*' << ctx.address_.asString();
        }
        print_strm << ", " << ctx.size_ << ", " << rOops::to_string(aReturnCode);
        if (!ctx.value_.empty()) {
            print_strm << "(" << ctx.value_ << ")";
        }
        if (!ctx.pointer_.empty()) {
            print_strm << "(&" << ctx.pointer_.asString() << ")";
        }
    }
    return print_strm.str();
}

using RefDataVector = std::vector<std::string>;

template <typename ParserT>
void testParser(const std::string& arInput, const RefDataVector& arRefData)
{
    std::stringstream strm(arInput);
    ParserT parser(strm, "TestParser");
    rOops::rOopsParserContext ctx;
    for (const auto& ref_data : arRefData) {
        rOops::ParserReturnCode ret_code = parser.parse(ctx);
        EXPECT_EQ(ref_data, printParserContext(ret_code, ctx));
    }
    parser.parseTopLevelSeparator(ctx);
    parser.skipWhiteSpace();
    // Check if the stream is empty.
    EXPECT_EQ(0, strm.rdbuf()->in_avail());
}

inline std::string clearAddressInOopsText(const std::string& str)
{
    std::string str_copy;
    std::size_t ii = 0;
    while (ii < str.size()) {
        if ('&' == str[ii]) {
            // Address found. Skip all characters until space or new line.
            while (ii < str.size() && ' ' != str[ii] && '\n' != str[ii] && '\r' != str[ii]) {
                ++ii;
            }
        }
        str_copy.push_back(str[ii]);
        ++ii;
    }
    return str_copy;
}

template <class ObjT, class FormatT, class ParserT>
void testSerializationClearAddress(std::uint8_t protocol, const char* aName, const ObjT& arObj, ObjT& arCopy, bool aDoPrint=cPrintByDefault)
{
    std::stringstream strm1(cStringStreamMode);
    FormatT frmt(strm1, protocol);
    save(frmt, arObj, aName);
    auto str = clearAddressInOopsText(strm1.str());
    if (aDoPrint) {
        std::cout << "======= addresses cleared =======" << std::endl;
        std::cout << str << std::endl;
        std::cout << "=================================" << std::endl;
    }
    std::stringstream strm2(cStringStreamMode);
    strm2 << str;
    ParserT parser(strm2, aName);
    load(parser, arCopy);
}
