#pragma once

/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
Enum definitions for testing.
*/

#include <oops/rPropInterface.h>
#include <oops/rPropEnumStringTool.h>
#include <set>

namespace test_enums {

enum class Month_e : int32_t
{
    eJanuary = 1,
    eFebruary,
    eMarch,
    eApril,
    eMay,
    eJune,
    eJuly,
    eAugust,
    eSeptember,
    eOctober,
    eNovember,
    eDecember
}; //enum Month_e

struct Month2Str : public rOops::rPropEnumStringTool<Month_e>
{
public:
    Month2Str()
            : rOops::rPropEnumStringTool<Month_e>()
    {
        insert( Month_e::eJanuary, "January" );
        insert( Month_e::eFebruary, "February" );
        insert( Month_e::eMarch, "March" );
        insert( Month_e::eApril, "April" );
        insert( Month_e::eMay, "May" );
        insert( Month_e::eJune, "June" );
        insert( Month_e::eJuly, "July" );
        insert( Month_e::eAugust, "August" );
        insert( Month_e::eSeptember, "September" );
        insert( Month_e::eOctober, "October" );
        insert( Month_e::eNovember, "November" );
        insert( Month_e::eDecember, "December" );
    }
};
}
rOOPS_DECLARE_ENUM_TYPE_INFO( test_enums::Month_e, test_enums::Month2Str )

enum Day_e
{
    eMonday = 1,
    eTuesday,
    eWednesday,
    eThursday,
    eFriday,
    eSaturday,
    eSunday
}; //enum Day_e
rOOPS_DECLARE_ENUM_TYPE_INFO( Day_e, rOops::rPropEnumStringIntConv<Day_e> )
