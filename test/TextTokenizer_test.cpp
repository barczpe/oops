/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2019:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rOopsTextTokenizer.h>
#include <gtest/gtest.h>

#include <fstream>
#include <sstream>
#include <tuple>
#include <vector>

using RefData = std::tuple<char, std::string, std::size_t>;
using RefDataVector = std::vector<RefData>;

void TestTextTokenizer(const std::string& arInput, const RefDataVector& arRefData, const rOops::PreprocessorDefines_t& arPreprocessorDefines={})
{
    std::stringstream strm{ arInput };
    rOops::rOopsTextTokenizer tokenizer(strm, "TestTextTokenizer", arPreprocessorDefines);
    rOops::TextToken tkn('x');
    for (const auto& ref : arRefData) {
        if ('\0' == tkn._token && '\0'!=std::get<0>(ref)) {
            FAIL() << "Unexpected end of token stream.";
            break;
        }
        tkn = tokenizer.getNextToken();
        //std::cout << std::setw(2) << std::get<2>(ref) << ": " << std::get<0>(ref) << " " << std::get<1>(ref) << "\t| " << tkn._token << " " << tkn._str << std::endl;
        EXPECT_EQ(std::get<0>(ref), tkn._token)  << "At line " << tkn._lineNr << ": " << tkn._token << " " << tkn._str;
        EXPECT_EQ(std::get<1>(ref), tkn._str)    << "At line " << tkn._lineNr << ": " << tkn._token << " " << tkn._str;;
        EXPECT_EQ(std::get<2>(ref), tkn._lineNr) << "At line " << tkn._lineNr << ": " << tkn._token << " " << tkn._str;;
    } //for
}

std::size_t PrintTextTokens(const std::string& arInput, const rOops::PreprocessorDefines_t& arPreprocessorDefines={}, bool aVerbose=false)
{
    std::stringstream strm{ arInput };
    rOops::rOopsTextTokenizer tokenizer(strm, "PrintTextTokens", arPreprocessorDefines);
    rOops::TextToken tkn('x');
    std::size_t token_cntr = 0;
    while ('\0' != tkn._token) {
        tkn = tokenizer.getNextToken();
        if (aVerbose) {
            std::cout << "RefData( '" << rOops::escaping(tkn._token) << "', \"" << tkn._str << "\", " << tkn._lineNr << " ),\n";
        }
        ++token_cntr;
    } //while
    return token_cntr;
}

TEST(TextTokenizerTest, EmptyValues)
{
    std::string TestStream =
        " # TestStream1:\n"
        "!sTut_01a::B B = {\n"
        "    !sTut_01a::A sTut_01a::A = {\n"
        "        chr = ;\n"
        "        str = ;\n"
        "        num = ;\n"
        "    };\n"
        "    float = -10.0e+3;\n"
        "};\n";
    RefDataVector TestTokens = {
        RefData('#', " TestStream1:", 1),
        RefData('!', "sTut_01a::B", 2),
        RefData('I', "B", 2),
        RefData('=', "", 2),
        RefData('{', "", 2),
        RefData('!', "sTut_01a::A", 3),
        RefData('I', "sTut_01a::A", 3),
        RefData('=', "", 3),
        RefData('{', "", 3),
        RefData('I', "chr", 4),
        RefData('=', "", 4),
        //RefData('\'', "C", 4),
        RefData(';', "", 4),
        RefData('I', "str", 5),
        RefData('=', "", 5),
        //RefData('"', "200", 5),
        RefData(';', "", 5),
        RefData('I', "num", 6),
        RefData('=', "", 6),
        //RefData('N', "300", 6),
        RefData(';', "", 6),
        RefData('}', "", 7),
        RefData(';', "", 7),
        RefData('I', "float", 8),
        RefData('=', "", 8),
        RefData('N', "-10.0e+3",8),
        RefData(';', "", 8),
        RefData('}', "", 9),
        RefData(';', "", 9),
        RefData('\0', "", 0),
    };
    std::size_t nr_of_tokens = PrintTextTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestTextTokenizer(TestStream, TestTokens);
}

TEST(TextTokenizerTest, Structure)
{
    std::string TestStream =
        " # TestStream1:\n"
        "!sTut_01a::B B = {\n"
        "    !sTut_01a::A sTut_01a::A = {\n"
        "        chr = 'C';\n"
        "        str = \"200\";\n"
        "        num = 300;\n"
        "    };\n"
        "    float = -10.0e+3;\n"
        "};\n";
    RefDataVector TestTokens = {
        RefData( '#', " TestStream1:", 1 ),
        RefData( '!', "sTut_01a::B", 2 ),
        RefData( 'I', "B", 2 ),
        RefData( '=', "", 2 ),
        RefData( '{', "", 2 ),
        RefData( '!', "sTut_01a::A", 3 ),
        RefData( 'I', "sTut_01a::A", 3 ),
        RefData( '=', "", 3 ),
        RefData( '{', "", 3 ),
        RefData( 'I', "chr", 4 ),
        RefData( '=', "", 4 ),
        RefData( '\'', "C", 4 ),
        RefData( ';', "", 4 ),
        RefData( 'I', "str", 5 ),
        RefData( '=', "", 5 ),
        RefData( '"', "200", 5 ),
        RefData( ';', "", 5 ),
        RefData( 'I', "num", 6 ),
        RefData( '=', "", 6 ),
        RefData( 'N', "300", 6 ),
        RefData( ';', "", 6 ),
        RefData( '}', "", 7 ),
        RefData( ';', "", 7 ),
        RefData( 'I', "float", 8 ),
        RefData( '=', "", 8 ),
        RefData( 'N', "-10.0e+3",8 ),
        RefData( ';', "", 8 ),
        RefData( '}', "", 9 ),
        RefData( ';', "", 9 ),
        RefData( '\0', "", 0 ),
    };
    std::size_t nr_of_tokens = PrintTextTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestTextTokenizer(TestStream, TestTokens);
}

TEST(TextTokenizerTest, Classes)
{
    std::string TestStream =
        "!sTut_02a::Internal::B B = {\n"
        "  !sTut_02a::Internal::A sTut_02a::Internal::A = {\n"
        "    _S32 = -11;\n"
        "    _S16 = -22;\n"
        "  };\n"
        "  enabled = true;\n"
        "  _F4 = 3.666667;\n"
        "  _F8 = 7.333333;\n"
        "};\n";
    RefDataVector TestTokens = {
        RefData( '!', "sTut_02a::Internal::B", 1 ),
        RefData( 'I', "B", 1 ),
        RefData( '=', "", 1 ),
        RefData( '{', "", 1 ),
        RefData( '!', "sTut_02a::Internal::A", 2 ),
        RefData( 'I', "sTut_02a::Internal::A", 2 ),
        RefData( '=', "", 2 ),
        RefData( '{', "", 2 ),
        RefData( 'I', "_S32", 3 ),
        RefData( '=', "", 3 ),
        RefData( 'N', "-11", 3 ),
        RefData( ';', "", 3 ),
        RefData( 'I', "_S16", 4 ),
        RefData( '=', "", 4 ),
        RefData( 'N', "-22", 4 ),
        RefData( ';', "", 4 ),
        RefData( '}', "", 5 ),
        RefData( ';', "", 5 ),
        RefData( 'I', "enabled", 6 ),
        RefData( '=', "", 6 ),
        RefData( 'I', "true", 6 ),
        RefData( ';', "", 6 ),
        RefData( 'I', "_F4", 7 ),
        RefData( '=', "", 7 ),
        RefData( 'N', "3.666667", 7 ),
        RefData( ';', "", 7 ),
        RefData( 'I', "_F8", 8 ),
        RefData( '=', "", 8 ),
        RefData( 'N', "7.333333", 8 ),
        RefData( ';', "", 8 ),
        RefData( '}', "", 9 ),
        RefData( ';', "", 9 ),
        RefData( '\0', "", 0 ),
    };
    std::size_t nr_of_tokens = PrintTextTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestTextTokenizer(TestStream, TestTokens);
}

TEST(TextTokenizerTest, ListAndPointers)
{
    std::string TestStream =
        "!sTut_06a::STL stl = {\n"
        "    !sTut_06a::StringDeque_t StringDeque[1] = [\"aaa\"];\n"
        "    !sTut_06a::IntList_t IntList[2] = [-90, -120];\n"
        "    !sTut_06a::IntPtrList_t IntPtrList[2] = [&1635937476896, &1635937475808];\n"
        "};\n"
        "!int32_t *1635937476896 = 0;\n"
        "!int32_t *1635937475808 = 300;\n";
    RefDataVector TestTokens = {
        RefData( '!', "sTut_06a::STL", 1 ),
        RefData( 'I', "stl", 1 ),
        RefData( '=', "", 1 ),
        RefData( '{', "", 1 ),  // end of line 1
        RefData( '!', "sTut_06a::StringDeque_t", 2 ),
        RefData( 'I', "StringDeque", 2 ),
        RefData( '[', "", 2 ),
        RefData( 'N', "1", 2 ),
        RefData( ']', "", 2 ),
        RefData( '=', "", 2 ),
        RefData( '[', "", 2 ),
        RefData( '"', "aaa", 2 ),
        RefData( ']', "", 2 ),
        RefData( ';', "", 2 ),  // end of line 2
        RefData( '!', "sTut_06a::IntList_t", 3 ),
        RefData( 'I', "IntList", 3 ),
        RefData( '[', "", 3 ),
        RefData( 'N', "2", 3 ),
        RefData( ']', "", 3 ),
        RefData( '=', "", 3 ),
        RefData( '[', "", 3 ),
        RefData( 'N', "-90", 3 ),
        RefData( ',', "", 3 ),
        RefData( 'N', "-120", 3 ),
        RefData( ']', "", 3 ),
        RefData( ';', "", 3 ),  // end of line 3
        RefData( '!', "sTut_06a::IntPtrList_t", 4 ),
        RefData( 'I', "IntPtrList", 4 ),
        RefData( '[', "", 4 ),
        RefData( 'N', "2", 4 ),
        RefData( ']', "", 4 ),
        RefData( '=', "", 4 ),
        RefData( '[', "", 4 ),
        RefData( '&', "1635937476896", 4 ),
        RefData( ',', "", 4 ),
        RefData( '&', "1635937475808", 4 ),
        RefData( ']', "", 4 ),
        RefData( ';', "", 4 ),  // end of line 4
        RefData( '}', "", 5 ),
        RefData( ';', "", 5 ),  // end of line 5
        RefData( '!', "int32_t", 6 ),
        RefData( '*', "1635937476896", 6 ),
        RefData( '=', "", 6 ),
        RefData( 'N', "0", 6 ),
        RefData( ';', "", 6 ),  // end of line 6
        RefData( '!', "int32_t", 7 ),
        RefData( '*', "1635937475808", 7 ),
        RefData( '=', "", 7 ),
        RefData( 'N', "300", 7 ),
        RefData( ';', "", 7 ),  // end of line 7
        RefData( '\0', "", 0 ),
    };
    std::size_t nr_of_tokens = PrintTextTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestTextTokenizer(TestStream, TestTokens);
}

TEST(TextTokenizerTest, IncludeFile)
{
    std::string TestFile =
        "A = { aa=1; bb=2; }\n"
        "B = {}\n";
    std::string TestStream =
        "AB = {\n"
        "#include <AB.inc>\n"
        "}\n";
    RefDataVector TestTokens = {
        RefData( 'I', "AB", 1 ),
        RefData( '=', "", 1 ),
        RefData( '{', "", 1 ),
        RefData( 'I', "A", 1 ),
        RefData( '=', "", 1 ),
        RefData( '{', "", 1 ),
        RefData( 'I', "aa", 1 ),
        RefData( '=', "", 1 ),
        RefData( 'N', "1", 1 ),
        RefData( ';', "", 1 ),
        RefData( 'I', "bb", 1 ),
        RefData( '=', "", 1 ),
        RefData( 'N', "2", 1 ),
        RefData( ';', "", 1 ),
        RefData( '}', "", 1 ),
        RefData( 'I', "B", 2 ),
        RefData( '=', "", 2 ),
        RefData( '{', "", 2 ),
        RefData( '}', "", 2 ),
        RefData( '}', "", 3 ),
        RefData( '\0', "", 0 ),
    };
    // Create the include file.
    std::ofstream include_file("AB.inc", std::ios_base::trunc|std::ios_base::out);
    include_file << TestFile;
    include_file.close();
    // Run the tokenizer and check the result.
    std::size_t nr_of_tokens = PrintTextTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestTextTokenizer(TestStream, TestTokens);
}

TEST(TextTokenizerTest, Preprocessor_ab0)
{
    std::string TestStream =
        "AB = {\n"
        "# ifdef AB_1\n"
        "    ab = 1;\n"
        "# else\n"
        "    ab = 0;\n"
        "# endif\n"
        "}\n";
    const std::size_t line_begin = 1;
    const std::size_t line_ab = 5;
    const std::size_t line_end = 7;
    RefDataVector TestTokens = {
        RefData( 'I', "AB", line_begin ),
        RefData( '=', "",   line_begin ),
        RefData( '{', "",   line_begin ),
        RefData( 'I', "ab", line_ab ),
        RefData( '=', "",   line_ab ),
        RefData( 'N', "0",  line_ab ),
        RefData( ';', "",   line_ab ),
        RefData( '}', "",   line_end ),
        RefData( '\0', "",  0 ),
    };
    // Run the tokenizer and check the result.
    std::size_t nr_of_tokens = PrintTextTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestTextTokenizer(TestStream, TestTokens);
}

TEST(TextTokenizerTest, Preprocessor_ab0_undef)
{
    std::string TestStream =
        "#define AB_1 1\n"
        "#undef AB_1 1\n"
        "AB = {\n"
        "#ifdef AB_1\n"
        "    ab = 1;\n"
        "#else\n"
        "    ab = 0;\n"
        "#endif\n"
        "}\n";
    const std::size_t line_begin = 3;
    const std::size_t line_ab = 7;
    const std::size_t line_end = 9;
    RefDataVector TestTokens = {
        RefData( 'I', "AB", line_begin ),
        RefData( '=', "",   line_begin ),
        RefData( '{', "",   line_begin ),
        RefData( 'I', "ab", line_ab ),
        RefData( '=', "",   line_ab ),
        RefData( 'N', "0",  line_ab ),
        RefData( ';', "",   line_ab ),
        RefData( '}', "",   line_end ),
        RefData( '\0', "",  0 ),
    };
    // Run the tokenizer and check the result.
    std::size_t nr_of_tokens = PrintTextTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestTextTokenizer(TestStream, TestTokens);
}

TEST(TextTokenizerTest, Preprocessor_ab1)
{
    std::string TestStream =
        "#define AB_1 1\n"
        "#define \t AB_2  2 \n"
        "AB = {\n"
        "# ifdef AB_1\n"
        "    ab = 1;\n"
        "# else\n"
        "    ab = 0;\n"
        "# endif\n"
        "}\n";
    const std::size_t line_begin = 3;
    const std::size_t line_ab = 5;
    const std::size_t line_end = 9;
    RefDataVector TestTokens = {
        RefData( 'I', "AB", line_begin ),
        RefData( '=', "",   line_begin ),
        RefData( '{', "",   line_begin ),
        RefData( 'I', "ab", line_ab ),
        RefData( '=', "",   line_ab ),
        RefData( 'N', "1",  line_ab ),
        RefData( ';', "",   line_ab ),
        RefData( '}', "",   line_end ),
        RefData( '\0', "",  0 ),
    };
    // Run the tokenizer and check the result.
    std::size_t nr_of_tokens = PrintTextTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestTextTokenizer(TestStream, TestTokens);
}

TEST(TextTokenizerTest, Preprocessor_ab1_ifndef)
{
    std::string TestStream =
        "AB = {\n"
        "# ifndef AB_1\n"
        "    ab = 1;\n"
        "# else\n"
        "    ab = 0;\n"
        "# endif\n"
        "}\n";
    const std::size_t line_begin = 1;
    const std::size_t line_ab = 3;
    const std::size_t line_end = 7;
    RefDataVector TestTokens = {
        RefData( 'I', "AB", line_begin ),
        RefData( '=', "",   line_begin ),
        RefData( '{', "",   line_begin ),
        RefData( 'I', "ab", line_ab ),
        RefData( '=', "",   line_ab ),
        RefData( 'N', "1",  line_ab ),
        RefData( ';', "",   line_ab ),
        RefData( '}', "",   line_end ),
        RefData( '\0', "",  0 ),
    };
    // Run the tokenizer and check the result.
    std::size_t nr_of_tokens = PrintTextTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestTextTokenizer(TestStream, TestTokens);
}

TEST(TextTokenizerTest, Preprocessor_ab2)
{
    std::string TestStream =
        "#define AB_1 1\n"
        "#define AB_2 2\n"
        "AB = {\n"
        "# if AB_2\n"
        "    ab = 2;\n"
        "# elif AB_1\n"
        "    ab = 1;\n"
        "# else\n"
        "    ab = 0;\n"
        "# endif\n"
        "}\n";
    const std::size_t line_begin = 3;
    const std::size_t line_ab = 5;
    const std::size_t line_end = 11;
    RefDataVector TestTokens = {
        RefData( 'I', "AB", line_begin ),
        RefData( '=', "",   line_begin ),
        RefData( '{', "",   line_begin ),
        RefData( 'I', "ab", line_ab ),
        RefData( '=', "",   line_ab ),
        RefData( 'N', "2",  line_ab ),
        RefData( ';', "",   line_ab ),
        RefData( '}', "",   line_end ),
        RefData( '\0', "",  0 ),
    };
    // Run the tokenizer and check the result.
    std::size_t nr_of_tokens = PrintTextTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestTextTokenizer(TestStream, TestTokens);
}

TEST(TextTokenizerTest, Preprocessor_ExternalDefines_ab0)
{
    std::string TestStream =
        "AB = {\n"
        "# if AB_2\n"
        "    ab = 2;\n"
        "# elif AB_1\n"
        "    ab = 1;\n"
        "# else\n"
        "    ab = 0;\n"
        "# endif\n"
        "}\n";
    const std::size_t line_begin = 1;
    const std::size_t line_ab = 7;
    const std::size_t line_end = 9;
    RefDataVector TestTokens = {
        RefData( 'I', "AB", line_begin ),
        RefData( '=', "",   line_begin ),
        RefData( '{', "",   line_begin ),
        RefData( 'I', "ab", line_ab ),
        RefData( '=', "",   line_ab ),
        RefData( 'N', "0",  line_ab ),
        RefData( ';', "",   line_ab ),
        RefData( '}', "",   line_end ),
        RefData( '\0', "",  0 ),
    };
    // Run the tokenizer and check the result.
    std::size_t nr_of_tokens = PrintTextTokens(TestStream, {});
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestTextTokenizer(TestStream, TestTokens, {});
}

TEST(TextTokenizerTest, Preprocessor_ExternalDefines_ab1)
{
    std::string TestStream =
        "AB = {\n"
        "# if AB_2\n"
        "    ab = 2;\n"
        "# elif AB_1\n"
        "    ab = 1;\n"
        "# else\n"
        "    ab = 0;\n"
        "# endif\n"
        "}\n";
    const std::size_t line_begin = 1;
    const std::size_t line_ab = 5;
    const std::size_t line_end = 9;
    RefDataVector TestTokens = {
        RefData( 'I', "AB", line_begin ),
        RefData( '=', "",   line_begin ),
        RefData( '{', "",   line_begin ),
        RefData( 'I', "ab", line_ab ),
        RefData( '=', "",   line_ab ),
        RefData( 'N', "1",  line_ab ),
        RefData( ';', "",   line_ab ),
        RefData( '}', "",   line_end ),
        RefData( '\0', "",  0 ),
    };
    // Run the tokenizer and check the result.
    rOops::PreprocessorDefines_t ext_def = { {"AB_1",""} };
    std::size_t nr_of_tokens = PrintTextTokens(TestStream, ext_def);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestTextTokenizer(TestStream, TestTokens, ext_def);
}

TEST(TextTokenizerTest, Preprocessor_ExternalDefines_ab2)
{
    std::string TestStream =
        "AB = {\n"
        "# if AB_2\n"
        "    ab = 2;\n"
        "# elif AB_1\n"
        "    ab = 1;\n"
        "# else\n"
        "    ab = 0;\n"
        "# endif\n"
        "}\n";
    const std::size_t line_begin = 1;
    const std::size_t line_ab = 3;
    const std::size_t line_end = 9;
    RefDataVector TestTokens = {
        RefData( 'I', "AB", line_begin ),
        RefData( '=', "",   line_begin ),
        RefData( '{', "",   line_begin ),
        RefData( 'I', "ab", line_ab ),
        RefData( '=', "",   line_ab ),
        RefData( 'N', "2",  line_ab ),
        RefData( ';', "",   line_ab ),
        RefData( '}', "",   line_end ),
        RefData( '\0', "",  0 ),
    };
    // Run the tokenizer and check the result.
    rOops::PreprocessorDefines_t ext_def = { {"AB_2",""} };
    std::size_t nr_of_tokens = PrintTextTokens(TestStream, ext_def);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestTextTokenizer(TestStream, TestTokens, ext_def);
}
