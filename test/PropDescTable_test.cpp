/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2019:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rPropInterface.h>
#include <oops/rPropDescriptor.h>
#include <gtest/gtest.h>

namespace PropDescTable
{

    // Declaration of struct 'A_s' and 'B_s'.
    // They have simple integer data members.

    // 'A_s' and 'B_s' must have a default constructor.

    struct A_s
    {
        A_s() = default;
        A_s(long aLong, short aShort)
            : _Long(aLong), _Short(aShort)
        {}
        bool operator==(const A_s& arR) const
        {
            return _Long == arR._Long && _Short == arR._Short;
        }
        long _Long{ 0 };
        short _Short{ 0 };
    }; //struct A_s

    struct B_s : public A_s
    {
        B_s()
            : A_s()
        {}
        B_s(unsigned char aByte, short aShort, long aLong)
            : A_s(aLong, aShort), _Byte(aByte)
        {}
        bool operator==(const B_s& arR) const
        {
            return A_s::operator==(arR) && _Byte == arR._Byte;
        }
        unsigned char _Byte{ 0 };
    }; //struct B_s
}

rOOPS_DECLARE_TYPEINFO_CLASS(rPropTypeInfo_A_s, PropDescTable::A_s)
{
    using class_type = PropDescTable::A_s;
    rOOPS_PROPERTY(_Short), "short";
    rOOPS_PROPERTY(_Long), "long", rOops::rPropFlag::cRequired;
}

rOOPS_DECLARE_TYPEINFO_CLASS(rPropTypeInfo_B_s, PropDescTable::B_s)
{
    using class_type = PropDescTable::B_s;
    rOOPS_INHERIT(PropDescTable::A_s); // No need for explicit .required() here. Base class has required properties, it automatically makes the base class required.
    rOOPS_PROPERTY(_Byte).name("byte").required();
}


#include "SerializationTest.h"
#include <gtest/gtest.h>


using namespace rOops;

TEST(PropertyDescriptorTableTest, SaveNonInvasiveTypeDescriptor)
{
    // Create an instance of struct 'A_s'.
    PropDescTable::A_s A(1, 2);
    testSerialization("TestClassA", A);

    // Create an instance of struct 'B_s'.
    PropDescTable::B_s B(100, 200, 300);
    testSerialization("TestClassB", B);
}

TEST(PropertyDescriptorTableTest, RequiredProperties)
{
    PropDescTable::B_s oops_B;
    try {
        std::string oops_str =
            "TestClassB = !PropDescTable::B_s {\n"
            "  PropDescTable::A_s = !PropDescTable::A_s {\n"
            "    short = 200;\n"
            "    long = 300;\n"
            "  };\n"
            //"  byte = 100;\n"
            "};\n";
        std::stringstream strm(oops_str);
        rOops::rOopsTextParser oops_parser(strm, "PropertyDescriptorTableTest.RequiredProperties");
        load(oops_parser, oops_B);
        FAIL() << "Required property error message is expected here.";
    }
    catch (const std::runtime_error& arEx) {
        EXPECT_EQ("PropertyDescriptorTableTest.RequiredProperties(6): Required property (byte) is missing.", std::string(arEx.what()));
    }

    try {
        std::string oops_str =
            "TestClassB = !PropDescTable::B_s {\n"
            "  PropDescTable::A_s = !PropDescTable::A_s {\n"
            "    short = 200;\n"
            //"    long = 300;\n"
            "  };\n"
            "  byte = 100;\n"
            "};\n";
        std::stringstream strm(oops_str);
        rOops::rOopsTextParser oops_parser(strm, "PropertyDescriptorTableTest.RequiredProperties");
        load(oops_parser, oops_B);
        FAIL() << "Required property error message is expected here.";
    }
    catch (const std::runtime_error& arEx) {
        EXPECT_EQ("PropertyDescriptorTableTest.RequiredProperties(4): Required property (long) is missing.", std::string(arEx.what()));
    }

    try {
        std::string oops_str =
            "TestClassB = !PropDescTable::B_s {\n"
            //"  PropDescTable::A_s = !PropDescTable::A_s {\n"
            //"    short = 200;\n"
            //"    long = 300;\n"
            //"  };\n"
            "  byte = 100;\n"
            "};\n";
        std::stringstream strm(oops_str);
        rOops::rOopsTextParser oops_parser(strm, "PropertyDescriptorTableTest.RequiredProperties");
        load(oops_parser, oops_B);
        FAIL() << "Required property error message is expected here.";
    }
    catch (const std::runtime_error& arEx) {
        EXPECT_EQ("PropertyDescriptorTableTest.RequiredProperties(3): Required property (PropDescTable::A_s) is missing.", std::string(arEx.what()));
    }

    PropDescTable::B_s yaml_B;
    try {
        std::string yaml_str =
            "TestClassB: !PropDescTable::B_s\n"
            "  PropDescTable::A_s: !PropDescTable::A_s\n"
            "    short: 200\n"
            "    long: 300\n";
            //"  byte: 100\n";
        std::stringstream strm(yaml_str);
        rOops::rOopsYamlParser yaml_parser(strm, "PropertyDescriptorTableTest.RequiredProperties");
        load(yaml_parser, yaml_B);
        FAIL() << "Required property error message is expected here.";
    }
    catch (const std::runtime_error& arEx) {
        EXPECT_EQ("PropertyDescriptorTableTest.RequiredProperties(5): Required property (byte) is missing.", std::string(arEx.what()));
    }
}
