/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/


#include <oops/rOopsConfig.h>
#include <oops/rPropTypeDef.h>
#include <gtest/gtest.h>

TEST(System, Endiannes)
{
    // Integer endianness.
    volatile std::uint32_t ii = 0x01234567;
    if ((*((std::uint8_t*)(&ii))) != 0x67) {
        std::cout << "Integer byte order is BIG ENDIAN.";
    }
    else {
        std::cout << "Integer byte order is little endian." << std::endl;
    }
    // Float endianness.
    {
        volatile float ffn = -0.0f;
        const std::uint8_t* fptr = ((std::uint8_t*)(&ffn));
        std::cout << std::hex << (unsigned)fptr[0] << ' ' << (unsigned)fptr[1] << ' ' << (unsigned)fptr[2] << ' ' << (unsigned)fptr[3] << std::endl;
    }
    // Double endianness.
    {
        volatile double ffn = -0.0f;
        const std::uint8_t* fptr = ((std::uint8_t*)(&ffn));
        std::cout << std::hex << (unsigned)fptr[0] << ' ' << (unsigned)fptr[1] << ' ' << (unsigned)fptr[2] << ' ' << (unsigned)fptr[3]
                       << ' ' << (unsigned)fptr[4] << ' ' << (unsigned)fptr[5] << ' ' << (unsigned)fptr[6] << ' ' << (unsigned)fptr[7] << std::endl;
    }
    EXPECT_EQ(rOops::isIntegerLittleEndian(), rOops::isFloatLittleEndian());
    EXPECT_EQ(rOops::isFloatLittleEndian(), rOops::isDoubleLittleEndian());
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    int result = 0;

    std::cout << "Test with never write type name." << std::endl;
    rOops::globalConfig().writeTypeNameLevel = rOops::OopsConfig::WriteTypeName::eNever;
    result = RUN_ALL_TESTS();
    if (0 != result) return result;  // Stop if failed.

    std::cout << "Test with type name when needed." << std::endl;
    rOops::globalConfig().writeTypeNameLevel = rOops::OopsConfig::WriteTypeName::eWhenNeeded;
    result = RUN_ALL_TESTS();
    if (0 != result) return result;  // Stop if failed.

    std::cout << "Test with type name always." << std::endl;
    rOops::globalConfig().writeTypeNameLevel = rOops::OopsConfig::WriteTypeName::eAlways;
    result = RUN_ALL_TESTS();
    if (0 != result) return result;  // Stop if failed.

    // TODO: Flow style parser does not work. Fix it!
    /*
    std::cout << "Test flow style yaml format." << std::endl;
    rOops::globalConfig().writeTypeNameLevel = rOops::OopsConfig::WriteTypeName::eWhenNeeded;
    rOops::globalConfig().flowStyleBlock = true;
    rOops::globalConfig().flowStyleList = true;
    result = RUN_ALL_TESTS();
    if (0 != result) return result;  // Stop if failed.

    if (0 != result) return result;  // Stop if failed.
    std::cout << "Test flow style yaml format." << std::endl;
    rOops::globalConfig().writeTypeNameLevel = rOops::OopsConfig::WriteTypeName::eWhenNeeded;
    rOops::globalConfig().flowStyleBlock = false;
    rOops::globalConfig().flowStyleList = true;
    result = RUN_ALL_TESTS();
    if (0 != result) return result;  // Stop if failed.

    std::cout << "Test flow style yaml format." << std::endl;
    rOops::globalConfig().writeTypeNameLevel = rOops::OopsConfig::WriteTypeName::eWhenNeeded;
    rOops::globalConfig().flowStyleBlock = true;
    rOops::globalConfig().flowStyleList = false;
    result = RUN_ALL_TESTS();
    if (0 != result) return result;  // Stop if failed.
    */

    return result;
}
