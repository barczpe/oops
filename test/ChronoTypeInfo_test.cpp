/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rPropInterface.h>
#include "SerializationTest.h"
#include <gtest/gtest.h>
#include <chrono>
#include <cstdlib> // for abs()

using namespace rOops;
using namespace std::chrono;

using unsigned_ms = std::chrono::duration<unsigned, std::milli>;
rOOPS_DECLARE_TYPEINFO(unsigned_ms)
rOOPS_CreateNamedTypeInfoObject(unsigned_ms, rOops::rPropTypeInfoStdChronoDuration<unsigned_ms>, StdChronoDurationUnsignedMilliSec)

namespace
{
    void skipWhiteSpace( rOops::istream_type& arStrm )
    {
        rOops::istream_type::char_type ch;
        while( !arStrm.eof() && (' '==arStrm.peek()) ) {
            arStrm.get(ch);
        } //while
    } //skipWhiteSpace()
}

TEST(PropertyInterfaceTest, ChronoReadTimeAndUnits)
{
    std::stringstream strm1{"1s 2ms 3us 4ns 5sec"};
    std::string str1;
    readTimeAndUnits(strm1, str1);
    EXPECT_EQ("1s", str1);

    str1 = ""; skipWhiteSpace(strm1);
    readTimeAndUnits(strm1, str1);
    EXPECT_EQ("2ms", str1);

    str1 = ""; skipWhiteSpace(strm1);
    readTimeAndUnits(strm1, str1);
    EXPECT_EQ("3us", str1);

    str1 = ""; skipWhiteSpace(strm1);
    readTimeAndUnits(strm1, str1);
    EXPECT_EQ("4ns", str1);

    str1 = ""; skipWhiteSpace(strm1);
    readTimeAndUnits(strm1, str1);
    EXPECT_EQ("5sec", str1);

    std::stringstream strm2{"10h 11day 12min"};
    std::string str2;
    readTimeAndUnits(strm2, str2);
    EXPECT_EQ("10h", str2);

    str2 = ""; skipWhiteSpace(strm1);
    readTimeAndUnits(strm2, str2);
    EXPECT_EQ("11day", str2);

    str2 = ""; skipWhiteSpace(strm1);
    readTimeAndUnits(strm2, str2);
    EXPECT_EQ("12min", str2);

    std::stringstream strm3{"12:34:56.789 23:45:67XYZ"};
    std::string str3;
    readTimeAndUnits(strm3, str3);
    EXPECT_EQ("12:34:56.789", str3);

    str3 = ""; skipWhiteSpace(strm1);
    readTimeAndUnits(strm3, str3);
    EXPECT_EQ("23:45:67", str3);
}


TEST(PropertyInterfaceTest, ChronoDurationToString)
{
    {
        nanoseconds dns{ 0 };
        EXPECT_EQ("0ns", rPropGetTypeInfo(&dns)->value(&dns));
        dns = 1ns;
        EXPECT_EQ("1ns", rPropGetTypeInfo(&dns)->value(&dns));
        dns = 999ns;
        EXPECT_EQ("999ns", rPropGetTypeInfo(&dns)->value(&dns));
        dns = 1000ns;
        EXPECT_EQ("1us", rPropGetTypeInfo(&dns)->value(&dns));
        dns = 1001ns;
        EXPECT_EQ("1001ns", rPropGetTypeInfo(&dns)->value(&dns));
        dns = 2000000ns;
        EXPECT_EQ("2ms", rPropGetTypeInfo(&dns)->value(&dns));
    }
    {
        microseconds dus{ 123000 };
        EXPECT_EQ("123ms", rPropGetTypeInfo(&dus)->value(&dus));
        dus = 0us;
        EXPECT_EQ("0us", rPropGetTypeInfo(&dus)->value(&dus));
    }
    {
        milliseconds dms{ 0 };
        EXPECT_EQ("0ms", rPropGetTypeInfo(&dms)->value(&dms));
        dms = 23ms;
        EXPECT_EQ("23ms", rPropGetTypeInfo(&dms)->value(&dms));
        dms = 48001ms;
        EXPECT_EQ("48001ms", rPropGetTypeInfo(&dms)->value(&dms));
        dms = 57000ms;
        EXPECT_EQ("57s", rPropGetTypeInfo(&dms)->value(&dms));
        dms = 60000ms;
        EXPECT_EQ("1min", rPropGetTypeInfo(&dms)->value(&dms));
    }
    {
        seconds dsec{ 0 };
        EXPECT_EQ("0s", rPropGetTypeInfo(&dsec)->value(&dsec));
        dsec = 1s;
        EXPECT_EQ("1s", rPropGetTypeInfo(&dsec)->value(&dsec));
        dsec = 59s;
        EXPECT_EQ("59s", rPropGetTypeInfo(&dsec)->value(&dsec));
        dsec = 60s;
        EXPECT_EQ("1min", rPropGetTypeInfo(&dsec)->value(&dsec));
        dsec = 90s;
        EXPECT_EQ("90s", rPropGetTypeInfo(&dsec)->value(&dsec));
        dsec = 120s;
        EXPECT_EQ("2min", rPropGetTypeInfo(&dsec)->value(&dsec));
    }
    {
        minutes dmin{ 0 };
        EXPECT_EQ("0min", rPropGetTypeInfo(&dmin)->value(&dmin));
        dmin = 1min;
        EXPECT_EQ("1min", rPropGetTypeInfo(&dmin)->value(&dmin));
        dmin = 60min;
        EXPECT_EQ("1h", rPropGetTypeInfo(&dmin)->value(&dmin));
        dmin = 61min;
        EXPECT_EQ("61min", rPropGetTypeInfo(&dmin)->value(&dmin));
    }
    {
        hours dhours{ 0 };
        EXPECT_EQ("0h", rPropGetTypeInfo(&dhours)->value(&dhours));
        dhours = 1h;
        EXPECT_EQ("1h", rPropGetTypeInfo(&dhours)->value(&dhours));
        dhours = 24h;
        EXPECT_EQ("1day", rPropGetTypeInfo(&dhours)->value(&dhours));
        dhours = 25h;
        EXPECT_EQ("25h", rPropGetTypeInfo(&dhours)->value(&dhours));
        dhours = 100*24h;
        EXPECT_EQ("100day", rPropGetTypeInfo(&dhours)->value(&dhours));
    }
}

/// Almost the same as EXPECT_THROW(), but this function checks the error message of the exception.
void testWrongDuration(const std::string& arValue, const char* arExpectedErrorMsg)
{
    try {
        nanoseconds dd{0ns};
        const rPropTypeInfo* pTI = rPropGetTypeInfo(&dd);
        pTI->setValue(&dd, arValue);
        FAIL() << "No exception has thrown.";
    }
    catch(const std::runtime_error& arEx) {
        EXPECT_STREQ(arExpectedErrorMsg, arEx.what());
    }
    catch(...) {
        FAIL() << "Unexpected exception caught.";
    }
}

TEST(PropertyInterfaceTest, ChronoStringToDuration)
{
    // Test string to chrono variable conversion.
    milliseconds dd{0ms};
    const rPropTypeInfo* pTI = rPropGetTypeInfo(&dd);
    pTI->setValue(&dd, std::string("888ms"));
    EXPECT_EQ(milliseconds(888), dd);
    pTI->setValue(&dd, std::string("1:2.3"));
    EXPECT_EQ(milliseconds(62300), dd);
    pTI->setValue(&dd, std::string("1:2.34"));
    EXPECT_EQ(milliseconds(62340), dd);
    pTI->setValue(&dd, std::string("1:2.345"));
    EXPECT_EQ(milliseconds(62345), dd);
    pTI->setValue(&dd, std::string("1:2:3"));
    EXPECT_EQ(seconds(3600+123), dd);
    pTI->setValue(&dd, std::string("1:2:3.456"));
    EXPECT_EQ(milliseconds(1000*(3600+123)+456), dd);
    pTI->setValue(&dd, std::string("1:2"));
    EXPECT_EQ(minutes(62), dd);
    pTI->setValue(&dd, std::string("1234:59"));
    EXPECT_EQ(minutes(60*1234+59), dd);
    pTI->setValue(&dd, std::string("1234567890987654321.0s"));
    EXPECT_EQ(1234567890987654321s, dd);

    testWrongDuration("", "Time value is empty.");
    testWrongDuration("987.6h", "Invalid time value. Hours does not support fractional part: 987.6h");
    testWrongDuration("12mm", "Invalid time unit: 12mm");
    testWrongDuration("12x", "Invalid time unit: 12x");
    testWrongDuration("12xs", "Invalid time unit: 12xs");
    testWrongDuration("123xyz", "Invalid time unit: 123xyz");
    testWrongDuration("123456seconds", "Invalid time unit. It is longer than 3 characters: 123456seconds");
    testWrongDuration("1.2345s", "Invalid time value. Fraction part is longer than 3 digits: 1.2345s");
    testWrongDuration("8x8x8ms", "Invalid time unit. It is longer than 3 characters: 8x8x8ms");
    testWrongDuration("x1:2y:3.45z6", "Time value is wrong. String to int conversion failed: x1:2y:3.45z6");
    testWrongDuration("12:34:56.", "Time value is wrong. String to int conversion for the fractional part failed: 12:34:56.");
    testWrongDuration("12:34.", "Time value is wrong. String to int conversion for the fractional part failed: 12:34.");
    testWrongDuration("12:34:", "Invalid time value: 12:34:");
    testWrongDuration("12:", "Invalid time value: 12:");
    testWrongDuration("7:6.5432", "Invalid time value. Fraction part is longer than 3 digits: 7:6.5432");
    testWrongDuration("9:7:6.5432", "Invalid time value. Fraction part is longer than 3 digits: 9:7:6.5432");
}

TEST(PropertyInterfaceTest, ChronoNanoseconds)
{
    nanoseconds dd{0ns};
    const rPropTypeInfo* pTI = rPropGetTypeInfo(&dd);
    pTI->setValue(&dd, std::string("12345678ns"));
    EXPECT_EQ(nanoseconds(12345678), dd);

    testWrongDuration("987.6ns", "Invalid time value. Nanosecond cannot have fractional part: 987.6ns");
}

TEST(PropertyInterfaceTest, ChronoSteadyClockTimePointSet)
{
    // Test string to chrono variable conversion.
    steady_clock::time_point tp1(steady_clock::time_point::duration(1));
    steady_clock::time_point tp2, tp3;
    const rPropTypeInfo* pTI = rPropGetTypeInfo(&tp1);

    // Set/get functions:
    std::string str = pTI->value(&tp1);
    EXPECT_EQ("1ns", str);
    pTI->setValue(&tp2, str);
    EXPECT_EQ(tp1, tp2);

    pTI->setValue(&tp2, std::string("888us"));
    EXPECT_EQ(steady_clock::time_point(microseconds(888)), tp2);
    pTI->setValue(&tp2, std::string("999ms"));
    EXPECT_EQ(steady_clock::time_point(milliseconds(999)), tp2);
    pTI->setValue(&tp2, std::string("10s"));
    EXPECT_EQ(steady_clock::time_point(seconds(10)), tp2);
    pTI->setValue(&tp2, std::string("12345sec"));
    EXPECT_EQ(steady_clock::time_point(seconds(12345)), tp2);
    pTI->setValue(&tp2, std::string("20min"));
    EXPECT_EQ(steady_clock::time_point(minutes(20)), tp2);
    pTI->setValue(&tp2, std::string("30h"));
    EXPECT_EQ(steady_clock::time_point(hours(30)), tp2);
    pTI->setValue(&tp2, std::string("40min"));
    EXPECT_EQ(steady_clock::time_point(minutes(40)), tp2);
    pTI->setValue(&tp2, std::string("50day"));
    EXPECT_EQ(steady_clock::time_point(hours(50*24)), tp2);

    // Fractional part tests.
    pTI->setValue(&tp2, std::string("98.76ms"));
    EXPECT_EQ(steady_clock::time_point(98760us), tp2);
    pTI->setValue(&tp2, std::string("67ms"));
    EXPECT_EQ(steady_clock::time_point(67ms), tp2);
    pTI->setValue(&tp2, std::string("1.23us"));
    EXPECT_EQ(steady_clock::time_point(1230ns), tp2);
    pTI->setValue(&tp2, std::string("45us"));
    EXPECT_EQ(steady_clock::time_point(45us), tp2);
    pTI->setValue(&tp2, std::string("1.1s"));
    EXPECT_EQ(steady_clock::time_point(1100ms), tp2);
    pTI->setValue(&tp2, std::string("1.23s"));
    EXPECT_EQ(steady_clock::time_point(1230ms), tp2);
    pTI->setValue(&tp2, std::string("1.234s"));
    EXPECT_EQ(steady_clock::time_point(1234ms), tp2);
    pTI->setValue(&tp2, std::string("1234s"));
    EXPECT_EQ(steady_clock::time_point(1234s), tp2);
}

TEST(PropertyInterfaceTest, ChronoSystemClockTimePointSet)
{
    system_clock::time_point tsc;
    const rPropTypeInfo* pTI = rPropGetTypeInfo(&tsc);
    std::string expected  = "2019-02-08 17:16:04";
    std::string date_time = "2019-02-08 17:16:04";
    pTI->setValue(&tsc, date_time);
    EXPECT_EQ(expected, pTI->value(&tsc));

    system_clock::time_point tsc2;
    date_time = "2019/02/08 17:16:04";
    pTI->setValue(&tsc2, date_time);
    EXPECT_EQ(tsc, tsc2) << pTI->value(&tsc2);
    EXPECT_EQ(expected, pTI->value(&tsc2));

    date_time = "02/08/2019 17:16:04";
    pTI->setValue(&tsc2, date_time);
    EXPECT_EQ(tsc, tsc2) << pTI->value(&tsc2);
    EXPECT_EQ(expected, pTI->value(&tsc2));

    date_time = "2/8/2019 17:16:04";
    pTI->setValue(&tsc2, date_time);
    EXPECT_EQ(tsc, tsc2) << pTI->value(&tsc2);
    EXPECT_EQ(expected, pTI->value(&tsc2));

    date_time = "08.02.2019 17:16:04";
    pTI->setValue(&tsc2, date_time);
    EXPECT_EQ(tsc, tsc2) << pTI->value(&tsc2);
    EXPECT_EQ(expected, pTI->value(&tsc2));

    date_time = "8.2.2019 17:16:04";
    pTI->setValue(&tsc2, date_time);
    EXPECT_EQ(tsc, tsc2) << pTI->value(&tsc2);
    EXPECT_EQ(expected, pTI->value(&tsc2));

    date_time = "31.12.2037 0:0:0";
    pTI->setValue(&tsc2, date_time);
    EXPECT_EQ("2037-12-31 00:00:00", pTI->value(&tsc2));
}

TEST(PropertyInterfaceTest, ChronoSystemClockTimePointPartialSet)
{
    {
        system_clock::time_point tsc;
        const rPropTypeInfo* pTI = rPropGetTypeInfo(&tsc);
        std::string expected  = "1986-11-30 00:00:00";
        std::string date_time = "1986-11-30";
        pTI->setValue(&tsc, date_time);
        EXPECT_EQ(expected, pTI->value(&tsc));

        system_clock::time_point tsc2;
        date_time = "1986/11/30";
        pTI->setValue(&tsc2, date_time);
        EXPECT_EQ(tsc, tsc2) << pTI->value(&tsc2);
        EXPECT_EQ(expected, pTI->value(&tsc2));

        date_time = "11/30/1986";
        pTI->setValue(&tsc2, date_time);
        EXPECT_EQ(tsc, tsc2) << pTI->value(&tsc2);
        EXPECT_EQ(expected, pTI->value(&tsc2));

        date_time = "11/30/1986";
        pTI->setValue(&tsc2, date_time);
        EXPECT_EQ(tsc, tsc2) << pTI->value(&tsc2);
        EXPECT_EQ(expected, pTI->value(&tsc2));

        date_time = "30.11.1986";
        pTI->setValue(&tsc2, date_time);
        EXPECT_EQ(tsc, tsc2) << pTI->value(&tsc2);
        EXPECT_EQ(expected, pTI->value(&tsc2));

        date_time = "30.11.1986";
        pTI->setValue(&tsc2, date_time);
        EXPECT_EQ(tsc, tsc2) << pTI->value(&tsc2);
        EXPECT_EQ(expected, pTI->value(&tsc2));

        date_time = "30.11.1986 ";
        pTI->setValue(&tsc2, date_time);
        EXPECT_EQ(tsc, tsc2) << pTI->value(&tsc2);
        EXPECT_EQ(expected, pTI->value(&tsc2));
    }
    {
        system_clock::time_point tsc;
        const rPropTypeInfo* pTI = rPropGetTypeInfo(&tsc);
        std::string expected  = "2019-02-08 23:59:00";
        std::string date_time = "2019-02-08 23:59";
        pTI->setValue(&tsc, date_time);
        EXPECT_EQ(expected, pTI->value(&tsc));
    }
    {
        system_clock::time_point tsc;
        const rPropTypeInfo* pTI = rPropGetTypeInfo(&tsc);
        std::string expected  = "2020-07-06 12:00:00";
        std::string date_time = "2020-7-6T12";
        pTI->setValue(&tsc, date_time);
        std::string date_time2;
        pTI->value(&tsc, date_time2);
        EXPECT_EQ(expected, date_time2);
        system_clock::time_point tsc2;
        pTI->setValue(&tsc2, date_time2);
        EXPECT_EQ(tsc, tsc2);
    }
}

// TODO: Add DST test somehow.

/// Almost the same as EXPECT_THROW(), but this function checks the error message of the exception.
void testWrongDateTime(const std::string& arDateTime, const std::string& arExpectedErrorMsg)
{
    try {
        system_clock::time_point tsc;
        const rPropTypeInfo* pTI = rPropGetTypeInfo(&tsc);
        pTI->setValue(&tsc, arDateTime);
        FAIL() << "No exception has thrown.";
    }
    catch(const std::runtime_error& arEx) {
        EXPECT_EQ(arExpectedErrorMsg, arEx.what());
    }
    catch(...) {
        FAIL() << "Unexpected exception caught.";
    }
}

TEST(PropertyInterfaceTest, ChronoSystemClockTimePointError)
{
    testWrongDateTime("1969-12-31T23:59:59", "Invalid date-time value. Year is less then 1970: 1969-12-31T23:59:59");
    testWrongDateTime("1970~1~2", "Invalid date-time value. None of the expected date separator characters (-/.) found: 1970~1~2");
    testWrongDateTime("", "Date-time string is empty.");
}

struct CronoDateTime
{
    system_clock::time_point systemClockTp;
    steady_clock::time_point steadyClockTp;
    high_resolution_clock::time_point highResClockTp;
    virtual ~CronoDateTime() = default;
    CronoDateTime() = default;
    void now()
    {
        systemClockTp  = system_clock::now();
        steadyClockTp  = steady_clock::now();
        highResClockTp = high_resolution_clock::now();
    }
    bool operator==(const CronoDateTime& dd) const
    {
        // chrono::abs() is C++17 feature: if (abs(systemClockTp - dd.systemClockTp) >= 1s) return false;
        if (std::abs(duration_cast<seconds>(systemClockTp - dd.systemClockTp).count()) >= 1) return false;
        if (steadyClockTp != dd.steadyClockTp) return false;
        if (high_resolution_clock::is_steady) {
            if (highResClockTp != dd.highResClockTp) return false;
        }
        else {
            if (std::abs( duration_cast<seconds>(highResClockTp - dd.highResClockTp).count()) >= 1) return false;
        }
        return true;
    }
    rOOPS_ADD_PROPERTY_INTERFACE(CronoDateTime)
    {
        rOOPS_PROPERTY(systemClockTp);
        rOOPS_PROPERTY(steadyClockTp);
        rOOPS_PROPERTY(highResClockTp);
    }
};

TEST(PropertyInterfaceTest, ChronoTimePointDefaultValues)
{
    CronoDateTime dt;
    testSerialization("ChronoTimePoints", dt);

    // This is not a deterministic test case, because the value is different at every execution.
    dt.now();
    testSerialization("ChronoTimePoints", dt);
}

TEST(PropertyInterfaceTest, ChronoTimePoint)
{
    try {
        CronoDateTime dtOops;
        std::string oops_str =
            "ChronoTimePoints = !CronoDateTime {\n"
            "  systemClockTp = \"2019:02:15 11:48:15\";\n"
            "  steadyClockTp = 180051534301600ns;\n"
            "  highResClockTp = 180051534301700ns;\n"
            "};\n";
        std::stringstream strm(oops_str);
        rOops::rOopsTextParser oops_parser(strm, "PropertyInterfaceTest.ChronoTimePoint");
        load(oops_parser, dtOops);
        FAIL() << "No exception has thrown.";
    }
    catch(const std::runtime_error& arEx) {
        EXPECT_EQ("PropertyInterfaceTest.ChronoTimePoint(2): Invalid date-time value. None of the expected date separator characters (-/.) found: 2019:02:15 11:48:15", std::string(arEx.what()));
    }
    catch(...) {
        FAIL() << "Unexpected exception caught.";
    }

    try {
        CronoDateTime dtYaml;
        std::string yaml_str =
            "ChronoTimePoints: !CronoDateTime\n"
            "  systemClockTp: \"2019-02-15 11:48:15\"\n"
            "  steadyClockTp: 180051534301700ps\n"
            "  highResClockTp: 180051534301600ns\n";
        std::stringstream strm(yaml_str);
        rOops::rOopsYamlParser yaml_parser(strm, "ChronoTimePoint");
        load(yaml_parser, dtYaml);
        FAIL() << "No exception has thrown.";
    }
    catch(const std::runtime_error& arEx) {
        EXPECT_EQ("ChronoTimePoint(3): Invalid time unit: 180051534301700ps", std::string(arEx.what()));
    }
    catch(...) {
        FAIL() << "Unexpected exception caught.";
    }

}
