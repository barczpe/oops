/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rOopsYamlParser.h>
#include "SerializationTest.h"
#include "../example/sTut_02_inherit.h"
#include <gtest/gtest.h>
#include <iostream>
#include <sstream>
#include <vector>


TEST(YamlParserTest, EmptyList1)
{
    std::string TestStream =
        "StdSetTest: !StdSetTest\n"
        "    int_set[0] : !std::set<int>\n"
        "        -\n"
        "    data_set[0] : !std::set<rOops::test::Data>\n"
        "        -\n";
        RefDataVector TestTokens = {
            "StdSetTest, StdSetTest, 1, eBlockBegin",
            "std::set<int>, int_set, 0, eListBegin",
            "eListEnd",
            "std::set<rOops::test::Data>, data_set, 0, eListBegin",
            "eListEnd",
            "eBlockEnd",
        };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, EmptyList2)
{
    // Test empty line as well.
    std::string TestStream =
        "StdSetTest: !StdSetTest\n"
        "  \n"
        "    int_set : []\n"
        "    data_set : []\n"
        "    list_of_lists : [[], []]\n";
        RefDataVector TestTokens = {
            "StdSetTest, StdSetTest, 1, eBlockBegin",
            ", int_set, 1, eListBegin",
            "eListEnd",
            ", data_set, 1, eListBegin",
            "eListEnd",
            ", list_of_lists, 1, eListBegin",
            ", , 1, eListBegin",
            "eListEnd",
            ", , 1, eListBegin",
            "eListEnd",
            "eListEnd",
            "eBlockEnd",
        };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, FlowStyleList )
{
    // Empty lines at the beginnng and end of the stream.
    std::string TestStream =
        "\n"
        "StdSetTest: !StdSetTest\n"
        "    int_set : [1, 2]\n"
        "    data_set : [\"123\"]\n"
        "    list_of_lists : [[10, 20], [30]]\n"
        "\n";
        RefDataVector TestTokens = {
            "StdSetTest, StdSetTest, 1, eBlockBegin",
            ", int_set, 1, eListBegin",
            ", , 1, eValue(1)",
            ", , 1, eValue(2)",
            "eListEnd",
            ", data_set, 1, eListBegin",
            ", , 1, eValue(123)",
            "eListEnd",
            ", list_of_lists, 1, eListBegin",
            ", , 1, eListBegin",
            ", , 1, eValue(10)",
            ", , 1, eValue(20)",
            "eListEnd",
            ", , 1, eListBegin",
            ", , 1, eValue(30)",
            "eListEnd",
            "eListEnd",
            "eBlockEnd",
        };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, EmptyValues)
{
    std::string TestStream =
        "\n"
        " # Empty values stream\n"
        "\n"
        "B: !sTut_01a::B\n"
        "    sTut_01a::A : !sTut_01a::A\n"
        "        chr:\n"
        "        str:\n"
        "        num:\n"
        "    float: 1.2\n";
    RefDataVector TestTokens = {
        "sTut_01a::B, B, 1, eBlockBegin",
        "sTut_01a::A, sTut_01a::A, 1, eBlockBegin",
        ", chr, 1, eValue",
        ", str, 1, eValue",
        ", num, 1, eValue",
        "eBlockEnd",
        ", float, 1, eValue(1.2)",
        "eBlockEnd"
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, Structure)
{
    std::string TestStream =
        "# Structure with out of indentation comment.:\n"
        "B: !sTut_01a::B\n"
        "    sTut_01a::A : !sTut_01a::A\n"
        "# out of tab comment\n"
        "        chr: 'C'\n"
        "        str: \"default,file-name.ini,2.ini,3.ini,4.ini,5.ini,6.ini,7.ini,image_name.ini,image header.ini\"\n"
        "        num: 300\n"
        "    float: -10.0e+3\n";
    RefDataVector TestTokens = {
        "sTut_01a::B, B, 1, eBlockBegin",
        "sTut_01a::A, sTut_01a::A, 1, eBlockBegin",
        ", chr, 1, eValue(C)",
        ", str, 1, eValue(default,file-name.ini,2.ini,3.ini,4.ini,5.ini,6.ini,7.ini,image_name.ini,image header.ini)",
        ", num, 1, eValue(300)",
        "eBlockEnd",
        ", float, 1, eValue(-10.0e+3)",
        "eBlockEnd"
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, MissingTopLevelTypeName)
{
    std::string TestStream =
        "# Structure with out of indentation comment.:\n"
        "B:\n"
        "    sTut_01a::A : !sTut_01a::A\n"
        "# out of tab comment\n"
        "        chr: 'C'\n"
        "        str: \"default,file-name.ini,2.ini,3.ini,4.ini,5.ini,6.ini,7.ini,image_name.ini,image header.ini\"\n"
        "        num: 300\n"
        "    float: -10.0e+3\n";
    RefDataVector TestTokens = {
        ", B, 1, eBlockBegin",
        "sTut_01a::A, sTut_01a::A, 1, eBlockBegin",
        ", chr, 1, eValue(C)",
        ", str, 1, eValue(default,file-name.ini,2.ini,3.ini,4.ini,5.ini,6.ini,7.ini,image_name.ini,image header.ini)",
        ", num, 1, eValue(300)",
        "eBlockEnd",
        ", float, 1, eValue(-10.0e+3)",
        "eBlockEnd"
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

namespace YamlParserTest {
    class A {
    public:
        A() = default;
        A(int i, std::string s)
            : str(std::move(s))
            , num(i)
            {}
        bool operator==( const A& rhs ) const
            { return str==rhs.str && num==rhs.num; }
    private:
        std::string str;
        int num = 0;
        rOOPS_ADD_PROPERTY_INTERFACE_NON_POLYMORPH(YamlParserTest::A)
        {
            rOOPS_PROPERTY(str);
            rOOPS_PROPERTY(num);
        }
    };
    class B : public A {
    public:
        B() = default;
        B(int i, std::string s)
            : A(i, std::move(s))
            , chr(static_cast<char>('A' + (i % 20)))
            {}
        bool operator==( const B& rhs ) const
            { return A::operator==(rhs) && chr==rhs.chr; }
    private:
        char chr = 'A';
        rOOPS_ADD_PROPERTY_INTERFACE_NON_POLYMORPH(YamlParserTest::B)
        {
            rOOPS_INHERIT(YamlParserTest::A);
            rOOPS_PROPERTY(chr);
        }
    };
}

TEST(YamlParserTest, LoadWithoutTypeName)
{
    std::string TestStream =
        "B:\n"
        "    YamlParserTest::A :\n"
        "        str: \"xyz\"\n"
        "        num: 1\n"
        "    chr: 'B'\n";
    std::stringstream strm(cStringStreamMode);
    strm << TestStream;
    rOops::rOopsYamlParser parser(strm, "B");
    YamlParserTest::B l;
    load(parser, l);
    YamlParserTest::B r(1, "xyz");
    EXPECT_EQ(r, l);
}

TEST(YamlParserTest, Classes)
{
    std::string TestStream =
        "B: !sTut_02a::Internal::B\n"
        "  sTut_02a::Internal::A : !sTut_02a::Internal::A\n"
        "    _S32 : -11\n"
        "    _S16 : -22\n"
        "    # end of block comment\n"
        "  enabled : true\n"
        "  _F4 : 3.666667\n"
        "  _F8 : 7.333333\n";
    RefDataVector TestTokens = {
        "sTut_02a::Internal::B, B, 1, eBlockBegin",
        "sTut_02a::Internal::A, sTut_02a::Internal::A, 1, eBlockBegin",
        ", _S32, 1, eValue(-11)",
        ", _S16, 1, eValue(-22)",
        "eBlockEnd",
        ", enabled, 1, eValue(true)",
        ", _F4, 1, eValue(3.666667)",
        ", _F8, 1, eValue(7.333333)",
        "eBlockEnd",
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, ListAndPointers)
{
    std::string TestStream =
        "stl: !sTut_06a::STL\n"
        "  StringDeque[1] : !sTut_06a::StringDeque_t\n"
        "    - aaa\n"
        "  IntList[2] : !sTut_06a::IntList_t\n"
        "    - -90\n"
        "    - -120\n"
        "  IntPtrList[2] : !sTut_06a::IntPtrList_t\n"
        "    - *2679155255712\n"
        "    - *2679155255520\n"
        "\"*2679155255712\" : !int32_t 0\n"
        "\"*2679155255520\" : !int32_t 300\n";
    RefDataVector TestTokens = {
        "sTut_06a::STL, stl, 1, eBlockBegin",
        "sTut_06a::StringDeque_t, StringDeque, 1, eListBegin",
        ", , 1, eValue(aaa)",
        "eListEnd",
        "sTut_06a::IntList_t, IntList, 2, eListBegin",
        ", , 1, eValue(-90)",
        ", , 1, eValue(-120)",
        "eListEnd",
        "sTut_06a::IntPtrList_t, IntPtrList, 2, eListBegin",
        ", , 1, ePointer(&2679155255712)",
        ", , 1, ePointer(&2679155255520)",
        "eListEnd",
        "eBlockEnd",
        "int32_t, *2679155255712, 1, eValue(0)",
        "int32_t, *2679155255520, 1, eValue(300)",
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, Pointers)
{
    std::string TestStream =
        "TestDisplayModule: !PointerTestClass\n"
        "  rawPtr: *obj_1\n"
        "  uniquePtr: *obj_2\n"
        "  sharedPtr: *obj_3\n"
        "  type: Derived1\n"
        "\"*obj_1\": !DerivedClass1\n"
        "  BaseClass: !<BaseClass>\n"
        "    version: 0\n"
        "  title: Test derived 1.\n"
        "\"*obj_2\": !DerivedClass1\n"
        "  BaseClass: !<BaseClass>\n"
        "    version: 0\n"
        "  title: Test derived 2.\n"
        "\"*obj_3\": !DerivedClass1\n"
        "  BaseClass: !<BaseClass>\n"
        "    version: 0\n"
        "  title: Test derived 3.\n";
    RefDataVector TestTokens = {
        "PointerTestClass, TestDisplayModule, 1, eBlockBegin",
        ", rawPtr, 1, ePointer(&obj_1)",
        ", uniquePtr, 1, ePointer(&obj_2)",
        ", sharedPtr, 1, ePointer(&obj_3)",
        ", type, 1, eValue(Derived1)",
        "eBlockEnd",
        "DerivedClass1, *obj_1, 1, eBlockBegin",
        "BaseClass, BaseClass, 1, eBlockBegin",
        ", version, 1, eValue(0)",
        "eBlockEnd",
        ", title, 1, eValue(Test derived 1.)",
        "eBlockEnd",
        "DerivedClass1, *obj_2, 1, eBlockBegin",
        "BaseClass, BaseClass, 1, eBlockBegin",
        ", version, 1, eValue(0)",
        "eBlockEnd",
        ", title, 1, eValue(Test derived 2.)",
        "eBlockEnd",
        "DerivedClass1, *obj_3, 1, eBlockBegin",
        "BaseClass, BaseClass, 1, eBlockBegin",
        ", version, 1, eValue(0)",
        "eBlockEnd",
        ", title, 1, eValue(Test derived 3.)",
        "eBlockEnd",
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, SequenceOfMappings1)
{
    std::string TestStream =
        "module_config: !aim::config::PEGModuleConfig\n"
        "      type: type\n"
        "      id: id\n"
        "      inputs[2]: !aim::config::InputPortConfigVector\n"
        "        - id: id1\n"
        "          enabled: true\n"
        "          count: 1\n"
        "        - id: id2\n"
        "          enabled: false\n"
        "          count: 2\n";
    RefDataVector TestTokens = {
        "aim::config::PEGModuleConfig, module_config, 1, eBlockBegin",
        ", type, 1, eValue(type)",
        ", id, 1, eValue(id)",
        "aim::config::InputPortConfigVector, inputs, 2, eListBegin",
        ", , 1, eBlockBegin",
        ", id, 1, eValue(id1)",
        ", enabled, 1, eValue(true)",
        ", count, 1, eValue(1)",
        "eBlockEnd",
        ", , 1, eBlockBegin",
        ", id, 1, eValue(id2)",
        ", enabled, 1, eValue(false)",
        ", count, 1, eValue(2)",
        "eBlockEnd",
        "eListEnd",
        "eBlockEnd",
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, SequenceOfMappings2)
{
    std::string TestStream =
        "module_config: !aim::config::PEGModuleConfig\n"
        "      type: type\n"
        "      deadline: 100000000000\n"
        "      inputs[2]: !aim::config::InputPortConfigVector\n"
        "        -\n"
        "          producerModule: SrcId\n"
        "          producerPort: SrcPort\n"
        "          count: 1\n"
        "        -\n"
        "          producerModule: SrcId\n"
        "          producerPort: SrcPort\n"
        "          count: 2\n";
    RefDataVector TestTokens = {
        "aim::config::PEGModuleConfig, module_config, 1, eBlockBegin",
        ", type, 1, eValue(type)",
        ", deadline, 1, eValue(100000000000)",
        "aim::config::InputPortConfigVector, inputs, 2, eListBegin",
        ", , 1, eBlockBegin",
        ", producerModule, 1, eValue(SrcId)",
        ", producerPort, 1, eValue(SrcPort)",
        ", count, 1, eValue(1)",
        "eBlockEnd",
        ", , 1, eBlockBegin",
        ", producerModule, 1, eValue(SrcId)",
        ", producerPort, 1, eValue(SrcPort)",
        ", count, 1, eValue(2)",
        "eBlockEnd",
        "eListEnd",
        "eBlockEnd",
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, SequenceOfMappings3)
{
    std::string TestStream =
        "module_config: !aim::config::PEGModuleConfig\n"
        "      type: type\n"
        "      id: id\n"
        "      enabled: true\n"
        "      deadline: 100000000000\n"
        "      inputs[2]: !aim::config::InputPortConfigVector\n"
        "        - !aim::config::InputPortConfig\n"
        "          id: id1\n"
        "          enabled: true\n"
        "          producerModule: SrcId\n"
        "          producerPort: SrcPort\n"
        "          count: 1\n"
        "        - !aim::config::InputPortConfig\n"
        "          id: id2\n"
        "          enabled: true\n"
        "          producerModule: SrcId\n"
        "          producerPort: SrcPort\n"
        "          count: 2\n";
    RefDataVector TestTokens = {
        "aim::config::PEGModuleConfig, module_config, 1, eBlockBegin",
        ", type, 1, eValue(type)",
        ", id, 1, eValue(id)",
        ", enabled, 1, eValue(true)",
        ", deadline, 1, eValue(100000000000)",
        "aim::config::InputPortConfigVector, inputs, 2, eListBegin",
        "aim::config::InputPortConfig, , 1, eBlockBegin",
        ", id, 1, eValue(id1)",
        ", enabled, 1, eValue(true)",
        ", producerModule, 1, eValue(SrcId)",
        ", producerPort, 1, eValue(SrcPort)",
        ", count, 1, eValue(1)",
        "eBlockEnd",
        "aim::config::InputPortConfig, , 1, eBlockBegin",
        ", id, 1, eValue(id2)",
        ", enabled, 1, eValue(true)",
        ", producerModule, 1, eValue(SrcId)",
        ", producerPort, 1, eValue(SrcPort)",
        ", count, 1, eValue(2)",
        "eBlockEnd",
        "eListEnd",
        "eBlockEnd",
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, SequenceWithoutTabulation1)
{
    std::string TestStream =
        "module_config: !aim::config::PEGModuleConfig\n"
        "      type: type\n"
        "      inputs: !aim::config::InputPortConfigVector\n"
        "      - id: id1\n"
        "        enabled: true\n"
        "        count: 1\n"
        "      - id: id2\n"
        "        enabled: false\n"
        "        count: 2\n";
    RefDataVector TestTokens = {
        "aim::config::PEGModuleConfig, module_config, 1, eBlockBegin",
        ", type, 1, eValue(type)",
        "aim::config::InputPortConfigVector, inputs, 1, eListBegin",
        ", , 1, eBlockBegin",
        ", id, 1, eValue(id1)",
        ", enabled, 1, eValue(true)",
        ", count, 1, eValue(1)",
        "eBlockEnd",
        ", , 1, eBlockBegin",
        ", id, 1, eValue(id2)",
        ", enabled, 1, eValue(false)",
        ", count, 1, eValue(2)",
        "eBlockEnd",
        "eListEnd",
        "eBlockEnd",
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, SequenceWithoutTabulation2)
{
    std::string TestStream =
        "module_config: !aim::config::PEGModuleConfig\n"
        "   unsigneds:\n"
        "   - 1\n"
        "   - 2\n"
        "   ints:\n"
        "   - -3\n"
        "   - -4\n"
        "   floats:\n"
        "     - 1.1\n"
        "     - 2.2\n"
        "   doubles:\n"
        "   - 3.3\n"
        "   - 4.4\n"
        "\n";
    RefDataVector TestTokens = {
        "aim::config::PEGModuleConfig, module_config, 1, eBlockBegin",
        ", unsigneds, 1, eListBegin",
        ", , 1, eValue(1)",
        ", , 1, eValue(2)",
        "eListEnd",
        ", ints, 1, eListBegin",
        ", , 1, eValue(-3)",
        ", , 1, eValue(-4)",
        "eListEnd",
        ", floats, 1, eListBegin",
        ", , 1, eValue(1.1)",
        ", , 1, eValue(2.2)",
        "eListEnd",
        ", doubles, 1, eListBegin",
        ", , 1, eValue(3.3)",
        ", , 1, eValue(4.4)",
        "eListEnd",
        "eBlockEnd",
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, SequenceWithoutTabulation3)
{
    std::string TestStream =
        "GraphConfig: !GraphConfig\n"
        "  modules:\n"
        "    - id: canRecordModule\n"
        "      deadline: 10ms\n"
        "\n"
        "    - id: report\n"
        "      deadline: 45us\n"
        "      inputs:\n"
        "      - id: KeyEvent\n"
        "        producerPort: KeyEvent\n"
        "        count: 8\n"
        "      - id: DbwEventData\n"
        "        producerPort: DbwEventData\n"
        "      config: !ReportConfig\n"
        "        title: Report\n"
        "        # ffmpeg compiled with `nvenc` must be in the PATH\n"
        "        ffmpeg_encodeopts_win: -y -f gdigrab -framerate 15 -offset_x 0 -offset_y 0 -video_size 1920x1080 -i desktop -c:v h264_nvenc -qp 30 @outfile@\n"
        "        diskFullCheckTimeout: 5\n"
        "\n";
    RefDataVector TestTokens = {
        "GraphConfig, GraphConfig, 1, eBlockBegin",
            ", modules, 1, eListBegin",
                ", , 1, eBlockBegin",
                    ", id, 1, eValue(canRecordModule)",
                    ", deadline, 1, eValue(10ms)",
                "eBlockEnd",
                ", , 1, eBlockBegin",
                ", id, 1, eValue(report)",
                ", deadline, 1, eValue(45us)",
                ", inputs, 1, eListBegin",
                    ", , 1, eBlockBegin",
                        ", id, 1, eValue(KeyEvent)",
                        ", producerPort, 1, eValue(KeyEvent)",
                        ", count, 1, eValue(8)",
                    "eBlockEnd",
                    ", , 1, eBlockBegin",
                        ", id, 1, eValue(DbwEventData)",
                        ", producerPort, 1, eValue(DbwEventData)",
                    "eBlockEnd",
                "eListEnd",
                "ReportConfig, config, 1, eBlockBegin",
                    ", title, 1, eValue(Report)",
                    ", ffmpeg_encodeopts_win, 1, eValue(-y -f gdigrab -framerate 15 -offset_x 0 -offset_y 0 -video_size 1920x1080 -i desktop -c:v h264_nvenc -qp 30 @outfile@)",
                    ", diskFullCheckTimeout, 1, eValue(5)",
                "eBlockEnd",
                "eBlockEnd",
            "eListEnd",
        "eBlockEnd",
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}
TEST(YamlParserTest, SequenceWithEmptyValues)
{
    std::string TestStream =
        "session_config: !SessionInfo\n"
        "  cameras: !std::set<aim::session::CameraSensorRecordSessionInfo>\n"
        "  - !CameraSensorRecordSessionInfo\n"
        "    metafile:\n"
        "  modified_session: true\n"
        "\n";
    RefDataVector TestTokens = {
        "SessionInfo, session_config, 1, eBlockBegin",
        "std::set<aim::session::CameraSensorRecordSessionInfo>, cameras, 1, eListBegin",
        "CameraSensorRecordSessionInfo, , 1, eBlockBegin",
        ", metafile, 1, eValue",
        "eBlockEnd",
        "eListEnd",
        ", modified_session, 1, eValue(true)",
        "eBlockEnd",
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}


TEST(YamlParserTest, SequencesWithoutTabulation1)
{
    std::string TestStream =
        "GraphConfig: !GraphConfig\n"
        "modules :\n"
        "- id : bbox_trafficlight\n"
        "  inputs :\n"
        "  - id : F_STEREO_L\n"
        "    producerPort : left_image\n"
        "  - id : F_NARROW\n"
        "    producerPort : F_NARROW\n"
        "  outputs :\n"
        "  - id : F_STEREO_L_BBOX\n"
        "    enabled : true\n"
        "  - id : F_NARROW_BBOX\n"
        "    enabled : true\n"
        "  config : !NeuralModuleConfig\n"
        "    networkFile : KratosLight\n"
        "    box_score_threshold : 0.0\n"
        "\n";
    RefDataVector TestTokens = {
        "GraphConfig, GraphConfig, 1, eValue",
        ", modules, 1, eListBegin",
        ", , 1, eBlockBegin",
        ", id, 1, eValue(bbox_trafficlight)",
        ", inputs, 1, eListBegin",
        ", , 1, eBlockBegin",
        ", id, 1, eValue(F_STEREO_L)",
        ", producerPort, 1, eValue(left_image)",
        "eBlockEnd",
        ", , 1, eBlockBegin",
        ", id, 1, eValue(F_NARROW)",
        ", producerPort, 1, eValue(F_NARROW)",
        "eBlockEnd",
        "eListEnd",
        ", outputs, 1, eListBegin",
        ", , 1, eBlockBegin",
        ", id, 1, eValue(F_STEREO_L_BBOX)",
        ", enabled, 1, eValue(true)",
        "eBlockEnd",
        ", , 1, eBlockBegin",
        ", id, 1, eValue(F_NARROW_BBOX)",
        ", enabled, 1, eValue(true)",
        "eBlockEnd",
        "eListEnd",
        "NeuralModuleConfig, config, 1, eBlockBegin",
        ", networkFile, 1, eValue(KratosLight)",
        ", box_score_threshold, 1, eValue(0.0)",
        "eBlockEnd",
        "eBlockEnd",
        "eListEnd",
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, SequencesWithoutTabulation2)
{
    std::string TestStream =
        "session: !SessionInfo\n"
        "    modules:  # end of line comment ...\n"
        "    - type: VehicleDataModule\n"
        "      id: vehicleDataModule\n"
        "      ports:\n"
        "      - count: 1\n"
        "        id: Info\n"
        "      - count: 2\n"
        "        id: Event\n"
        "      - count: 3\n"
        "        id: Stat\n"
        "    modified_session: true\n"
        "\n";
    RefDataVector TestTokens = {
        "SessionInfo, session, 1, eBlockBegin",
            ", modules, 1, eListBegin",
                ", , 1, eBlockBegin",
                    ", type, 1, eValue(VehicleDataModule)",
                    ", id, 1, eValue(vehicleDataModule)",
                    ", ports, 1, eListBegin",
                        ", , 1, eBlockBegin",
                            ", count, 1, eValue(1)",
                            ", id, 1, eValue(Info)",
                        "eBlockEnd",
                        ", , 1, eBlockBegin",
                            ", count, 1, eValue(2)",
                            ", id, 1, eValue(Event)",
                        "eBlockEnd",
                        ", , 1, eBlockBegin",
                            ", count, 1, eValue(3)",
                            ", id, 1, eValue(Stat)",
                        "eBlockEnd",
                    "eListEnd",  // ports
                "eBlockEnd",
            "eListEnd",  // modules
            ", modified_session, 1, eValue(true)",
        "eBlockEnd",
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, IndentedFirstLine)
{
    std::string TestStream =
        "    session: !SessionInfo\n"
        "        modified_session: true\n"
        "    \n";
    RefDataVector TestTokens = {
        "SessionInfo, session, 1, eBlockBegin",
            ", modified_session, 1, eValue(true)",
        "eBlockEnd",
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

TEST(YamlParserTest, EmptyFirstLineIndentedSecondLine)
{
    std::string TestStream =
        "  \n"
        "    session: !SessionInfo\n"
        "        modified_session: true\n"
        "    \n";
    RefDataVector TestTokens = {
        "SessionInfo, session, 1, eBlockBegin",
            ", modified_session, 1, eValue(true)",
        "eBlockEnd",
    };
    testParser<rOops::rOopsYamlParser>(TestStream, TestTokens);
}

#include <oops/rPropInterface.h>
#include <oops/rOopsSaveLoad.h>

namespace aim {
namespace config {
    class SensorParameters
    {
    public:
        std::string         label;
        std::string         device_id;
        std::string         product_name;
        double              misalign_ref_angle_error = -1.0;
        std::string         misalign_ref_file;
        std::string         misalign_mask_file;
        std::string         obstruction_mask_file;
        std::string         native_config_path;
        std::string         uri;
        // Nvmedia camera specific custom_vars
        std::string csi_group_type;               // NvSIPL platform config identifier. Used to query available driver plugins for each sensor group on the board
        std::string csi_group_isp_settings_path;  // Nvmedia specific ISP configuration file path, relative to the application working directory
        std::string csi_link_id;                  // Camera link index inside a deserializer group: A[0-3] B[4-7] C[8-11] D [12-15]
        rOOPS_ADD_PROPERTY_INTERFACE_NON_POLYMORPH(SensorParameters)
        {
            rOOPS_PROPERTY(label);
            rOOPS_PROPERTY(device_id);
            rOOPS_PROPERTY(product_name);
            rOOPS_PROPERTY(misalign_ref_angle_error);
            rOOPS_PROPERTY(misalign_ref_file);
            rOOPS_PROPERTY(misalign_mask_file);
            rOOPS_PROPERTY(obstruction_mask_file);
            rOOPS_PROPERTY(native_config_path);
            rOOPS_PROPERTY(uri);
            rOOPS_PROPERTY(csi_group_type);
            rOOPS_PROPERTY(csi_group_isp_settings_path);
            rOOPS_PROPERTY(csi_link_id);
        }
    };

}
}

rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::vector<std::unique_ptr<aim::config::SensorParameters>>)

namespace aim {
namespace config {
    class SensorConfig
    {
    public:
        SensorConfig() = default;
        using SensorContainer = std::vector<std::unique_ptr<SensorParameters>>;
        SensorContainer sensors_;
        std::string dbwDeviceHint_;
        void validate(std::uint8_t)
        {
        }
        rOOPS_ADD_PROPERTY_INTERFACE_NON_POLYMORPH(SensorConfig)
        {
            rOOPS_PROPERTY(sensors_), "sensors";
            rOOPS_PROPERTY(dbwDeviceHint_), "dbwDeviceHint";
            rOOPS_VALIDATE(&SensorConfig::validate);
        }
    };
}
}

TEST(YamlParserTest, NonOopsYamlFileSegfault)
{
    std::string test_strm =
        "sensors:\n"
        "  - label: F_NARROW\n"
        "    device_id: UNLABELED\n"
        "    pos_meter: [1.955000042915344, -0.1500000059604645, 1.275851011276245]\n"
        "    yaw_pitch_roll_deg: [0.2100000083446503, -2.396000146865845, -0.08199997246265411]\n"
        "    extrinsic_error: 0.0343693"
        "  - label: Sensorbox\n"
        "    device_id: sensorbox_dev_3\n"
        "include:\n"
        "  - ../../SENSORS/TE03748.yaml\n";
    try {
        std::stringstream strm(test_strm);
        aim::config::SensorConfig sensorConfig;
        rOops::rOopsYamlParser  yaml_parser(strm, "TestYamlParser");
        rOops::load(yaml_parser, sensorConfig);
        FAIL() << "No exception has thrown.";
    }
    catch(const std::runtime_error& arEx) {
        EXPECT_STREQ("TestYamlParser(2): rPropTypeInfoCompoundTypeBase::load() is called when parser context is not block.", arEx.what());
    }
    catch(...) {
        FAIL() << "Unexpected exception caught.";
    }
}
