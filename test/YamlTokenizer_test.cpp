/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2021:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rOopsYamlTokenizer.h>
#include <gtest/gtest.h>

//#include <iostream>
#include <sstream>
#include <tuple>
#include <vector>

using RefData = std::tuple<std::string, char, std::string, std::size_t>;
using RefDataVector = std::vector<RefData>;

void TestYamlTokenizer(const std::string& arInput, const RefDataVector& arRefData, bool aPrint=false, const rOops::PreprocessorDefines_t& arPreprocessorDefines={})
{
    std::stringstream strm{ arInput };
    rOops::rOopsYamlTokenizer tokenizer(strm, "TestYamlTokenizer", arPreprocessorDefines);
    rOops::YamlToken tkn('x');
    for (const auto& ref : arRefData) {
        if ('\0' == tkn._token) {
            FAIL() << "Unexpected end of token stream.";
            break;
        }
        tkn = tokenizer.getNextToken();
        if (aPrint) {
            std::cout << std::get<0>(ref) << std::setw(3) << std::get<3>(ref) << ": " << rOops::escaping(std::get<1>(ref)) << " " << std::get<2>(ref) << "\t| ";
            std::cout << to_string(tkn._status) << std::setw(3) << tkn._lineNr << ": " << rOops::escaping(tkn._token) << " " << tkn._str << std::endl;
        }
        EXPECT_EQ(std::get<0>(ref), to_string(tkn._status)) << "At line: " << tkn._lineNr << ": " << rOops::escaping(tkn._token) << " " << tkn._str;
        EXPECT_EQ(std::get<1>(ref), tkn._token)  << "At line: " << tkn._lineNr << ": " << rOops::escaping(tkn._token) << " " << tkn._str;
        EXPECT_EQ(std::get<2>(ref), tkn._str)    << "At line: " << tkn._lineNr << ": " << rOops::escaping(tkn._token) << " " << tkn._str;
        EXPECT_EQ(std::get<3>(ref), tkn._lineNr) << "At line: " << tkn._lineNr << ": " << rOops::escaping(tkn._token) << " " << tkn._str;
    } //for
}

std::size_t PrintYamlTokens(const std::string& arInput, bool aPrint=false, const rOops::PreprocessorDefines_t& arPreprocessorDefines={})
{
    std::stringstream strm{ arInput };
    rOops::rOopsYamlTokenizer tokenizer(strm, "PrintYamlTokens", arPreprocessorDefines);
    rOops::YamlToken tkn('x');
    std::size_t token_cntr = 0;
    while ('\0' != tkn._token) {
        tkn = tokenizer.getNextToken();
        if (aPrint) {
            std::cout << "RefData( \"" << rOops::to_string(tkn._status) << "\", '" << rOops::escaping(tkn._token) << "', \"" << tkn._str << "\", " << tkn._lineNr << " ),\n";
        }
        ++token_cntr;
    } //while
    return token_cntr;
}

TEST(YamlTokenizerTest, EmptyValues)
{
    std::string TestStream =
        " # TestStream1:\n"
        "B: !<sTut_01a::B>\n"
        "    sTut_01a::A: !sTut_01a::A\n"
        "        chr: \n"
        "        str: \n"
        "        num: \n"
        "    float: -10.0e+3\n";
    RefDataVector TestTokens = {
        // RefData("Base", '#', " TestStream1:", 1),
        RefData("Base", '\n', "", 2),
        RefData("Base", 'S', "B", 2),
        RefData("Base", ':', "", 2),
        RefData("Val_", '!', "<sTut_01a::B>", 2),
        RefData("Val_", '\n', "    ", 3),
        RefData("Val_", 'S', "sTut_01a::A", 3),
        RefData("Base", ':', "", 3),
        RefData("Val_", '!', "sTut_01a::A", 3),
        RefData("Val_", '\n', "        ", 4),
        RefData("Val_", 'S', "chr", 4),
        RefData("Base", ':', "", 4),
        //RefData("Val_", '\'', "C", 4),
        RefData("Val_", '\n', "        ", 5),
        RefData("Val_", 'S', "str", 5),
        RefData("Base", ':', "", 5),
        //RefData("Val_", 'S', "default,file-name.ini,2.ini,3.ini,4.ini,5.ini,6.ini,7.ini,image_name.ini,image header.ini", 5),
        RefData("Val_", '\n', "        ", 6),
        RefData("Val_", 'S', "num", 6),
        RefData("Base", ':', "", 6),
        //RefData("Val_", 'S', "300", 6),
        RefData("Val_", '\n', "    ", 7),
        RefData("Val_", 'S', "float", 7),
        RefData("Base", ':', "", 7),
        RefData("Val_", 'S', "-10.0e+3", 7),
        RefData("Base", '\n', "", 8),
        RefData("Base", '\0', "", 8),
    };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, MultiLineQuotedString)
{
    std::string TestStream =
        "B: !<sTut_01a::B>\n"
        "    sTut_01a::A: !sTut_01a::A\n"
        "        str1: 'line3\nline4'\n"
        "        str2: \"line5 \n    line6 \n      line7\"\n";
    RefDataVector TestTokens = {
        RefData("Base", 'S', "B", 1),
        RefData("Base", ':', "", 1),
        RefData("Val_", '!', "<sTut_01a::B>", 1),
        RefData("Val_", '\n', "    ", 2),
        RefData("Val_", 'S', "sTut_01a::A", 2),
        RefData("Base", ':', "", 2),
        RefData("Val_", '!', "sTut_01a::A", 2),
        RefData("Val_", '\n', "        ", 3),
        RefData("Val_", 'S', "str1", 3),
        RefData("Base", ':', "", 3),
        RefData("Val_", '\'', "line3\nline4", 3),
        RefData("Base", '\n', "        ", 5),
        RefData("Base", 'S', "str2", 5),
        RefData("Base", ':', "", 5),
        RefData("Val_", '"', "line5 \n    line6 \n      line7", 5),
        RefData( "Base", '\n', "", 8 ),
        RefData("Base", '\0', "", 8),
    };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream, false);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, Structure)
{
    std::string TestStream =
        " # TestStream1:\n"
        "B: !<sTut_01a::B>\n"
        "    sTut_01a::A: !sTut_01a::A\n"
        "        chr: 'C'\n"
        "        str: default,file-name.ini,2.ini,3.ini,4.ini,5.ini,6.ini,7.ini,image_name.ini,image header.ini\n"
        "        num: 300\n"
        "    float: -10.0e+3\n";
    RefDataVector TestTokens = {
        // RefData( "Base", '#', " TestStream1:", 1 ),
        RefData( "Base", '\n', "", 2 ),
        RefData( "Base", 'S', "B", 2 ),
        RefData( "Base", ':', "", 2 ),
        RefData( "Val_", '!', "<sTut_01a::B>", 2 ),
        RefData( "Val_", '\n', "    ", 3 ),
        RefData( "Val_", 'S', "sTut_01a::A", 3 ),
        RefData( "Base", ':', "", 3 ),
        RefData( "Val_", '!', "sTut_01a::A", 3 ),
        RefData( "Val_", '\n', "        ", 4 ),
        RefData( "Val_", 'S', "chr", 4 ),
        RefData( "Base", ':', "", 4 ),
        RefData( "Val_", '\'', "C", 4 ),
        RefData( "Base", '\n', "        ", 5 ),
        RefData( "Base", 'S', "str", 5 ),
        RefData( "Base", ':', "", 5 ),
        RefData( "Val_", 'S', "default,file-name.ini,2.ini,3.ini,4.ini,5.ini,6.ini,7.ini,image_name.ini,image header.ini", 5 ),
        RefData( "Base", '\n', "        ", 6 ),
        RefData( "Base", 'S', "num", 6 ),
        RefData( "Base", ':', "", 6 ),
        RefData( "Val_", 'S', "300", 6 ),
        RefData( "Base", '\n', "    ", 7 ),
        RefData( "Base", 'S', "float", 7 ),
        RefData( "Base", ':', "", 7 ),
        RefData( "Val_", 'S', "-10.0e+3", 7 ),
        RefData( "Base", '\n', "", 8 ),
        RefData( "Base", '\0', "", 8 ),
    };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, Classes)
{
    std::string TestStream =
        "B: !sTut_02a::Internal::B\n"
        "  sTut_02a::Internal::A : !sTut_02a::Internal::A\n"
        "    _S32 : -11\n"
        "    _S16 : -22\n"
        "  enabled : true\n"
        "  _F4 : 3.666667\n"
        "  _F8 : 7.333333\n";
    RefDataVector TestTokens = {
        RefData( "Base", 'S', "B", 1 ),
        RefData( "Base", ':', "", 1 ),
        RefData( "Val_", '!', "sTut_02a::Internal::B", 1 ),
        RefData( "Val_", '\n', "  ", 2 ),
        RefData( "Val_", 'S', "sTut_02a::Internal::A", 2 ),
        RefData( "Base", ':', "", 2 ),
        RefData( "Val_", '!', "sTut_02a::Internal::A", 2 ),
        RefData( "Val_", '\n', "    ", 3 ),
        RefData( "Val_", 'S', "_S32", 3 ),
        RefData( "Base", ':', "", 3 ),
        RefData( "Val_", 'S', "-11", 3 ),
        RefData( "Base", '\n', "    ", 4 ),
        RefData( "Base", 'S', "_S16", 4 ),
        RefData( "Base", ':', "", 4 ),
        RefData( "Val_", 'S', "-22", 4 ),
        RefData( "Base", '\n', "  ", 5 ),
        RefData( "Base", 'S', "enabled", 5 ),
        RefData( "Base", ':', "", 5 ),
        RefData( "Val_", 'S', "true", 5 ),
        RefData( "Base", '\n', "  ", 6 ),
        RefData( "Base", 'S', "_F4", 6 ),
        RefData( "Base", ':', "", 6 ),
        RefData( "Val_", 'S', "3.666667", 6 ),
        RefData( "Base", '\n', "  ", 7 ),
        RefData( "Base", 'S', "_F8", 7 ),
        RefData( "Base", ':', "", 7 ),
        RefData( "Val_", 'S', "7.333333", 7 ),
        RefData( "Base", '\n', "", 8 ),
        RefData( "Base", '\0', "", 8 ),
    };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, SequenceOfMappings1)
{
    std::string TestStream =
        "module_config: !aim::config::PEGModuleConfig\n"
        "      type: type\n"
        "      id: id\n"
        "      inputs[2]: !aim::config::InputPortConfigVector\n"
        "        - id: id1\n"
        "          enabled: true\n"
        "          count: 1\n"
        "        - id: id2\n"
        "          enabled: true\n"
        "          count: 2\n";
    RefDataVector TestTokens = {
        RefData( "Base", 'S', "module_config", 1 ),
        RefData( "Base", ':', "", 1 ),
        RefData( "Val_", '!', "aim::config::PEGModuleConfig", 1 ),
        RefData( "Val_", '\n', "      ", 2 ),
        RefData( "Val_", 'S', "type", 2 ),
        RefData( "Base", ':', "", 2 ),
        RefData( "Val_", 'S', "type", 2 ),
        RefData( "Base", '\n', "      ", 3 ),
        RefData( "Base", 'S', "id", 3 ),
        RefData( "Base", ':', "", 3 ),
        RefData( "Val_", 'S', "id", 3 ),
        RefData( "Base", '\n', "      ", 4 ),
        RefData( "Base", 'S', "inputs", 4 ),
        RefData( "Base", '[', "", 4 ),
        RefData( "Base", 'S', "2", 4 ),
        RefData( "Base", ']', "", 4 ),
        RefData( "Base", ':', "", 4 ),
        RefData( "Val_", '!', "aim::config::InputPortConfigVector", 4 ),
        RefData( "Val_", '\n', "        ", 5 ),
        RefData( "Val_", '-', "", 5 ),
        RefData( "Val_", 'S', "id", 5 ),
        RefData( "Base", ':', "", 5 ),
        RefData( "Val_", 'S', "id1", 5 ),
        RefData( "Base", '\n', "          ", 6 ),
        RefData( "Base", 'S', "enabled", 6 ),
        RefData( "Base", ':', "", 6 ),
        RefData( "Val_", 'S', "true", 6 ),
        RefData( "Base", '\n', "          ", 7 ),
        RefData( "Base", 'S', "count", 7 ),
        RefData( "Base", ':', "", 7 ),
        RefData( "Val_", 'S', "1", 7 ),
        RefData( "Base", '\n', "        ", 8 ),
        RefData( "Base", '-', "", 8 ),
        RefData( "Val_", 'S', "id", 8 ),
        RefData( "Base", ':', "", 8 ),
        RefData( "Val_", 'S', "id2", 8 ),
        RefData( "Base", '\n', "          ", 9 ),
        RefData( "Base", 'S', "enabled", 9 ),
        RefData( "Base", ':', "", 9 ),
        RefData( "Val_", 'S', "true", 9 ),
        RefData( "Base", '\n', "          ", 10 ),
        RefData( "Base", 'S', "count", 10 ),
        RefData( "Base", ':', "", 10 ),
        RefData( "Val_", 'S', "2", 10 ),
        RefData( "Base", '\n', "", 11 ),
        RefData( "Base", '\0', "", 11 ),
    };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, SequenceOfMappings2)
{
    std::string TestStream =
        "module_config: !aim::config::PEGModuleConfig\n"
        "      type: type\n"
        "      deadline: 100000000000\n"
        "      inputs[2]: !aim::config::InputPortConfigVector\n"
        "        -\n"
        "          producerModule: SrcId\n"
        "          producerPort: SrcPort\n"
        "          count: 1\n"
        "        -\n"
        "          producerModule: SrcId\n"
        "          producerPort: SrcPort\n"
        "          count: 2\n";
    RefDataVector TestTokens = {
        RefData( "Base", 'S', "module_config", 1 ),
        RefData( "Base", ':', "", 1 ),
        RefData( "Val_", '!', "aim::config::PEGModuleConfig", 1 ),
        RefData( "Val_", '\n', "      ", 2 ),
        RefData( "Val_", 'S', "type", 2 ),
        RefData( "Base", ':', "", 2 ),
        RefData( "Val_", 'S', "type", 2 ),
        RefData( "Base", '\n', "      ", 3 ),
        RefData( "Base", 'S', "deadline", 3 ),
        RefData( "Base", ':', "", 3 ),
        RefData( "Val_", 'S', "100000000000", 3 ),
        RefData( "Base", '\n', "      ", 4 ),
        RefData( "Base", 'S', "inputs", 4 ),
        RefData( "Base", '[', "", 4 ),
        RefData( "Base", 'S', "2", 4 ),
        RefData( "Base", ']', "", 4 ),
        RefData( "Base", ':', "", 4 ),
        RefData( "Val_", '!', "aim::config::InputPortConfigVector", 4 ),
        RefData( "Val_", '\n', "        ", 5 ),
        RefData( "Val_", '-', "", 5 ),
        RefData( "Val_", '\n', "          ", 6 ),
        RefData( "Val_", 'S', "producerModule", 6 ),
        RefData( "Base", ':', "", 6 ),
        RefData( "Val_", 'S', "SrcId", 6 ),
        RefData( "Base", '\n', "          ", 7 ),
        RefData( "Base", 'S', "producerPort", 7 ),
        RefData( "Base", ':', "", 7 ),
        RefData( "Val_", 'S', "SrcPort", 7 ),
        RefData( "Base", '\n', "          ", 8 ),
        RefData( "Base", 'S', "count", 8 ),
        RefData( "Base", ':', "", 8 ),
        RefData( "Val_", 'S', "1", 8 ),
        RefData( "Base", '\n', "        ", 9 ),
        RefData( "Base", '-', "", 9 ),
        RefData( "Val_", '\n', "          ", 10 ),
        RefData( "Val_", 'S', "producerModule", 10 ),
        RefData( "Base", ':', "", 10 ),
        RefData( "Val_", 'S', "SrcId", 10 ),
        RefData( "Base", '\n', "          ", 11 ),
        RefData( "Base", 'S', "producerPort", 11 ),
        RefData( "Base", ':', "", 11 ),
        RefData( "Val_", 'S', "SrcPort", 11 ),
        RefData( "Base", '\n', "          ", 12 ),
        RefData( "Base", 'S', "count", 12 ),
        RefData( "Base", ':', "", 12 ),
        RefData( "Val_", 'S', "2", 12 ),
        RefData( "Base", '\n', "", 13 ),
        RefData( "Base", '\0', "", 13 ),
    };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, SequenceOfMappings3)
{
    std::string TestStream =
        "module_config: !aim::config::PEGModuleConfig\n"
        "      type: type\n"
        "      id: id\n"
        "      enabled: true\n"
        "      deadline: 100000000000\n"
        "      inputs[2]: !aim::config::InputPortConfigVector\n"
        "        - !aim::config::InputPortConfig\n"
        "          id: id1\n"
        "          enabled: true\n"
        "          producerModule: SrcId\n"
        "          producerPort: SrcPort\n"
        "          count: 1\n"
        "        - !aim::config::InputPortConfig\n"
        "          id: id2\n"
        "          enabled: true\n"
        "          producerModule: SrcId\n"
        "          producerPort: SrcPort\n"
        "          count: 2";  // Note! No new line at the end.
    RefDataVector TestTokens = {
        RefData( "Base", 'S', "module_config", 1 ),
        RefData( "Base", ':', "", 1 ),
        RefData( "Val_", '!', "aim::config::PEGModuleConfig", 1 ),
        RefData( "Val_", '\n', "      ", 2 ),
        RefData( "Val_", 'S', "type", 2 ),
        RefData( "Base", ':', "", 2 ),
        RefData( "Val_", 'S', "type", 2 ),
        RefData( "Base", '\n', "      ", 3 ),
        RefData( "Base", 'S', "id", 3 ),
        RefData( "Base", ':', "", 3 ),
        RefData( "Val_", 'S', "id", 3 ),
        RefData( "Base", '\n', "      ", 4 ),
        RefData( "Base", 'S', "enabled", 4 ),
        RefData( "Base", ':', "", 4 ),
        RefData( "Val_", 'S', "true", 4 ),
        RefData( "Base", '\n', "      ", 5 ),
        RefData( "Base", 'S', "deadline", 5 ),
        RefData( "Base", ':', "", 5 ),
        RefData( "Val_", 'S', "100000000000", 5 ),
        RefData( "Base", '\n', "      ", 6 ),
        RefData( "Base", 'S', "inputs", 6 ),
        RefData( "Base", '[', "", 6 ),
        RefData( "Base", 'S', "2", 6 ),
        RefData( "Base", ']', "", 6 ),
        RefData( "Base", ':', "", 6 ),
        RefData( "Val_", '!', "aim::config::InputPortConfigVector", 6 ),
        RefData( "Val_", '\n', "        ", 7 ),
        RefData( "Val_", '-', "", 7 ),
        RefData( "Val_", '!', "aim::config::InputPortConfig", 7 ),
        RefData( "Val_", '\n', "          ", 8 ),
        RefData( "Val_", 'S', "id", 8 ),
        RefData( "Base", ':', "", 8 ),
        RefData( "Val_", 'S', "id1", 8 ),
        RefData( "Base", '\n', "          ", 9 ),
        RefData( "Base", 'S', "enabled", 9 ),
        RefData( "Base", ':', "", 9 ),
        RefData( "Val_", 'S', "true", 9 ),
        RefData( "Base", '\n', "          ", 10 ),
        RefData( "Base", 'S', "producerModule", 10 ),
        RefData( "Base", ':', "", 10 ),
        RefData( "Val_", 'S', "SrcId", 10 ),
        RefData( "Base", '\n', "          ", 11 ),
        RefData( "Base", 'S', "producerPort", 11 ),
        RefData( "Base", ':', "", 11 ),
        RefData( "Val_", 'S', "SrcPort", 11 ),
        RefData( "Base", '\n', "          ", 12 ),
        RefData( "Base", 'S', "count", 12 ),
        RefData( "Base", ':', "", 12 ),
        RefData( "Val_", 'S', "1", 12 ),
        RefData( "Base", '\n', "        ", 13 ),
        RefData( "Base", '-', "", 13 ),
        RefData( "Val_", '!', "aim::config::InputPortConfig", 13 ),
        RefData( "Val_", '\n', "          ", 14 ),
        RefData( "Val_", 'S', "id", 14 ),
        RefData( "Base", ':', "", 14 ),
        RefData( "Val_", 'S', "id2", 14 ),
        RefData( "Base", '\n', "          ", 15 ),
        RefData( "Base", 'S', "enabled", 15 ),
        RefData( "Base", ':', "", 15 ),
        RefData( "Val_", 'S', "true", 15 ),
        RefData( "Base", '\n', "          ", 16 ),
        RefData( "Base", 'S', "producerModule", 16 ),
        RefData( "Base", ':', "", 16 ),
        RefData( "Val_", 'S', "SrcId", 16 ),
        RefData( "Base", '\n', "          ", 17 ),
        RefData( "Base", 'S', "producerPort", 17 ),
        RefData( "Base", ':', "", 17 ),
        RefData( "Val_", 'S', "SrcPort", 17 ),
        RefData( "Base", '\n', "          ", 18 ),
        RefData( "Base", 'S', "count", 18 ),
        RefData( "Base", ':', "", 18 ),
        RefData( "Val_", 'S', "2", 18 ),
        RefData( "Base", '\0', "", 18 ),
    };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, SequenceWithNoIndent)
{
    std::string TestStream =
        "module_config: !aim::config::PEGModuleConfig\n"
        "      type: type\n"
        "      id: id\n"
        "      deadline: 100000000000\n"
        "      inputs[2]: !aim::config::InputPortConfigVector\n"
        "      - id: id1\n"
        "        enabled: true\n"
        "      - id: id2\n"
        "        enabled: true\n";
    RefDataVector TestTokens = {
        RefData( "Base", 'S', "module_config", 1 ),
        RefData( "Base", ':', "", 1 ),
        RefData( "Val_", '!', "aim::config::PEGModuleConfig", 1 ),
        RefData( "Val_", '\n', "      ", 2 ),
        RefData( "Val_", 'S', "type", 2 ),
        RefData( "Base", ':', "", 2 ),
        RefData( "Val_", 'S', "type", 2 ),
        RefData( "Base", '\n', "      ", 3 ),
        RefData( "Base", 'S', "id", 3 ),
        RefData( "Base", ':', "", 3 ),
        RefData( "Val_", 'S', "id", 3 ),
        RefData( "Base", '\n', "      ", 4 ),
        RefData( "Base", 'S', "deadline", 4 ),
        RefData( "Base", ':', "", 4 ),
        RefData( "Val_", 'S', "100000000000", 4 ),
        RefData( "Base", '\n', "      ", 5 ),
        RefData( "Base", 'S', "inputs", 5 ),
        RefData( "Base", '[', "", 5 ),
        RefData( "Base", 'S', "2", 5 ),
        RefData( "Base", ']', "", 5 ),
        RefData( "Base", ':', "", 5 ),
        RefData( "Val_", '!', "aim::config::InputPortConfigVector", 5 ),
        RefData( "Val_", '\n', "      ", 6 ),
        RefData( "Val_", '-', "", 6 ),
        RefData( "Val_", 'S', "id", 6 ),
        RefData( "Base", ':', "", 6 ),
        RefData( "Val_", 'S', "id1", 6 ),
        RefData( "Base", '\n', "        ", 7 ),
        RefData( "Base", 'S', "enabled", 7 ),
        RefData( "Base", ':', "", 7 ),
        RefData( "Val_", 'S', "true", 7 ),
        RefData( "Base", '\n', "      ", 8 ),
        RefData( "Base", '-', "", 8 ),
        RefData( "Val_", 'S', "id", 8 ),
        RefData( "Base", ':', "", 8 ),
        RefData( "Val_", 'S', "id2", 8 ),
        RefData( "Base", '\n', "        ", 9 ),
        RefData( "Base", 'S', "enabled", 9 ),
        RefData( "Base", ':', "", 9 ),
        RefData( "Val_", 'S', "true", 9 ),
        RefData( "Base", '\n', "", 10 ),
        RefData( "Base", '\0', "", 10 ),
    };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, ListAndPointers)
{
    std::string TestStream =
        "stl: !sTut_06a::STL\n"
        "  StringDeque[1]: !<sTut_06a::StringDeque_t>\n"
        "    - aaa\n"
        "  IntList: !<sTut_06a::IntList_t>\n"
        "    - -90\n"
        "    - -120\n"
        "  IntPtrList: !<sTut_06a::IntPtrList_t>\n"
        "    - \"&2679155255712\"\n"
        "    - \"&2679155255520\"\n"
        "\"*2679155255712\": !<int32_t> 0\n"
        "\"*2679155255520\": !<int32_t> 300";  // No new line !!!
    RefDataVector TestTokens = {
        RefData( "Base", 'S', "stl", 1 ),
        RefData( "Base", ':', "", 1 ),
        RefData( "Val_", '!', "sTut_06a::STL", 1 ),
        RefData( "Val_", '\n', "  ", 2 ),
        RefData( "Val_", 'S', "StringDeque", 2 ),
        RefData( "Base", '[', "", 2 ),
        RefData( "Base", 'S', "1", 2 ),
        RefData( "Base", ']', "", 2 ),
        RefData( "Base", ':', "", 2 ),
        RefData( "Val_", '!', "<sTut_06a::StringDeque_t>", 2 ),
        RefData( "Val_", '\n', "    ", 3 ),
        RefData( "Val_", '-', "", 3 ),
        RefData( "Val_", 'S', "aaa", 3 ),
        RefData( "Base", '\n', "  ", 4 ),
        RefData( "Base", 'S', "IntList", 4 ),
        RefData( "Base", ':', "", 4 ),
        RefData( "Val_", '!', "<sTut_06a::IntList_t>", 4 ),
        RefData( "Val_", '\n', "    ", 5 ),
        RefData( "Val_", '-', "", 5 ),
        RefData( "Val_", 'S', "-90", 5 ),
        RefData( "Base", '\n', "    ", 6 ),
        RefData( "Base", '-', "", 6 ),
        RefData( "Val_", 'S', "-120", 6 ),
        RefData( "Base", '\n', "  ", 7 ),
        RefData( "Base", 'S', "IntPtrList", 7 ),
        RefData( "Base", ':', "", 7 ),
        RefData( "Val_", '!', "<sTut_06a::IntPtrList_t>", 7 ),
        RefData( "Val_", '\n', "    ", 8 ),
        RefData( "Val_", '-', "", 8 ),
        RefData( "Val_", '"', "&2679155255712", 8 ),
        RefData( "Base", '\n', "    ", 9 ),
        RefData( "Base", '-', "", 9 ),
        RefData( "Val_", '"', "&2679155255520", 9 ),
        RefData( "Base", '\n', "", 10 ),
        RefData( "Base", '"', "*2679155255712", 10 ),
        RefData( "Base", ':', "", 10 ),
        RefData( "Val_", '!', "<int32_t>", 10 ),
        RefData( "Val_", 'S', "0", 10 ),
        RefData( "Base", '\n', "", 11 ),
        RefData( "Base", '"', "*2679155255520", 11 ),
        RefData( "Base", ':', "", 11 ),
        RefData( "Val_", '!', "<int32_t>", 11 ),
        RefData( "Val_", 'S', "300", 11 ),
        RefData( "Base", '\0', "", 11 ),
    };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, ListAndPointers2)
{
    std::string TestStream =
        "stl: !sTut_06a::STL\n"
        "  IntPtrList: !<sTut_06a::IntPtrList_t>\n"
        "    - \"&2679155255712\"\n"
        "    - \"&2679155255520\"\n"
        "\"*2679155255712\": !<int32_t> 0\n"
        "\"*2679155255520\": !<int32_t> 300\n";
    RefDataVector TestTokens = {
        RefData( "Base", 'S', "stl", 1 ),
        RefData( "Base", ':', "", 1 ),
        RefData( "Val_", '!', "sTut_06a::STL", 1 ),
        RefData( "Val_", '\n', "  ", 2 ),
        RefData( "Val_", 'S', "IntPtrList", 2 ),
        RefData( "Base", ':', "", 2 ),
        RefData( "Val_", '!', "<sTut_06a::IntPtrList_t>", 2 ),
        RefData( "Val_", '\n', "    ", 3 ),
        RefData( "Val_", '-', "", 3 ),
        RefData( "Val_", '"', "&2679155255712", 3 ),
        RefData( "Base", '\n', "    ", 4 ),
        RefData( "Base", '-', "", 4 ),
        RefData( "Val_", '"', "&2679155255520", 4 ),
        RefData( "Base", '\n', "", 5 ),
        RefData( "Base", '"', "*2679155255712", 5 ),
        RefData( "Base", ':', "", 5 ),
        RefData( "Val_", '!', "<int32_t>", 5 ),
        RefData( "Val_", 'S', "0", 5 ),
        RefData( "Base", '\n', "", 6 ),
        RefData( "Base", '"', "*2679155255520", 6 ),
        RefData( "Base", ':', "", 6 ),
        RefData( "Val_", '!', "<int32_t>", 6 ),
        RefData( "Val_", 'S', "300", 6 ),
        RefData( "Base", '\n', "", 7 ),
        RefData( "Base", '\0', "", 7 ),
    };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, SequenceOfSequences)
{
    std::string TestStream =
        "- [name, hr, avg]\n"
        "- [Mark McGwire, 65, 0.278]\n"
        "- [Sammy Sosa, 63, 0.288]\n";
    RefDataVector TestTokens = {
        RefData( "Base", '-', "", 1 ),
        RefData( "Flow", '[', "", 1 ),
        RefData( "Flow", 'S', "name", 1 ),
        RefData( "Flow", ',', "", 1 ),
        RefData( "Flow", 'S', "hr", 1 ),
        RefData( "Flow", ',', "", 1 ),
        RefData( "Flow", 'S', "avg", 1 ),
        RefData( "Val_", ']', "", 1 ),
        RefData( "Val_", '\n', "", 2 ),
        RefData( "Val_", '-', "", 2 ),
        RefData( "Flow", '[', "", 2 ),
        RefData( "Flow", 'S', "Mark McGwire", 2 ),
        RefData( "Flow", ',', "", 2 ),
        RefData( "Flow", 'S', "65", 2 ),
        RefData( "Flow", ',', "", 2 ),
        RefData( "Flow", 'S', "0.278", 2 ),
        RefData( "Val_", ']', "", 2 ),
        RefData( "Val_", '\n', "", 3 ),
        RefData( "Val_", '-', "", 3 ),
        RefData( "Flow", '[', "", 3 ),
        RefData( "Flow", 'S', "Sammy Sosa", 3 ),
        RefData( "Flow", ',', "", 3 ),
        RefData( "Flow", 'S', "63", 3 ),
        RefData( "Flow", ',', "", 3 ),
        RefData( "Flow", 'S', "0.288", 3 ),
        RefData( "Val_", ']', "", 3 ),
        RefData( "Val_", '\n', "", 4 ),
        RefData( "Base", '\0', "", 4 ),
    };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, MappingOfMappings)
{
    std::string TestStream =
        "Mark McGwire : {hr: 65, avg : 0.278}\n"
        "Sammy Sosa : {\n"
        "    hr: 63,\n"
        "    avg: 0.288\n"
        "}";  // No new line !!!
    RefDataVector TestTokens = {
        RefData( "Base", 'S', "Mark McGwire", 1 ),
        RefData( "Base", ':', "", 1 ),
        RefData( "Flow", '{', "", 1 ),
        RefData( "Flow", 'S', "hr", 1 ),
        RefData( "Flow", ':', "", 1 ),
        RefData( "Flow", 'S', "65", 1 ),
        RefData( "Flow", ',', "", 1 ),
        RefData( "Flow", 'S', "avg", 1 ),
        RefData( "Flow", ':', "", 1 ),
        RefData( "Flow", 'S', "0.278", 1 ),
        RefData( "Val_", '}', "", 1 ),
        RefData( "Val_", '\n', "", 2 ),
        RefData( "Val_", 'S', "Sammy Sosa", 2 ),
        RefData( "Base", ':', "", 2 ),
        RefData( "Flow", '{', "", 2 ),
        RefData( "Flow", 'S', "hr", 3 ),
        RefData( "Flow", ':', "", 3 ),
        RefData( "Flow", 'S', "63", 3 ),
        RefData( "Flow", ',', "", 3 ),
        RefData( "Flow", 'S', "avg", 4 ),
        RefData( "Flow", ':', "", 4 ),
        RefData( "Flow", 'S', "0.288", 4 ),
        RefData( "Val_", '}', "", 5 ),
        RefData( "Base", '\0', "", 5 ),
    };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, CompactNestedMapping)
{
    std::string TestStream =
        "---\n"
        "# Products purchased\n"
        "- item    : Super Hoop\n"
        "  quantity: 1\n"
        "- item    : Basketball\n"
        "  quantity: 4\n"
        "- item    : Big Shoes\n"
        "  quantity: 1\n";
    RefDataVector TestTokens = {
        RefData( "Base", '\x02', "", 1 ),
        // RefData( "Base", '#', " Products purchased", 2 ),
        RefData( "Base", '\n', "", 3 ),
        RefData( "Base", '-', "", 3 ),
        RefData( "Val_", 'S', "item", 3 ),
        RefData( "Base", ':', "", 3 ),
        RefData( "Val_", 'S', "Super Hoop", 3 ),
        RefData( "Base", '\n', "  ", 4 ),
        RefData( "Base", 'S', "quantity", 4 ),
        RefData( "Base", ':', "", 4 ),
        RefData( "Val_", 'S', "1", 4 ),
        RefData( "Base", '\n', "", 5 ),
        RefData( "Base", '-', "", 5 ),
        RefData( "Val_", 'S', "item", 5 ),
        RefData( "Base", ':', "", 5 ),
        RefData( "Val_", 'S', "Basketball", 5 ),
        RefData( "Base", '\n', "  ", 6 ),
        RefData( "Base", 'S', "quantity", 6 ),
        RefData( "Base", ':', "", 6 ),
        RefData( "Val_", 'S', "4", 6 ),
        RefData( "Base", '\n', "", 7 ),
        RefData( "Base", '-', "", 7 ),
        RefData( "Val_", 'S', "item", 7 ),
        RefData( "Base", ':', "", 7 ),
        RefData( "Val_", 'S', "Big Shoes", 7 ),
        RefData( "Base", '\n', "  ", 8 ),
        RefData( "Base", 'S', "quantity", 8 ),
        RefData( "Base", ':', "", 8 ),
        RefData( "Val_", 'S', "1", 8 ),
        RefData( "Base", '\n', "", 9 ),
        RefData( "Base", '\0', "", 9 ),
    };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, IncludeFile)
{
    std::string TestFile =
        "    A: { aa: 1, bb: 2 }\n"
        "    B:\n"
        "        cc: 25\n";
    std::string TestStream =
        "AB:\n"
        "#include <AB.yaml>\n";
    RefDataVector TestTokens = {
        RefData( "Base", 'S', "AB", 1 ),
        RefData( "Base", ':', "", 1 ),
        RefData( "Val_", '\n', "    ", 1 ),
        RefData( "Val_", 'S', "A", 1 ),
        RefData( "Base", ':', "", 1 ),
        RefData( "Flow", '{', "", 1 ),
        RefData( "Flow", 'S', "aa", 1 ),
        RefData( "Flow", ':', "", 1 ),
        RefData( "Flow", 'S', "1", 1 ),
        RefData( "Flow", ',', "", 1 ),
        RefData( "Flow", 'S', "bb", 1 ),
        RefData( "Flow", ':', "", 1 ),
        RefData( "Flow", 'S', "2 ", 1 ),
        RefData( "Val_", '}', "", 1 ),
        RefData( "Val_", '\n', "    ", 2 ),
        RefData( "Val_", 'S', "B", 2 ),
        RefData( "Base", ':', "", 2 ),
        RefData( "Val_", '\n', "        ", 3 ),
        RefData( "Val_", 'S', "cc", 3 ),
        RefData( "Base", ':', "", 3 ),
        RefData( "Val_", 'S', "25", 3 ),
        RefData( "Base", '\n', "", 2 ),
        RefData( "Base", '\n', "", 3 ),
        RefData( "Base", '\0', "", 3 ),
    };
    // Create the include file.
    std::ofstream include_file("AB.yaml", std::ios_base::trunc | std::ios_base::out);
    include_file << TestFile;
    include_file.close();
    // Run the tokenizer and check the result.
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream, false);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, Preprocessor_ab0)
{
    std::string TestStream =
        "AB :\n"
        "# ifdef AB_1\n"
        "    ab : 1\n"
        "# else\n"
        "    ab : 0\n"
        "# endif\n"
        "end : true\n";
    const std::size_t line_begin = 1;
    const std::size_t line_ab = 5;
    const std::size_t line_end = 7;
    RefDataVector TestTokens = {
        RefData( "Base", 'S', "AB", line_begin ),
        RefData( "Base", ':', "",   line_begin ),
        RefData( "Val_", '\n', "    ",  line_ab ),
        RefData( "Val_", 'S', "ab", line_ab ),
        RefData( "Base", ':', "",   line_ab ),
        RefData( "Val_", 'S', "0",  line_ab ),
        RefData( "Base", '\n', "",  line_end ),
        RefData( "Base", 'S', "end", line_end ),
        RefData( "Base", ':', "",   line_end ),
        RefData( "Val_", 'S', "true",line_end ),
        RefData( "Base", '\n', "",  line_end+1 ),
        RefData( "Base", '\0', "",  line_end+1 ),
    };
    // Run the tokenizer and check the result.
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, Preprocessor_ab0_undef)
{
    std::string TestStream =
        "#define AB_1 1\n"
        "#undef AB_1 1\n"
        "AB :\n"
        "#ifdef AB_1\n"
        "    ab : 1\n"
        "#else\n"
        "    ab : 0\n"
        "#endif\n"
        "\n";
    const std::size_t line_begin = 3;
    const std::size_t line_ab = 7;
    const std::size_t line_end = 9;
    RefDataVector TestTokens = {
        RefData( "Base", '\n', "",  line_begin ),
        RefData( "Base", 'S', "AB", line_begin ),
        RefData( "Base", ':', "",   line_begin ),
        RefData( "Val_", '\n', "    ",  line_ab ),
        RefData( "Val_", 'S', "ab", line_ab ),
        RefData( "Base", ':', "",   line_ab ),
        RefData( "Val_", 'S', "0",  line_ab ),
        RefData( "Base", '\n', "",  line_end ),
        RefData( "Base", '\n', "",  line_end+1 ),
        RefData( "Base", '\0', "",  line_end+1 ),
    };
    // Run the tokenizer and check the result.
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, Preprocessor_ab1)
{
    std::string TestStream =
        "#define AB_1 1\n"
        "#define \t AB_2  2 \n"
        "AB :\n"
        "# ifdef AB_1\n"
        "    ab : 1\n"
        "# else\n"
        "    ab : 0\n"
        "# endif\n"
        "\n";
    const std::size_t line_begin = 3;
    const std::size_t line_ab = 5;
    const std::size_t line_end = 9;
    RefDataVector TestTokens = {
        RefData( "Base", '\n', "",  line_begin ),
        RefData( "Base", 'S', "AB", line_begin ),
        RefData( "Base", ':', "",   line_begin ),
        RefData( "Val_", '\n', "    ", line_ab ),
        RefData( "Val_", 'S', "ab", line_ab ),
        RefData( "Base", ':', "",   line_ab ),
        RefData( "Val_", 'S', "1",  line_ab ),
        RefData( "Base", '\n', "",  line_end ),
        RefData( "Base", '\n', "",  line_end+1 ),
        RefData( "Base", '\0', "",  line_end+1 ),
    };
    // Run the tokenizer and check the result.
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, Preprocessor_ab1_ifndef)
{
    std::string TestStream =
        "AB :\n"
        "# ifndef AB_1\n"
        "    ab : 1\n"
        "# else\n"
        "    ab : 0\n"
        "# endif\n"
        "\n";
    const std::size_t line_begin = 1;
    const std::size_t line_ab = 3;
    const std::size_t line_end = 7;
    RefDataVector TestTokens = {
        RefData( "Base", 'S', "AB", line_begin ),
        RefData( "Base", ':', "",   line_begin ),
        RefData( "Val_", '\n', "    ", line_ab ),
        RefData( "Val_", 'S', "ab", line_ab ),
        RefData( "Base", ':', "",   line_ab ),
        RefData( "Val_", 'S', "1",  line_ab ),
        RefData( "Base", '\n', "",  line_end ),
        RefData( "Base", '\n', "",  line_end+1 ),
        RefData( "Base", '\0', "",  line_end+1 ),
    };
    // Run the tokenizer and check the result.
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, Preprocessor_ab2)
{
    std::string TestStream =
        "#define AB_1 1\n"
        "#define AB_2 2\n"
        "AB :\n"
        "# if AB_2\n"
        "    ab : 2\n"
        "# elif AB_1\n"
        "    ab : 1\n"
        "# else\n"
        "    ab : 0\n"
        "# endif\n"
        "\n";
    const std::size_t line_begin = 3;
    const std::size_t line_ab = 5;
    const std::size_t line_end = 11;
    RefDataVector TestTokens = {
        RefData( "Base", '\n', "",  line_begin ),
        RefData( "Base", 'S', "AB", line_begin ),
        RefData( "Base", ':', "",   line_begin ),
        RefData( "Val_", '\n', "    ", line_ab ),
        RefData( "Val_", 'S', "ab", line_ab ),
        RefData( "Base", ':', "",   line_ab ),
        RefData( "Val_", 'S', "2",  line_ab ),
        RefData( "Base", '\n', "",  line_end ),
        RefData( "Base", '\n', "",  line_end+1 ),
        RefData( "Base", '\0', "",  line_end+1 ),
    };
    // Run the tokenizer and check the result.
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens);
}

TEST(YamlTokenizerTest, Preprocessor_ExternalDefines_ab0)
{
    std::string TestStream =
        "AB :\n"
        "# if AB_2\n"
        "    ab : 2\n"
        "# elif AB_1\n"
        "    ab : 1\n"
        "# else\n"
        "    ab : 0\n"
        "# endif\n"
        "\n";
    const std::size_t line_begin = 1;
    const std::size_t line_ab = 7;
    const std::size_t line_end = 9;
    RefDataVector TestTokens = {
        RefData( "Base", 'S', "AB", line_begin ),
        RefData( "Base", ':', "",   line_begin ),
        RefData( "Val_", '\n', "    ",  line_ab ),
        RefData( "Val_", 'S', "ab", line_ab ),
        RefData( "Base", ':', "",   line_ab ),
        RefData( "Val_", 'S', "0",  line_ab ),
        RefData( "Base", '\n', "",  line_end ),
        RefData( "Base", '\n', "",  line_end+1 ),
        RefData( "Base", '\0', "",  line_end+1 ),
    };
    // Run the tokenizer and check the result.
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream, false, {});
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens, {});
}

TEST(YamlTokenizerTest, Preprocessor_ExternalDefines_ab1)
{
    std::string TestStream =
        "AB :\n"
        "# if AB_2\n"
        "    ab : 2\n"
        "# elif AB_1\n"
        "    ab : 1\n"
        "# else\n"
        "    ab : 0\n"
        "# endif\n"
        "\n";
    const std::size_t line_begin = 1;
    const std::size_t line_ab = 5;
    const std::size_t line_end = 9;
    RefDataVector TestTokens = {
        RefData( "Base", 'S', "AB", line_begin ),
        RefData( "Base", ':', "",   line_begin ),
        RefData( "Val_", '\n', "    ", line_ab ),
        RefData( "Val_", 'S', "ab", line_ab ),
        RefData( "Base", ':', "",   line_ab ),
        RefData( "Val_", 'S', "1",  line_ab ),
        RefData( "Base", '\n', "",  line_end ),
        RefData( "Base", '\n', "",  line_end+1 ),
        RefData( "Base", '\0', "",  line_end+1 ),
    };
    // Run the tokenizer and check the result.
    rOops::PreprocessorDefines_t ext_def = { {"AB_1",""} };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream, false, ext_def);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens, false, ext_def);
}

TEST(YamlTokenizerTest, Preprocessor_ExternalDefines_ab2)
{
    std::string TestStream =
        "AB :\n"
        "# if AB_2\n"
        "    ab : 2\n"
        "# elif AB_1\n"
        "    ab : 1\n"
        "# else\n"
        "    ab : 0\n"
        "# endif\n"
        "\n";
    const std::size_t line_begin = 1;
    const std::size_t line_ab = 3;
    const std::size_t line_end = 9;
    RefDataVector TestTokens = {
        RefData( "Base", 'S', "AB", line_begin ),
        RefData( "Base", ':', "",   line_begin ),
        RefData( "Val_", '\n', "    ",  line_ab ),
        RefData( "Val_", 'S', "ab", line_ab ),
        RefData( "Base", ':', "",   line_ab ),
        RefData( "Val_", 'S', "2",  line_ab ),
        RefData( "Base", '\n', "",  line_end ),
        RefData( "Base", '\n', "",  line_end+1 ),
        RefData( "Base", '\0', "",  line_end+1 ),
    };
    // Run the tokenizer and check the result.
    rOops::PreprocessorDefines_t ext_def = { {"AB_1",""}, {"AB_2",""} };
    std::size_t nr_of_tokens = PrintYamlTokens(TestStream, false, ext_def);
    EXPECT_EQ(TestTokens.size(), nr_of_tokens);
    TestYamlTokenizer(TestStream, TestTokens, false, ext_def);
}
