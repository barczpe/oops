/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include "BaseDerived.h"
#include "SerializationTest.h"
#include <oops/rPropInterface.h>
#include <oops/rPropFind.h>
#include <gtest/gtest.h>
#include <string>
#include <utility>

using namespace rOops;

namespace orig
{
    // Test class original version.
    class Test
    {
    public:
        virtual ~Test() = default;
        Test() = default;
        Test(std::string aTitle, std::uint32_t aValue)
            : title_(std::move(aTitle))
            , value_(aValue)
        {}
        bool operator==(const Test& arR) const
        {
            return title_ == arR.title_ && value_ == arR.value_;
        }
    private:
        std::string title_;
        std::uint32_t value_ = 0;
        rOOPS_ADD_PROPERTY_INTERFACE(orig::Test)
        {
            rOOPS_PROPERTY(title_);
            rOOPS_PROPERTY(value_);
        }
    };
}


TEST(BackwardCompatibilityTest, ChangeTheOrderOfMembers)
{
    class ChangeOrder
    {
    public:
        virtual ~ChangeOrder() = default;
        ChangeOrder() = default;
        ChangeOrder(std::string aTitle, std::uint32_t aValue)
            : value_(aValue)
            , title_(std::move(aTitle))
        {}
        bool operator==(const ChangeOrder& arR) const
        {
            return title_ == arR.title_ && value_ == arR.value_;
        }
    private:
        std::uint32_t value_ = 0;
        std::string title_;
        rOOPS_ADD_PROPERTY_INTERFACE(ChangeOrder)
        {
            rOOPS_PROPERTY(value_);
            rOOPS_PROPERTY(title_);
        }
    };

    constexpr const char* test_name = "ChangeTheOrderOfMembers";
    std::stringstream strm(cStringStreamMode);
    orig::Test v1("v1", 1);
    rOops::rOopsTextFormat frmt(strm);
    save(frmt, v1, test_name);
    ChangeOrder v2;
    rOops::rOopsTextParser parser(strm, test_name);
    load(parser, v2);
    ChangeOrder ref("v1", 1);
    EXPECT_EQ(ref, v2);
    testSerialization(test_name, v1);
    testSerialization(test_name, v2);
}

/**
 * Add and remove members.
 */
TEST(BackwardCompatibilityTest, AddRemoveRenameMembers)
{
    class AddRemove
    {
    public:
        virtual ~AddRemove() = default;
        AddRemove() = default;
        AddRemove(std::string aTitle, double aValue)
            : newValue_(aValue)
            , title_(std::move(aTitle))
        {}
        bool operator==(const AddRemove& arR) const
        {
            return title_ == arR.title_ && std::abs(newValue_-arR.newValue_) < 1e-5;
        }
    private:
        // std::uint32_t value_ = 0;
        double newValue_ = 0.0;
        std::string title_;
        rOOPS_ADD_PROPERTY_INTERFACE(AddRemove)
        {
            rOOPS_PROPERTY(newValue_);
            rOOPS_PROPERTY(title_);
        }
    };

    constexpr const char* test_name = "AddRemoveRenameMembers";
    std::stringstream strm(cStringStreamMode);
    orig::Test v1("v1", 1);
    rOops::rOopsTextFormat frmt(strm);
    save(frmt, v1, test_name);
    AddRemove v2;
    rOops::rOopsTextParser parser(strm, test_name);
    load(parser, v2);
    AddRemove ref("v1", 0.0);
    EXPECT_EQ(ref, v2);
    auto iterOops = rPropGetTypeInfo(&v2)->begin(&v2, "v2");
    rOops::findPath(iterOops, "value_");
    EXPECT_FALSE(iterOops.isEnd());
    if (!iterOops.isEnd()) {
        EXPECT_EQ("1", iterOops.value());
    }
    testSerialization(test_name, v1);
    testSerialization(test_name, v2);
}

/**
 * Rename members.
 */
TEST(BackwardCompatibilityTest, RenameMembers)
{
    class Rename
    {
    public:
        virtual ~Rename() = default;
        Rename() = default;
        Rename(std::string aTitle, std::uint32_t aValue)
            : value2_(aValue)
            , title2_(std::move(aTitle))
        {}
        bool operator==(const Rename& arR) const
        {
            return title2_ == arR.title2_ && value2_ == arR.value2_;
        }
    private:
        std::uint32_t value2_ = 0;
        std::string title2_;
        void validate(std::uint32_t aVersion)
        {
            if (0 == aVersion) {
                rOops::updateProperty(this, "value_", value2_);
                rOops::updateProperty(this, "title_", title2_);
                rOops::rPropIterator iter = rPropGetTypeInfo(this)->begin(this, "");
                rOops::findPath(iter, "title_");
                if (!iter.isEnd()) {
                    title2_ = iter.value() + "_2";
                }
            }
        }
        rOOPS_ADD_PROPERTY_INTERFACE(Rename)
        {
            rOOPS_PROPERTY(value2_);
            rOOPS_PROPERTY(title2_);
            rOOPS_VERSION(1);
            rOOPS_VALIDATE(&Rename::validate);
        }
    };

    constexpr const char* test_name = "RenameMembers";
    std::stringstream strm(cStringStreamMode);
    orig::Test v1("v1", 1);
    rOops::rOopsTextFormat frmt(strm);
    save(frmt, v1, test_name);
    Rename v2;
    rOops::rOopsTextParser parser(strm, test_name);
    load(parser, v2);
    Rename ref("v1_2", 1);
    EXPECT_EQ(ref, v2);
    testSerialization(test_name, v1);
    testSerialization(test_name, v2);
}

using CharArray32 = std::array<char, 32>;
rOOPS_DECLARE_STL_ARRAY_TYPE_INFO(CharArray32)

TEST(BackwardCompatibilityTest, ChangeTypeOfMember)
{
    class ChangeType
    {
    public:
        virtual ~ChangeType() = default;
        ChangeType() = default;
        ChangeType(std::string aTitle, std::uint32_t aValue)
            : titleStr_(std::move(aTitle))
            , value_(aValue)
        {
            validate(0);
        }
        bool operator==(const ChangeType& arR) const
        {
            return title_ == arR.title_ && value_ == arR.value_;
        }
    private:
        std::string titleStr_;
        CharArray32 title_{};
        double value_ = 0; // Nothing to do, the old int value can be stored in a double. TODO: Be sure it works in binary version!!!
        void validate(std::uint32_t aVersion)
        {
            if (0 == aVersion) {
                if (titleStr_.size() > title_.size()) {
                    throw std::runtime_error("Title string is too long.");
                }
                std::copy(titleStr_.begin(), titleStr_.end(), title_.begin());
            }
        }
        rOOPS_ADD_PROPERTY_INTERFACE(ChangeType)
        {
            rOOPS_PROPERTY(titleStr_), "title_", rOops::rPropFlag::cReadOnly; // keep a variable with original type and property name and make it read-only.
            rOOPS_PROPERTY(title_), "title"; // Add a new property for the variable having the new type. The property has to be renamed, but the variable name may be the same.
            rOOPS_PROPERTY(value_);
            rOOPS_VERSION(1);
            rOOPS_VALIDATE(&ChangeType::validate);
        }
    };

    constexpr const char* test_name = "ChangeTypeOfMember";
    std::stringstream strm(cStringStreamMode);
    orig::Test v1("v1", 1);
    rOops::rOopsTextFormat frmt(strm);
    save(frmt, v1, test_name);
    ChangeType v2;
    rOops::rOopsTextParser parser(strm, test_name);
    load(parser, v2);
    ChangeType ref("v1", 1);
    EXPECT_EQ(ref, v2);
    testSerialization(test_name, v1);
    testSerialization(test_name, v2);
}

TEST(BackwardCompatibilityTest, AddBaseClass)
{
    class AddBaseClass : public BaseClass
    {
    public:
        ~AddBaseClass() override = default;
        AddBaseClass() = default;
        AddBaseClass(std::string aTitle, std::uint32_t aValue)
            : title_(std::move(aTitle))
            , value_(aValue)
        {
        }
        bool operator==(const AddBaseClass& arR) const
        {
            // Do not compare  base class. It was not saved, and always will be different.
            return title_ == arR.title_ && value_ == arR.value_;
        }
        std::string getTitle() const override { return title_; }
    private:
        std::string title_;
        std::uint32_t value_ = 0;
        rOOPS_ADD_PROPERTY_INTERFACE(AddBaseClass)
        {
            rOOPS_INHERIT(BaseClass);
            rOOPS_PROPERTY(title_);
            rOOPS_PROPERTY(value_);
        }
    };

    constexpr const char* test_name = "AddBaseClass";
    std::stringstream strm(cStringStreamMode);
    orig::Test v1("v1", 1);
    rOops::rOopsTextFormat frmt(strm);
    save(frmt, v1, test_name);
    AddBaseClass v2;
    rOops::rOopsTextParser parser(strm, test_name);
    load(parser, v2);
    AddBaseClass ref("v1", 1);
    EXPECT_EQ(ref, v2);
    testSerialization(test_name, v1);
    testSerialization(test_name, v2);
}
