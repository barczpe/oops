/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2019:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rPropInterface.h>
#include <oops/rOopsTextFormat.h>
#include <oops/rOopsTextParser.h>
#include "SerializationTest.h"
#include <gtest/gtest.h>
#include <chrono>
#include <cstdlib> // for abs()

using namespace rOops;


TEST(PropertyInterfaceTest, TypeInfo_bool )
{
    const rPropTypeInfo* pTIByName = rPropGetTypeInfoByName("bool");
    bool val = true;
    const rPropTypeInfo* pTI = rPropGetTypeInfo(static_cast<const bool*>(nullptr));
    const rPropTypeInfo* pTI2 = rPropGetTypeInfo(&val);
    EXPECT_EQ(pTI, pTI2);
    rPropTypeInfo::id_type type_id = pTI->TypeId();
    EXPECT_TRUE( pTI == pTIByName );
    EXPECT_TRUE( pTI == rPropGetTypeInfoById(type_id) );
    EXPECT_EQ( "bool", pTI->typeName() );
    EXPECT_EQ( sizeof(bool), pTI->Size() );

    EXPECT_TRUE( pTI->isValue() );
    EXPECT_FALSE( pTI->isCompound() );
    EXPECT_FALSE( pTI->isAbstract() );
    EXPECT_FALSE( pTI->isPolymorphic() );
    EXPECT_FALSE( pTI->isIntrusive() );
    EXPECT_EQ( (const rPropDescriptor*)nullptr, pTI->getPropDesc(0) );
    EXPECT_FALSE( pTI->isSTLContainer() );
    EXPECT_EQ( 1u, pTI->nrOfElements(&val) );
    EXPECT_EQ( (const rPropTypeInfo*)nullptr, pTI->elementTypeInfo() );
    EXPECT_EQ( (const rPropDescriptor*)nullptr, pTI->elementPropDesc() );

    val = true;
    EXPECT_EQ( "True", pTI->value(&val) );
    pTI->setValue(&val, std::string("0"));
    EXPECT_EQ( val, false );
    EXPECT_EQ( "False", pTI->value(&val) );
    pTI->setValue(&val,"true");
    EXPECT_EQ( val, true );
    EXPECT_EQ( "True", pTI->value(&val) );
    pTI->setValue(&val, std::string("1"));
    EXPECT_EQ( "True", pTI->value(&val) );
    pTI->setValue(&val,"false");
    EXPECT_EQ( "False", pTI->value(&val) );
    EXPECT_EQ( val, false );
    pTI->setValue(&val, std::string("false"));
    EXPECT_EQ(val, false);
    EXPECT_EQ("False", pTI->value(&val));
    pTI->setValue(&val, std::string("true"));
    EXPECT_EQ("True", pTI->value(&val));
    EXPECT_EQ(val, true);
}


TEST(PropertyInterfaceTest, TypeInfo_StdString )
{
    std::string val;
    const rPropTypeInfo* pTI = rPropGetTypeInfo(static_cast<const std::string*>(nullptr));
    rPropTypeInfo::id_type type_id = pTI->TypeId();
    EXPECT_TRUE( pTI == rPropGetTypeInfoByName("std::string") );
    EXPECT_TRUE( pTI == rPropGetTypeInfoById(type_id) );
    EXPECT_EQ( "std::string", pTI->typeName() );
    EXPECT_EQ( sizeof(std::string), pTI->Size() );

    EXPECT_TRUE( pTI->isValue() );
    EXPECT_FALSE( pTI->isCompound() );
    EXPECT_FALSE( pTI->isAbstract() );
    EXPECT_FALSE( pTI->isPolymorphic() );
    EXPECT_FALSE( pTI->isIntrusive() );
    EXPECT_EQ( (const rPropDescriptor*)nullptr, pTI->getPropDesc(0) );
    EXPECT_FALSE( pTI->isSTLContainer() );
    EXPECT_EQ( 1u, pTI->nrOfElements(&val) );
    EXPECT_EQ( (const rPropTypeInfo*)nullptr, pTI->elementTypeInfo() );
    EXPECT_EQ( (const rPropDescriptor*)nullptr, pTI->elementPropDesc() );

    val = "xxx";
    EXPECT_EQ( "xxx", pTI->value(&val) );
    pTI->setValue(&val, std::string("yyy"));
    EXPECT_EQ( val, "yyy" );
    EXPECT_EQ( "yyy", pTI->value(&val) );
    pTI->setValue(&val, std::string("abcdefg"));
    EXPECT_EQ( "abcdefg", pTI->value(&val) );
    EXPECT_EQ( val, "abcdefg" );
}


template <typename IntT>
void testIntegerTypesTypeInfo(const std::string& arAliasTypeName)
{
    IntT val = 0;
    const rPropTypeInfo* pTI = rPropGetTypeInfo(static_cast<IntT*>(nullptr));
    std::string type_name = pTI->typeName();
    rPropTypeInfo::id_type type_id = pTI->TypeId();
    EXPECT_TRUE( pTI == rPropGetTypeInfoByName(type_name) ) << "type name: " << type_name;
    auto pAlias = rPropGetTypeInfoByName(arAliasTypeName);
    EXPECT_TRUE( pTI == pAlias ) << "alias name: " << arAliasTypeName;
    EXPECT_TRUE( pTI == rPropGetTypeInfoById(type_id) ) << "type name: " << type_name;
    EXPECT_EQ( sizeof(IntT), pTI->Size() ) << "type name: " << type_name;

    EXPECT_TRUE( pTI->isValue() );
    EXPECT_FALSE( pTI->isCompound() );
    EXPECT_FALSE( pTI->isAbstract() );
    EXPECT_FALSE( pTI->isPolymorphic() );
    EXPECT_FALSE( pTI->isIntrusive() );
    EXPECT_EQ( (const rPropDescriptor*)nullptr, pTI->getPropDesc(0) );
    EXPECT_FALSE( pTI->isSTLContainer() );
    EXPECT_EQ( 1u, pTI->nrOfElements(&val) );
    EXPECT_EQ( (const rPropTypeInfo*)nullptr, pTI->elementTypeInfo() );
    EXPECT_EQ( (const rPropDescriptor*)nullptr, pTI->elementPropDesc() );

    std::string str;
    val = 1;
    EXPECT_EQ( "1", pTI->value(&val) );
    pTI->value(&val,str);
    EXPECT_EQ( "1", str );
    pTI->setValue(&val,std::string("2"));
    EXPECT_EQ( val, IntT{2} );
    EXPECT_EQ( "2", pTI->value(&val) );
    pTI->value(&val,str);
    EXPECT_EQ( "2", str );

    pTI->setValue(&val, std::string("3"));
    EXPECT_EQ( val, IntT{3} );
}

TEST(PropertyInterfaceTest, IntCommonTypeInfoTest)
{
    testIntegerTypesTypeInfo<short>("short");
    testIntegerTypesTypeInfo<int>("int");
    testIntegerTypesTypeInfo<long>("long");
    testIntegerTypesTypeInfo<long long>("long long");
    testIntegerTypesTypeInfo<unsigned short>("unsigned short");
    testIntegerTypesTypeInfo<unsigned int>("unsigned");
    testIntegerTypesTypeInfo<unsigned long>("unsigned long");
}

struct StructWithSimpleData_s
{
    bool b;
    char c;
    short s;
    int i;
    long long l;
    unsigned char uc;
    unsigned short us;
    unsigned ui;
    unsigned long long ul;
    signed char sc;
    signed short ss;
    signed si;
    signed long long sl;
    float f;
    double d;
    std::string str;
    virtual ~StructWithSimpleData_s() = default;
    explicit StructWithSimpleData_s(short aBase=0)
        : b(1==aBase%2)
        , c(static_cast<char>('a'+ aBase))
        , s(aBase), i(2*aBase), l(3*static_cast<long long>(aBase))
        , uc(4 * aBase), us(5 * aBase), ui(6 * aBase), ul(7 * static_cast<unsigned long long>(aBase))
        , sc(static_cast<signed char>(8 * aBase))
        , ss(static_cast<short>(9 * aBase))
        , si(10 * aBase)
        , sl(11 * static_cast<signed long long>(aBase))
        , f(12.13f*float(aBase))
        , d(14.15*double(aBase))
        , str("qwerty")
        {}
    bool operator==(const StructWithSimpleData_s& arR) const
    {
        using namespace cmp;
        using T = StructWithSimpleData_s;
        if (!eq(*this, arR, &T::b)) return false;
        if (!eq(*this, arR, &T::c)) return false;
        if (!eq(*this, arR, &T::s)) return false;
        if (!eq(*this, arR, &T::i)) return false;
        if (!eq(*this, arR, &T::l)) return false;
        if (!eq(*this, arR, &T::uc)) return false;
        if (!eq(*this, arR, &T::us)) return false;
        if (!eq(*this, arR, &T::ui)) return false;
        if (!eq(*this, arR, &T::ul)) return false;
        if (!eq(*this, arR, &T::sc)) return false;
        if (!eq(*this, arR, &T::ss)) return false;
        if (!eq(*this, arR, &T::si)) return false;
        if (!eq(*this, arR, &T::sl)) return false;
        if (!eq(*this, arR, &T::f)) return false;
        if (!eq(*this, arR, &T::d)) return false;
        if (!eq(*this, arR, &T::str)) return false;
        return true;
    }
    rOOPS_ADD_PROPERTY_INTERFACE(StructWithSimpleData_s)
    {
        rOOPS_PROPERTY(b);
        rOOPS_PROPERTY(c);
        rOOPS_PROPERTY(s);
        rOOPS_PROPERTY(i);
        rOOPS_PROPERTY(l);
        rOOPS_PROPERTY(uc);
        rOOPS_PROPERTY(us);
        rOOPS_PROPERTY(ui);
        rOOPS_PROPERTY(ul);
        rOOPS_PROPERTY(sc);
        rOOPS_PROPERTY(ss);
        rOOPS_PROPERTY(si);
        rOOPS_PROPERTY(sl);
        rOOPS_PROPERTY(f);
        rOOPS_PROPERTY(d);
        rOOPS_PROPERTY(str);
    }
};


TEST(PropertyInterfaceTest, SerializeSimpleData)
{
    const rPropTypeInfo* pTI = checkClassPropTypeInfo<StructWithSimpleData_s>("StructWithSimpleData_s", 16);
    EXPECT_TRUE(nullptr != pTI->getPropDesc(0) );
    EXPECT_TRUE(nullptr != pTI->getPropDesc(15) );
    EXPECT_TRUE(nullptr == pTI->getPropDesc(16) );

    StructWithSimpleData_s v1;
    EXPECT_EQ(1u, pTI->nrOfElements(&v1));
    StructWithSimpleData_s val1(11), val2(22), val3(33);
    testSerialization("StructOfSimpleData", v1);

    StructWithSimpleData_s v2(2);
    testSerialization("StructOfSimpleData", v2);
}

template < class ObjT >
std::string testBinaryFormat(std::uint8_t protocol, const char* aName, const ObjT& arObj)
{
    try {
        std::stringstream os(cStringStreamMode);
        rOops::rOopsBinaryFormat frmt(os, protocol);
        rOops::save(frmt, arObj, aName);
        // DebugPrint the binary file to std::cout.
        rOops::rOopsBinaryPrinter printer;
        return printer.printOopsBinaryFile(os);
    }
    catch (std::exception& e) {
        std::cout << "Exception: " << e.what() << std::endl;
        return {};
    }
}

TEST(PropertyInterfaceTest, SaveSimpleData)
{
    const rPropTypeInfo* pTI = checkClassPropTypeInfo<StructWithSimpleData_s>("StructWithSimpleData_s", 16);
    EXPECT_TRUE(nullptr != pTI->getPropDesc(0) );
    EXPECT_TRUE(nullptr != pTI->getPropDesc(15) );
    EXPECT_TRUE(nullptr == pTI->getPropDesc(16) );

    StructWithSimpleData_s v1;
    EXPECT_EQ(1u, pTI->nrOfElements(&v1));
    testSerialization("StructOfSimpleData", v1);

    StructWithSimpleData_s v2(2);
    testSerialization("StructOfSimpleData", v2);

    std::string test_result_N = testBinaryFormat('N', "StructOfSimpleData", v2);
    if (rOops::globalConfig().writeTypeNameLevel <= rOops::OopsConfig::WriteTypeName::eWhenNeeded) {
        std::string expectedBinaryFormat =
            "MetaData integer is little endian.\n"
            "MetaData float is little endian.\n"
            "MetaData version: 1.0\n"
            "N T__a Size:1 TN:StructWithSimpleData_s Name:StructOfSimpleData Value: Block BGN\n"
            "N ___7 Size:0 Name:b Value: false\n"
            "N ___9 Size:1 Name:c Value: 'c'\n"
            "N ___1 Size:2 Name:s Value: 2\n"
            "N ___1 Size:4 Name:i Value: 4\n"
            "N ___1 Size:8 Name:l Value: 6\n"
            "N ___2 Size:1 Name:uc Value: 8\n"
            "N ___2 Size:2 Name:us Value: 10\n"
            "N ___2 Size:4 Name:ui Value: 12\n"
            "N ___2 Size:8 Name:ul Value: 14\n"
            "N ___1 Size:1 Name:sc Value: 16\n"
            "N ___1 Size:2 Name:ss Value: 18\n"
            "N ___1 Size:4 Name:si Value: 20\n"
            "N ___1 Size:8 Name:sl Value: 22\n"
            "N ___3 Size:4 Name:f Value: 24.26\n"
            "N ___3 Size:8 Name:d Value: 28.3\n"
            "N ___4 Size:6 Name:str Value: \"qwerty\"\n"
            "N ___b Size:1 Value: Block END\n";
        EXPECT_EQ(expectedBinaryFormat, test_result_N);
    }
}

TEST(PropertyInterfaceTest, SerializationOfSimpleData)
{
    StructWithSimpleData_s v3(3);
    testSerialization("StructOfSimpleData3", v3);
}

TEST(PropertyInterfaceTest, ReadingOutOfOrderTextFormat)
{
    const std::string textStr =
        "StructOfSimpleData = !StructWithSimpleData_s {\n"
        "    str = \"qwerty\";\n"
        "    f = 24.260000;\n"
        "    d = 28.300000;\n"
        "    sl = 22;\n"
        "    si = 20;\n"
        "    ss = 18;\n"
        "    sc = 16;\n"
        "    ul = 14;\n"
        "    ui = 12;\n"
        "    us = 10;\n"
        "    uc = 8;\n"
        "    l = 6;\n"
        "    i = 4;\n"
        "    s = 2;\n"
        "    c = 'c';\n"
        "    b = false;\n"
        "};\n";
    std::stringstream str_strm(textStr);
    rOops::rOopsTextParser parser(str_strm, "PropertyInterfaceTest, ReadingOutOfOrderTextFormat");
    StructWithSimpleData_s dc;
    load(parser, dc);
    StructWithSimpleData_s ref(2);
    EXPECT_EQ(ref, dc);
}

TEST(PropertyInterfaceTest, ReadingOutOfOrderYamlFormat)
{
    std::string yamlStr =
        "StructOfSimpleData: !StructWithSimpleData_s\n"
        "  f : 24.260000\n"
        "  d : 28.300000\n"
        "  c : 'c'\n"
        "  l : 6\n"
        "  uc : 8\n"
        "  s : 2\n"
        "  i : 4\n"
        "  b : false\n"
        "  sc : 16\n"
        "  ss : 18\n"
        "  us : 10\n"
        "  ui : 12\n"
        "  ul : 14\n"
        "  si : 20\n"
        "  sl : 22\n"
        "  str : qwerty\n";
    std::stringstream str_strm(yamlStr);
    rOops::rOopsYamlParser parser(str_strm, "ReadingOutOfOrderYamlFormat");
    StructWithSimpleData_s dc;
    load(parser, dc);
    StructWithSimpleData_s ref(2);
    EXPECT_EQ(ref, dc);
}

TEST(PropertyInterfaceTest, ReadingPartiallyTextFormat)
{
    const std::string textStr =
        "StructOfSimpleData = !StructWithSimpleData_s {\n"
        "    l = 66;\n"
        "    i = 44;\n"
        "    s = 22;\n"
        "    c = 'z';\n"
        "    b = false;\n"
        "};\n";
    std::stringstream str_strm(textStr);
    rOops::rOopsTextParser parser(str_strm, "PropertyInterfaceTest.ReadingPartiallyTextFormat");
    StructWithSimpleData_s dc;
    load(parser, dc);
    StructWithSimpleData_s ref;
    ref.l = 66;
    ref.i = 44;
    ref.s = 22;
    ref.c = 'z';
    ref.b = false;
    EXPECT_EQ(ref, dc);
}

TEST(PropertyInterfaceTest, ReadingPartiallyYamlFormat)
{
    std::string yamlStr =
        "StructOfSimpleData: !StructWithSimpleData_s\n"
        "  str : 'ABCD, WXYZ'\n"
        "  f : 24.0\n"
        "  d : 28\n";
    std::stringstream str_strm(yamlStr);
    rOops::rOopsYamlParser parser(str_strm, "ReadingPartiallyYamlFormat");
    StructWithSimpleData_s dc(2);
    load(parser, dc);
    StructWithSimpleData_s ref(2);
    ref.str = "ABCD, WXYZ";
    ref.f = 24.0f;
    ref.d = 28.0;
    EXPECT_EQ(ref, dc);
}
