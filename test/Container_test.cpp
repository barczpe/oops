/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2019:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include "BaseDerived.h"
#include <oops/rPropInterface.h>
#include <string>
#include <utility>
#include <array>
#include <deque>
#include <list>
#include <set>
#include <vector>
#include <gtest/gtest.h>

namespace rOops
{
    namespace test
    {

        struct Data
        {
            explicit Data(int aVal=0)
                : val(aVal)
                , str(std::to_string(aVal))
            {
                for(int ii=1; ii<4; ++ii) {
                    fv.push_back( std::pair<double,std::string>(aVal/ii, std::to_string(aVal/ii)) );
                }
            }
            virtual ~Data() = default;
            bool operator < (const Data& arR) const
            {
                return (val < arR.val);
            }
            bool operator==(const Data& arR) const
            {
                return val == arR.val && str == arR.str;
            }
            int val{ 0 };
            std::string str{ "0" };
            std::vector<std::pair<double,std::string>> fv;
            rOOPS_ADD_PROPERTY_INTERFACE(rOops::test::Data)
                {
                    rOOPS_PROPERTY(val);
                    rOOPS_PROPERTY(str);
                    //rOOPS_PROPERTY(fv);
                }
        };

    }
}

using TestDataArray_5 = std::array<rOops::test::Data, 5>;

rOOPS_DECLARE_STL_ARRAY_TYPE_INFO(TestDataArray_5)
rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::vector<rOops::test::Data>)
rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::deque<rOops::test::Data>)
rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::list<rOops::test::Data>)
rOOPS_DECLARE_STL_SET_TYPE_INFO(std::set<rOops::test::Data>)


template <typename ContainerT>
void expectContainer(const ContainerT& ref, const ContainerT& data)
{
    EXPECT_EQ(ref.size(), data.size());
    auto iRef = ref.begin();
    auto iDat = data.begin();
    while (iRef != ref.end() && iDat != data.end() )
    {
        EXPECT_EQ(iRef->val, iDat->val);
        EXPECT_EQ(iRef->str, iDat->str);
        ++iRef;
        ++iDat;
    }
}

class SimpleContainer
{
public:
    virtual ~SimpleContainer()
        {}
    void push_back(int aVal)
        { _dataList.push_back( rOops::test::Data(aVal) ); }
    void expect(const SimpleContainer& dc)
        { expectContainer(_dataList, dc._dataList); }
private:
    std::list<rOops::test::Data> _dataList;
    rOOPS_ADD_PROPERTY_INTERFACE(SimpleContainer)
        {
            rOOPS_PROPERTY(_dataList), "dataList";
        }
};


class DataContainer
{
public:
    DataContainer()
    {
        for (int ii = 0; ii < 5; ++ii) {
            _dataArray[ii] = rOops::test::Data(-1);
        }
    }
    virtual ~DataContainer() = default;
    void push_back(int aVal)
    {
        _enumVector.emplace_back( static_cast<test_enums::Month_e>((aVal+1) % 12) );
        _dataArray[aVal%_dataArray.size()] = rOops::test::Data(aVal);
        _dataVector.emplace_back( aVal ); 
        _dataDeque.emplace_back( 10*aVal ); 
        _dataList.emplace_back( 100*aVal ); 
    }
    bool operator==(const DataContainer& arR) const
    {
        return
            _enumVector == arR._enumVector &&
            _dataArray == arR._dataArray &&
            _dataVector == arR._dataVector &&
            _dataDeque == arR._dataDeque &&
            _dataList == arR._dataList;
    }
private:
    std::vector<test_enums::Month_e> _enumVector;
    TestDataArray_5 _dataArray;
    std::vector<rOops::test::Data> _dataVector;
    std::deque<rOops::test::Data> _dataDeque;
    std::list<rOops::test::Data> _dataList;
    rOOPS_ADD_PROPERTY_INTERFACE(DataContainer)
        {
            rOOPS_PROPERTY(_enumVector), "enumVector";
            rOOPS_PROPERTY(_dataArray), "dataArray";
            rOOPS_PROPERTY(_dataVector), "dataVector";
            rOOPS_PROPERTY(_dataDeque), "dataDeque";
            rOOPS_PROPERTY(_dataList), "dataList";
        }
};


#include "SerializationTest.h"
#include <gtest/gtest.h>

TEST(ContainerTest, ListOfStructures)
{
    DataContainer dc;
    for (int ii=0; ii<5; ++ii) {
        dc.push_back(ii);
    }
    testSerialization("ListOfStructure", dc);
}

TEST(ContainerTest, EmptyListOfStructures)
{
    DataContainer dc;
    testSerialization("EmptyListOfStructures", dc);
}

TEST(ContainerTest, LoadTextWithoutSize)
{
    const std::string textStr =
        "ListOfStructure = !SimpleContainer {\n"
        "   dataList = !std::list<rOops::test::Data> [ !rOops::test::Data { val=1; str=\"1\"; }, !rOops::test::Data { val=2; str=\"2\"; }, !rOops::test::Data { val=3; str=\"3\"; } ];\n"
        "};\n";
    std::stringstream str_strm(textStr);
    rOops::rOopsTextParser parser(str_strm, "ContainerTest.LoadTextWithoutSize");
    SimpleContainer dc;
    load(parser, dc);
    SimpleContainer ref;
    for (int ii=1; ii<4; ++ii) {
        ref.push_back(ii);
    }
    ref.expect(dc);
}

TEST(ContainerTest, LoadTextWithoutTypes)
{
    const std::string textStr =
        "ListOfStructure = !SimpleContainer {\n"
        "   dataList[3] = !std::list<rOops::test::Data> [ { val=1; str=\"1\"; }, { val=2; str=\"2\"; }, { val=3; str=\"3\"; } ];\n"
        "};\n";
    std::stringstream str_strm(textStr);
    rOops::rOopsTextParser parser(str_strm, "ContainerTest.LoadTextWithoutTypes");
    SimpleContainer dc;
    load(parser, dc);
    SimpleContainer ref;
    for (int ii=1; ii<4; ++ii) {
        ref.push_back(ii);
    }
    ref.expect(dc);
}

TEST(ContainerTest, LoadYamlWithoutSize)
{
    std::string yamlStr =
        "ListOfStructure: !SimpleContainer\n"
        "  dataList: !std::list<rOops::test::Data>\n"
        "    - !rOops::test::Data\n"
        "      val: 1\n"
        "      str: 1\n"
        "    - !rOops::test::Data\n"
        "      val: 2\n"
        "      str: 2\n"
        "    - !rOops::test::Data\n"
        "      val: 3\n"
        "      str: 3\n";
    std::stringstream str_strm(yamlStr);
    rOops::rOopsYamlParser parser(str_strm, "LoadYamlWithoutSize");
    SimpleContainer dc;
    load(parser, dc);
    SimpleContainer ref;
    for (int ii=1; ii<4; ++ii) { ref.push_back(ii); }
    ref.expect(dc);
}

TEST(ContainerTest, LoadYamlWithoutTypes)
{
    std::string yamlStr =
        "ListOfStructure: !SimpleContainer\n"
        "  dataList[3]: !std::list<rOops::test::Data>\n"
        "    -\n"
        "      val: 1\n"
        "      str: 1\n"
        "    -\n"
        "      val: 2\n"
        "      str: 2\n"
        "    -\n"
        "      val: 3\n"
        "      str: 3\n";
    std::stringstream str_strm(yamlStr);
    rOops::rOopsYamlParser parser(str_strm, "LoadYamlWithoutTypes");
    SimpleContainer dc;
    load(parser, dc);
    SimpleContainer ref;
    for (int ii=1; ii<4; ++ii) { ref.push_back(ii); }
    ref.expect(dc);
}

TEST(ContainerTest, LoadYamlWithoutTypes2)
{
    std::string yamlStr =
        "ListOfStructure: !SimpleContainer\n"
        "  dataList[3]: !std::list<rOops::test::Data>\n"
        "    - val: 1\n"
        "      str: 1\n"
        "    - val: 2\n"
        "      str: 2\n"
        "    - val: 3\n"
        "      str: 3\n";
    std::stringstream str_strm(yamlStr);
    rOops::rOopsYamlParser parser(str_strm, "LoadYamlWithoutTypes2");
    SimpleContainer dc;
    load(parser, dc);
    SimpleContainer ref;
    for (int ii=1; ii<4; ++ii) { ref.push_back(ii); }
    ref.expect(dc);
}

rOOPS_DECLARE_STL_SET_TYPE_INFO(std::set<int>)

class StdSetTest
{
public:
    virtual ~StdSetTest() = default;
    void insert(int aVal)
    {
        _intSet.insert(aVal);
        _dataSet.insert( rOops::test::Data(aVal) );
    }
    void expect(const StdSetTest& other)
    {
        {
            EXPECT_EQ(_intSet.size(), other._intSet.size());
            auto iThis = _intSet.begin();
            auto iOther = other._intSet.begin();
            while (iThis != _intSet.end() && iOther != other._intSet.end() )
            {
                EXPECT_EQ(*iThis, *iOther);
                ++iThis;
                ++iOther;
            }
        }
        {
            EXPECT_EQ(_dataSet.size(), other._dataSet.size());
            auto iThis = _dataSet.begin();
            auto iOther = other._dataSet.begin();
            while (iThis != _dataSet.end() && iOther != other._dataSet.end() )
            {
                EXPECT_EQ(iThis->val, iOther->val);
                EXPECT_EQ(iThis->str, iOther->str);
                ++iThis;
                ++iOther;
            }
        }
    }
    bool operator==(const StdSetTest& arR) const
    {
        return _intSet == arR._intSet && _dataSet == arR._dataSet;
    }
private:
    std::set<int> _intSet;
    std::set<rOops::test::Data> _dataSet;
    rOOPS_ADD_PROPERTY_INTERFACE(StdSetTest)
        {
            rOOPS_PROPERTY(_intSet), "int_set";
            rOOPS_PROPERTY(_dataSet), "data_set";
        }
};

TEST(ContainerTest, StdSet)
{
    StdSetTest obj;
    for (int ii = 1; ii<4; ++ii) {
        obj.insert(ii);
    }
    testSerialization("StdSetTest", obj);
}

TEST(ContainerTest, EmptyStdSet)
{
    StdSetTest obj;
    testSerialization("EmptyStdSet", obj);
}

rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::vector<short>)
rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::vector< std::vector<short> >)

class IntMatrix
{
public:
    virtual ~IntMatrix() = default;
    IntMatrix() = default;
    IntMatrix(short dimx, short dimy, short val_init)
    {
        short val = val_init;
        data.reserve(dimx);
        for (short xx=0; xx<dimx; ++xx) {
            std::vector<short> temp;
            data.push_back(temp);
            data.back().reserve(dimy);
            val = 10 * xx + val_init;
            for (short yy=0; yy<dimy; ++yy) {
                data.back().push_back(val+yy);
            }
        }
    }
    void expect(const IntMatrix &m2) const
    {
        for (std::size_t xx=0; xx<data.size(); ++xx) {
            const std::vector<short>& row1 = data.at(xx);
            const std::vector<short>& row2 = m2.data.at(xx);
            for (std::size_t yy=0; yy<row1.size(); ++yy) {
                EXPECT_EQ(row1[yy], row2[yy]);
            }
        }
    }
    bool operator==(const IntMatrix& arR) const
    {
        if (data.size() != arR.data.size()) return false;
        for (std::size_t xx=0; xx<data.size(); ++xx) {
            const std::vector<short>& rowL = data.at(xx);
            const std::vector<short>& rowR = arR.data.at(xx);
            if (rowL.size() != rowR.size()) return false;
            for (std::size_t yy=0; yy<rowL.size(); ++yy) {
                if (rowL[yy] != rowR[yy]) return false;
            }
        }
        return true;
    }
private:
    std::vector< std::vector<short> > data;
    rOOPS_ADD_PROPERTY_INTERFACE(IntMatrix)
        {
            rOOPS_PROPERTY(data);
        }
};

TEST(ContainerTest, VectorOfVectors)
{
    IntMatrix m3x4(3, 4, 1);
    testSerialization("m3x4", m3x4);
}


rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::vector<std::string>)

struct S1
{
    virtual ~S1() = default;
    int x = 0;
    std::string s;
    std::vector<std::string> n;
    rOOPS_ADD_PROPERTY_INTERFACE(S1)
    {
        rOOPS_PROPERTY(x);
        rOOPS_PROPERTY(s);
        rOOPS_PROPERTY(n);
    }
};

TEST(valid_yaml, correct)
{
    const char* correct = R"(
    s1:
      x: 15
      s: "cat"
      n: [v1, v2]
    )";
    std::stringstream ss;
    ss << correct;
    rOops::rOopsYamlParser parser(ss, "s1");
    auto c = S1();
    load(parser, c);
    EXPECT_EQ(c.x, 15);
    EXPECT_EQ(c.s, "cat");
    EXPECT_EQ(c.n[0], "v1");
    EXPECT_EQ(c.n[1], "v2");
}

TEST(field_typo, typo_in_pod)
{
    const char* typo_in_pod = R"(
    s1:
      x_typo: 15
      s: "cat"
      n: [v1, v2]
    )";
    std::stringstream ss;
    ss << typo_in_pod;
    rOops::rOopsYamlParser parser(ss, "s1");
    auto c = S1();
    load(parser, c);
    EXPECT_EQ(c.x, 0);
    EXPECT_EQ(c.s, "cat");
    EXPECT_EQ(c.n[0], "v1");
    EXPECT_EQ(c.n[1], "v2");
}

TEST(field_typo, typo_in_str)
{
    const char* typo_in_str = R"(
    s1:
      x: 15
      s_typo: "cat"
      n: [v1, v2]
    )";
    std::stringstream ss;
    ss << typo_in_str;
    rOops::rOopsYamlParser parser(ss, "s1");
    auto c = S1();
    load(parser, c);
    EXPECT_EQ(c.x, 15);
    EXPECT_EQ(c.s, "");
    EXPECT_EQ(c.n[0], "v1");
    EXPECT_EQ(c.n[1], "v2");
}

TEST(field_typo, typo_in_vector)
{
    const char* typo_in_vector = R"(
    s1:
      x: 15
      s: "cat"
      n_typo: [v1, v2]
    )";
    std::stringstream ss;
    ss << typo_in_vector;
    rOops::rOopsYamlParser parser(ss, "s1");
    auto c = S1();
    load(parser, c);
    EXPECT_EQ(c.x, 15);
    EXPECT_EQ(c.s, "cat");
    EXPECT_TRUE(c.n.empty()) << c.n[0] << " " << c.n[1];
}

TEST(field_typo, typo_in_vector_change_order)
{
    const char* typo_in_vector = R"(
    s1:
      x: 15
      n_typo: [v1, v2]
      s: "cat"
    )";
    std::stringstream ss;
    ss << typo_in_vector;
    rOops::rOopsYamlParser parser(ss, "s1");
    auto c = S1();
    load(parser, c);
    EXPECT_EQ(c.x, 15);
    EXPECT_EQ(c.s, "cat");
    EXPECT_TRUE(c.n.empty()) << c.n[0] << " " << c.n[1];
}


rOOPS_DECLARE_STL_LIST_TYPE_INFO(std::vector<S1>)
struct S2
{
    virtual ~S2() = default;
    std::vector<S1> s1v;
    rOOPS_ADD_PROPERTY_INTERFACE(S2)
    {
        rOOPS_PROPERTY(s1v);
    }
};

TEST(missing_space, ok)
{
    const char* ok = R"(
    s2:
      s1v:
        - x: 15
          s: "cat"
          n: [v1, v2]
        - x: 10
          s: "dog"
          n: [q1, q2]
    )";
    std::stringstream ss;
    ss << ok;
    rOops::rOopsYamlParser parser(ss, "s2");
    S2 c;
    load(parser, c);
    EXPECT_EQ(2u, c.s1v.size());
    EXPECT_EQ(c.s1v[0].x, 15);
    EXPECT_EQ(c.s1v[0].s, "cat");
    EXPECT_EQ(c.s1v[0].n[0], "v1");
    EXPECT_EQ(c.s1v[0].n[1], "v2");
    EXPECT_EQ(c.s1v[1].x, 10);
    EXPECT_EQ(c.s1v[1].s, "dog");
    EXPECT_EQ(c.s1v[1].n[0], "q1");
    EXPECT_EQ(c.s1v[1].n[1], "q2");
}

TEST(missing_space, no_space)
{
    const char* ok = R"(
    s2:
      s1v:
        - x: 15
          s: "cat"
          n: [v1, v2]
        - x: 10
          s: "dog"
          n: [q1, q2]
    )";
    std::stringstream ss;
    ss << ok;
    rOops::rOopsYamlParser parser(ss, "s2");
    S2 c;
    load(parser, c);
    EXPECT_EQ(2u, c.s1v.size());
    EXPECT_EQ(c.s1v[0].x, 15);
    EXPECT_EQ(c.s1v[0].s, "cat");
    EXPECT_EQ(c.s1v[0].n[0], "v1");
    EXPECT_EQ(c.s1v[0].n[1], "v2");
    EXPECT_EQ(c.s1v[1].x, 10);
    EXPECT_EQ(c.s1v[1].s, "dog");
    EXPECT_EQ(c.s1v[1].n[0], "q1");
    EXPECT_EQ(c.s1v[1].n[1], "q2");
}
