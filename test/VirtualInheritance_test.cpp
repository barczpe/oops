/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rPropInterface.h>
#include <oops/rOopsTextFormat.h>
#include <oops/rOopsTextParser.h>
#include "SerializationTest.h"
#include <gtest/gtest.h>
#include <string>


using namespace rOops;

class VBase
{
public:
    virtual ~VBase() = default;
    explicit VBase( const char* apName="VBase" )
        : name(apName)
        {}
    bool operator==(const VBase& arR) const
    {
        return name==arR.name;
    }
    std::string name;
    rOOPS_ADD_PROPERTY_INTERFACE(VBase)
    {
        rOOPS_PROPERTY(name);
    }
}; //class VBase

#ifdef VIRTUAL_INHERITANCE_IS_SUPPORTED
class VB : virtual public VBase
#else
class VB : public VBase
#endif
{
public:
    VB()
        : VBase("VB"), i2(2)
        {}
    bool operator==(const VB& arR) const
    {
        if (!VBase::operator==(arR)) return false;
        return i2==arR.i2;
    }
    int i2;
    rOOPS_ADD_PROPERTY_INTERFACE(VB)
    {
        rOOPS_INHERIT(VBase);
        rOOPS_PROPERTY(i2);
    }
}; //class VB

#ifdef VIRTUAL_INHERITANCE_IS_SUPPORTED
class VC : virtual public VBase
#else
class VC : public VBase
#endif
{
public:
    VC()
        : VBase("VC")
        , i3(3)
        {}
    bool operator==(const VC& arR) const
    {
        if (!VBase::operator==(arR)) return false;
        return i3==arR.i3;
    }
    int i3;
    rOOPS_ADD_PROPERTY_INTERFACE(VC)
    {
        rOOPS_INHERIT(VBase);
        rOOPS_PROPERTY(i3);
    }
}; //class VC


class BC : public VB, public VC
{
public:
    BC()
        : VB(), VC(), u4(123)
        { VB::name = VC::name = "BC"; }
    bool operator==(const BC& arR) const
    {
        if (!VB::operator==(arR)) return false;
        if (!VC::operator==(arR)) return false;
        return u4==arR.u4;
    }
    unsigned u4;
    rOOPS_ADD_PROPERTY_INTERFACE(BC)
    {
        rOOPS_INHERIT(VB);
        rOOPS_INHERIT(VC);
        rOOPS_PROPERTY(u4);
    }
}; //class BC


TEST(VirtualInheritanceTest, VirtualInheritance_B)
{
    VB b;

    const rPropTypeInfo* pTI = rPropGetTypeInfo((VB*)nullptr);
    rPropTypeInfo::id_type type_id = pTI->TypeId();
    EXPECT_TRUE( pTI == rPropGetTypeInfoByName("VB") );
    EXPECT_TRUE( pTI == rPropGetTypeInfoById(type_id) );
    EXPECT_EQ( "VB", pTI->typeName() );
    EXPECT_EQ( sizeof(VB), pTI->Size() );

    EXPECT_FALSE( pTI->isValue() );
    EXPECT_TRUE( pTI->isCompound() );
    EXPECT_FALSE( pTI->isAbstract() );
    EXPECT_FALSE( pTI->isPolymorphic() );
    EXPECT_FALSE( pTI->isIntrusive() );
    EXPECT_TRUE(nullptr != pTI->getPropDesc(0) ); // VBase
    EXPECT_TRUE(nullptr != pTI->getPropDesc(1) ); // i2
    EXPECT_TRUE(nullptr == pTI->getPropDesc(2) );
    EXPECT_FALSE( pTI->isSTLContainer() );
    EXPECT_EQ( 1u, pTI->nrOfElements(&b) );
    EXPECT_EQ( (const rPropTypeInfo*)nullptr, pTI->elementTypeInfo() );
    EXPECT_EQ( (const rPropDescriptor*)nullptr, pTI->elementPropDesc() );

    b.i2 = 222;
    testSerialization("Test_Inheritance_B", b);
}

// TODO: virtual inheritance has not work yet.
// TODO: casting must go through the real class type.
// TODO: VBase can be find through BC only.
// TODO: When iterating over VB we have to know, that it is a base class of BC.
TEST(VirtualInheritanceTest, DISABLED_VirtualInheritance)
{
    BC bc;

    // Sizes are different on Linux and Windows.
    //auto offsetB = getBaseClassOffset<BC, VB>();        EXPECT_EQ(0x00, offsetB);
    //auto offsetC = getBaseClassOffset<BC, VC>();        EXPECT_EQ(0x30, offsetC);
    //auto offsetVB = getBaseClassOffset<VB, VBase>();    EXPECT_EQ(0x38, offsetVB);
    //auto offsetVC = getBaseClassOffset<VC, VBase>();    EXPECT_EQ(0x38, offsetVC);
    //auto offsetVBase = getBaseClassOffset<BC, VBase>(); EXPECT_EQ(0x90, offsetVBase);
    //auto size_VBase = sizeof(VBase);                    EXPECT_EQ(0x50u, size_VBase);
    //auto size_VB = sizeof(VB);                          EXPECT_EQ(0x88u, size_VB);
    //auto size_VC = sizeof(VC);                          EXPECT_EQ(0x88u, size_VC);
    //auto size_BC = sizeof(BC);                          EXPECT_EQ(0xe0u, size_BC);

    const rPropTypeInfo* pTI = rPropGetTypeInfo((BC*)nullptr);
    rPropTypeInfo::id_type type_id = pTI->TypeId();
    EXPECT_TRUE( pTI == rPropGetTypeInfoByName("BC") );
    EXPECT_TRUE( pTI == rPropGetTypeInfoById(type_id) );
    EXPECT_EQ( "BC", pTI->typeName() );
    EXPECT_EQ( sizeof(BC), pTI->Size() );

    EXPECT_FALSE( pTI->isValue() );
    EXPECT_TRUE( pTI->isCompound() );
    EXPECT_FALSE( pTI->isAbstract() );
    EXPECT_FALSE( pTI->isPolymorphic() );
    EXPECT_FALSE( pTI->isIntrusive() );
    EXPECT_TRUE(nullptr != pTI->getPropDesc(0) ); // VB
    EXPECT_TRUE(nullptr != pTI->getPropDesc(1) ); // VC
    EXPECT_TRUE(nullptr != pTI->getPropDesc(2) ); // u4
    EXPECT_TRUE(nullptr == pTI->getPropDesc(3) );
    EXPECT_FALSE( pTI->isSTLContainer() );
    EXPECT_EQ( 1u, pTI->nrOfElements(&bc) );
    EXPECT_EQ( (const rPropTypeInfo*)nullptr, pTI->elementTypeInfo() );
    EXPECT_EQ( (const rPropDescriptor*)nullptr, pTI->elementPropDesc() );

    testSerialization("Test_VirtualInheritance", bc);
    bc.i2 = -222222;
    bc.i3 = -333333;
    bc.u4 = 4444444;
    testSerialization("Test_VirtualInheritance", bc);
}
