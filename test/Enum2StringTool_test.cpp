/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2021:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/Enum2StringTool.h>
#include <gtest/gtest.h>

/**
 * Global enum definition inside a namespace.
 */
namespace Enum2Str {
    DECLARE_ENUM_CLASS(Colors, int, Cyan = 1, Magenta, Yellow, Black);
}

TEST(Enum2StringTest, Color)
{
    Enum2Str::Colors color = Enum2Str::Colors::Cyan;
    EXPECT_EQ("Cyan", Enum2Str::Colors2String::to_string(color));
    EXPECT_EQ("Cyan", Enum2Str::Colors2String::to_string(Enum2Str::Colors::Cyan));
    EXPECT_EQ("Yellow", Enum2Str::Colors2String::to_string(Enum2Str::Colors::Yellow));
    EXPECT_EQ(Enum2Str::Colors::Yellow, Enum2Str::Colors2String::from_string("Yellow"));
    EXPECT_THROW(Enum2Str::Colors2String::from_string("XYZ"), std::runtime_error);
    // Underlying type.
    EXPECT_EQ(1, Enum2Str::Colors2String::to_integral(Enum2Str::Colors::Cyan));
    EXPECT_EQ(Enum2Str::Colors::Cyan, Enum2Str::Colors2String::from_integral(1));
    // Out of range value.
    EXPECT_THROW(Enum2Str::Colors2String::from_integral(5), std::runtime_error);
    EXPECT_THROW(Enum2Str::Colors2String::to_string(static_cast<Enum2Str::Colors>(5)), std::runtime_error);
    // Size, first, last:
    EXPECT_EQ(4u, Enum2Str::Colors2String::size());
    EXPECT_EQ(Enum2Str::Colors::Cyan, Enum2Str::Colors2String::first());
    EXPECT_EQ(Enum2Str::Colors::Black, Enum2Str::Colors2String::last());
}

/**
 * Enum declared inside a function.
 */
TEST(Enum2StringTest, Month)
{
    DECLARE_ENUM_CLASS(Months, int32_t,
        eJanuary = 1,
        eFebruary,
        eMarch,
        eApril,
        eMay,
        eJune,
        eJuly,
        eAugust,
        eSeptember,
        eOctober,
        eNovember,
        eDecember
    );
    Months mm = Months::eJanuary;
    EXPECT_EQ("eJanuary", Months2String::to_string(mm));
    // Size, first, last:
    EXPECT_EQ(12u, Months2String::size());
    EXPECT_EQ(Months::eJanuary, Months2String::first());
    EXPECT_EQ(Months::eDecember, Months2String::last());
}

/**
 * Enum declared in a class.
 */
class EnumTestClass
{
public:
    DECLARE_ENUM_CLASS(Days, int8_t,
        eMonday,
        eTuesday,
        eWednesday,
        eThursday,
        eFriday,
        eSaturday,
        eSunday
    );
};

TEST(Enum2StringTest, Days)
{
    EnumTestClass::Days dd = EnumTestClass::Days::eThursday;
    EXPECT_EQ("eThursday", EnumTestClass::Days2String::to_string(dd));
    // Iterate over all enum values:
    std::string all_days;
    for (auto it=EnumTestClass::Days2String::begin(); it!=EnumTestClass::Days2String::end(); ++it) {
        all_days += it->second;
        all_days += ',';
    }
    EXPECT_EQ("eMonday,eTuesday,eWednesday,eThursday,eFriday,eSaturday,eSunday,", all_days);
}

/**
 * Sometimes the DECLARE_ENUM_CLASS cannot be used:
 *  - The enumerator list is too long.
 *  - The enumerators and the string representations are different.
 *  - The enum declaration is not complete (e.g. enum name or enum base are missing).
 *  The Enum2String class can be implemented by hand as in this test-case.
 */
TEST(Enum2StringTest, Animals)
{
    enum class Animals : unsigned {Dog, Cat, Owl, Sharp};
    class Animals2String : public Enum2StringTool<Animals, Animals2String>
    {
    public:
        Animals2String() : Enum2StringTool<Animals, Animals2String>()
        {
            insert(Animals::Dog, "Kutya");
            insert(Animals::Cat, "Macska");
            alias(Animals::Cat, "Cica");
            insert(Animals::Owl, "Bagoly");
            insert(Animals::Sharp, "Capa");
        }
    };
    Animals cat = Animals::Cat;
    EXPECT_EQ("Macska", Animals2String::to_string(cat));
    EXPECT_EQ(Animals::Cat, Animals2String::from_string("Cica"));
    EXPECT_EQ(Animals::Owl, Animals2String::from_string("Bagoly"));
    // Iterate over all enum values:
    std::string animal_names;
    for (auto it=Animals2String::rbegin(); it!=Animals2String::rend(); ++it) {
        animal_names += it->second;
    }
    EXPECT_EQ("CapaBagolyMacskaKutya", animal_names);
}
