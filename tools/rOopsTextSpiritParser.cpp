/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
Type Info loader.
*/

#include "rOopsTextBNFParser.h"
#include <oops/rPropLog.h>
#include <boost/lexical_cast.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/lex_lexertl.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>

#include <iostream>
#include <list>
#include <string>

using namespace std;

namespace qi = boost::spirit::qi;
namespace lex = boost::spirit::lex;
namespace ph = boost::phoenix;
namespace fs = boost::fusion;


namespace rOops
{

template <typename LexerT>
class rOopsTextBNFParserTokens : public lex::lexer<LexerT>
{
public:
    rOopsTextBNFParserTokens()
    {
        // define the tokens to match
        BlockBgn = '{'; BlockEnd = '}';
        ListBgn = '['; ListEnd = ']';
        MapBgn = '<'; MapEnd = '>';
        Assign = '=';
        SemiColon = ';';
        Coma = ',';
        TypeMark = '!';
        Modifier = "[\\*\\&]";
        Character = "'[ -Z]'";
        String = "\"[ -Z]\"";
        Number = "[0-9]";
        //Identifier = "[a-zA-Z_\\*][a-zA-Z0-9:_]*";
        Identifier = "[a-zA-Z_][a-zA-Z0-9:_]*";
        // associate the tokens and the token set with the lexer
        this->self = lex::token_def<>('=') | ',' | '*' | '&';
        this->self += BlockBgn | BlockEnd;
        this->self += ListBgn | ListEnd;
        this->self += MapBgn | MapEnd;
        this->self += Assign | SemiColon | Coma | TypeMark | Modifier;
        this->self += Identifier;

        // define the whitespace to ignore (spaces, tabs, newlines and C-style comments)
        this->self("WS")
            = lex::token_def<>(R"([ \t\n\r]+)") // white spaces
            | "#[^\n]*"  // line comment
            ;
        rDbgTrace(("kwClass %1% %2%") % kwClass.id() % kwClass.definition() );
        rDbgTrace(("BlockBgn %1% %2%") % BlockBgn.id() % BlockBgn.definition() );
        rDbgTrace(("BlockEnd %1% %2%") % BlockEnd.id() % BlockEnd.definition() );
        rDbgTrace(("Assign %1% %2%") % Assign.id() % Assign.definition() );
        rDbgTrace(("SemiColon %1% %2%") % SemiColon.id() % SemiColon.definition() );
        rDbgTrace(("Coma %1% %2%") % Coma.id() % Coma.definition() );
        rDbgTrace(("Modifier %1% %2%") % Modifier.id() % Modifier.definition() );
        rDbgTrace(("Identifier %1% %2%") % Identifier.id() % Identifier.definition() );
    }

    lex::token_def<std::string> Character, String, Number, Identifier;
    lex::token_def<std::string> BlockBgn, BlockEnd;
    lex::token_def<std::string> ListBgn, ListEnd;
    lex::token_def<std::string> MapBgn, MapEnd;
    lex::token_def<std::string> Assign, SemiColon, Coma, TypeMark, Modifier;
}; //class rOopsTextBNFParserTokens


class cbIdentifier
{
public:
    cbIdentifier() = default;
    template < typename ContextT >
    void operator()( const std::string& arStr, ContextT& /*arCntx*/, bool& arPass ) const
    {
        cout << "cbIdentifier: " << arStr << endl;
        arPass = true;
    }
};

class cbTypeName
{
public:
    cbTypeName() = default;
    template <typename ContextT>
    void operator()( const std::string& arStr, ContextT& /*arCntx*/, bool& arPass ) const
    {
        cout << "cbTypeName: " << arStr << endl;
        arPass = true;
    }
};

class cbStatement
{
public:
    cbStatement() = default;
    template <typename ContextT>
    void operator()( const std::string& arL, const std::string& arAssign, const std::string& arR, const std::string& arSemiColon, ContextT& /*arCntx*/, bool& arPass ) const
    {
        cout << "cbStatement: " << arL << arAssign << arR << arSemiColon << endl;
        arPass = true;
    }
};

class cbBlockBegin
{
public:
    cbBlockBegin() = default;
    template < typename ContextT >
    void operator()( const std::string& arStr, ContextT& arCntx, bool& arPass ) const
    {
        cout << "cbBlockBegin" << endl;
        arPass = true;
    }
};

class cbBlockEnd
{
public:
    cbBlockEnd() = default;
    template < typename ContextT >
    void operator()( const std::string& arStr, ContextT& arCntx, bool& arPass ) const
    {
        cout << "cbBlockEnd" << endl;
    }
};


template <typename IteratorT, typename LexerT>
class rOopsTextBNFParserGrammar : public qi::grammar< IteratorT, qi::in_state_skipper<LexerT> >
{
public:
    rOopsTextBNFParserGrammar()
        : rOopsTextBNFParserGrammar::base_type(BlockDef,"rOopsTextBNFParserGrammar")
        {}
    template <typename TokenDefT>
    void init(TokenDefT const& arToken )
        {
            Statement = (arToken.Identifier >> lex::char_(arToken.Assign) >> arToken.Identifier >> lex::char_(arToken.SemiColon))[cbStatement()];
            TypeDef = arToken.TypeMark >> arToken.Identifier[cbTypeName()];
            BlockDef = arToken.Identifier[cbIdentifier()] >> lex::char_(arToken.Assign) >> -(TypeDef) >>
                arToken.BlockBgn[cbBlockBegin()] >
                *(Statement) >
                arToken.BlockEnd[cbBlockEnd()] >> -arToken.SemiColon;
            //Stream = *ClassDef;
            Statement.name("statement");
            BlockDef.name("class");
            //Stream.name("stream");
            //qi::on_error<qi::fail>( Stream, ErrorHandler<rOopsTextParserImpl::iterator_type> );
            qi::on_error<qi::fail>(
                BlockDef,
                std::cout
                << ph::val("Error! Expecting ")
                << qi::_4                               // what failed?
                << ph::val(" here: \"")
                << ph::construct<std::string>(qi::_3, qi::_2)   // iterators to error-pos, end
                << ph::val("\"")
                << std::endl
            );
        }
    void setOutput(  )
        {

        }
protected:
    qi::rule< IteratorT, qi::in_state_skipper<LexerT> > Statement;
    qi::rule< IteratorT, qi::in_state_skipper<LexerT> > TypeDef;
    qi::rule< IteratorT, qi::in_state_skipper<LexerT> > BlockDef;
    //qi::rule< IteratorT, qi::in_state_skipper<LexerT> > Stream;
}; //class rOopsTextBNFParserGrammar



class rOopsTextBNFParserImpl
{
public:
    rOopsTextBNFParserImpl()
        : lexer_()
        , parser_()
    {
        parser_.init(lexer_);
    }
    void parse(const std::string& arStr )
    {
        parser_.init(lexer_);
        std::string::const_iterator iStr = arStr.begin();
        iterator_type iTok = lexer_.begin(iStr, arStr.end());
        iterator_type iEnd = lexer_.end();
        bool res = qi::phrase_parse(iTok, iEnd, parser_, qi::in_state("WS")[lexer_.self]);
        if( !res ) throw std::runtime_error(rOOPS_BUILD_EXCEPTION_MESSAGE("Error while parsing: " << *iTok));
        if( iTok!=iEnd ) throw std::runtime_error(rOOPS_BUILD_EXCEPTION_MESSAGE("Not all input is parsed: " << *iTok));
    } //parse()
public:
    typedef lex::lexertl::token<
        std::string::const_iterator, boost::mpl::vector<std::string>
    > token_type;
    typedef lex::lexertl::lexer<token_type> lexer_type;
    typedef rOopsTextBNFParserTokens<lexer_type> tokens_type;
    typedef tokens_type::iterator_type iterator_type;
    typedef rOopsTextBNFParserGrammar<iterator_type, tokens_type::lexer_def> grammar_type;
protected:
    tokens_type lexer_;
    grammar_type parser_;
}; //class rOopsTextBNFParserImpl


// class rOopsTextBNFParser
//==========================

rOopsTextBNFParser::rOopsTextBNFParser()
{
    _pImpl = new rOopsTextBNFParserImpl();
}

rOopsTextBNFParser::~rOopsTextBNFParser()
{
    delete _pImpl;
}

void rOopsTextBNFParser::parse(const std::string& arStr)
{
    _pImpl->parse(arStr );
}

} //namespace rOops
