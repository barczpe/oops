# Generated from rOopsText.g4 by ANTLR 4.9.3
from rOopsTextLexer import rOopsTextLexer
from rOopsTextParser import rOopsTextParser
from rOopsTextListener import rOopsTextListener
from antlr4 import *
import sys

# This class defines a complete listener for a parse tree produced by rOopsTextParser.
class rOopsTextPythonParser(rOopsTextListener):

    # Enter a parse tree produced by rOopsTextParser#top_level.
    def enterTop_level(self, ctx:rOopsTextParser.Top_levelContext):
        print("TOP_BGN: ", ctx, ctx.getText())

    # Exit a parse tree produced by rOopsTextParser#top_level.
    def exitTop_level(self, ctx:rOopsTextParser.Top_levelContext):
        print("TOP_END", ctx, ctx.getText())


    # Enter a parse tree produced by rOopsTextParser#value.
    def enterValue(self, ctx:rOopsTextParser.ValueContext):
        print("value_BGN: ", ctx, ctx.getText())

    # Exit a parse tree produced by rOopsTextParser#value.
    def exitValue(self, ctx:rOopsTextParser.ValueContext):
        print("value_END", ctx, ctx.getText())


    # Enter a parse tree produced by rOopsTextParser#block.
    def enterBlock(self, ctx:rOopsTextParser.BlockContext):
        print("BLOCK_BGN: ", ctx, ctx.getText())

    # Exit a parse tree produced by rOopsTextParser#block.
    def exitBlock(self, ctx:rOopsTextParser.BlockContext):
        print("BLOCK_END", ctx, ctx.getText())


    # Enter a parse tree produced by rOopsTextParser#list_value.
    def enterList_value(self, ctx:rOopsTextParser.List_valueContext):
        print("LIST_BGN: ", ctx, dir(ctx))

    # Exit a parse tree produced by rOopsTextParser#list_value.
    def exitList_value(self, ctx:rOopsTextParser.List_valueContext):
        print("LIST_END", ctx, dir(ctx))


    # Enter a parse tree produced by rOopsTextParser#map_value.
    def enterMap_value(self, ctx:rOopsTextParser.Map_valueContext):
        print("MAP_BGN: ", ctx, dir(ctx))

    # Exit a parse tree produced by rOopsTextParser#map_value.
    def exitMap_value(self, ctx:rOopsTextParser.Map_valueContext):
        print("MAP_END", ctx, dir(ctx))

def main(argv):
    input = FileStream(argv[1])
    lexer = rOopsTextLexer(input)
    stream = CommonTokenStream(lexer)
    parser = rOopsTextParser(stream)
    parser.buildParseTrees = True
    tree = parser.top_level()
    listener = rOopsTextPythonParser()
    walker = ParseTreeWalker()
    walker.walk(listener, tree)
    print(tree)

if __name__ == '__main__':
    main(sys.argv)
