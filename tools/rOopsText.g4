grammar rOopsText;

/*
 * Lexer Rules
 */
fragment DIGIT : [0-9] ;

COLON      : ',' ;
SEMI_COLON : ';' ;
ASSIGN     : '=' ;
BLOCK_BGN  : '{' ;
BLOCK_END  : '}' ;
LIST_BGN   : '[' ;
LIST_END   : ']' ;
MAP_BGN    : '<' ;
MAP_END    : '>' ;
NUMBER_VAL : [+-]? DIGIT+ ;
FLOAT_NUM  : [+-]? ( ( DIGIT+ '.' DIGIT* ) | ('.' DIGIT+) );
FLOAT_VAL  : FLOAT_NUM ([eE] FLOAT_NUM)? ;
BOOL_VAL   : 'TRUE' | 'FALSE' | 'True' | 'False' | 'true' | 'false';
WORD       : ~(' ' | [\n\r\t] | [{}[\]=,;#!"'])+ ;
STRING     : '"' ~["]+ '"' ;
CHARACTER  : '\'' ~[']+ '\'' ;
TYPENAME   : '!' WORD ;
ADDRESS    : '*' WORD ;
POINTER    : '&' WORD ;
COMMENT    : '#' ~[\n\r]+ ;
WHITESPACE : (' ' | [\n\r\t]) -> skip ;

/*
 * Parser Rules
 */

top_level  : ( (WORD | POINTER) '=' TYPENAME? value)+ ;
value      : block | list_value | map_value | STRING | NUMBER_VAL | FLOAT_VAL | CHARACTER | BOOL_VAL | ADDRESS;
block      : '{' (WORD '=' TYPENAME? value ';')+ '}' ;
list_value : '[' TYPENAME? value (',' TYPENAME? value)+ ']' ;
key_value_pair  : TYPENAME? value '=' TYPENAME? value;
map_value  : '<' key_value_pair (',' key_value_pair)+ '>' ;
