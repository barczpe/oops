/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
rOops Tutorial Program 1st
Structure having properies.
Non invasive property description.
*/

#include <oops/rPropInterface.h>
#include <iostream>

using namespace std;

namespace sTutEx_10
{

    struct AA
    {
        AA() = default;
        AA(long aLong, short aShort)
                : long_(aLong)
                , short_(aShort)
        {}

        bool operator!=(const AA &arR) const { return long_ != arR.long_ || short_ != arR.short_; }

        long long_ = 0;
        short short_ = 0;
    };

    struct BB : public AA
    {
        BB() = default;
        BB(unsigned char aByte, short aShort, long aLong)
                : AA(aLong, aShort)
                , byte_(aByte)
        {}

        bool operator!=(const BB &arR) const { return AA::operator!=(arR) || byte_ != arR.byte_; }

        unsigned char byte_ = 0;
    };

}


//rOOPS_DECLARE_TYPEINFO_CLASS(rPropTypeInfo_AA, sTutEx_10::AA)
using rPropTypeInfo_AA_BaseClass = rOops::CompoundTypeInfoSelector< sTutEx_10::AA, std::is_abstract<sTutEx_10::AA>::value, std::is_polymorphic<sTutEx_10::AA>::value, false >::TypeInfo_type;
template < class ClassT >
class rPropTypeInfo_AA : public rPropTypeInfo_AA_BaseClass
{
public:
    rPropTypeInfo_AA()
        : rPropTypeInfo_AA_BaseClass("sTutEx_10::AA")
    {
        createPropDescTable(this);
        addAlias(rOops::getTypeName<sTutEx_10::AA>());
    }
private:
    inline void createPropDescTable(rOops::rPropTypeInfoCompoundTypeBase* apTypeInfo);
    static const rOops::rPropTypeInfo* createPropTypeInfo()
    {
        static rPropTypeInfo_AA<sTutEx_10::AA> sTypeInfo;
        return &sTypeInfo;
    }
    static const rOops::rPropTypeInfo* _pTypeInfo;
    friend const rOops::rPropTypeInfo* rPropGetTypeInfo(const sTutEx_10::AA&);
    friend const rOops::rPropTypeInfo* rPropGetTypeInfo(const sTutEx_10::AA*);
};
template<class ClassT>
const rOops::rPropTypeInfo* rPropTypeInfo_AA<ClassT>::_pTypeInfo = rPropTypeInfo_AA<ClassT>::createPropTypeInfo();
const rOops::rPropTypeInfo* rPropGetTypeInfo(const sTutEx_10::AA&)
{
    rPropTypeInfo_AA<sTutEx_10::AA>::_pTypeInfo = rPropTypeInfo_AA<sTutEx_10::AA>::createPropTypeInfo();
    return rPropTypeInfo_AA<sTutEx_10::AA>::_pTypeInfo;
}
const rOops::rPropTypeInfo* rPropGetTypeInfo(const sTutEx_10::AA*)
{
    rPropTypeInfo_AA<sTutEx_10::AA>::_pTypeInfo = rPropTypeInfo_AA<sTutEx_10::AA>::createPropTypeInfo();
    return rPropTypeInfo_AA<sTutEx_10::AA>::_pTypeInfo;
}
template<class ClassT>
inline void rPropTypeInfo_AA<ClassT>::createPropDescTable(rOops::rPropTypeInfoCompoundTypeBase* apTypeInfo)
{
    using class_type = sTutEx_10::AA;
    rOOPS_PROPERTY(short_), "short";
    rOOPS_PROPERTY(long_), "long";
}


//rOOPS_DECLARE_TYPEINFO_CLASS(rPropTypeInfo_BB, sTutEx_10::BB)
using rPropTypeInfo_BB_BaseClass = rOops::CompoundTypeInfoSelector< sTutEx_10::BB, std::is_abstract<sTutEx_10::BB>::value, std::is_polymorphic<sTutEx_10::BB>::value, false >::TypeInfo_type;
template < class ClassT >
class rPropTypeInfo_BB : public rPropTypeInfo_BB_BaseClass
{
public:
    rPropTypeInfo_BB() : rPropTypeInfo_BB_BaseClass("sTutEx_10::BB")
    {
        createPropDescTable(this);
        addAlias(rOops::getTypeName<sTutEx_10::BB>());
    }
private:
    void createPropDescTable(rOops::rPropTypeInfoCompoundTypeBase* apTypeInfo);
    static const rOops::rPropTypeInfo* createPropTypeInfo()
    {
        static rPropTypeInfo_BB<sTutEx_10::BB> sTypeInfo;
        return &sTypeInfo;
    }
    static const rOops::rPropTypeInfo* _pTypeInfo;
    friend const rOops::rPropTypeInfo* rPropGetTypeInfo(const sTutEx_10::BB&);
    friend const rOops::rPropTypeInfo* rPropGetTypeInfo(const sTutEx_10::BB*);
};
template<class ClassT>
const rOops::rPropTypeInfo* rPropTypeInfo_BB<ClassT>::_pTypeInfo = rPropTypeInfo_BB<ClassT>::createPropTypeInfo();
const rOops::rPropTypeInfo* rPropGetTypeInfo(const sTutEx_10::BB&)
{
    rPropTypeInfo_BB<sTutEx_10::BB>::_pTypeInfo = rPropTypeInfo_BB<sTutEx_10::BB>::createPropTypeInfo();
    return rPropTypeInfo_BB<sTutEx_10::BB>::_pTypeInfo;
}
const rOops::rPropTypeInfo* rPropGetTypeInfo(const sTutEx_10::BB*)
{
    rPropTypeInfo_BB<sTutEx_10::BB>::_pTypeInfo = rPropTypeInfo_BB<sTutEx_10::BB>::createPropTypeInfo();
    return rPropTypeInfo_BB<sTutEx_10::BB>::_pTypeInfo;
}
// The Property Descriptor Table of 'BB' describes
// that 'AA' is the ancestor of 'BB' and
// defines one property.
template<class ClassT>
void rPropTypeInfo_BB<ClassT>::createPropDescTable(rOops::rPropTypeInfoCompoundTypeBase* apTypeInfo)
{
    using class_type = sTutEx_10::BB;
    //rOOPS_INHERIT(sTutEx_10::AA);
    apTypeInfo->inheritBaseClass(
        ::rPropGetTypeInfo(static_cast<const sTutEx_10::AA*>(nullptr)),
        rOops::getBaseClassOffset<class_type,sTutEx_10::AA>(),
        rOOPS_PROP_FLAG_DEFAULT);
    //rOOPS_PROPERTY(byte_), "byte";
    apTypeInfo->addPropDesc(
        ::rPropGetTypeInfo(&(static_cast<const class_type*>(nullptr)->byte_)),
        (std::is_pointer<decltype(class_type::byte_)>::value) ? std::make_shared<rOops::rApplyRawPointer>() : std::make_shared<rOops::rApplyPointer>(),
        "byte_", offsetof(class_type,byte_), rOOPS_PROP_FLAG_DEFAULT), "byte";
}

#include "sTutorial.h"

namespace sTutEx_10
{
    void Main()
    {
        cout << "\n>>> " << __FILE__ << " >>>\n";
        // Create an instance of struct 'AA'.
        AA A0(1, 2);
        AA A1, A2, A3;
        serializeObject("A0", A0, A1, A2, A3);
        if (A0 != A1) cout << "Error(" << incErrorCounter() << "): A0!=A1" << endl;
        if (A0 != A2) cout << "Error(" << incErrorCounter() << "): A0!=A2" << endl;
        if (A0 != A2) cout << "Error(" << incErrorCounter() << "): A0!=A3" << endl;

        // Create an instance of struct 'BB'.
        BB B0(100, 200, 300);
        BB B1, B2, B3;
        serializeObject("B0", B0, B1, B2, B3);
        if (B0 != B1) cout << "Error(" << incErrorCounter() << "): B0!=B1" << endl;
        if (B0 != B2) cout << "Error(" << incErrorCounter() << "): B0!=B2" << endl;
        if (B0 != B3) cout << "Error(" << incErrorCounter() << "): B0!=B3" << endl;
    } // Main()
}
