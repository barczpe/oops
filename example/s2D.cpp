/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
Complex example of using properties.
Implement 2D classes with property interface.
*/

#include <oops/rPropInterface.h>
#include "s2D.h"

namespace s2D {

// class s2DPoint
//================

s2DPoint::s2DPoint( int aX, int aY, int aSize )
    : s2D_i(aX,aY), size_(aSize)
{}

//virtual
s2DCoord_s s2DPoint::Center() const
{
    return s2DCoord_s( pos_.x, pos_.y );
}

//virtual
void
s2DPoint::setCenter( const s2DCoord_s &arCenter )
{
    pos_.x = arCenter.x;
    pos_.y = arCenter.y;
}

//virtual
s2D_i::ObjType_e
s2DPoint::GetVertex( int aIdx, s2DCoord_s &arPos )
{
    switch( aIdx ) {
        case 0: arPos = pos_; return e2DCircle; // center
        case 1: arPos.x = 1; arPos.y = 1; return e2DCircle; // radius
        default: return e2DEnd;
    } //switch
} //GetVertex()



// class s2DLine
//===============

s2DLine::s2DLine()
    :  s2D_i(0,0), B_(20,10)
{}

s2DLine::s2DLine( int aAx, int aAy, int aBx, int aBy )
    :  s2D_i(aAx,aAy), B_(aBx,aBy)
{}

//virtual
s2DCoord_s s2DLine::Center() const
{
    return s2DCoord_s( (B_.x + pos_.x) / 2, (B_.y + pos_.y) / 2 );
}

//virtual
void
s2DLine::setCenter( const s2DCoord_s &arCenter )
{
    int W = B_.x - pos_.x;
    int H = B_.y - pos_.y;
    pos_.x = arCenter.x - W/2;
    pos_.y = arCenter.y - H/2;
    B_.x = pos_.x + W;
    B_.y = pos_.y + H;
}

//virtual
s2D_i::ObjType_e
s2DLine::GetVertex( int aIdx, s2DCoord_s &arPos )
{
    switch( aIdx ) {
        case 0: arPos = pos_; return e2DLines; // A
        case 1: arPos = B_; return e2DLines; // B
        default: return e2DEnd;
    } //switch
} //GetVertex()



// class s2DRect
//===============

s2DRect::s2DRect()
    :  s2D_i(-10,10), LR_(10,-10)
{}

s2DRect::s2DRect( int aULx, int aULy, int aLRx, int aLRy )
    :  s2D_i(aULx,aULy), LR_(aLRx,aLRy)
{}

//virtual
s2DCoord_s s2DRect::Center() const
{
    return s2DCoord_s( (LR_.x + pos_.x) / 2, (LR_.y + pos_.y) / 2 );
}

//virtual
void
s2DRect::setCenter( const s2DCoord_s &arCenter )
{
    int W = LR_.x - pos_.x;
    int H = LR_.y - pos_.y;
    pos_.x = arCenter.x - W/2;
    pos_.y = arCenter.y - H/2;
    LR_.x = pos_.x + W;
    LR_.y = pos_.y + H;
}

//virtual
s2D_i::ObjType_e
s2DRect::GetVertex( int aIdx, s2DCoord_s &arPos )
{
    switch( aIdx ) {
        case 0: arPos = pos_; return e2DLines; // upper left
        case 1: arPos.x = pos_.x; arPos.y = LR_.y; return e2DLines; // lower left
        case 2: arPos = LR_; return e2DLines; // lower right
        case 3: arPos.x = LR_.x; arPos.y = pos_.y; return e2DLines; // upper right
        default: return e2DEnd;
    } //switch
} //GetVertex()

}
