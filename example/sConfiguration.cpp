/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
How to use Oops for handling configuration file and command line arguments.
 */

#include <oops/rPropInterface.h>
#include <oops/rOopsTextFormat.h>
#include <oops/rOopsTextParser.h>
#include <oops/rOopsSaveLoad.h>
#include <oops/rPropFind.h>
#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

struct sName
{
    std::string title{ "Dr." };
    std::string firstName{ "no-one" };
    std::string familyName{ "nobody" };
    rOOPS_ADD_PROPERTY_INTERFACE_NON_POLYMORPH(sName)
    {
        rOOPS_PROPERTY(title);
        rOOPS_PROPERTY(firstName);
        rOOPS_PROPERTY(familyName);
    }
};

struct sConfiguration
{
    sName name;
    unsigned id{ 0 };
    std::chrono::milliseconds timeout{ 0 };
    rOOPS_ADD_PROPERTY_INTERFACE_NON_POLYMORPH(sConfiguration)
    {
        rOOPS_PROPERTY(name);
        rOOPS_PROPERTY(id);
        rOOPS_PROPERTY(timeout);
    }
};

std::ostream& operator<<(std::ostream& o, const sName& name)
{
    o << name.title << ' ' << name.firstName << ' ' << name.familyName;
    return o;
}

std::ostream& operator<<(std::ostream& o, const sConfiguration& config)
{
    o << "name: " << config.name << " id: " << config.id << " timeout: " << config.timeout.count() << "ms";
    return o;
}

int
main( int argc, char *argv[] )
{
    if (2 == argc && (std::string("-h")==argv[1] || std::string("--help")==argv[1])) {
        std::cout << "Configuration example program.\nUsage:\n";
        std::cout << "  config_example <config_key=value> <config_key=value>\n";
        return 1;
    } //if

    sConfiguration config;
    // Print default config:
    std::cout << "Default: " << config << std::endl;

    // Load config file:
    std::string fileName = "SampleConfig.txt";
    std::ifstream istrm(fileName);
    if (istrm.good()) {
        rOops::rOopsTextParser parser(istrm, fileName);
        rOops::load(parser, config);
    }
    std::cout << "Loaded : " << config << std::endl;

    for (int idx=1; idx<argc; ++idx) {
        std::string arg = argv[idx];
        auto eq_pos = arg.find('=');
        if (std::string::npos != eq_pos) {
            std::string key = arg.substr(0, eq_pos);
            std::string val = arg.substr(eq_pos+1);
            auto iter = rPropGetTypeInfo(&config)->begin(&config, "config");
            if (!rOops::findPath(iter, key).isEnd()) {
                iter.setValue(val);
            }
        }
    }
    std::cout << "Final  : " << config << std::endl;
} //main()
