/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
 rOops Tutorial Program
 The simplest example.
 Class having properties.
 */

#include <oops/rOopsTextFormat.h>
#include <oops/rOopsTextParser.h>
#include <oops/rOopsSaveLoad.h>
#include <oops/rPropInterface.h>
#include <iostream>

using namespace std;

namespace sTut_01
{

    // Declaration of class 'A'.
    // They have simple data members,
    // and must have a default constructor.
    class A
    {
    public:
        virtual ~A() = default;
        A() = default;
        A(int i, std::string s)
            : i1(i), s1(std::move(s))
            {}
        bool operator!=( const A& rhs ) const
            { return i1!=rhs.i1 || s1!=rhs.s1; }
    private:
        int i1 = 0;
        std::string s1;
        rOOPS_ADD_PROPERTY_INTERFACE(sTut_01::A)
        {
            rOOPS_PROPERTY(i1);
            rOOPS_PROPERTY(s1);
        }
    };

    void Main()
    {
        std::cout << "\n>>> " << __FILE__ << " >>>\n";
        // Create an instance of struct 'A'.
        A a(1, "2");

        // Saving:
        // Create the string stream buffer.
        std::stringstream strm_txt2;
        // Create format object.
        rOops::rOopsTextFormat frmt(strm_txt2);
        // Save 'a' to the stream object.
        rOops::save(frmt, a, "a");
        // Now 'strm_txt2' contains the serialized version of 'a'. Print it to the standard output.
        std::cout << strm_txt2.str() << std::endl;

        // Loading:
        // Create a parser and an input stream object for loading properties to aCopy2.
        rOops::rOopsTextParser parser(strm_txt2, "a");
        // Load 'a2' from 'strm_txt2'.
        A a2;
        rOops::load(parser, a2);

        if (a != a2) {
            std::cerr << "Something is wrong: a != a2" << std::endl;
        }
    } // Main()

}
