/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
rOops Tutorial Program 6th.
Using STL associative containers:
 map, multi-map, unordered-map
*/

#include "sTut_06_map.h"
#include "sTutorial.h"

using namespace std;

namespace sTut_06
{
    void Main()
    {
        std::cout << "\n>>> " << __FILE__ << " >>>\n";
        for (int nn = 1; nn < 4; ++nn) {
            // Create an instance of class 'STLMaps'.
            STLMaps stl;
            stl.init( 10*nn );
            {
                // Yaml format does not support associative containers.
                // serializeObject() called with 2 destinations for saving only to binary and text formats.
                STLMaps stl1, stl2;
                serializeObject("stl", stl, stl1, stl2);
                // Check if we loaded back the same values.
                if (stl1.check(10*nn)) cout << "Error(" << incErrorCounter() << "): stl1 check failed." << endl;
                if (stl2.check(10*nn)) cout << "Error(" << incErrorCounter() << "): stl2 check failed." << endl;
            }
            cout << "=========================================" << endl;
        } //for
    }
}

