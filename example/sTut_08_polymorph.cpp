/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
Properties Tutorial Program 8th.
Using polymorph classes.
*/

#include "sTutorial.h"
#include "s2D.h"
#include <oops/rPropInterface.h>
#include <iostream>
#include <list>
#include <vector>

using namespace std;

namespace sTut_08
{
    typedef std::vector<s2D::s2D_i *> Vector_t;
    typedef std::list<s2D::s2D_i *> List_t;
}

// Implement the type info classes.
// Already defined as  s2DChildList_t in s2D.h
//rOOPS_DECLARE_STL_LIST_TYPE_INFO(sTut_08::Vector_t)
rOOPS_DECLARE_STL_LIST_TYPE_INFO(sTut_08::List_t)

namespace sTut_08
{
    class s2DApp
    {
    public:
        Vector_t ObjVector;
        List_t ObjList;
        virtual ~s2DApp() = default;
        s2DApp() = default;
        s2DApp(int aSize)
        {
            s2D::s2D_i* pObj = new s2D::s2DPoint(1, 2, aSize); ObjVector.push_back(pObj); ObjList.push_back(pObj);
            pObj = new s2D::s2DLine(3, 4, 5, 6);  ObjVector.push_back(pObj); ObjList.push_back(pObj);
            pObj = new s2D::s2DRect(7, 8, 9, 10);  ObjVector.push_back(pObj); ObjList.push_back(pObj);
            auto* pCntnr = new s2D::s2DCntnr(98, 99); ObjVector.push_back(pCntnr); ObjList.push_back(pCntnr);
            pObj = new s2D::s2DPoint(10, 20); pCntnr->add(pObj);
            pObj = new s2D::s2DLine(30, 40, 50, 60); pCntnr->add(pObj);
            pObj = new s2D::s2DRect(70, 80, 90, 100); pCntnr->add(pObj);
        }
        bool operator!=(const s2DApp& arR) const
        {
            if (ObjVector.size() != arR.ObjVector.size()) return true;
            if (ObjList.size() != arR.ObjList.size()) return true;
            auto iL = ObjVector.begin();
            auto iR = arR.ObjVector.begin();
            while (iL != ObjVector.end()) {
                if (**iL != **iR) return true;
                ++iL; ++iR;
            } //while
            return false;
        }
        rOOPS_ADD_PROPERTY_INTERFACE(sTut_08::s2DApp)
        {
            rOOPS_PROPERTY(ObjVector);
            rOOPS_PROPERTY(ObjList);
        }
    }; //class s2DApp

   void Main()
    {
        std::cout << "\n>>> " << __FILE__ << " >>>\n";
        // Create an instance of class 's2DApp'.
        s2DApp s2D(2);
        {
            s2DApp s2D1, s2D2, s2D3;
            serializeObject("s2D", s2D, s2D1, s2D2, s2D3);
            // Check if s2D is the same as s2Dx.
            if (s2D != s2D1) cout << "Error(" << incErrorCounter() << "): s2D!=s2D1" << endl;
            if (s2D != s2D2) cout << "Error(" << incErrorCounter() << "): s2D!=s2D2" << endl;
            if (s2D != s2D3) cout << "Error(" << incErrorCounter() << "): s2D!=s2D3" << endl;
        }
    } //Main()

} //namespace
