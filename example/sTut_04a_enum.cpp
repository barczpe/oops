/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2019:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
Properties Tutorial Program 4th.

The 4th tutorial program declares a simple date & time class
for demonstrating how to use enumerations and sub-structures.
(There is also another way for using sub-class or structures,
when they have their own type description and property table.)
*/

#include "sTut_04a_enum.h"
#include "sTutorial.h"
#include <oops/rPropFind.h>
#include <oops/rPropEnumStringTool.h>
#include <iostream>

using namespace std;

namespace sTut_04a
{

void Main()
{
    printf( ">>> %s >>>\n", __FILE__ );

    // Create an instance of class 'DateTime'.
    A::DateTime date_time_1;
    date_time_1.Date.Year = 1997;
    date_time_1.Date.Month = A::Month_e::eMay;
    date_time_1.Date.Day = 19;
    date_time_1.Date.WeekDay = A::eFriday;
    date_time_1.Time.Hour = 22;
    date_time_1.Time.Min = 33;
    date_time_1.Time.Sec = 44;
    {
        A::DateTime dt1, dt2, dt3;
        serializeObject("date_time_1", date_time_1, dt1, dt2, dt3);
        if (date_time_1 != dt1) cout << "Error(" << incErrorCounter() << "): date_time_1 != dt1" << endl;
        if (date_time_1 != dt2) cout << "Error(" << incErrorCounter() << "): date_time_1 != dt2" << endl;
        if (date_time_1 != dt3) cout << "Error(" << incErrorCounter() << "): date_time_1 != dt3" << endl;
    }

    cout << "=========================================" << endl;
    const auto* pTypeInfo = rPropGetTypeInfo(&date_time_1);
    rOops::rPropConstIterator iter = pTypeInfo->cbegin(&date_time_1, "date_time_1");
    while (!iter.isEnd()) {
        std::cout << iter.propName() << " = " << iter.value() << std::endl;
        ++iter;
    } //while

    std::cout << "\nFind 'Year' in class DateTime." << std::endl;
    iter = rOops::find1st(&date_time_1, "Year");
    if (!iter.isEnd()) {
        std::cout << iter.propName() << " = " << iter.value() << std::endl;
    }
    else {
        std::cout << iter.propName() << " not found. " << std::endl;
    }
} //main()

} //namespace
