#pragma once

/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
Complex example of using properties.
Header of 2D objects.

Define simple series of 2D classes
with property interface.
*/

#include <oops/rPropInterface.h>
#include <vector>

namespace s2D {

// Relative position.
struct s2DCoord_s
{
    int x, y;
    virtual ~s2DCoord_s() = default;
    explicit s2DCoord_s( int aX=0, int aY=0 )
        : x(aX), y(aY)
        {}
    bool operator==( const s2DCoord_s& arR ) const
        {
            return x==arR.x && y==arR.y;
        }
    rOOPS_ADD_PROPERTY_INTERFACE(s2DCoord_s)
        {
            rOOPS_PROPERTY(x), "x";
            rOOPS_PROPERTY(y), "y";
        }
};


typedef unsigned char rUInt8;


// Color info.
struct s2DColor_s
{
    rUInt8 R, G, B;
    virtual ~s2DColor_s() = default;
    explicit s2DColor_s( rUInt8 aRed=255, rUInt8 aGreen=255, rUInt8 aBlue=255 )
        : R(aRed), G(aGreen), B(aBlue)
        {}
    bool operator==( const s2DColor_s& arR ) const
        {
            return R==arR.R && G==arR.G && B==arR.B;
        }
    rOOPS_ADD_PROPERTY_INTERFACE(s2DColor_s)
        {
            rOOPS_PROPERTY(R), "R";
            rOOPS_PROPERTY(G), "G";
            rOOPS_PROPERTY(B), "B";
        }
};



// class s2D_i
//=============

class s2D_i
{
public:
    s2DCoord_s pos_;
    s2DColor_s color_;
public:
    explicit s2D_i( int aX=0, int aY=0, rUInt8 aRed=255, rUInt8 aGreen=255, rUInt8 aBlue=255 )
        : pos_(aX,aY), color_(aRed,aGreen,aBlue)
        {}
    virtual ~s2D_i() = default;
    void Pos( int aX, int aY )
        { pos_.x = aX; pos_.y = aY; }
    void Pos( s2DCoord_s aPos )
        { pos_ = aPos; }
    const s2DCoord_s &Pos() const
        { return pos_; }
    void Color( rUInt8 aRed, rUInt8 aGreen, rUInt8 aBlue )
        { color_.R = aRed; color_.G = aGreen; color_.B = aBlue; }
    void Color( s2DColor_s aColor )
        { color_ = aColor; }
    const s2DColor_s &Color() const
        { return color_; }
    // Center:
    virtual s2DCoord_s Center() const
        { return pos_; }
    virtual void setCenter( const s2DCoord_s& arCenter )
        { pos_ = arCenter; }
    // Container:
    virtual s2D_i* GetChild( int /*aIdx*/ )
        { return nullptr; }
    // Drawing:
    enum ObjType_e {
        e2DEnd = 0,
        e2DCntnr,
        e2DLines,
        e2DCircle,
        e2DEllipse,
    };
    virtual ObjType_e GetVertex( int aIdx, s2DCoord_s &arPos ) = 0;
    bool operator==( const s2D_i& arR ) const
    {
        return pos_==arR.pos_ && color_==arR.color_;
    }
    bool operator!=(const s2D_i& arR) const
    {
        return !operator==(arR);
    }
    rOOPS_ADD_PROPERTY_INTERFACE_ABSTRACT(s2D_i)
    {
        rOOPS_PROPERTY(pos_), "Pos";
        rOOPS_PROPERTY(color_), "Color";
        //rOOPS_PROPERTY_SET_GET_FUNC(s2DCoord_s,setCenter,Center), "Center";
    }
}; //class s2D_i

using s2DChildList_t = std::vector<s2D_i*>;

}

rOOPS_DECLARE_STL_LIST_TYPE_INFO(s2D::s2DChildList_t)


namespace s2D {

// class s2DCntnr
//================

class s2DCntnr : public s2D_i
{
protected:
    s2DChildList_t childList_;
public:
    explicit s2DCntnr( int aX=0, int aY=0 )
        : s2D_i(aX,aY)
        {}
    ~s2DCntnr() override = default;
    void add( s2D_i *apChild )
        { childList_.push_back(apChild); }
    virtual s2D_i* getChild( unsigned int aIdx )
        { return (aIdx<childList_.size()) ? childList_[aIdx] : nullptr; }
    // Drawing:
    ObjType_e GetVertex( int /*aIdx*/, s2DCoord_s &arPos ) override
        { arPos = pos_; return e2DCntnr; }
    bool operator==(const s2DCntnr& arR) const
    {
        if (!s2D_i::operator==(arR)) return false;
        if (childList_.size()!=arR.childList_.size()) return false;
        return std::equal( childList_.begin(), childList_.end(), arR.childList_.begin(), [](const s2D_i* pL, const s2D_i* pR)->bool { return *pL==*pR; } );
    }
    rOOPS_ADD_PROPERTY_INTERFACE(s2DCntnr)
    {
        rOOPS_INHERIT(s2D_i);
        rOOPS_PROPERTY(childList_), "ChildList";
    }
}; //class s2DCntnr


// class s2DPoint
//================

class s2DPoint : public s2D_i
{
protected:
    int size_;
public:
    explicit s2DPoint( int aX=0, int aY=0, int aSize=1 );
    // Center:
    s2DCoord_s Center() const override;
    void setCenter( const s2DCoord_s &arCenter ) override;
    // Drawing:
    ObjType_e GetVertex( int aIdx, s2DCoord_s &arPos ) override;
    bool operator==(const s2DPoint& arR) const
    {
        return s2D_i::operator==(arR) && size_==arR.size_;
    }
    rOOPS_ADD_PROPERTY_INTERFACE(s2DPoint)
    {
        rOOPS_INHERIT(s2D_i);
        rOOPS_PROPERTY(size_), "Size";
    }
}; //class s2DPoint


// class s2DLine
//===============

class s2DLine : public s2D_i
{
protected:
    // Line described with points A and B. A stored in 'pos_'.
    s2DCoord_s B_;
public:
    s2DLine();
    s2DLine( int aAx, int aAy, int aBx, int aBy );
    // Center:
    s2DCoord_s Center() const override;
    void setCenter( const s2DCoord_s &arCenter ) override;
    // Drawing:
    ObjType_e GetVertex( int aIdx, s2DCoord_s &arPos ) override;
    bool operator==(const s2DLine& arR) const
    {
        return pos_==arR.pos_ && B_==arR.B_ && color_==arR.color_;
    }
    rOOPS_ADD_PROPERTY_INTERFACE(s2DLine)
    {
        // Instead of inheriting pos_ from s2D_i, define a new property called A refering to pos_.
        rOOPS_PROPERTY(pos_), "A"; // Rename pos_ to A.
        rOOPS_PROPERTY(B_), "B";
        rOOPS_PROPERTY(color_), "Color";
    }
}; //class s2DLine



// class s2DRect
//===============

class s2DRect : public s2D_i
{
protected:
    s2DCoord_s LR_;
public:
    s2DRect();
    s2DRect( int aULx, int aULy, int aLRx, int aLRy );
    // Center:
    s2DCoord_s Center() const override;
    void setCenter( const s2DCoord_s &arCenter ) override;
    // Drawing:
    ObjType_e GetVertex( int aIdx, s2DCoord_s &arPos ) override;
    bool operator==(const s2DRect& arR) const
    {
        return pos_==arR.pos_ && LR_==arR.LR_;
    }
    rOOPS_ADD_PROPERTY_INTERFACE(s2DRect)
    {
        //rOOPS_INHERIT( s2D_i );
        rOOPS_PROPERTY(pos_), "UL"; // Rename pos_ to UL.
        rOOPS_PROPERTY(LR_), "LR";
        rOOPS_PROPERTY(color_), "Color";
    }
}; //class s2DRect

}
