/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
rOops Tutorial Program 6th.
Using STL containers.
*/

#include "sTutorial.h"
#include <oops/rPropInterface.h>
#include <algorithm>
#include <iostream>
#include <string>
#include <map>

using namespace std;

namespace sTut_06
{
    using StringMap = std::map<std::string, int>;
}

rOOPS_DECLARE_ASSOCIATIVE_CONTAINER_TYPE_INFO(sTut_06::StringMap)

namespace sTut_06
{
    static constexpr const char* str_tbl[] = { "aaa","bbb","ccc","ddd","eee","fff", };

    // Declaration of class 'STLMaps' with STLMaps container type properties.
    class STLMaps
    {
    public:
        virtual ~STLMaps() = default;
        void init(int /*aVal*/)
        {
            for (int idx = 0; idx < 5; ++idx) {
                str2Int_[std::to_string(idx)] = idx;
            } //for idx
        }
        bool check(int /*aVal*/) const
        {
            if (5 != str2Int_.size()) return true;
            for (int idx = 0; idx < 5; ++idx) {
                if( idx != str2Int_.at(std::to_string(idx)) ) return true;
            } //for idx
            return false;
        }
        bool operator==(const STLMaps& arR) const
        {
            if (str2Int_.size()     != arR.str2Int_.size()    ) return false;

            if (str2Int_     != arR.str2Int_    ) return false;
            return true;
        }
    protected:
        StringMap str2Int_;
        // Declare property interface.
        rOOPS_ADD_PROPERTY_INTERFACE(sTut_06::STLMaps)
        {
            rOOPS_PROPERTY(str2Int_), "Str2Int";
        }
    }; //class STLMaps

} //namespace
