#pragma once

/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rPropInterface.h>

/**
rOops Tutorial Program 1st
Using non-invasive type description.
*/

// Declaration of struct 'AA' and 'BB'.
// They have simple integer data members.
// 'A' and 'B' must have a default constructor.

namespace sTut_10
{

    struct AA
    {
        AA() = default;
        AA(long aLong, short aShort)
                : long_(aLong)
                , short_(aShort)
        {}

        bool operator!=(const AA &arR) const { return long_ != arR.long_ || short_ != arR.short_; }

        long long_ = 0;
        short short_ = 0;
    };

    struct BB : public AA
    {
        BB() = default;
        BB(unsigned char aByte, short aShort, long aLong)
                : AA(aLong, aShort)
                , byte_(aByte)
        {}

        bool operator!=(const BB &arR) const { return AA::operator!=(arR) || byte_ != arR.byte_; }

        unsigned char byte_ = 0;
    };

}

// The Property Descriptor Table of 'B' describes
// that 'A' is the ancestor of 'B' and
// defines one property.
rOOPS_DECLARE_TYPEINFO_CLASS(PropInfo_sTut_10_A, sTut_10::AA)
{
    using class_type = sTut_10::BB;
    rOOPS_PROPERTY(long_), "long";
    rOOPS_PROPERTY(short_), "short";
}

// The Property Descriptor Table of 'B' describes
// that 'A' is the ancestor of 'B' and
// defines one property.
rOOPS_DECLARE_TYPEINFO_CLASS(PropInfo_sTut_10_B, sTut_10::BB)
{
    using class_type = sTut_10::BB;
    rOOPS_INHERIT(sTut_10::AA);
    rOOPS_PROPERTY(byte_), "byte";
}
