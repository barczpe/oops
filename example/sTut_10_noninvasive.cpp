/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
rOops Tutorial Program 1st
Using non-invasive type description.
*/

#include "sTut_10_noninvasive.h"
#include <iostream>
#include "sTutorial.h"

using namespace std;

namespace sTut_10
{

    void Main()
    {
        cout << "\n>>> " << __FILE__ << " >>>\n";

        // Serializing an object using rOops format and stream objects.
        AA A0( 3, 4 ), A1, A2, A3;
        serializeObject("A", A0, A1, A2, A3);
        if( A0 != A1 ) cout << "Error(" << incErrorCounter() << "): A0!=A1" << endl;
        if( A0 != A2 ) cout << "Error(" << incErrorCounter() << "): A0!=A2" << endl;
        if( A0 != A3 ) cout << "Error(" << incErrorCounter() << "): A0!=A3" << endl;

        BB B0( 30, 40, 50 ), B1, B2, B3;
        serializeObject("B", B0, B1, B2, B3);
        if( B0 != B1 ) cout << "Error(" << incErrorCounter() << "): B0!=B1" << endl;
        if( B0 != B2 ) cout << "Error(" << incErrorCounter() << "): B0!=B2" << endl;
        if( B0 != B3 ) cout << "Error(" << incErrorCounter() << "): B0!=B3" << endl;
    } //Main()

}


