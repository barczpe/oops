/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
Properties Tutorial Program 4th.

The 4th tutorial program declares a simple date & time class
for demonstrating how to use enumerations and sub-structures.
(There is also another way for using sub-class or structures,
when they have their own type description and property table.)
*/


#include "sTutorial.h"
#include <oops/rPropInterface.h>
#include <oops/rPropFind.h>
#include <oops/Enum2StringTool.h>
#include <chrono>
#include <iostream>
#include <vector>

using namespace std;

// Month of the year:
//--------------------

namespace B
{

DECLARE_ENUM_CLASS(Months, int32_t,
    eJanuary = 1,
    eFebruary,
    eMarch,
    eApril,
    eMay,
    eJune,
    eJuly,
    eAugust,
    eSeptember,
    eOctober,
    eNovember,
    eDecember
);

DECLARE_ENUM_CLASS(Days, int32_t,
    eMonday = 1,
    eTuesday,
    eWednesday,
    eThursday,
    eFriday,
    eSaturday,
    eSunday
);

}

rOOPS_DECLARE_ENUM_TYPE_INFO( B::Months, B::Months2String )
rOOPS_DECLARE_ENUM_TYPE_INFO( B::Days, B::Days2String )

namespace B
{
    class DateTime
    {
    public:
        virtual ~DateTime() = default;
        struct Date_s
        {
            int Year{ 2018 };
            Months Month{ Months::eJanuary };
            int Day{ 1 };
            Days WeekDay{ Days::eMonday };
        } Date; //struct Date_s
        struct Time_s
        {
            int Hour{ 1 };
            int Min{ 0 };
            int Sec{ 0 };
        } Time; //struct Time_s
        std::chrono::nanoseconds interval_ns{ 123456789 };
        std::chrono::seconds interval_s{ 123 };
        bool operator!=(const DateTime& arR) const
        {
            return Date.Year != arR.Date.Year
                || Date.Month != arR.Date.Month
                || Date.Day != arR.Date.Day
                || Date.WeekDay != arR.Date.WeekDay
                || Time.Hour != arR.Time.Hour
                || Time.Min != arR.Time.Min
                || Time.Sec != arR.Time.Sec
                || interval_ns != arR.interval_ns
                || interval_s != arR.interval_s
            ;
        }
        bool operator==(const DateTime& arR) const
        {
            return !operator!=(arR);
        }
        rOOPS_ADD_PROPERTY_INTERFACE(B::DateTime)
        {
            rOOPS_PROPERTY(Date.Year), "Year";
            rOOPS_PROPERTY(Date.Month), "Month";
            rOOPS_PROPERTY(Date.Day), "Day";
            rOOPS_PROPERTY(Date.WeekDay), "WeekDay";
            rOOPS_PROPERTY(Time.Hour), "Hour";
            rOOPS_PROPERTY(Time.Min), "Min";
            rOOPS_PROPERTY(Time.Sec), "Sec";
            rOOPS_PROPERTY(interval_ns);
            rOOPS_PROPERTY(interval_s);
        }
    }; //class DateTime
}
