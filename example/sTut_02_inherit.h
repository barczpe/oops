#pragma once

/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
rOops Tutorial Program 2nd
Inheritance

Two simple classes A and B.
B is the descendant of class A.
*/

#include <oops/rPropInterface.h>
#include <cmath>

namespace sTut_02
{
    namespace InternalA
    {
        // Declaration of class 'A' with two properties:
        // one 32 and one 16-bit signed integer.
        // The properties are protected data members of class A.
        // Predefined, platform independent types are used.
        class A
        {
        public:
            virtual ~A() = default;
            A() = default;
            A(int32_t a32, int16_t a16)
                : s32_(a32), s16_(a16)
            {}
            bool operator==(const A& arR) const
            {
                return s32_ == arR.s32_ && s16_ == arR.s16_;
            }
            bool operator!=(const A& arR) const
            {
                return !operator==(arR);
            }
        protected: // or private:
            int32_t s32_{ 0 };
            int16_t s16_{ 0 };
            rOOPS_ADD_PROPERTY_INTERFACE(InternalA::A)
            {
                rOOPS_PROPERTY(s32_);
                rOOPS_PROPERTY(s16_);
            }
        }; //class A
    }

    namespace InternalB
    {
        // Declaration of class 'B'.
        // It is derived from class 'A', and has some new properties:
        // the 'str_', 'f4_' and 'f8_' protected data members.
        class B : public InternalA::A
        {
        public:
            ~B() override = default;
            B() = default;
            B(int32_t a32, int16_t a16)
                : InternalA::A(-a32, -a16)
                , enabled_(true)
                , f4_(a32 / 3.0f)
                , f8_(a16 / 3.0)
                , str_("sTut_02")
            {}
            bool operator==(const B& arR) const
            {
                return InternalA::A::operator==(arR) && enabled_ == arR.enabled_ && 0.001 > std::fabs(f4_ - arR.f4_) && 0.001 > std::fabs(f8_ - arR.f8_);
            }
            bool operator!=(const B& arR) const
            {
                return !operator==(arR);
            }
        protected: // or private:
            bool enabled_{ false };
            float f4_{ 0.0f };
            double f8_{ 0.0 };
            std::string str_;
            rOOPS_ADD_PROPERTY_INTERFACE(InternalB::B)
            {
                rOOPS_INHERIT(InternalA::A);
                rOOPS_PROPERTY(enabled_), "enabled";
                rOOPS_PROPERTY(f4_), "f4";
                rOOPS_PROPERTY(f8_), "f8";
                rOOPS_PROPERTY(str_), "str";
            }
        }; //class B
    } //namespace InternalB
} //namespace sTut_02
