#pragma once

/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

#include <oops/rPropInterface.h>
#include <oops/rOopsYamlFormat.h>
#include <oops/rOopsTextFormat.h>
#include <oops/rOopsBinaryFormat.h>
#include <oops/rOopsBinaryParser.h>
#include <oops/rOopsYamlParser.h>
#include <oops/rOopsTextParser.h>
#include <oops/rOopsSaveLoad.h>
#include <sstream>


namespace sTut_01 { void Main(); }
namespace sTut_02 { void Main(); }
namespace sTut_03 { void Main(); }
namespace sTut_04a { void Main(); }
namespace sTut_04b { void Main(); }
namespace sTut_05 { void Main(); }
namespace sTut_06 { void Main(); }
namespace sTut_07 { void Main(); }
namespace sTut_08 { void Main(); }
namespace sTut_09 { void Main(); }
namespace sTut_10 { void Main(); }
namespace sTutEx_10 { void Main(); }

/**
 * Increment the static error counter.
 * @return the incremented value of the counter.
 */
int incErrorCounter();

/**
 * Get the static error counter.
 * @return the value of the static error counter.
 */
int getErrorCounter();

/**
 * Set the static error counter to 0.
 */
void clearErrorCounter();

/**
Serialize and deserialize the object using different formats.
*/
template < typename ObjT >
inline void serializeObject(const char* aName, const ObjT& aObj, ObjT& aCopyBin, ObjT& aCopyText)
{
    std::cout << "\n=== Serializing an object using rOops binary format. ===" << std::endl;
    try {
        // Create the string stream buffer.
        std::stringstream strm_bin;
        // Create format object.
        rOops::rOopsBinaryFormat frmt(strm_bin);
        // Save 'aObj' to the stream object.
        rOops::save(frmt, aObj, aName);
        // Now 'strm_bin' contains the serialized version of 'aObj'. Print it to the standard output.
        rOops::rOopsBinaryPrinter printer;
        std::cout << printer.printOopsBinaryFile(strm_bin) << std::endl;
        // Create a parser and an input stream object for loading properties to aCopyBin.
        rOops::rOopsBinaryParser parser(strm_bin, aName);
        // Load 'aCopyBin' from 'strm_bin'.
        load(parser, aCopyBin);
    }
    catch (std::exception& e) {
        std::cout << "Exception: " << e.what() << std::endl;
    }

    std::cout << "\n=== Serializing an object using rOops text format. ===" << std::endl;
    try {
        // Create the string stream buffer.
        std::stringstream strm_txt;
        // Create format object.
        rOops::rOopsTextFormat frmt(strm_txt);
        // Save 'aObj' to the stream object.
        rOops::save(frmt, aObj, aName);
        // Now 'strm_txt' contains the serialized version of 'aObj'. Print it to the standard output.
        std::cout << strm_txt.str() << std::endl;
        // Create a parser and an input stream object for loading properties to aCopyText.
        rOops::rOopsTextParser parser(strm_txt, aName);
        // Load 'aCopyText' from 'strm_txt'.
        load(parser, aCopyText);
    }
    catch (std::exception& e) {
        std::cout << "Exception: " << e.what() << std::endl;
    }
} //serializeObject()

template < typename ObjT >
inline void serializeObject(const char* aName, const ObjT& aObj, ObjT& aCopyBin, ObjT& aCopyText, ObjT& aCopyYaml)
{
    serializeObject(aName, aObj, aCopyBin, aCopyText);
    std::cout << "\n=== Serializing an object using Yaml format. ===" << std::endl;
    try{
        std::stringstream yaml_strm;
        rOops::rOopsYamlFormat yaml_frmt(yaml_strm);
        save(yaml_frmt, aObj, aName);
        std::cout << yaml_strm.str() << std::endl;
        rOops::rOopsYamlParser parser(yaml_strm, aName);
        load(parser, aCopyYaml);
    }
    catch (std::exception& e) {
        std::cout << "Exception: " << e.what() << std::endl;
    }
} //serializeObject()
