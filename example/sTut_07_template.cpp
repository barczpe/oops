/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
Properties Tutorial Program 7th.
Classes having template type properties.
*/

#include <oops/rPropInterface.h>
#include "sTutorial.h"

// Declaration of template class 'Point<>' with two properties.

template<class T_m>
class Point
{
public:
    virtual ~Point() = default;
    Point()
        : _x(0), _y(0) {};
    Point( T_m aX, T_m aY )
        : _x(aX), _y(aY) {};
    bool operator!=(const Point<T_m>& arP) const
    {
        return (_x!=arP._x) || (_y != arP._y);
    }
protected:
    T_m _x, _y;
    rOOPS_ADD_PROPERTY_INTERFACE(Point<T_m>)
    {
        rOOPS_PROPERTY(_x);
        rOOPS_PROPERTY(_y);
    }
}; //class Point<>

// Instantiate 'Point<>' template class with 'rSInt32'.

// The following typedef gives unique name for 'rPoint<>' instance.
using PointInt32 = Point<std::int32_t>;

// Instantiate 'Point<>' template class with 'rSInt16'.
using PointInt16 = Point<std::int16_t>;


class Rect
{
public:
    virtual ~Rect() = default;
    Rect()
        : UL_(0,0), LR_(0,0) {};
    Rect( int aULx, int aULy, int aLRx, int aLRy )
        : UL_(aULx,aULy), LR_(aLRx,aLRy) {};
    bool operator!=(const Rect& arR) const
    {
        return (UL_ != arR.UL_) || (LR_ != arR.LR_);
    }
protected: // or private:
    PointInt16 UL_;
    PointInt32 LR_;
    rOOPS_ADD_PROPERTY_INTERFACE(Rect)
    {
        rOOPS_PROPERTY(UL_);
        rOOPS_PROPERTY(LR_);
    }
}; //class Rect


namespace sTut_07
{

    void Main()
    {
        std::cout << "\n>>> " << __FILE__ << " >>>\n";

        // Create an instance of class 'Point<>'.
        PointInt32 P32(1, 2);
        PointInt32 P32_1, P32_2, P32_3;
        serializeObject("P32", P32, P32_1, P32_2, P32_3);
        if (P32 != P32_1) std::cout << "Error(" << incErrorCounter() << "): P32!=P32_1" << std::endl;
        if (P32 != P32_2) std::cout << "Error(" << incErrorCounter() << "): P32!=P32_2" << std::endl;
        if (P32 != P32_3) std::cout << "Error(" << incErrorCounter() << "): P32!=P32_3" << std::endl;

        // Create an instance of class 'Point<>'.
        PointInt16 P16(10, 20);
        PointInt16 P16_1, P16_2, P16_3;
        serializeObject("P16", P16, P16_1, P16_2, P16_3);
        if (P16 != P16_1) std::cout << "Error(" << incErrorCounter() << "): P16!=P16_1" << std::endl;
        if (P16 != P16_2) std::cout << "Error(" << incErrorCounter() << "): P16!=P16_2" << std::endl;
        if (P16 != P16_3) std::cout << "Error(" << incErrorCounter() << "): P16!=P16_3" << std::endl;

        Rect R(-10, -20, 300, 400);
        Rect R_1, R_2, R_3;
        serializeObject("R", R, R_1, R_2, R_3);
        if (R != R_1) std::cout << "Error(" << incErrorCounter() << "): R!=R_1" << std::endl;
        if (R != R_2) std::cout << "Error(" << incErrorCounter() << "): R!=R_2" << std::endl;
        if (R != R_3) std::cout << "Error(" << incErrorCounter() << "): R!=R_3" << std::endl;
    } // Main()

} //namespace sTut_07
