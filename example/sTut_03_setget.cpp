/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
Properties Tutorial Program 3rd.

Example of member function properties.
Rectangle class has gate functions used as properties:
    Height()
    Width()
    setCenter(), Center()
*/

#include "sTutorial.h"
#include <oops/rPropInterface.h>
#include <iostream>

using namespace std;


class rProp_SetGetFunctor_i
{
public:
    virtual void set() = 0;
    virtual void get() = 0;
}; //class rProp_SetGetFunctor_i

template < typename CompoundT, typename MemberT >
class rProp_SetGetFunctor : public rProp_SetGetFunctor_i
{
public:
    typedef void (CompoundT::*SetMemberFuncPtr_t)(MemberT);
    typedef MemberT (CompoundT::*GetMemberFuncPtr_t)() const;
    void set() override { _pThis->*_pSetMemberFunc(_val); }
    void get() override { _val = _pThis->*_pGetMemberFunc(); }
protected:
    CompoundT* _pThis;
    MemberT _val;
    SetMemberFuncPtr_t _pSetMemberFunc;
    GetMemberFuncPtr_t _pGetMemberFunc;
//rOOPS_ADD_PROPERTY_INTERFACE(rProp_SetGetFunctor);
public:
    virtual const rOops::rPropTypeInfo* OOPSGetTypeInfo() const;
    static void createPropDescTableStaticImplementation( rOops::rPropTypeInfoCompoundTypeBase* apTypeInfo,const rProp_SetGetFunctor<CompoundT,MemberT>* apClass);
}; //class rProp_SetGetFunctor

////rOOPS_DECLARE_TYPEINFO(rProp_SetGetFunctor)
//    template < typename CompoundT, typename MemberT >
//    const rOops::rPropTypeInfo* rPropGetTypeInfo_impl(const rProp_SetGetFunctor<CompoundT,MemberT>*);
//    template < typename CompoundT, typename MemberT >
//    inline const rOops::rPropTypeInfo* rPropGetTypeInfo(const rProp_SetGetFunctor<CompoundT,MemberT>* aVal)
//        { return rPropGetTypeInfo_impl(aVal); }

//rOOPS_IMPLEMENT_PROP_INTERFACE(rProp_SetGetFunctor)
//    rOOPS_PROPERTY( _val );
//rOOPS_END_OF_IMPLEMENT
    template < typename CompoundT, typename MemberT >
    const rOops::rPropTypeInfo* rProp_SetGetFunctor<CompoundT,MemberT>::OOPSGetTypeInfo() const
        { return rPropGetTypeInfo(this); }
    template < typename CompoundT, typename MemberT >
    const rOops::rPropTypeInfo* rPropGetTypeInfo_impl(const rProp_SetGetFunctor<CompoundT,MemberT>*)
        {
            using TemplateType_t = rProp_SetGetFunctor<CompoundT,MemberT>;
            using TypeInfo_t = typename rOops::CompoundTypeInfoSelector< TemplateType_t,
                std::is_abstract<TemplateType_t>::value, std::is_polymorphic<TemplateType_t>::value, true >::TypeInfo_type;
            static TypeInfo_t sTypeInfo("rProp_SetGetFunctor<>");
            return &sTypeInfo;
        }
    template < typename CompoundT, typename MemberT >
    void createPropDescTableStaticImplementation(rOops::rPropTypeInfoCompoundTypeBase* apTypeInfo,const rProp_SetGetFunctor<CompoundT,MemberT>* apClass)
        {
            typedef rProp_SetGetFunctor<CompoundT,MemberT> class_type;
            apTypeInfo->addPropDesc(
                rPropGetTypeInfo((MemberT*)nullptr),
                std::make_shared<rOops::rApplyRawPointer>(),
                "value",
                offsetof(class_type, _val),
                rOOPS_PROP_FLAG_DEFAULT
            );
        }


// Header
namespace sTut_03
{
    // Declaration of class 'Point'.
    class Point
    {
    public:
        virtual ~Point() = default;
        Point() = default;
        Point(int aX, int aY)
            : x(aX), y(aY)
        {}
        int x{0};
        int y{0};
        bool operator!=(const Point& arR) const
        {
            return x != arR.x || y != arR.y;
        }
        rOOPS_ADD_PROPERTY_INTERFACE(sTut_03::Point)
        {
            rOOPS_PROPERTY(x);
            rOOPS_PROPERTY(y);
        }
    }; //class Point


    // Declaration of class 'Rect'
    // with two artificial properties: height and width.
    class Rect
    {
    protected:
        Point UL; // upper - left
        Point LR; // lower - right
    public:
        virtual ~Rect() = default;
        Rect() = default;
        Rect(int aULx, int aULy, int aLRx, int aLRy)
            : UL(aULx, aULy), LR(aLRx, aLRy)
        {}
        // Width:
        virtual int Width() const
        {
            return(LR.x - UL.x);
        }
        virtual void setWidth(int aWidth)
        {
            LR.x = UL.x + aWidth;
        }
        // Height:
        virtual int Height() const
        {
            return LR.y - UL.y;
        }
        virtual void setHeight(int arHeight)
        {
            LR.y = UL.y + arHeight;
        }
        // Center:
        virtual Point Center() const
        {
            Point point;
            point.x = (LR.x + UL.x) / 2;
            point.y = (LR.y + UL.y) / 2;
            return point;
        }
        virtual void Center(Point& arCenter) const
        {
            arCenter.x = (LR.x + UL.x) / 2;
            arCenter.y = (LR.y + UL.y) / 2;
        }
        virtual void setCenter(const Point& arCenter)
        {
            int w = LR.x - UL.x;
            int h = LR.y - UL.y;
            UL.x = arCenter.x - w / 2;
            UL.y = arCenter.y - h / 2;
            LR.x = UL.x + w;
            LR.y = UL.y + h;
        }
        bool operator!=(const Rect& arR) const
        {
            return UL != arR.UL || LR != arR.LR;
        }
        rOOPS_ADD_PROPERTY_INTERFACE(sTut_03::Rect)
        {
            rOOPS_PROPERTY(UL);
            rOOPS_PROPERTY(LR);
            // TODO: Make it work.
            //rOOPS_PROPERTY_SET_GET_FUNC( int, setWidth, Width );
            //rOOPS_PROPERTY_SET_GET_FUNC( int, setHeight, Height );
            //rOOPS_PROPERTY_SET_GET_FUNC2( sTut_03::Point, setCenter, getCenter ), "Center", rOOPS_PROP_FLAG_DEFAULT;
        }
    }; //class Rect


    // Declaration of class 'WRect'.
    // It is derived from class 'Rect',
    // and has a new property: 'Thickness'.
    class WRect : public Rect
    {
    protected:
        int Thickness;
    public:
        ~WRect() override = default;
        WRect()
            : Rect(), Thickness(1)
        {}
        WRect(int aThickness, int aULx, int aULy, int aLRx, int aLRy)
            : Rect(aULx, aULy, aLRx, aLRy), Thickness(aThickness)
        {}
        // Width:
        int Width() const override
        {
            return(LR.x - UL.x + Thickness);
        }
        void setWidth(int arWidth) override
        {
            LR.x = UL.x + arWidth - Thickness;
        }
        // Height:
        int Height() const override
        {
            return(LR.y - UL.y + Thickness);
        }
        void setHeight(int aHeight) override
        {
            LR.y = UL.y + aHeight - Thickness;
        }
        bool operator!=(const WRect& arR) const
        {
            return Rect::operator!=(arR) || Thickness != arR.Thickness;
        }
        rOOPS_ADD_PROPERTY_INTERFACE(sTut_03::WRect)
        {
            rOOPS_INHERIT(sTut_03::Rect);
            rOOPS_PROPERTY(Thickness), "Thickness", rOOPS_PROP_FLAG_DEFAULT;
        }
    }; //class WRect
}


namespace sTut_03
{
    struct RectPair
    {
    protected:
        Rect *pR1 = nullptr;
        Rect *pR2 = nullptr;
    public:
        RectPair() = default;
        explicit RectPair(int aBase)
            : pR1(nullptr), pR2(nullptr)
        {
            // Create an instance of class 'Rect'.
            pR1 = new Rect(aBase, 2 * aBase, 3 * aBase, 4 * aBase);
            // Create an instance of class 'WRect'.
            pR2 = new WRect(5 * aBase, 10 * aBase, 20 * aBase, 30 * aBase, 40 * aBase);
        }
        virtual ~RectPair()
        {
            delete pR1;
            delete pR2;
        }
        bool operator!=(const RectPair& arR) const
        {
            return (*pR1) != (*arR.pR1) || (*pR2) != (*arR.pR2);
        }
        rOOPS_ADD_PROPERTY_INTERFACE(sTut_03::RectPair)
        {
            rOOPS_PROPERTY(pR1);
            rOOPS_PROPERTY(pR2);
        }
    }; //struct RectPair


    void Main()
    {
        printf( "\n>>> %s >>>\n", __FILE__ );

        // Serializing an object using the type descriptor.
        // Create an instance of 'RectPair'.
        RectPair Pair1(10), Pair2, Pair3, Pair4;
        serializeObject("Pair1", Pair1, Pair2, Pair3, Pair4);
        if( Pair1 != Pair2 ) cout << "Error(" << incErrorCounter() << "): Pair1!=Pair2" << endl;
        if( Pair1 != Pair3 ) cout << "Error(" << incErrorCounter() << "): Pair1!=Pair3" << endl;
        if( Pair1 != Pair4 ) cout << "Error(" << incErrorCounter() << "): Pair1!=Pair4" << endl;
    } //Main()

} //namespace sTut_03
