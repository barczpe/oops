/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
Main of rOops example programs.
All examples built with this main,
and can be executed by passing
the number of the tutorial in the command line.
*/

#include "sTutorial.h"
#include <oops/rPropLog.h>
#include <exception>
#include <fstream>

namespace {
    int s_error_counter = 0;
}

int incErrorCounter()
{
    return ++s_error_counter;
}
int getErrorCounter()
{
    return s_error_counter;
}

void sTutorialLogger(int aLogLevel, const char* aFileName, int aLine, const std::string& aLogMsg)
{
    static std::ofstream log_strm("sTutorial.log");
    if (log_strm.good()) {
        log_strm << aLogLevel << " " << aFileName << '(' << aLine << "): " << aLogMsg << std::endl;
    }
    else {
        std::cerr << "Cannot write to sTutorial.log." << std::endl;
    }
}

int
main( int argc, char *argv[] )
{
    rOops::registerLogCallback(sTutorialLogger);
    std::vector<long> selected_tutorials;
    selected_tutorials.reserve(12);
    if( 2 == argc && (std::string("-h")==argv[1] || std::string("--help")==argv[1]) ) {
        std::cout << "Arguments are missing.\nUsage:\n";
        std::cout << "\tsTutorial n1 <n2> <n3>\n\t<nX> - [0..10], the number of tutorial is selected to run." << std::endl;
        return 1;
    } //if
    else if( 2 > argc ) {
        // Add all tutorials to selected_tutorials.
        for (int idx=0; idx<=10; ++idx) {
            selected_tutorials.push_back(idx);
        }
    } //if
    else {
        // Add the tutorials given on the command line to selected_tutorials.
        for (int argv_idx=1; argv_idx<argc; ++argv_idx) {
            auto tut_idx = strtol(argv[argv_idx], nullptr, 0);
            if (0 < tut_idx) {
                selected_tutorials.push_back(tut_idx);
            }
            else {
                std::cerr << "Invalid argument (" << argv_idx << "): " << argv[argv_idx] << std::endl;
            }
        }
    }

    try {
        for (auto idx : selected_tutorials) {
            switch (idx) {
                case 1: sTut_01::Main(); break;
                case 2: sTut_02::Main(); break;
                case 3: sTut_03::Main(); break;
                case 4: sTut_04a::Main(); sTut_04b::Main(); break;
                case 5: sTut_05::Main(); break;
                case 6: sTut_06::Main(); break;
                case 7: sTut_07::Main(); break;
                case 8: sTut_08::Main(); break;
                case 9: sTut_09::Main(); break;
                case 10: sTut_10::Main(); sTutEx_10::Main(); break;
                default:
                    std::cerr << "Invalid tutorial index: " << idx << std::endl;
                    break;
            }
            std::cout << "=========================================" << std::endl;
        }
    }
    catch( const std::exception& arEx ) {
        std::cerr << "std::exception:" << arEx.what() << std::endl;
    }
    catch( ... ) {
        std::cerr << "Unknown exception." << std::endl;
    }
    if (0 < getErrorCounter()) {
        std::cout << "Error counter: " << getErrorCounter() << std::endl;
    }
    return getErrorCounter();
} //main()
