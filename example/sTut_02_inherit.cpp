/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
rOops Tutorial Program 2nd
Inheritance

Two simple classes A and B.
B is the descendant of class A.
*/


#include "sTut_02_inherit.h"
#include "sTutorial.h"
#include <iostream>

namespace sTut_02
{

    void Main()
    {
        std::cout << "\n>>> " << __FILE__ << " >>>\n";
        // Create an instance of class 'B'.
        InternalB::B B(11, 22);
        {
            InternalB::B B1, B2, B3; // Create 3 object for loading.
            serializeObject("B", B, B1, B2, B3);
            if (B != B1) std::cout << "Error(" << incErrorCounter() << "): B!=B1" << std::endl;
            if (B != B2) std::cout << "Error(" << incErrorCounter() << "): B!=B2" << std::endl;
            if (B != B3) std::cout << "Error(" << incErrorCounter() << "): B!=B3" << std::endl;
        }
    } // Main()

} //namespace sTut_02
