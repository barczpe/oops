/*****************************************************************************
 The MIT License (MIT)

 Object Oriented Property Stream Library
 Copyright (C) 1998-2022:
    Robot Control Software Ltd.
    Peter Barczikay
    Andrew Tantos

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
******************************************************************************/

/**
rOops Tutorial Program 5th.
Using STL list style containers:
 array, vector, deque, list, set, multi-set, unordered-set
*/

#include "sTutorial.h"
#include <oops/rPropInterface.h>
#include <algorithm>
#include <iostream>
#include <string>
#include <array>
#include <deque>
#include <list>
#include <set>
#include <vector>

using namespace std;

namespace sTut_05
{
    using CharArray_t = std::array<char, 5>;
    using StringDeque_t = std::deque<std::string>;
    using IntVector_t = std::vector<int>;
    using IntList_t = std::list<short>;
    using IntPtrList_t = std::list<long*>;
    //using DoubleLess_t = std::less<double>;
    // Typedef is required, because typename contains a ',' character, and cannot be passed to a macro.
    //using DoubleSet_t = std::set<double, DoubleLess_t>;
    using DoubleSet_t = std::set<double>;
}

rOOPS_DECLARE_STL_ARRAY_TYPE_INFO(sTut_05::CharArray_t)
rOOPS_DECLARE_STL_LIST_TYPE_INFO(sTut_05::StringDeque_t)
//rOOPS_DECLARE_STL_LIST_TYPE_INFO(sTut_05::IntVector_t) -- already declared in oops/rPropInterface.h
rOOPS_DECLARE_STL_LIST_TYPE_INFO(sTut_05::IntList_t)
rOOPS_DECLARE_STL_LIST_TYPE_INFO(sTut_05::IntPtrList_t)
rOOPS_DECLARE_STL_SET_TYPE_INFO(sTut_05::DoubleSet_t)

namespace sTut_05
{
    static constexpr const char* str_tbl[] = { "aaa","bbb","ccc","ddd","eee","fff", };

    // Declaration of class 'STLLists' with STLLists container type properties.
    class STLLists
    {
    public:
        virtual ~STLLists() = default;
        void init(int aVal)
        {
            for (int idx = 0; idx < 5; ++idx) {
                charArray_[idx] = str_tbl[idx][0];
                stringDeque_.push_back(str_tbl[idx]);
                intVector_.push_back(aVal*idx);
                intList_.push_back(static_cast<short>(-aVal * idx));
                intPtrList_.push_back(new long(10 * aVal*idx));
                doubleSet_.insert(10.0*idx);
            } //for idx
        }
        bool check(int aVal) const
        {
            if (5 != stringDeque_.size()) return true;
            if (5 != intVector_.size()) return true;
            if (5 != intList_.size()) return true;
            if (5 != intPtrList_.size()) return true;
            if (5 != doubleSet_.size()) return true;
            auto iD = stringDeque_.begin();
            auto iV = intVector_.begin();
            auto iL = intList_.begin();
            auto iP = intPtrList_.begin();
            for (int idx = 0; idx < 5; ++idx, ++iD, ++iV, ++iL, ++iP) {
                if (*iD != str_tbl[idx]) return true;
                if (*iV != aVal * idx) return true;
                if (*iL != -aVal * idx) return true;
                if (**iP != 10 * aVal*idx) return true;
                if(doubleSet_.end() == doubleSet_.find(10.0*idx) ) return true;
            } //for idx
            return false;
        }
        bool operator==(const STLLists& arR) const
        {
            if (stringDeque_.size() != arR.stringDeque_.size()) return false;
            if (intVector_.size()   != arR.intVector_.size()  ) return false;
            if (intList_.size()     != arR.intList_.size()    ) return false;
            if (intPtrList_.size()  != arR.intPtrList_.size() ) return false;
            if (doubleSet_.size()   != arR.doubleSet_.size()  ) return false;

            if (stringDeque_ != arR.stringDeque_) return false;
            if (intVector_   != arR.intVector_  ) return false;
            if (intList_     != arR.intList_    ) return false;
            if (!std::equal(intPtrList_.begin(), intPtrList_.end(), arR.intPtrList_.begin(), [](const long* aL, const long* aR)->bool { return *aL == *aR; }) ) return false;
            if (doubleSet_   != arR.doubleSet_  ) return false;
            return true;
        }
    protected:
        CharArray_t charArray_{{'x'}};
        StringDeque_t stringDeque_;
        IntVector_t intVector_;
        IntList_t intList_;
        IntPtrList_t intPtrList_;
        DoubleSet_t doubleSet_;
        // Declare property interface.
        rOOPS_ADD_PROPERTY_INTERFACE(sTut_05::STLLists)
        {
            rOOPS_PROPERTY(charArray_), "CharArray";
            rOOPS_PROPERTY(stringDeque_), "StringDeque";
            rOOPS_PROPERTY(intVector_), "IntVector";
            rOOPS_PROPERTY(intList_), "IntList";
            rOOPS_PROPERTY(intPtrList_), "IntPtrList";
            rOOPS_PROPERTY(doubleSet_), "DoubleSet";
        }
    }; //class STLLists

} //namespace
